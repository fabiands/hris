import React, { Suspense } from "react";
import { lazyComponent as lazy } from '../../../components/lazyComponent';
import { Switch, Redirect, withRouter } from "react-router-dom";
import { Nav, TabContent, TabPane } from "reactstrap";
import { translate } from "react-switch-lang";
import AuthRoute from "../../../components/AuthRoute";
import Spinner from "reactstrap/lib/Spinner";
import RecruitmentToken from "./RecruitmentToken";

const RecruitmentMenu = lazy(() => import("./RecruitmentMenu"));
const RecruitmentCreate = lazy(() => import("./RecruitmentCreate"));
// const ApplicantList = lazy(() => import('./Applicants/ApplicantList'));
const ApplicantDetail = lazy(() =>
  import("./Applicants/ApplicantDetail")
);
const RecruitmentDetail = lazy(() => import("./RecruitmentDetail"));
const VacancyApplicantList = lazy(() =>
  import("./Applicants/VacancyApplicantList")
);

function RecruitmentWrapper({ location, match }) {
  const routes = [
    {
      path: match.path + "/",
      exact: true,
      privileges: ["browse-job"],
      component: RecruitmentMenu,
    },
    {
      path: match.path + "/vacancies/create",
      exact: true,
      privileges: ["add-job"],
      component: RecruitmentCreate,
    },
    {
      path: match.path + "/vacancies/:id/edit",
      exact: true,
      privileges: ["edit-job"],
      component: RecruitmentDetail,
    },
    {
      path: match.path + "/vacancies/:id/applicants",
      exact: true,
      privileges: ["browse-job-applicant"],
      component: VacancyApplicantList,
    },
    // {path: match.path + '/applicants', exact: true, component: ApplicantList, tab: t('daftarpelamar'), active: match.path + '/applicants*'},
    {
      path: match.path + "/applicants/:applicantId",
      exact: true,
      privileges: ["read-job-applicant"],
      component: ApplicantDetail,
    },
  ];
  return (
    <div>
      <div className="d-flex justify-content-end recruitment-header">
        <Nav navbar>
          <RecruitmentToken visible={true} />
        </Nav>
      </div>
      <TabContent className="shadow-sm rounded" style={{ marginTop: 45 }}>
        <TabPane>
          <Suspense
            fallback={<div
              style={{
                position: "absolute",
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                background: "rgba(255,255,255, 0.5)",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Spinner style={{ width: 48, height: 48 }} />
            </div>}
          >
            <Switch>
              {routes.map((route) => (
                <AuthRoute key={route.path} {...route} />
              ))}
              {routes[0] && (
                <Redirect exact from={match.path} to={routes[0].path} />
              )}
            </Switch>
          </Suspense>
        </TabPane>
      </TabContent>
    </div>
  );
}

export default translate(withRouter(RecruitmentWrapper));
