import React, { useState, useEffect } from 'react';
import { Button, Card, CardHeader, Badge, CardBody, Col, Row, Modal, ModalBody, Spinner, Nav, NavItem, ModalHeader, ModalFooter } from 'reactstrap';
import request from '../../../../utils/request';
import DataNotFound from '../../../../components/DataNotFound';
import { Link, useLocation, useRouteMatch } from 'react-router-dom';
import { t, translate } from 'react-switch-lang';
import { convertToRupiah } from '../../../../utils/formatter';
import { toast } from 'react-toastify';
import { useUserPrivileges } from '../../../../store';
import profilePhotoNotFound from '../../../../assets/img/no-photo.png'
import SkeletonVacancyApplicantList from '../Skeleton/SkeletonVacancyApplicantList';

const confirm = {
    accepted: t('sesuai'),
    rejected: t('tidak sesuai')
}

function VacancyApplicantList(props) {
    const { search, state: locationState = {} } = useLocation();
    const { params } = useRouteMatch();
    const { data: dataVacancy } = locationState;
    const { can } = useUserPrivileges();

    const [vacancy, setVacancy] = useState(dataVacancy);
    const [statusVacancy, setStatusVacancy] = useState(null);
    const [statusId, setStatusId] = useState(null);
    const [changeStat, setChangeStat] = useState(null);
    const [applicantName, setApplicantName] = useState(null);
    const [applicants, setApplicants] = useState([]);
    const [dataNotFound, setDataNotFound] = useState(false);
    const [loading, setLoading] = useState(true);
    const [isLoading, setIsLoading] = useState(false)

    const toggle = () => {
        if (!!statusId) {
            setStatusId(null);
            setChangeStat(null);
            setApplicantName(null);
        }
    }

    useEffect(() => {
        const queryParams = new URLSearchParams(search);
        setLoading(true);
        setDataNotFound(false);
        request.get('v1/recruitment/applicants', { params: { job_vacancy: params.id, status: queryParams.get('status') } }).then(res => {
            setApplicants(res.data.data);
        }).catch(err => {
            if (err.response.status === 404) {
                setDataNotFound(true);
            }
        }).finally(() => setLoading(false));
    }, [params.id, search])

    useEffect(() => {
        if (!dataVacancy) {
            request.get(`v1/recruitment/vacancies/${params.id}`).then(res => {
                setVacancy(res.data.data);
            });
        }
    }, [dataVacancy, params.id])

    useEffect(() => {
        let data = vacancy;
        if (statusVacancy === 'accepted') {
            data.unprocessedApplicantCount = vacancy?.unprocessedApplicantCount - 1;
            data.acceptedApplicantCount = vacancy?.acceptedApplicantCount + 1;
            setVacancy(data);
        }
        else if (statusVacancy === 'rejected') {
            data.unprocessedApplicantCount = vacancy?.unprocessedApplicantCount - 1;
            data.rejectedApplicantCount = vacancy?.rejectedApplicantCount + 1;
            setVacancy(data);
        }
        // eslint-disable-next-line
    }, [statusVacancy])

    // const confirmStatus = (applicantId, stat, name) => {
    //     setStatusId(applicantId);
    //     setChangeStat(stat);
    //     setApplicantName(name);
    // }

    const UbahStatus = (applicantId, stat) => {
        setIsLoading(true)
        request.put(`/v1/recruitment/applicants/${applicantId}`, { status: stat })
            .then(res => {
                setStatusVacancy(stat);
                toast.success(t('statuspelamarberhasildiubah'));
                setApplicants(applicants.filter(applicant => applicant.id !== applicantId));
            })
            .catch(err => {
                toast.error(t('statuspelamargagaldiubah'));
                throw err;
            })
            .finally(() => {
                setIsLoading(false)
                setStatusId(null)
                setChangeStat(null)
                setApplicantName(null)
            })
    }

    const queryParams = new URLSearchParams(search);
    const status = queryParams.get('status');

    const onErrorImage = (e) => {
        e.target.src = profilePhotoNotFound;
        e.target.onerror = null;
    }

    return (
        <div>
            <Card>
                <CardHeader className="d-flex align-items-center bg-netis-primary">
                    <h5 className="mb-0" style={{ color: '#ffff' }}>{vacancy?.name}</h5>
                </CardHeader>
                <CardBody>
                    <div className="ml-auto mr-n1">
                        <Nav tabs>
                            <NavItem>
                                <Link className={`nav-link ${status === 'unprocessed' ? 'active' : ''}`} replace={true} to={location => ({ ...location, search: '?status=unprocessed', state: { data: vacancy } })} >
                                    Belum diproses
                                    {vacancy?.unprocessedApplicantCount !== 0 ?
                                        <Badge pill color={`${status === 'unprocessed' ? 'danger' : 'secondary'}`} className="ml-1 pt-1" style={{ top: -2 }}>{vacancy?.unprocessedApplicantCount}</Badge>
                                        :
                                        ''
                                    }
                                </Link>
                            </NavItem>
                            <NavItem>
                                <Link className={`nav-link ${status === 'accepted' ? 'active' : ''}`} replace={true} to={location => ({ ...location, search: '?status=accepted', state: { data: vacancy } })} >
                                    Sesuai
                                    {vacancy?.acceptedApplicantCount !== 0 ?
                                        <Badge pill color={`${status === 'accepted' ? 'danger' : 'secondary'}`} className="ml-1 pt-1" style={{ top: -2 }}>{vacancy?.acceptedApplicantCount}</Badge>
                                        :
                                        ''
                                    }
                                </Link>
                            </NavItem>
                            <NavItem>
                                <Link className={`nav-link ${status === 'rejected' ? 'active' : ''}`} replace={true} to={location => ({ ...location, search: '?status=rejected', state: { data: vacancy } })} >
                                    Tidak Sesuai
                                    {vacancy?.rejectedApplicantCount !== 0 ?
                                        <Badge pill color={`${status === 'rejected' ? 'danger' : 'secondary'}`} className="ml-1 pt-1" style={{ top: -2 }}>{vacancy?.rejectedApplicantCount}</Badge>
                                        :
                                        ''
                                    }
                                </Link>
                            </NavItem>
                        </Nav>
                    </div>
                    <Row className="mt-5">
                        {
                            loading ?
                                <Col>
                                    <SkeletonVacancyApplicantList />
                                </Col>
                                :
                                (dataNotFound || applicants.length === 0) ?
                                    <Col xs={12} className="py-5 text-center">
                                        <DataNotFound />
                                    </Col>
                                    :
                                    [...applicants].map((applicant, idx) => (
                                        <Col sm="12" md="4" lg="4" key={idx} className="pt-3">
                                            <Link to={`/recruitment/applicants/${applicant.id}`} className={`${can('read-job-applicant') ? '' : ' disabled'} card-detail-job`}>
                                                <Card className="h-100 shadow-sm rounded border-0">
                                                    <CardBody>
                                                        <Row>
                                                            <Col xs="12" className="text-center">
                                                                <div className="card-image d-flex justify-content-center p-3">
                                                                    <img src={applicant.detail.avatar ?? profilePhotoNotFound} alt="photos profile" className="rounded-circle" onError={(e) => onErrorImage(e)} />
                                                                </div>
                                                            </Col>
                                                            <Col xs="12" className="text-center">
                                                                <h6 className="mt-3 text-netis-primary font-weight-bold">{applicant.detail?.fullName ?? applicant.userEmail}</h6>
                                                            </Col>
                                                            <Col xs="12" className="px-5">
                                                                <p className="mb-1">
                                                                    <i className="fa fa-graduation-cap mr-2" style={{ width: 20 }}></i>
                                                                    {applicant.detail?.lastEducation ?? (<i> - </i>)} {applicant.detail?.major && `(${applicant.detail.major})`}
                                                                </p>
                                                                <p>
                                                                    <i className="fa fa-money mr-2 " style={{ width: 20 }}></i>
                                                                    {(applicant.detail?.expectedSalary && convertToRupiah(applicant.detail.expectedSalary)) || ' - '}
                                                                </p>
                                                            </Col>
                                                            {/* <Col xs="12">
                                                                {applicant.status === 'pending' && (
                                                                    <div className="d-flex text-center pt-4 px-3 w-100 justify-content-between">
                                                                        <Button
                                                                            className={`btn btn-md btn-success mr-1 ${can('verify-job-applicant') ? '' : ' d-none'}`}
                                                                            onClick={() => { confirmStatus(applicant.id, 'accepted', applicant.detail?.fullName); }}>
                                                                            <i className="fa fa-check mr-1"></i> {t('sesuai')}
                                                                        </Button>
                                                                        <Button
                                                                            className={`btn btn-md btn-danger ${can('verify-job-applicant') ? '' : ' d-none'}`}
                                                                            onClick={() => { confirmStatus(applicant.id, 'rejected', applicant.detail?.fullName); }}>
                                                                            <i className="fa fa-times mr-1"></i> {t('Tidak sesuai')}
                                                                        </Button>
                                                                    </div>
                                                                )}
                                                            </Col> */}
                                                        </Row>
                                                    </CardBody>
                                                </Card>
                                            </Link>
                                        </Col>
                                    ))
                        }
                    </Row>
                </CardBody>
            </Card >
            <Modal isOpen={!!statusId} backdropClassName="back-home" toggle={toggle}>
                <ModalHeader toggle={toggle}>
                    Konfirmasi Perubahan Status Pelamar
                </ModalHeader>
                <ModalBody>
                    <h5>{t("Apakah anda yakin ingin mengubah status pelamar")}&nbsp;
                    <span className="text-capitalize"><b>{applicantName}</b></span>&nbsp;menjadi&nbsp;
                    <span className={changeStat === 'accepted' ? 'text-success' : 'text-danger'}>{confirm[changeStat]}</span> ?
                    </h5>
                </ModalBody>
                <ModalFooter className="border-top-0">
                    <div className="d-flex justify-content-end">
                        {!isLoading && (
                            <Button
                                className="mr-2"
                                color="light"
                                onClick={() => {
                                    setStatusId(null)
                                    setChangeStat(null)
                                    setApplicantName(null)
                                }}
                            >
                                {t("batal")}
                            </Button>
                        )}
                        <Button
                            color={changeStat === 'accepted' ? 'netis-success' : 'netis-danger'}
                            onClick={() => UbahStatus(statusId, changeStat)}
                            disabled={isLoading}
                        >
                            {isLoading ? (
                                <>
                                    <Spinner size="sm" color="light" /> loading...
                                </>
                            ) : (
                                    t("Ubah Status")
                                )}
                        </Button>
                    </div>
                </ModalFooter>
            </Modal>
        </div >
    )
}

export default translate(VacancyApplicantList);
