import React from 'react'
import { Badge } from 'reactstrap'
import { t } from 'react-switch-lang';

const statuses = {
    'pending': 'warning',
    'accepted': 'success',
    'rejected': 'danger'
}

const statusLamaran = (stat) => {
    let status = '';
    if (stat === 'pending') {
        status = t('belum diproses')
    }
    if (stat === 'accepted') {
        status = t('terpilih')
    }
    if (stat === 'rejected') {
        status = t('tidak sesuai')
    }

    return status
}

function StatusBadge({ status }) {
    return <Badge className="text-capitalize badge-lg" style={{ fontSize: 14, fontHeight: 16, color: 'white' }} color={statuses[status]}>
        {statusLamaran(status)}
    </Badge>
}

export default StatusBadge
