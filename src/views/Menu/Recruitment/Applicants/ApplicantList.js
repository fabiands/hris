import React, { useState, useEffect } from 'react';
import { Table, Button } from 'reactstrap';
import Select from 'react-select';
import request from '../../../../utils/request';
import LoadingAnimation from '../../../../components/LoadingAnimation';
import DataNotFound from '../../../../components/DataNotFound';
import * as moment from 'moment';
import StatusBadge from './StatusBadge';
import { Link } from 'react-router-dom';
import { t, translate } from 'react-switch-lang';

function ApplicantList() {
    const [applicants, setApplicants] = useState([]);
    const [dataNotFound, setDataNotFound] = useState(false);
    const [loading, setLoading] = useState(true);
    const [search, setSearch] = useState(null);
    // const [job, setJob] = useState([]);
    // const [selectedValue, setSelectedValue] = useState(null);

    useEffect(() => {
        request.get('v1/recruitment/applicants').then(res => {
            setApplicants(res.data.data);
        }).catch(err => {
            if (err.response.status === 404) {
                setDataNotFound(true);
            }
        }).finally(() => setLoading(false));
    }, [])

    // useEffect(() => {
    //     request.get('v1/recruitment/vacancies').then(response => {
    //         // console.log(response);
    //         setJob(response.data.data);
    //     }).catch(err => {
    //         if (err.response.status === 404) {
    //             setDataNotFound(true);
    //         }
    //     }).finally(() => setLoading(false));
    //     // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [])

    const optionJob = job.map(getJob =>
        ({ value: getJob.id, label: getJob.name })
    )

    const filterPelamar = (e) => {
        setSearch(e.target.value);
    };

    return (
        <div className="animated fadeIn p-4">
            <div className="col-4 mb-3">
                <Select
                    name="jobtype"
                    id="jobtype"
                    options={optionJob}
                    onChange={filterPelamar}
                    isClearable={true}
                // value={selectedValue}
                />
            </div>

            <Table responsive>
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>{t('tanggal')}</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        loading ?
                            <tr><td colSpan="5"><LoadingAnimation /></td></tr>
                            :
                            dataNotFound ?
                                <tr><td colSpan="5"><DataNotFound /></td></tr>
                                :
                                applicants.filter(item => {
                                    if (selectedValue != null) {
                                        return item.job_vacancy.name === selectedValue;
                                    }

                                    return true;
                                })
                                    .map(applicant => (
                                        <tr key={applicant.id}>
                                            <td>{applicant.userEmail}</td>
                                            <td>{applicant.job_vacancy.name}</td>
                                            <td>{moment(applicant.modifiedAt).format('DD MMM YYYY')}</td>
                                            <td><StatusBadge status={applicant.status} /></td>
                                            <td>
                                                <Link to={`/recruitment/applicants/${applicant.id}`}>
                                                    <Button color="netis-color">{t('lihatdetail')}</Button>
                                                </Link>
                                            </td>
                                        </tr>
                                    ))
                    }
                </tbody>
            </Table>
        </div>
    )
}

export default translate(ApplicantList);
