import { useFormik } from 'formik';
import React, { Fragment, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { Button, Card, Col, Form, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row, Spinner, Tooltip } from 'reactstrap';
import request from '../../../utils/request';
import { t } from 'react-switch-lang';
import { useAuthUser } from '../../../store';
import TokenIcon from "./TokenIconHris";

function RecruitmentTokenHris({visible}) {
    const me = useAuthUser()
    const [myBalance, setMyBalance] = useState(0)
    const [modalToken, setModalToken] = useState(false)
    const [hasPurchased, setHasPurchased] = useState(false)
    const [loading, setLoading] = useState(false)
    const [hint, setHint] = useState(false)
    const [tooltipOpen, setTooltipOpen] = useState(false);

    const toggleTooltip = () => setTooltipOpen(!tooltipOpen);
    const toggle = () => {
        setModalToken(!modalToken)
    }
    const toggleHasPurchased = () => {
        setHasPurchased(!hasPurchased)
    }

    useEffect(() => {
        setLoading(true)
        request.get(`v1/token`)
            .then((res) => {
                setMyBalance(res.data.balance);
            })
            .finally(() => setLoading(false));
    }, []);

    const { values, isSubmitting, setValues, ...formik } = useFormik({
        initialValues: {
            nominal: 0
        },
        onSubmit: (values, { setSubmitting, setErrors }) => {
            setSubmitting(true)
            request.post(`v1/token`, values)
                .then(res => {
                    toast.success(t('berhasiltopup'));
                    setModalToken(false)
                    setHasPurchased(true)
                })
                .catch(err => {
                    if (err.response?.status === 422) {
                        setErrors(err.response.data.errors);
                        return;
                    }
                    Promise.reject(err);
                })
                .finally(() => setSubmitting(false));
        }
    })

    const changeNominal = function (e) {
        const value = parseInt(e.target.value);
        formik.setFieldValue('nominal', value)
        formik.setFieldTouched('nominal', true)
    }

    return (
        <div className="p-1">
            <Card body>
                <Row>
                    <Col md="5">
                        <h5 className="text-center text-sm-left mb-3 mb-sm-0" style={{ lineHeight: '38px' }}>{me?.personnel?.company?.name}</h5>
                    </Col>
                    <Col md="7">
                        <Row className="no-gutters justify-content-end">
                            <Col sm="4" className="px-1 mb-2 mb-sm-0">
                                {visible ?
                                    <TokenIcon />
                                : null
                                }
                            </Col>
                            <Col sm="4" className="px-1 mb-2 mb-sm-0">
                            <Button className="btn-block btn-netis-primary" onClick={toggle}>
                                <i className="fa fa-plus-square mr-2" /> <b>Top Up</b>
                            </Button>
                            </Col>
                            <Col sm="4" className="px-1 mb-2 mb-sm-0">
                            <Button className="button-token btn-block" disabled>
                                {loading ?
                                    <Spinner size="sm" color="primary" className="mx-auto" />
                                    :
                                    <><i className="fa fa-database mr-2" /> <b>{myBalance}</b></>
                                }
                            </Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Card>
            {/* <Table responsive className="border-bottom-0 table-token">
                <tbody>
                    <tr>
                        <td className="w-50 text-left text-nowrap">
                            <h5>{me?.personnel?.company?.name}</h5>
                        </td>
                        <td className="text-right">

                            <Button className="button-token mx-2 my-1" onClick={toggle} style={{ width: 100 }}>
                                <i className="fa fa-plus-square mr-2" /> <b>Top-Up</b>
                            </Button>
                            <Button className="button-token mx-2 my-1" disabled={true} style={{ width: 100 }}>
                                {loading ?
                                    <Spinner size="sm" color="primary" className="mx-auto" />
                                    :
                                    <><i className="fa fa-ticket mr-2" /> <b>{myBalance}</b></>
                                }
                            </Button>
                        </td>
                    </tr>
                </tbody>
            </Table> */}
            <Modal isOpen={modalToken} className="token-master">
                <ModalHeader className="border-bottom-0">
                    {t('Top Up Kuota Token')}
                </ModalHeader>
                <Form onSubmit={formik.handleSubmit}>
                    <ModalBody>
                        <div className="d-flex justify-content-between align-middle mb-3">
                            <div className="mt-2">
                                {t('Pilihlah salah satu paket kuota yang anda inginkan')}
                            </div>
                            <Button onClick={() => setHint(!hint)} className="text-nowrap" style={{ backgroundColor: "transparent", border: "transparent" }} id="TooltipExample">
                                <i className="fa fa-lg fa-question-circle text-primary" />
                            </Button>
                            <Tooltip placement="right" isOpen={tooltipOpen} target="TooltipExample" toggle={toggleTooltip}>
                                Kuota token yang Anda miliki dapat membuka akses terkunci pada fitur mengenai data-data
                                tiap pelamar lowongan yang telah anda publikasikan sebelumnya.
                            <br />
                            </Tooltip>
                        </div>
                        <Row className="mt-3">
                            <Col lg="6" md="6" sm="12">
                                <Label className="my-1">
                                    <Input type="radio" className="d-none"
                                        value={500}
                                        checked={values.nominal === 500}
                                        onChange={changeNominal}
                                    />
                                    <div className="card bg-light d-flex flex-column">
                                        <div className="card-header">
                                            <b>Paket A</b>
                                        </div>
                                        <div className="card-body">
                                            <i className="fa fa-credit-card mr-1" /> 500 token<br />
                                            <i className="fa fa-money mr-1" /> Rp 10.000.000,00
                                    </div>
                                    </div>
                                </Label>
                            </Col>
                            <Col lg="6" md="6" sm="12">
                                <Label className="my-1">
                                    <Input type="radio" className="d-none"
                                        value={1000}
                                        checked={values.nominal === 1000}
                                        onChange={changeNominal}
                                    />
                                    <div className="card bg-light d-flex flex-column">
                                        <div className="card-header">
                                            <b>Paket B</b>
                                        </div>
                                        <div className="card-body">
                                            <i className="fa fa-credit-card mr-1" /> 1000 token<br />
                                            <i className="fa fa-money mr-1" /> Rp 16.000.000,00
                                    </div>
                                    </div>
                                </Label>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter className="border-top-0">
                        <Button onClick={() =>{
                            formik.setFieldValue('nominal', null)
                            formik.setFieldTouched('nominal', false)
                            toggle()
                            }}
                            color="netis-danger"
                        >
                            <i className="fa fa-times mr-1" /> Batal
                        </Button>
                        <Button color="netis-color" className="mr-1" disabled={!values.nominal || isSubmitting}>
                            {isSubmitting ? (
                                <Fragment>
                                    <Spinner size="sm" /> Tunggu...
                                </Fragment>
                            ) : (
                                    <Fragment>
                                        <i className="fa fa-money mr-1" /> Beli
                                    </Fragment>
                                )}
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
            <Modal isOpen={hasPurchased} className="token-master">
                <ModalHeader className="border-bottom-0">
                    Pembelian Token Berhasil
                </ModalHeader>
                <ModalBody>
                    <div className="d-flex justify-content-between align-middle mb-3">
                        <div className="mt-2">
                            {/* Pembelian token anda berhasil dan sedang diproses oleh admin. Harap tunggu, admin akan menghubungi anda. Terima Kasih. */}
                            {t('konfirmasitoken')}
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter className="border-top-0">
                    <Button onClick={toggleHasPurchased} color="netis-danger">
                        Tutup
                    </Button>
                </ModalFooter>
            </Modal>
        </div>
    )
}

export default RecruitmentTokenHris
