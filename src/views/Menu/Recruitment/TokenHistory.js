import React, { useCallback, useState } from "react"
import { ListGroup, ListGroupItem, Nav, NavItem, NavLink, TabContent, TabPane, Row, Col } from "reactstrap";
import { translate, t } from "react-switch-lang";
import { useTokenNotification } from '../../../hooks/useTokenNotification';
import LoadingAnimation from "../../../components/LoadingAnimation";
import { Bar } from "react-chartjs-2";
import classnames from "classnames";
import DataNotFound from "../../../components/DataNotFound";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function TokenHistory({ history }) {
    const { data, loading, error } = useTokenNotification([])
    const [activeTab, setActiveTab] = useState("1");
    const toggle = (tab) => {
        if (activeTab !== tab) setActiveTab(tab);
    };
    const [list, setList] = useState([])

    const action = {
        topup: t('melakukan Top Up Token'),
        usage: t('menggunakan 1 token')
    }

    const desc = {
        topup: t('sejumlah'),
        usage: t('untuk fitur')
    }
    const fisiognomi = data.filter(a => a.type === 'usage' && a.usage.tokenType === 'fisiognomi')
    const video = data.filter(a => a.type === 'usage' && a.usage.tokenType === 'video')
    const gesture = data.filter(a => a.type === 'usage' && a.usage.tokenType === 'gesture')
    const palmistry = data.filter(a => a.type === 'usage' && a.usage.tokenType === 'palmistry')
    const bazi = data.filter(a => a.type === 'usage' && a.usage.tokenType === 'bazi')
    const shio = data.filter(a => a.type === 'usage' && a.usage.tokenType === 'shio')
    const zodiac = data.filter(a => a.type === 'usage' && a.usage.tokenType === 'zodiac')
    const disc = data.filter(a => a.type === 'usage' && a.usage.tokenType === 'disc')
    const listTopup = data.filter(a => a.type === 'topup')

    const dataChart = {
        labels: ['DISC', 'Video', 'Gesture', 'Fisiognomi', 'Palmistry', 'Bazi', 'Shio', 'Zodiac'],
        datasets: [
            {
                label: 'Penggunaan Token',
                backgroundColor: ['rgba(122, 154, 81, 1)', 'rgba(172, 165, 62, 1)', 'rgba(172, 114, 62, 1)', 'rgba(62, 125, 172, 1)', 'rgba(219, 132, 61, 1)', 'rgba(170, 70, 68, 1)', 'rgba(147, 169, 208, 1)', 'rgba(147, 169, 208, 1)'],
                borderColor: ['rgba(122, 154, 81, 1)', 'rgba(172, 165, 62, 1)', 'rgba(172, 114, 62, 1)', 'rgba(62, 125, 172, 1)', 'rgba(219, 132, 61, 1)', 'rgba(170, 70, 68, 1)', 'rgba(147, 169, 208, 1)', 'rgba(147, 169, 208, 1)'],
                borderWidth: 1,
                hoverBackgroundColor: ['rgba(122, 154, 81, 0.5)', 'rgba(172, 165, 62, 0.5)', 'rgba(172, 114, 62, 0.5)', 'rgba(62, 125, 172, 0.5)', 'rgba(219, 132, 61, 0.5)', 'rgba(170, 70, 68, 0.5)', 'rgba(147, 169, 208, 0.5)', 'rgba(147, 169, 208, 0.5)'],
                hoverBorderColor: ['rgba(122, 154, 81, 0.5)', 'rgba(172, 165, 62, 0.5)', 'rgba(172, 114, 62, 0.5)', 'rgba(62, 125, 172, 0.5)', 'rgba(219, 132, 61, 0.5)', 'rgba(170, 70, 68, 0.5)', 'rgba(147, 169, 208, 0.5)', 'rgba(147, 169, 208, 0.5)'],
                data: [
                    disc.length, video.length, gesture.length, fisiognomi.length, palmistry.length, bazi.length, shio.length, zodiac.length
                ]
            },
        ]
    };

    const changeList = useCallback((keyword) => {
        setList(data.filter(a => a.type === 'usage' && a.usage.tokenType === keyword.toLowerCase()))
        window.scroll({ top: 500, behavior: 'smooth' })
        // const list = document.getElementById("listgroup");
        // list.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
    }, [data])

    return (
        <TabContent className="shadow-sm rounded">
            <TabPane>
                <div className="animated fadeIn">
                    <div className="d-flex bd-highlight mb-3">
                        <div className="mr-auto bd-highlight">
                            {/* <Button onClick={() => history.goBack()} color="netis-color">
                                <i className="fa fa-chevron-left mr-1"></i>
                                {t("kembali")}
                            </Button> */}
                        </div>
                    </div>
                    {loading ?
                        <LoadingAnimation />
                        : error ?
                            <div className="alert alert-danger">
                                <h5>Maaf, Terjadi Galat!</h5>
                                <span>Silahkan muat ulang halaman ini atau laporkan kepada tim IT Support kami</span>
                            </div>
                            :
                            <>
                                <Nav tabs>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({ active: activeTab === "1" })}
                                            onClick={() => {
                                                toggle("1");
                                            }}
                                        >
                                            Penggunaan Token
                                </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({ active: activeTab === "2" })}
                                            onClick={() => {
                                                toggle("2");
                                            }}
                                        >
                                            Top Up
                            </NavLink>
                                    </NavItem>
                                </Nav>
                                <TabContent activeTab={activeTab}>
                                    <TabPane tabId="1">
                                        <Row>
                                            <Col sm="12"><div style={{ maxHeight: '400px' }}>
                                                <Bar
                                                    data={dataChart}
                                                    width={100}
                                                    height={400}
                                                    options={{
                                                        maintainAspectRatio: false,
                                                        legend: false
                                                    }}
                                                    onElementsClick={e => changeList(e[0]?._model.label ?? '')}
                                                />
                                            </div>
                                                <ListGroup id="listgroup" className="mt-4">
                                                    {list.length === 0 ? <DataNotFound />
                                                        :
                                                        <>
                                                            <h3 className="mb-3">Daftar penggunaan token pada fitur {list[0]?.usage?.tokenType}</h3>
                                                            {list?.map((data, idx) => {
                                                                return (
                                                                    <ListGroupItem className="p-3" key={idx}>
                                                                        <i className="fa fa-lg fa-ticket text-info mr-1" />Penggunaan Fitur : <span className="text-capitalize">{data?.usage?.tokenType}</span>
                                                                        <span className="text-muted float-right">{data?.date}</span>
                                                                        <br />
                                                                        <span className="text-primary">
                                                                            <b>{data?.causer?.name}</b>
                                                                        </span>
                                                        &nbsp;
                                                                        {action[data?.type]}
                                                        &nbsp;
                                                                        {desc[data?.type]}
                                                        &nbsp;
                                                                        <span className="text-capitalize">
                                                                            {data?.usage?.tokenType}
                                                                        </span>
                                                                &nbsp;pada pelamar&nbsp;
                                                                        <span className="text-capitalize">
                                                                            {data?.usage?.identifier}
                                                                        </span>
                                                                    </ListGroupItem>
                                                                )
                                                            })}
                                                        </>
                                                    }
                                                </ListGroup>
                                            </Col>
                                        </Row>
                                    </TabPane>
                                    <TabPane tabId="2">
                                        <Row>
                                            <Col sm="12">
                                                <ListGroup>
                                                    {listTopup.length === 0 ? <DataNotFound /> :
                                                        listTopup?.map((data, idx) => (
                                                            data.causer.id === 0 ?
                                                                data.nominal === 0 ?
                                                                    <>
                                                                        <ListGroupItem className="p-3" key={idx}>
                                                                            <FontAwesomeIcon icon="calendar-times" className="mx-auto" style={{ color: "#e0474d" }} />
                                                                            <span className="text-muted float-right">{data?.date}</span>
                                                                            <span className="ml-2">
                                                                                Masa berlaku token anda telah habis
                                                                        </span>
                                                                        </ListGroupItem>
                                                                    </>
                                                                    :
                                                                    <>
                                                                        <ListGroupItem className="p-3" key={idx}>
                                                                            <FontAwesomeIcon icon="plus-square" className="mx-auto" style={{ color: "#47e0af" }} />
                                                                            <span className="text-muted float-right">{data?.date}</span>
                                                                            <span className="ml-2">
                                                                                Anda mendapatkan gratis {data.nominal} token
                                                                        </span>
                                                                        </ListGroupItem>
                                                                    </>
                                                                :
                                                                <>
                                                                    <ListGroupItem className="p-3" key={idx}>
                                                                        <FontAwesomeIcon icon="plus-square" className="mx-auto" style={{ color: "#47e0af" }} />
                                                                        <span className="text-muted float-right">{data?.date}</span>
                                                                        <span className="text-primary ml-2">
                                                                            <b>{data?.causer?.name}</b>
                                                                        </span>
                                                            &nbsp;
                                                                    {action[data?.type]}
                                                            &nbsp;
                                                                    {desc[data?.type]}
                                                            &nbsp;
                                                                    <span className="text-primary">{data?.nominal}</span>
                                                                    </ListGroupItem>
                                                                </>
                                                        ))}
                                                </ListGroup>
                                            </Col>
                                        </Row>
                                    </TabPane>
                                </TabContent>
                            </>
                    }
                </div>
            </TabPane>
        </TabContent>
    )
}

export default translate(TokenHistory);
