import { useFormik } from 'formik';
import React, { Fragment, useState } from 'react';
import { toast } from 'react-toastify';
import { Button, Col, Form, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row, Spinner, Tooltip } from 'reactstrap';
import request from '../../../utils/request';
import { translate, t } from 'react-switch-lang';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const TokenNull = ({ open, close }) => {
    const [modalToken, setModalToken] = useState(open)
    const [hasPurchased, setHasPurchased] = useState(false)
    const [hint, setHint] = useState(false)
    const [tooltipOpen, setTooltipOpen] = useState(false);

    const toggleTooltip = () => setTooltipOpen(!tooltipOpen);
    const toggle = () => {
        setModalToken(false)
        close()
    }
    const toggleHasPurchased = () => {
        setHasPurchased(false)
        close()
    }

    const { values, isSubmitting, setValues, ...formik } = useFormik({
        initialValues: {
            nominal: 0
        },
        onSubmit: (values, { setSubmitting, setErrors }) => {
            setSubmitting(true)
            request.post(`v1/token`, values)
                .then(res => {
                    toast.success(t('berhasiltopup'));
                    setModalToken(false)
                    setHasPurchased(true)
                })
                .catch(err => {
                    if (err.response?.status === 422) {
                        setErrors(err.response.data.errors);
                        return;
                    }
                    Promise.reject(err);
                })
                .finally(() => setSubmitting(false));
        }
    })

    const changeNominal = function (e) {
        const value = parseInt(e.target.value);
        formik.setFieldValue('nominal', value)
        formik.setFieldTouched('nominal', true)
    }

    return (
        <div>
            <Modal isOpen={modalToken} className="token-master">
                <ModalHeader className="border-bottom-0">
                    {t('Top Up Kuota Token')}
                </ModalHeader>
                <Form onSubmit={formik.handleSubmit}>
                    <ModalBody>
                        <div className="d-flex justify-content-between align-middle mb-3">
                            <div className="mt-2">
                                {t('Pilihlah salah satu paket kuota yang anda inginkan')}
                            </div>
                            <Button onClick={() => setHint(!hint)} className="text-nowrap" style={{ backgroundColor: "transparent", border: "transparent" }} id="TooltipExample">
                                <i className="fa fa-lg fa-question-circle text-primary" />
                            </Button>
                            <Tooltip placement="right" isOpen={tooltipOpen} target="TooltipExample" toggle={toggleTooltip}>
                                Kuota token yang Anda miliki dapat membuka akses terkunci pada fitur mengenai data-data
                                tiap pelamar lowongan yang telah anda publikasikan sebelumnya.
                            <br />
                            </Tooltip>
                        </div>
                        <Row className="mt-3">
                            <Col lg="6" md="6" sm="12">
                                <Label className="my-1">
                                    <Input type="radio" className="d-none"
                                        value={500}
                                        checked={values.nominal === 500}
                                        onChange={changeNominal}
                                    />
                                    <div className="card bg-light d-flex flex-column">
                                        <div className="card-header">
                                            <b>Paket A</b>
                                        </div>
                                        <div className="card-body">
                                            <FontAwesomeIcon icon="coins" className="mr-1" /> 500 token<br />
                                            <i className="fa fa-money mr-1" /> Rp 10.000.000,00
                                    </div>
                                    </div>
                                </Label>
                            </Col>
                            <Col lg="6" md="6" sm="12">
                                <Label className="my-1">
                                    <Input type="radio" className="d-none"
                                        value={1000}
                                        checked={values.nominal === 1000}
                                        onChange={changeNominal}
                                    />
                                    <div className="card bg-light d-flex flex-column">
                                        <div className="card-header">
                                            <b>Paket B</b>
                                        </div>
                                        <div className="card-body">
                                            <FontAwesomeIcon icon="coins" className="mr-1" /> 1000 token<br />
                                            <i className="fa fa-money mr-1" /> Rp 16.000.000,00
                                    </div>
                                    </div>
                                </Label>
                            </Col>
                        </Row>
                    </ModalBody>
                    <ModalFooter className="border-top-0">
                        <Button onClick={() => {
                            formik.setFieldValue('nominal', null)
                            formik.setFieldTouched('nominal', false)
                            toggle()
                            // close()
                        }}
                            color="netis-danger"
                        >
                            <i className="fa fa-times mr-1" /> Batal
                        </Button>
                        <Button color="netis-color" className="mr-1" disabled={!values.nominal || isSubmitting}>
                            {isSubmitting ? (
                                <Fragment>
                                    <Spinner size="sm" /> Tunggu...
                                </Fragment>
                            ) : (
                                    <Fragment>
                                        <i className="fa fa-money mr-1" /> Beli
                                    </Fragment>
                                )}
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
            <Modal isOpen={hasPurchased} className="token-master">
                <ModalHeader className="border-bottom-0">
                    Pembelian Token Berhasil
                </ModalHeader>
                <ModalBody>
                    <div className="d-flex justify-content-between align-middle mb-3">
                        <div className="mt-2">
                            {t('konfirmasitoken')}
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter className="border-top-0">
                    <Button onClick={toggleHasPurchased} color="netis-danger">
                        Tutup
                    </Button>
                </ModalFooter>
            </Modal>
        </div>
    )
}

export default translate(TokenNull);
