import React, { useState } from 'react';
import { TabContent, Row, Col, TabPane, Nav, NavItem, NavLink } from "reactstrap";
import RecruitmentMenu from "./RecruitmentMenu"
import ApplicantList from "./Applicants/ApplicantList"
import classnames from "classnames";
import { t } from "react-switch-lang";

function RecruitmentVacancies() {
    const [activeTab, setActiveTab] = useState("1");
    const toggle = (tab) => {
        if (activeTab !== tab) setActiveTab(tab);
    };

    return (
        <div>
            <Nav tabs>
                <NavItem>
                    <NavLink
                        className={classnames({ active: activeTab === "1" })}
                        onClick={() => {
                            toggle("1");
                        }}
                    >
                        {t("daftarlowongan")}
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink
                        className={classnames({ active: activeTab === "2" })}
                        onClick={() => {
                            toggle("2");
                        }}
                    >
                        {t("daftarpelamar")}
                    </NavLink>
                </NavItem>
            </Nav>
            <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                    <Row>
                        <Col sm="12">
                            <RecruitmentMenu />
                        </Col>
                    </Row>
                </TabPane>

                <TabPane tabId="2">
                    <Row>
                        <Col sm="12">
                            <ApplicantList />
                        </Col>
                    </Row>
                </TabPane>
            </TabContent>
        </div>
    )
}

export default RecruitmentVacancies;