import React, { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import DataNotFound from '../../../components/DataNotFound';
import LoadingAnimation from '../../../components/LoadingAnimation';
import request from '../../../utils/request';
import { Nav, NavItem, NavLink, TabContent, TabPane, Badge } from 'reactstrap';
import classnames from "classnames";
import Card from 'reactstrap/lib/Card';
import CardHeader from 'reactstrap/lib/CardHeader';
import CardBody from 'reactstrap/lib/CardBody';
import moment from "moment";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { convertNumber } from '../../../utils/formatter';

function TopupInvoices({ history }) {

    const [data, setData] = useState(null)
    const [notFound, setNotFound] = useState(false)
    const [loading, setLoading] = useState(true)
    const [activeTab, setActiveTab] = useState("1");
    const [pendingData, setPendingData] = useState(null);
    const [paidData, setPaidData] = useState(null);
    const [expiredData, setExpiredData] = useState(null);

    const toggle = (tab) => {
        if (activeTab !== tab) setActiveTab(tab);
    };

    useEffect(() => {
        setLoading(true)
        request.get('v1/invoice')
            .then((res) => {
                setData(res.data.data)
            })
            .catch((err) => {
                console.log(err)
                setNotFound(true)
                toast.error("Terjadi Kesalahan, silahkan coba lagi")
            })
        // .finally(() => {
        //     setLoading(false)
        // })
    }, [])

    useEffect(() => {
        if (data) {
            let pending = data?.filter((data) => {
                return data.status === 'pending'
            })
            let paid = data?.filter((data) => {
                return data.status === 'paid'
            })
            let expired = data?.filter((data) => {
                return data.status === 'expired'
            })
            setPendingData(pending)
            setPaidData(paid)
            setExpiredData(expired)
        }
    }, [data])

    useEffect(() => {
        if (pendingData) {
            setLoading(false)
        }
    }, [pendingData])

    if (loading) {
        return <LoadingAnimation />;
    }
    else if (notFound) {
        return <DataNotFound />
    }

    return (
        <TabContent className="shadow-sm rounded">
            <TabPane>
                <div className="animated fadeIn">
                    <div className="d-flex bd-highlight mb-3">
                        <div className="mr-auto bd-highlight">
                            {/* <Button onClick={() => history.goBack()} color="netis-color">
                                <i className="fa fa-chevron-left mr-1"></i>
                                    Kembali
                                </Button> */}
                        </div>
                    </div>
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: activeTab === "1" })}
                                onClick={() => {
                                    toggle("1");
                                }}
                            >
                                Belum Dibayar {pendingData.length !== 0 ? <Badge color="danger" className="ml-2">{pendingData.length}</Badge> : null}
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: activeTab === "2" })}
                                onClick={() => {
                                    toggle("2");
                                }}
                            >
                                Sudah Dibayar
                    </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                className={classnames({ active: activeTab === "3" })}
                                onClick={() => {
                                    toggle("3");
                                }}
                            >
                                Kadaluarsa
                    </NavLink>
                        </NavItem>
                    </Nav>
                    <TabContent activeTab={activeTab}>
                        <TabPane tabId="1">
                            {pendingData.length !== 0 ? <InvoicesList data={pendingData} /> : <DataNotFound />}
                        </TabPane>
                        <TabPane tabId="2">
                            {paidData.length !== 0 ? <InvoicesList data={paidData} /> : <DataNotFound />}
                        </TabPane>
                        <TabPane tabId="3">
                            {expiredData.length !== 0 ? <InvoicesList data={expiredData} /> : <DataNotFound />}
                        </TabPane>
                    </TabContent>
                </div>
            </TabPane>
        </TabContent>
    )
}

const InvoicesList = ({ data }) => {
    const paket = {
        500: "Paket A",
        1000: "Paket B"
    }
    return (
        data && data?.map((item) => {
            return (
                <div key={item.id} className="card-link-invoice" onClick={() => window.open(item.url)}>
                    <Card style={{ borderRadius: "12px" }}>
                        <CardHeader style={{ backgroundColor: "#fff" }} className="d-flex justify-content-between">
                            <div>{moment(item.createAt).locale("ID").format("DD MMMM YYYY LT")}</div>
                            <div>
                                {item.status === 'pending' ?
                                    <small className="text-muted">
                                        Bayar sebelum {moment(item.expiredAt).locale("ID").format("DD MMMM YYYY LT")}</small>
                                    : null
                                }
                            </div>
                        </CardHeader>
                        <CardBody>
                            <div className="d-flex align-items-center">
                                <div className="mr-4 font-xl dropdown-token">
                                    <FontAwesomeIcon icon="coins" className="mx-auto" style={{ color: "rgba(243, 216, 50, 1)" }} />
                                </div>
                                <div className="flex-fill small">
                                    <div className="d-flex">
                                        <div>
                                            <span style={{ fontSize: 15 }} className="font-weight-bold mr-3 text-netis-primary">{paket[item.balance]}</span><br />
                                            <span style={{ fontSize: 15 }} className="ml-auto"><FontAwesomeIcon icon="coins" color="#f3d832" className="mr-1" /> {item.balance} token</span><br />
                                            <span style={{ fontSize: 14 }} className="mr-auto"><FontAwesomeIcon icon="money-bill-wave" color="#137500" className="mr-1" /> {convertNumber(item.amount, '$ 0,0[.]00')}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </CardBody>
                    </Card>
                </div>
            );
        })
    )
}

export default TopupInvoices;
