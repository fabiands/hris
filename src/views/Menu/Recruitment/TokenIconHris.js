import React, { useMemo, memo } from 'react'
import { DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown } from 'reactstrap'
import { useTokenNotification } from '../../../hooks/useTokenNotification';
import { Link } from 'react-router-dom';
import {translate, t} from "react-switch-lang"

const TokenIcon = memo(() => {
    const { data, loading, error } = useTokenNotification([])

    const notifications = useMemo(() => data.slice(0, 4), [data]);
    const notif = notifications.reverse()

    return (
        <UncontrolledDropdown direction="down" className="dropdown-token" style={{backgroundColor:"transparent"}}>
            <DropdownToggle className="dropdown-toggle-token btn-block">
                <i className={`fa fa-history mr-2`} aria-hidden="true" /> <b>{t('riwayat')}</b>
            </DropdownToggle>
            <DropdownMenu style={{zIndex:999}}>
                {error ? <DropdownItem><i className="fa fa-warning text-danger"></i> {t('Terjadi Kesalahan!')}</DropdownItem> :
                    loading ? <DropdownItem>Loading...</DropdownItem> :
                        data.length === 0 ? <DropdownItem>Belum ada riwayat penggunaan atau Top-Up Token</DropdownItem> :
                            notif.map(notification => (
                                <DropdownItem key={notification.id} className="text-capitalize d-flex justify-content-between">
                                    <span>{notification.type}<br />
                                    <small><i>(Fitur {notification.usage.tokenType})</i></small>&nbsp;</span>
                                    <b>
                                        {notification.type === "usage" ? "-" : "+" }&nbsp;
                                        {notification.nominal}
                                    </b>
                                </DropdownItem>
                            ))
                }
                <Link to="/tokenhistory" className="text-center dropdown-header">{t('Lihat Semua')}</Link>
            </DropdownMenu>
        </UncontrolledDropdown>
    )

})

export default translate(TokenIcon)
