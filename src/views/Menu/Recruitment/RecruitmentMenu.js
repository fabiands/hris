import React from "react";
import { useEffect, useState } from "react";
import { FormGroup, CustomInput, Button, Modal, ModalBody, Spinner, Card, CardBody, Row, Col, InputGroup, InputGroupAddon, InputGroupText, Input, Label } from "reactstrap";
import { Link } from "react-router-dom";
import request from "../../../utils/request";
import { translate, t } from "react-switch-lang";
import { toast } from "react-toastify";
import { useUserPrivileges } from "../../../store";
import * as moment from "moment";
import SkeletonRecruitmentMenu from '../Recruitment/Skeleton/SkeletonRecruitmentMenu'
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";
import DataNotFound from "../../../components/DataNotFound";
import { convertNumber } from "../../../utils/formatter";


function RecruitmentMenu(props) {
  const [loading, setLoading] = useState(true);
  const [lowongan, setLowongan] = useState([]);
  const [filteredLowongan, setFilteredLowongan] = useState([]);
  const [deletingId, setDeletingId] = useState(null);
  const [updateId, setUpdateId] = useState([]);
  const [deleteLoading, setDeleteLoading] = useState(false);
  const { privileges } = useUserPrivileges();
  const [updateLoading, setUpdateLoading] = useState(false);
  const [filterType, setFilterType] = useState([]);
  const [filterJob, setFilterJob] = useState([]);
  const [search, setSearch] = useState(null);
  const [range, setRange] = useState({
    min: 0,
    max: 50000000
  })
  const [inputRange, setInputRange] = useState({
    min: range.min,
    max: range.max
  })

  const today = new Date();
  const momentToday = moment(today).format("YYYY-MM-DD");

  useEffect(() => {
    getJob();
  }, []);

  function getJob() {
    request
      .get("v1/recruitment/vacancies")
      .then((res) => {
        setLowongan(res.data.data);
        setFilteredLowongan(res.data.data);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  function doDelete() {
    if (deleteLoading) return;
    setDeleteLoading(true);
    request
      .delete("v1/recruitment/vacancies/" + deletingId)
      .then(() => {
        setLowongan(lowongan.filter((data) => data.id !== deletingId));
        setDeletingId(null);
        toast.success(t("berhasilhapus"));
      })
      .catch((err) => {
        toast.error(t("gagalhapus"));
        throw err;
      })
      .finally(() => {
        setDeleteLoading(false);
      });
  }

  function doUpdate() {
    if (deleteLoading) return;
    setUpdateLoading(true);
    if (updateId[1] === "repost") {
      request
        .put("v1/recruitment/vacancies/renew/" + updateId[0])
        .then(() => {
          getJob();
          setUpdateId([]);
          if (updateId[0]) {
            toast.success(t("berhasilrepost"));
          }
        })
        .catch((err) => {
          toast.error("Terjadi kesalahan");
          throw err;
        })
        .finally(() => {
          setUpdateLoading(false);
        });
    } else if (updateId[1] === "tutup") {
      request
        .put("v1/recruitment/vacancies/" + updateId[0], {
          published: false,
        })
        .then(() => {
          getJob();
          setUpdateId([]);
          if (updateId[0]) {
            toast.success(t("berhasiltutup"));
          }
        })
        .catch((err) => {
          toast.error("Terjadi kesalahan");
          throw err;
        })
        .finally(() => {
          setUpdateLoading(false);
        });
    } else if (updateId[1] === "publish") {
      request
        .put("v1/recruitment/vacancies/" + updateId[0], {
          published: true,
        })
        .then(() => {
          getJob();
          setUpdateId([]);
          if (updateId[0]) {
            toast.success(t("berhasilpublish"));
          }
        })
        .catch((err) => {
          toast.error("Terjadi kesalahan");
          throw err;
        })
        .finally(() => {
          setUpdateLoading(false);
        });
    } else {
      toast.error("Terjadi kesalahan");
    }
  }

  const changeFilterJob = (e) => {
    const { value, checked } = e.target;
    if (value === "allJob") {
      setFilterJob([]);
    }
    else {
      let arr = filterJob;
      arr.push(value);
      const set = new Set(arr);
      if (!checked) {
        set.delete(value);
      }
      const setArr = Array.from(set);
      setFilterJob(setArr);
    }
  }

  const changeFilterType = (e) => {
    const { value, checked } = e.target;
    if (value === "all") {
      setFilterType([])
    }
    else {
      let arr = filterType;
      arr.push(value);
      const set = new Set(arr);
      if (!checked) {
        set.delete(value);
      }
      const setArr = Array.from(set);
      setFilterType(setArr);
    }
  }

  useEffect(() => {
    setFilteredLowongan(lowongan.filter(item => {
      return filterType.length > 0 ? filterType.includes(item.type) : true;
    }))
    // eslint-disable-next-line
  }, [filterType])

  const handleSearch = (e) => {
    setSearch(e.target.value)
  }

  const handleReset = () => {
    setSearch(null);
    setFilterJob([]);
    setFilterType([]);
    setRange({ ...range, min: 0, max: 1e8 })
  }

  const handleNumberOnly = (evt) => {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      evt.preventDefault()
    }
    return true;
  }

  const handleRange = () => {
    setRange({ ...range, min: inputRange.min, max: inputRange.max })
  }

  return (
    <div className="animated fadeIn p-4">
      <Row className="menu-dashboard">
        <Col className="mb-3">
          {privileges.includes("add-job") && (
            <div className="p-2">
              <h4 className="d-inline mr-3">Daftar Lowongan</h4>
              <Link to={"/recruitment/vacancies/create"} className="btn btn-sm btn-outline-netis-primary px-4" style={{ borderRadius: "8px" }}>
                <i className="fa fa-plus mr-2"></i>
                {t("buatlowonganbaru")}
              </Link>
            </div>
          )}
          <hr />
        </Col>
        <Col sm="12">
          <Row>
            <Col sm="3">
              <InputGroup className="my-2" style={{ borderRadius: "12px" }}>
                <InputGroupAddon addonType="prepend">
                  <InputGroupText className="input-group-transparent">
                    <i className="fa fa-search"></i>
                  </InputGroupText>
                </InputGroupAddon>
                <Input type="text" placeholder="Cari Nama Pekerjaan" className="input-search"
                  onChange={handleSearch}
                />
              </InputGroup><br />
              <b>Status Lowongan</b><br />
              <br />
              <CustomInput className="my-2" name="allJob" id="allJob" onChange={changeFilterJob} checked={filterJob.length === 0} value="allJob" type="checkbox" label="Semua lowongan" />
              <CustomInput className="my-2" name="published" id="published" onChange={changeFilterJob} checked={filterJob.length > 0 && filterJob.includes("published")} value="published" type="checkbox" label="Publish" />
              <CustomInput className="my-2" name="draft" id="draft" onChange={changeFilterJob} checked={filterJob.length > 0 && filterJob.includes("draft")} value="draft" type="checkbox" label="Draft" />
              <CustomInput className="my-2" name="expired" id="expired" onChange={changeFilterJob} checked={filterJob.length > 0 && filterJob.includes("expired")} value="expired" type="checkbox" label="Kadaluarsa" />
              <CustomInput className="my-2" name="closed" id="closed" onChange={changeFilterJob} checked={filterJob.length > 0 && filterJob.includes("closed")} value="closed" type="checkbox" label="Ditutup" />
              <br /><hr />
              <b>Tipe Pekerjaan</b><br /><br />
              <FormGroup>
                <CustomInput className="my-2" name="all" id="all" onChange={changeFilterType} checked={filterType.length === 0} value="all" type="checkbox" label="Semua Tipe" />
                <CustomInput className="my-2" name="Full-time" id="Full-time" onChange={changeFilterType} checked={filterType.length > 0 && filterType.includes("Full-time")} value="Full-time" type="checkbox" label="Full Time" />
                <CustomInput className="my-2" name="Part-time" id="Part-time" onChange={changeFilterType} checked={filterType.length > 0 && filterType.includes("Part-time")} value="Part-time" type="checkbox" label="Part Time" />
                <CustomInput className="my-2" name="Magang" id="Magang" onChange={changeFilterType} checked={filterType.length > 0 && filterType.includes("Magang")} value="Magang" type="checkbox" label="Magang" />
                <CustomInput className="my-2" name="Kontrak" id="Kontrak" onChange={changeFilterType} checked={filterType.length > 0 && filterType.includes("Kontrak")} value="Kontrak" type="checkbox" label="Kontrak" />
                <CustomInput className="my-2" name="Freelance" id="Freelance" onChange={changeFilterType} checked={filterType.length > 0 && filterType.includes("Freelance")} value="Freelance" type="checkbox" label="Freelace" />
              </FormGroup>
              <br /><hr />
              <b>Rentan Gaji</b><br /><br />
              <div className="px-1 my-2">
                <InputRange
                  maxValue={100000000}
                  value={range}
                  formatLabel={range => `${convertNumber(range)}`}
                  onChange={(value) => {
                    setRange(value)
                    setInputRange(value)
                  }}
                  onChangeComplete={(value) => {
                    setRange(value)
                    setInputRange(value)
                  }}
                />
              </div>
              <br />
              <Row className="d-flex justify-content-between mt-1">
                <Col sm="5">
                  <Label htmlFor="setMin"><small>Min.</small></Label>
                  <Input
                    type="text"
                    pattern="[0-9]*"
                    inputMode="numeric"
                    onKeyPress={handleNumberOnly}
                    id="setMin"
                    name="setMin"
                    value={inputRange.min}
                    onChange={(e) => setInputRange({ ...inputRange, min: e.target.value })}
                  />
                </Col>
                <Col sm="5">
                  <Label htmlFor="setMax"><small>Max.</small></Label>
                  <Input
                    type="text"
                    pattern="[0-9]*"
                    inputMode="numeric"
                    onKeyPress={handleNumberOnly}
                    id="setMax"
                    name="setMax"
                    value={inputRange.max}
                    onChange={(e) => setInputRange({ ...inputRange, max: e.target.value })}
                  />
                </Col>
              </Row>
              <Button
                color="netis-color"
                className="btn btn-sm my-3"
                onClick={handleRange}
                disabled={inputRange.min < 0 || inputRange.max > 1e8}
                style={{ borderRadius: "10px" }}
              >Filter Gaji</Button>
              <hr />
              <div className="text-center mt-2">
                <Button onClick={handleReset} className="button-reset btn btn-sm btn-outline-netis-primary px-5 mx-auto">
                  Reset Filter
                  </Button>
              </div>
            </Col>
            <Col sm="9">
              {loading ?
                <SkeletonRecruitmentMenu />
                :
                filteredLowongan.length > 0 ?
                  filteredLowongan.filter((item) => (
                    search ? item.name.toLowerCase().includes(search.toLowerCase()) : true
                  ))
                    .filter((itemJob) => {
                      return filterJob.length > 0 ? filterJob.includes(itemJob.status) : true
                    })
                    .filter((itemSalary) => {
                      return itemSalary.minSalary >= range.min && itemSalary.maxSalary <= range.max
                    })
                    .map((data) => (
                      <Card key={data.id} className="shadow-sm border-0">
                        <CardBody>
                          <Row>
                            <Col sm md="6">
                              {data.expiredAt !== null && data.published ? (
                                <div>
                                  <h5 style={{ display: "inline" }}>
                                    {data.name.toUpperCase()}
                                  </h5>
                                  {moment(momentToday).isSameOrAfter(data.expiredAt, "day") ? (
                                    <small
                                      className="text-danger"
                                      style={{ display: "inline" }}
                                    >
                                      {" "}
                            Expired
                                    </small>
                                  ) : (
                                      <small
                                        className="text-secondary"
                                        style={{ display: "inline" }}
                                      >
                                        {" "}
                            Berlaku hingga{" "}
                                        {moment(data.expiredAt)?.format("DD MMM YYYY")}
                                      </small>
                                    )}
                                </div>
                              ) : (
                                  <div>
                                    <h5 style={{ display: "inline" }}>
                                      {data.name.toUpperCase()}
                                    </h5>
                                    {data.expiredAt ? (
                                      <small
                                        className="text-warning"
                                        style={{ display: "inline" }}
                                      >
                                        {" "}
                            Closed
                                      </small>
                                    ) : (
                                        <small
                                          className="text-secondary"
                                          style={{ display: "inline" }}
                                        >
                                          {" "}
                            Draft
                                        </small>
                                      )}
                                  </div>
                                )}
                              <br />
                              <a
                                href={data.skillanaURL}
                                target="_blank"
                                rel="noopener noreferrer"
                              >
                                <Button
                                  style={{ border: 0 }}
                                  className="btn bg-transparent btn-sm mr-1 mt-1"
                                >
                                  <i className="fa fa-eye"></i> Lihat
                      </Button>
                              </a>
                              <Link to={`/recruitment/vacancies/${data.id}/edit`}>
                                <Button
                                  style={{ border: 0 }}
                                  className="btn bg-transparent btn-sm mr-1 mt-1"
                                >
                                  <i className="fa fa-pencil"></i> Edit
                      </Button>
                              </Link>
                              <Button
                                style={{ border: 0 }}
                                className="btn bg-transparent btn-sm mx-1 mt-1"
                                onClick={() => setDeletingId(data.id)}
                              >
                                <i className="fa fa-trash-o"></i> {t("hapus")}
                              </Button>
                              {data.expiredAt !== null && data.published ? (
                                <div style={{ display: "inline" }}>
                                  {moment(momentToday).isSameOrAfter(data.expiredAt, "day")
                                    ? (
                                      <Button
                                        style={{ border: 0 }}
                                        className="btn bg-transparent btn-sm mx-1 mt-1"
                                        onClick={() => setUpdateId([data.id, "repost"])}
                                      >
                                        <p
                                          className="text-info"
                                          style={{ display: "inline" }}
                                        >
                                          <i className="fa fa-repeat"></i> Repost
                            </p>
                                      </Button>
                                    ) : (
                                      <Button
                                        style={{ border: 0 }}
                                        className="btn bg-transparent btn-sm mx-1 mt-1"
                                        onClick={() => setUpdateId([data.id, "tutup"])}
                                      >
                                        <p
                                          className="text-danger"
                                          style={{ display: "inline" }}
                                        >
                                          <i className="fa fa-pause"></i> Tutup
                            </p>
                                      </Button>
                                    )}
                                </div>
                              ) : (
                                  <div style={{ display: "inline" }}>
                                    <Button
                                      style={{ border: 0 }}
                                      className="btn bg-transparent btn-sm mx-1 mt-1"
                                      onClick={() => setUpdateId([data.id, "publish"])}
                                    >
                                      <p
                                        className="text-success"
                                        style={{ display: "inline" }}
                                      >
                                        <i className="fa fa-play"></i>{" "}
                                        {data.expiredAt ? "Reopen" : "Publish"}
                                      </p>
                                    </Button>
                                  </div>
                                )}
                            </Col>
                            <Col sm md="6" className="text-center">
                              <Link
                                to={`/recruitment/vacancies/${data.id}/applicants?status=unprocessed`}
                                // state={data}
                                className="btn text-wrap btn-outline-dark mr-2 my-1 text-capitalize"
                              >
                                <h5 className="mb-0">{data.unprocessedApplicantCount}</h5>
                                {t("belumproses")}
                              </Link>
                              <Link
                                to={`/recruitment/vacancies/${data.id}/applicants?status=accepted`}
                                className="btn text-wrap btn-outline-success mr-2 my-1 text-capitalize"
                              >
                                <h5 className="mb-0">{data.acceptedApplicantCount}</h5>
                                {t("sesuai")}
                              </Link>
                              <Link
                                to={`/recruitment/vacancies/${data.id}/applicants?status=rejected`}
                                className="btn text-wrap btn-outline-danger mr-2 my-1 text-capitalize"
                              >
                                <h5 className="mb-0">{data.rejectedApplicantCount}</h5>
                                {t("tidaksesuai")}
                              </Link>
                            </Col>
                          </Row>
                        </CardBody>
                      </Card>
                    ))
                  : <DataNotFound />
              }
            </Col>
          </Row>
        </Col>
      </Row>
      <div className="menu-mobile">
        <Row className="mb-2">
          <Col xs="9">
            <InputGroup style={{ borderRadius: "12px" }}>
              <InputGroupAddon addonType="prepend">
                <InputGroupText className="input-group-transparent">
                  <i className="fa fa-search"></i>
                </InputGroupText>
              </InputGroupAddon>
              <Input type="text" placeholder="Cari Nama Pekerjaan" className="input-search"
                onChange={handleSearch}
              />
            </InputGroup>
          </Col>
          <Col xs="3" className="p-0 text-nowrap text-center">
            <Button
              onClick={() => console.log("a")}
              style={{
                backgroundColor: "transparent",
                border: "0px"
              }}
            >
              <i className="fa fa-2x fa-filter" style={{ color: "#335877" }} />&nbsp;
              <small style={{ color: "#335877" }}>Filter</small>
            </Button>
          </Col>
        </Row>
        {loading ?
          <SkeletonRecruitmentMenu />
          :
          filteredLowongan.length > 0 ?
            filteredLowongan.filter((item) => (
              search ? item.name.toLowerCase().includes(search.toLowerCase()) : true
            ))
              .filter((itemJob) => {
                return filterJob.length > 0 ? filterJob.includes(itemJob.status) : true
              })
              .filter((itemSalary) => {
                return itemSalary.minSalary >= range.min && itemSalary.maxSalary <= range.max
              })
              .map((data, idx) => (
                <Card key={idx} className="shadow" style={{ borderRadius: "10px" }}>
                  <div className="recruitment-label text-center" style={data.status === "published" ? { backgroundColor: "#335877" } : { backgroundColor: "#bfbfbf" }}>
                    {data.status === "published" ?
                      <span style={{ color: "#fff" }}>Berlaku hingga {moment(data.expiredAt)?.format("DD MMM YYYY")}</span>
                      :
                      <span style={data.status === "draft" ? { color: "#655c5c" } :
                        data.status === "expired" ? { color: "#ff0000" } :
                          { color: "#fdec13" }
                      }>
                        {data.status}</span>
                    }
                  </div>
                  <CardBody className="text-center">
                    <i className="fa fa-4x fa-briefcase job-icon" /><br />
                    <strong style={{ fontSize: 18, color: "#335877" }}><b>{data.name} - </b></strong>
                    <small className="text-muted">{data.type}</small><br />
                    {data.acceptedApplicantCount + data.rejectedApplicantCount + data.unprocessedApplicantCount}
                      &nbsp;pelamar<br />
                    <a
                      href={data.skillanaURL}
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      <Button
                        style={{ border: 0 }}
                        className="btn bg-transparent btn-sm mr-1 mt-1"
                      >
                        <i className="fa fa-eye"></i> Lihat
                        </Button>
                    </a>
                    <Link to={`/recruitment/vacancies/${data.id}/edit`}>
                      <Button
                        style={{ border: 0 }}
                        className="btn bg-transparent btn-sm mr-1 mt-1"
                      >
                        <i className="fa fa-pencil"></i> Edit
                        </Button>
                    </Link>
                    <Button
                      style={{ border: 0 }}
                      className="btn bg-transparent btn-sm mx-1 mt-1"
                      onClick={() => setDeletingId(data.id)}
                    >
                      <i className="fa fa-trash-o"></i> {t("hapus")}
                    </Button>
                    {data.expiredAt !== null && data.published ? (
                      <div style={{ display: "inline" }}>
                        {moment(momentToday).isSameOrAfter(data.expiredAt, "day")
                          ? (
                            <Button
                              style={{ border: 0 }}
                              className="btn bg-transparent btn-sm mx-1 mt-1"
                              onClick={() => setUpdateId([data.id, "repost"])}
                            >
                              <p
                                className="text-info"
                                style={{ display: "inline" }}
                              >
                                <i className="fa fa-repeat"></i> Repost
                                </p>
                            </Button>
                          ) : (
                            <Button
                              style={{ border: 0 }}
                              className="btn bg-transparent btn-sm mx-1 mt-1"
                              onClick={() => setUpdateId([data.id, "tutup"])}
                            >
                              <p
                                className="text-danger"
                                style={{ display: "inline" }}
                              >
                                <i className="fa fa-pause"></i> Tutup
                                </p>
                            </Button>
                          )}
                      </div>
                    ) : (
                        <div style={{ display: "inline" }}>
                          <Button
                            style={{ border: 0 }}
                            className="btn bg-transparent btn-sm mx-1 mt-1"
                            onClick={() => setUpdateId([data.id, "publish"])}
                          >
                            <p
                              className="text-success"
                              style={{ display: "inline" }}
                            >
                              <i className="fa fa-play"></i>{" "}
                              {data.expiredAt ? "Reopen" : "Publish"}
                            </p>
                          </Button>
                        </div>
                      )}
                    <br />
                    <Link
                      to={`/recruitment/vacancies/${data.id}/applicants?status=unprocessed`}
                      className="btn btn-netis-color mt-2"
                    >
                      Selengkapnya &nbsp; <i className="fa fa-arrow-right" />
                    </Link>
                  </CardBody>
                </Card>
              ))
            : <DataNotFound />
        }
      </div>
      <Modal
        isOpen={!!deletingId}
        toggle={() => {
          if (!deleteLoading) {
            setDeletingId(null);
          }
        }}
      >
        <ModalBody>
          <h6>{t("yakinmenghapus")}</h6>
          <div className="d-flex justify-content-end">
            {!deleteLoading && (
              <Button
                className="mr-2"
                color="light"
                onClick={() => setDeletingId(null)}
              >
                {t("batal")}
              </Button>
            )}
            <Button
              color="netis-primary"
              onClick={doDelete}
              disabled={deleteLoading}
            >
              {deleteLoading ? (
                <React.Fragment>
                  <Spinner size="sm" color="light" /> menghapus...
                </React.Fragment>
              ) : (
                  t("hapus")
                )}
            </Button>
          </div>
        </ModalBody>
      </Modal>

      <Modal
        isOpen={updateId[0] ? true : false}
        toggle={() => {
          if (!updateLoading) {
            setUpdateId([]);
          }
        }}
      >
        <ModalBody>
          <h6>
            {
              updateId[1] === "repost" ?
                "Apakah anda yakin akan mempublikasikan kembali lowongan ini?" :
                updateId[1] === "publish" ?
                  "Apakah anda yakin akan mempublikasikan kembali lowongan ini?" :
                  "Apakah anda yakin akan menutup lowongan ini?"
            }
          </h6>
          <div className="d-flex justify-content-end">
            {!updateLoading && (
              <Button
                className="mr-2"
                color="light"
                onClick={() => setUpdateId([])}
              >
                {t("batal")}
              </Button>
            )}
            <Button
              color="netis-primary"
              onClick={doUpdate}
              disabled={updateLoading}
            >
              {updateLoading ? (
                <React.Fragment>
                  <Spinner size="sm" color="light" /> Merubah...
                </React.Fragment>
              ) : (
                  t("ubah")
                )}
            </Button>
          </div>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default translate(RecruitmentMenu);
