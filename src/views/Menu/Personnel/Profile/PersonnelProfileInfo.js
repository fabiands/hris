import React, { useState } from 'react';
import { Button, Row, Form, FormGroup, Label, Input, Spinner } from 'reactstrap';
import { translate, t } from 'react-switch-lang';
import { useUserPrivileges } from '../../../../store';
import Select from 'react-select';
import { DatePickerInput } from 'rc-datepicker';
import { useFormik } from 'formik';
import request from '../../../../utils/request';
import { toast } from 'react-toastify';
import { formatDate } from '../../../../utils/formatter';
import useSWR from 'swr';
import LoadingAnimation from '../../../../components/LoadingAnimation';

const statuses = [
    { value: 'permanen', label: t('pekerjatetap') },
    { value: 'kontrak', label: t('pekerjakontrak') },
    { value: 'pekerja lepas', label: t('pekerjalepas') },
    { value: 'magang', label: t('magang') },
]

function PersonnelProfileInfo({ info }) {
    const { data: jobsResponse, jobsError } = useSWR('v1/master/jobs');
    const { data: unitsResponse, unitsError } = useSWR('v1/master/units');
    const { data: rolesResponse, rolesError } = useSWR('v1/master/roles');
    const { data: parentsResponse, parentsError } = useSWR('v1/personnels/parent/' + info.id);

    const pageLoading = !jobsResponse && !jobsError
        && !unitsResponse && !unitsError
        && !rolesResponse && !rolesError
        && !parentsResponse && !parentsError;

    const jobs = (jobsResponse?.data?.data ?? []);
    const units = (unitsResponse?.data?.data ?? []);
    const roles = [{ id: 'User', name: 'Non Admin' }, ...(rolesResponse?.data?.data ?? [])];
    const parents = (parentsResponse?.data?.data ?? []);

    const { can } = useUserPrivileges();
    const [loading, setLoading] = useState(false);
    const [editInfo, setEditInfo] = useState(false)
    const [avatar, setAvatar] = useState(info.avatar)
    const [initial, setInitial] = useState({
        fullName: info.fullName,
        nickName: info.nickName,
        job: {
            value: info.job?.id,
            label: info.job?.name
        },
        unit: {
            value: info.unit?.id,
            label: info.unit?.name
        },
        parent: {
            value: info.parentId?.id,
            label: info.parentId?.name
        },
        role: {
            value: info.roleId?.id ?? 'User',
            label: info.roleId?.name ?? 'Non Admin'
        },
        status: info.status,
        joinDate: info.joinDate
    })
    const [image, setImage] = useState({
        preview: '',
        raw: ''
    })
    const { values, ...formik } = useFormik({
        initialValues: {
            fullName: initial.fullName,
            nickName: initial.nickName,
            job: {
                value: initial.job.value,
                label: initial.job.label
            },
            unit: {
                value: initial.unit.value,
                label: initial.unit.label
            },
            parent: {
                value: initial.parent.value,
                label: initial.parent.label
            },
            role: {
                value: initial.role.value,
                label: initial.role.label
            },
            status: initial.status,
            joinDate: initial.joinDate
        },
        onSubmit: (values, { setSubmitting, setErrors }) => {
            setLoading(true)
            setSubmitting(true)
            const formData = new FormData();
            formData.append('fullName', values.fullName);
            formData.append('nickName', values.nickName);
            formData.append('roleId', values.role.value);
            formData.append('joinDate', formatDate(values.joinDate));
            if (values.job?.value) formData.append('jobId', values.job.value);
            if (values.unit?.value) formData.append('unitId', values.unit.value);
            formData.append('parent', values.parent.value);
            if (values.status) formData.append('status', values.status);
            if (image.preview) formData.append('avatar', avatar);

            request.post('v1/personnels/' + info.id, formData)
                .then(res => {
                    toast.success(t('Berhasil Mengedit Informasi Umum'))
                    setEditInfo(false)
                    setInitial({
                        fullName: values.fullName,
                        nickName: values.nickName,
                        job: {
                            value: values.job?.value,
                            label: values.job?.label
                        },
                        unit: {
                            value: values.unit?.value,
                            label: values.unit?.label
                        },
                        parent: {
                            value: values.parent?.value,
                            label: values.parent?.label
                        },
                        role: {
                            value: values.role?.value,
                            label: values.role?.label
                        },
                        status: values.status,
                        joinDate: values.joinDate
                    })
                })
                .catch(err => {
                    console.log(err)
                    toast.error(t('Gagal Mengedit Informasi Umum'))
                    return;
                })
                .finally(() => {
                    setLoading(false)
                    setSubmitting(false)
                })
        }
    })

    const changeAvatar = (e) => {
        e.preventDefault();
        if (e.target.files.length) {
            let preview = { ...image };
            preview['preview'] = URL.createObjectURL(e.target.files[0]);
            setAvatar(e.target.files[0])
            setImage(preview)
        }
    }

    const changeSelectOption = (value, opt) => {
        formik.setFieldTouched(opt.name);
        formik.setFieldValue(opt.name, value);
    }

    const changeStatus = function (pilihan) {
        formik.setFieldTouched('status', true)
        formik.setFieldValue('status', pilihan.value);
    }
    const onDatepickerChange = function (val) {
        formik.setFieldTouched('joinDate', true);
        formik.setFieldValue('joinDate', val instanceof Date && !isNaN(val) ? val : undefined, true);
    };
    const onDatepickerClear = React.useCallback(function () {
        formik.setFieldTouched('joinDate', true);
        formik.setFieldValue('joinDate', undefined, true);
    }, [formik]);

    const cancelEditInfo = () => {
        setEditInfo(false)
        formik.handleReset()
        setImage({
            preview: '',
            raw: ''
        })
        setAvatar(info.avatar)
    }


    if (pageLoading) {
        return <LoadingAnimation />
    }

    return (
        <div>
            {/* <Button onClick={() => console.log(info)} /> */}
            <Form onSubmit={formik.handleSubmit}>
                <Row className="my-1">
                    <div className="col-12 d-flex justify-content-between align-items-center mb-3">
                        <h5 className="content-sub-title mb-0">{t('informasiumum')}</h5>
                        {!editInfo ?
                            <Button color="netis-color"
                                className={`${can('edit-employee-profile') ? '' : ' d-none'}`}
                                onClick={() => setEditInfo(true)}
                            >
                                <i className="fa fa-pencil" style={{ marginRight: 5 }}></i>Edit
                            </Button>
                            : null
                        }
                    </div>
                </Row>
                <Row className="my-1">
                    <div className="col-md-4">
                        <div className="text-center">
                            {avatar === null ?
                                image.preview ?
                                    (
                                        <img src={image.preview} alt="dummy" width="200px" height="auto" />
                                    ) :
                                    (
                                        <div className="frame-profile-picture-empty">
                                            {t('belumadafoto')}
                                        </div>
                                    ) :
                                image.preview ?
                                    (
                                        <img src={image.preview} alt="dummy" width="200px" height="auto" />
                                    ) :
                                    (
                                        <div className="frame-profile-picture">
                                            <img src={process.env.REACT_APP_DOMAIN + "" + info.avatar} alt="avatar" className="img-fluid" />
                                        </div>
                                    )
                            }
                        </div>
                        {editInfo ?
                            <FormGroup>
                                <p className="text-center"> Upload Avatar <span style={{ fontWeight: 'bold' }} >( Max. 5 MB )</span></p>
                                <Input type="file" id="avatar" accept="image/x-png,image/jpeg" name="avatar" onChange={changeAvatar} />
                            </FormGroup>
                            : null
                        }
                    </div>
                    <div className="col-md-8">
                        <FormGroup>
                            <Label htmlFor="fullName" className="input-label">{t('namalengkap')}</Label>
                            <Input type="text" id="fullName" name="fullName" placeholder={t('namalengkap')}
                                disabled={editInfo === false ? true : false}
                                value={values.fullName} onChange={formik.handleChange} required
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label htmlFor="nickName" className="input-label">{t('namapanggilan')}</Label>
                            <Input type="text" id="nickName" name="nickName" placeholder={t('namapanggilan')}
                                disabled={editInfo === false ? true : false}
                                value={values.nickName} onChange={formik.handleChange} required
                            />
                        </FormGroup>
                    </div>
                </Row>
                {can('edit-employee-profile') ?
                    <>
                        <Row style={{ marginBottom: 20 }} className="my-1">
                            <div className="col-md-4">
                                <FormGroup >
                                    <Label htmlFor="job" className="input-label">{t('jabatan')}</Label>
                                    <Select
                                        id="job"
                                        name="job"
                                        value={values.job}
                                        onChange={changeSelectOption}
                                        options={jobs.map(jobs => ({ value: jobs.id, label: jobs.name }))}
                                        isDisabled={editInfo === false ? true : false}
                                    />
                                </FormGroup>
                            </div>
                            <div className="col-md-4">
                                <FormGroup style={{ marginBottom: 5 }}>
                                    <Label htmlFor="unit" className="input-label">Unit</Label>
                                    <Select
                                        id="unit"
                                        name="unit"
                                        value={values.unit}
                                        onChange={changeSelectOption}
                                        options={units.map(units => ({ value: units.id, label: units.name }))}
                                        isDisabled={editInfo === false ? true : false}
                                    />
                                </FormGroup>
                            </div>
                            <div className="col-md-4">
                                <FormGroup>
                                    <Label htmlFor="parentId" className="input-label">{t('atasan')}</Label>
                                    <Select
                                        id="parentId"
                                        name="parent"
                                        value={values.parent.value ? values.parent : { value: null, label: t('Tanpa Atasan') }}
                                        onChange={changeSelectOption}
                                        options={[{ value: null, label: t('Tanpa Atasan') }].concat(parents.map(parents => ({ value: parents.personnelId, label: parents.fullName })))}
                                        isDisabled={editInfo === false ? true : false}
                                    />
                                </FormGroup>
                            </div>
                        </Row>
                        <Row>
                            <div className="col-md-4">
                                <FormGroup>
                                    <Label htmlFor="roleId" className="input-label">{t('Role User')}</Label>
                                    <Select
                                        id="roleId"
                                        name="role"
                                        value={values.role}
                                        onChange={changeSelectOption}
                                        options={roles.map(roles => ({ value: roles.id, label: roles.name }))}
                                        isDisabled={editInfo === false ? true : false}
                                    />
                                </FormGroup>
                            </div>
                            <div className="col-md-4">
                                <FormGroup>
                                    <Label htmlFor="joinDate" className="input-label">{t('tanggalbergabung')}</Label>
                                    <DatePickerInput
                                        onClear={onDatepickerClear}
                                        showOnInputClick={true}
                                        closeOnClickOutside={true}
                                        onChange={onDatepickerChange}
                                        disabled={editInfo === false ? true : false}
                                        value={values.joinDate}
                                        className='my-custom-datepicker-component'
                                        displayFormat="DD MMMM YYYY"
                                        readOnly
                                        maxDate={new Date()}
                                    />
                                </FormGroup>
                            </div>
                            <div className="col-md-4">
                                <FormGroup>
                                    <Label htmlFor="contractStatus" className="input-label">{t('kontrakstatus')}</Label>
                                    <Select
                                        id="contractStatus"
                                        name="status"
                                        value={statuses.find(stat => stat.value === values.status)}
                                        onChange={changeStatus}
                                        options={statuses}
                                        isDisabled={editInfo === false ? true : false}
                                    />
                                </FormGroup>
                            </div>
                        </Row>
                    </>
                    :
                    null
                }
                <Row>
                    <div className="col-12 d-flex justify-content-end">
                        {
                            editInfo ?
                                <>
                                    <Button color="white" disabled={loading} className="btn-white mr-3" onClick={cancelEditInfo}>{t('batal')}</Button>
                                    <Button type="submit" disabled={loading} color="netis-color" style={{ width: '67px' }}>
                                        {loading ? <Spinner color="light" size="sm" /> : t('simpan')}
                                    </Button>
                                </>
                                : null
                        }
                    </div>
                </Row>
            </Form>
        </div>
    )
}

export default translate(PersonnelProfileInfo)
