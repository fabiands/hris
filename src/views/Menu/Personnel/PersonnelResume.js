import React from "react";
import DataFormal from "./PersonnelResume/PersonnelResumeDataFormal";
import DataNonFormal from "./PersonnelResume/PersonnelResumeDataNonFormal";
import DataWorkingHistory from "./PersonnelResume/PersonnelResumeDataWorkingHistory";
import DataSkill from "./PersonnelResume/PersonnelResumeDataSkill";

function PersonnelResume({personnel}) {

    return (
        <div className="animated fadeIn">
            {/* <h4 className="content-title mb-4">{t('informasiperusahaan')}</h4> */}
            <DataFormal user={personnel} />
            <DataNonFormal user={personnel} />
            <DataWorkingHistory user={personnel} />
            <DataSkill user={personnel} />
        </div>
    )
}

export default PersonnelResume
