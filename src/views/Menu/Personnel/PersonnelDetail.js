import React, { Component, Fragment, memo, useRef } from 'react';
import { Row, Col, Nav, NavItem, NavLink, TabContent, TabPane, Button, Modal, ModalBody, Spinner, FormGroup, Label, CustomInput, DropdownToggle, DropdownMenu, DropdownItem, UncontrolledDropdown } from 'reactstrap';
import PersonnelDocument from './PersonnelDocument';
import PersonnelResume from './PersonnelResume';
import PersonnelProfileNew from './PersonnelProfileNew';
import PersonnelContractHistory from './PersonnelContractHistory';
import LoadingAnimation from './component/atom/LoadingAnimation';
import { connect } from 'react-redux';
import PersonnelTaxId from './PersonnelTaxId';
import { toast } from 'react-toastify';
import { DatePickerInput } from 'rc-datepicker';
import request, { requestDownload } from '../../../utils/request';
import { translate, t } from 'react-switch-lang';
// import PersonnelAssessmentResult from './PersonnelAssessmentResult';
import { setUser } from '../../../actions/auth';
import DataNotFound from '../../../components/DataNotFound';
import { withPrivileges } from '../../../store';
import Select from 'react-select';
class PersonnelDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeTab: 0,
            loading: true,
            loadingButton: false,
            modalDeleteData: false,
            modalNonaktifData: false,
            personnel: {},
            parents: [],
            endDate: this.formatDate(new Date()),
            checkSaved: false,
            downloadProfileLoading: false,
            userPrivileges: props.user.privileges,
            supervisor: null,
        };
    }

    toggleCheckSaved = () => {
        this.setState({ checkSaved: !this.state.checkSaved });
    }

    downloadProfile = () => {
        if (this.state.downloadProfileLoading) {
            return;
        }
        this.setState({ downloadProfileLoading: true });
        requestDownload(`v1/personnels/${this.state.personnel.id}/download-profile`)
            .finally(() => {
                this.setState({ downloadProfileLoading: false })
            })
    }

    componentDidMount = () => {
        request.get('v1/personnels?id=' + this.props.match.params.id)
            .then(res => {
                const personnel = (res?.data?.data ?? {});
                this.setState({ personnel });
            })
            .catch(error => {
                console.log(error)
            })
            .finally(() => {
                this.setState({ loading: false })
            });

        request.get('v1/personnels/parent/' + this.props.match.params.id)
            .then(res => {
                const parents = (res?.data?.data ?? {});
                this.setState({ parents });
            })
            .catch(error => {
                console.log(error)
            })
            .finally(() => {
                this.setState({ loading: false })
            });
    }

    handleChangeDate = (date, formattedDate) => {
        this.setState({ endDate: formattedDate })
    }

    handleChangeSupervisor = (value) => {
        this.setState({ supervisor: value })
    }

    formatDate = (date) => {
        let d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }
    modalNonaktifData = () => {
        this.setState({ modalNonaktifData: !this.state.modalNonaktifData, modalDeleteData: false })
    }
    modalDeleteData = () => {
        this.setState({ modalDeleteData: !this.state.modalDeleteData, modalNonaktifData: false })
    }

    disableData = () => {
        if (!this.state.userPrivileges.includes('verify-employee')) {
            toast.error(t('Maaf anda tidah boleh melakukan aksi ini'))
            return
        }
        this.setState({ loadingButton: true })
        const objectData = {
            fullName: this.state.personnel.fullName,
            nickName: this.state.personnel.nickName,
            phone: this.state.personnel.phone,
            email: this.state.personnel.email,
            dateOfBirth: this.state.personnel.dateOfBirth,
            active: 'nonaktif',
            endDate: this.state.endDate,
            replacementParent: this.state.supervisor?.value ?? null
        }
        request.put('v1/personnels/' + this.state.personnel.id, objectData)
            .then(res => {
                setTimeout(() => {
                    this.setState({ loadingButton: false })
                    toast.success('Success Non Active', { autoClose: 3000 })
                    this.props.history.push('/personnels')
                }, 500)
            })
            .catch(error => {
                toast.error('Error', { autoClose: 3000 })
            })
    }

    deleteData = () => {
        if (!this.state.userPrivileges.includes('delete-employee')) {
            toast.error(t('Maaf anda tidah boleh melakukan aksi ini'))
            return
        }
        this.setState({ loadingButton: true })
        request.delete('v1/personnels/' + this.state.personnel.id)
            .then(res => {
                setTimeout(() => {
                    this.setState({ loadingButton: false })
                    toast.success('Success Delete', { autoClose: 3000 })
                    this.props.history.push('/personnels')
                }, 500)
            })
            .catch(error => {
                toast.error('Error', { autoClose: 3000 })
                console.log(error)
            })
    }

    handleResign(e) {
        if (!this.state.userPrivileges.includes('edit-employee')) {
            toast.error(t('Maaf anda tidah boleh melakukan aksi ini'))
            return
        }
        e.preventDefault();
        let active = { ...this.state.personnel, active: 2 };
        this.setState({
            personnel: active,
        }, () => {
            request.put('v1/personnels/' + this.state.personnel.id + '/active', this.state.personnel)
                .then(res => {
                    setTimeout(() => {
                        this.setState({ loadingButton: false })
                        toast.success('Success Non Active', { autoClose: 3000 })
                        this.props.history.push('/personnels')
                    }, 500)
                })
                .catch(
                    toast.error('Error', { autoClose: 3000 }))
        });

    };

    handleChangeDataVerification = (status) => {
        this.setState({ personnel: { ...this.state.personnel, verif: status } });
    }

    tabsAvailable = () => {
        const { can, t } = this.props;
        const tabs = [];
        if (can('read-employee-profile')) {
            tabs.push({ title: t('Profil'), component: PersonnelProfileNew });
        }
        if (can('browse-employee-document')) {
            tabs.push({ title: t('dokumen'), component: PersonnelDocument });
        }
        if (can('read-employee-pajak')) {
            tabs.push({ title: t('identitaspajak'), component: PersonnelTaxId });
        }
        if (can('browse-employee-resume')) {
            tabs.push({ title: 'Resume', component: PersonnelResume });
        }
        if (can('browse-employee-history')) {
            tabs.push({ title: t('riwayatkontrak'), component: PersonnelContractHistory });
        }
        // if (can('browse-assessment-result')) {
        //     tabs.push({ title: t('hasilasesmen'), component: PersonnelAssessmentResult })
        // }
        return tabs;
    }

    render() {
        if (this.state.loading) {
            return <LoadingAnimation />
        }

        const tabsAvailable = this.tabsAvailable();
        const ActiveTabComponent = tabsAvailable[this.state.activeTab].component;
        if (!ActiveTabComponent || !this.state.personnel) {
            return <DataNotFound />
        }

        const { t, can, canAny } = this.props;

        return (
            <div className="">
                <Row className="align-items-center mb-3">
                    <Col sm="6" md="6">
                        <h4 className="content-title">{t('Detail Karyawan')}</h4>
                    </Col>
                    <Col sm="6" className="text-right">
                        <div className="d-flex justify-content-end align-items-center h-100">
                            {can('verify-employee') &&
                                <DataVerificationButton personnel={this.state.personnel} onChanged={this.handleChangeDataVerification} />
                            }
                            {canAny(['verify-employee', 'download-employee']) && <UncontrolledDropdown className="ml-3">
                                <DropdownToggle outline color="dark">
                                    <i className="fa fa-bars mr-2"></i> {t('lainnya')}
                                </DropdownToggle>
                                <DropdownMenu right>
                                    {can('download-employee') &&
                                        <DropdownItem toggle={false} onClick={this.downloadProfile} disabled={this.state.downloadProfileLoading}>
                                            {this.state.downloadProfileLoading ? <Fragment><Spinner size="sm" /> Loading... </Fragment> : <span><i className="fa fa-download mr-1"></i> {t('Unduh Profil')} (PDF)</span>}
                                        </DropdownItem>
                                    }
                                    {can('verify-employee') &&
                                        <DropdownItem onClick={this.modalDeleteData}>
                                            {(this.state.personnel.active === 'aktif' || this.state.personnel.active === null) ?
                                                <Fragment><i className="fa fa-user-times mr-2"></i> {t('nonaktifkan')} {t('karyawan')}</Fragment>
                                                :
                                                <Fragment><i className="fa fa-trash mr-2"></i> {t('hapus')} {t('karyawan')}</Fragment>
                                            }
                                        </DropdownItem>
                                    }
                                </DropdownMenu>
                            </UncontrolledDropdown>}
                        </div>
                    </Col>
                </Row>
                <Nav tabs>
                    {tabsAvailable.map(({ title }, tabIndex) => (
                        <NavItem key={title}>
                            <NavLink active={this.state.activeTab === tabIndex} onClick={() => this.setState({ activeTab: tabIndex })}>
                                {title}
                            </NavLink>
                        </NavItem>
                    ))}
                </Nav>
                <TabContent className="mb-12" activeTab={this.state.activeTab}>
                    <TabPane tabId={this.state.activeTab}>
                        <ActiveTabComponent personnel={this.state.personnel} />
                    </TabPane>
                </TabContent>
                {/* Modal Box {t('nonaktifkan')} Karyawan */}
                <Modal isOpen={this.state.modalDeleteData} toggle={this.modalDeleteData} className={this.props.className}>
                    <ModalBody>
                        <h5 className="content-sub-title mb-5">
                            {this.state.personnel.active === 'aktif' || this.state.personnel.active === null ? t('nonaktifkan') : t('hapus') + " Data"}  {t('karyawan')}
                        </h5>
                        <Row className="mb-5">
                            {this.state.personnel.active === 'aktif' || this.state.personnel.active === null ?
                                <div className="col-12">
                                    <h6>{t('yakinnonaktif')} <strong>{this.state.personnel.fullName}</strong> {t('telahresign')}</h6>
                                    <FormGroup>
                                        <Label htmlFor="endDate" className="input-label">{t('pilihtglresign')}</Label>
                                        <DatePickerInput
                                            name="endDate"
                                            id="endDate"
                                            onChange={this.handleChangeDate}
                                            returnFormat="YYYY-MM-DD"
                                            value={this.state.endDate}
                                            className='my-custom-datepicker-component'
                                            required
                                            showOnInputClick={true}
                                            closeOnClickOutside={true}
                                            displayFormat="DD MMMM YYYY"
                                            readOnly
                                        />
                                    </FormGroup>

                                    <FormGroup>
                                        <Label htmlFor="parentId" className="input-label">{t('Pilih Atasan Pengganti')}</Label>
                                        <Select
                                            id="parentId"
                                            name="parent"
                                            onChange={this.handleChangeSupervisor}
                                            value={this.state.supervisor?.value ? this.state.supervisor : { value: null, label: t('Tanpa Atasan') }}
                                            options={[{ value: null, label: t('Tanpa Atasan') }].concat(this.state.parents.map(parents => ({ value: parents.personnelId, label: parents.fullName })))}
                                        />
                                    </FormGroup>
                                </div> :
                                <div className="col-12 text-center">
                                    <h6>{t('yakinmenghapuskaryawan')} <strong>{this.state.personnel.fullName}</strong> ?</h6>
                                </div>
                            }
                        </Row>
                        <Row>
                            {(this.state.personnel.active === 'aktif' || this.state.personnel.active === null) &&
                                <div className="col-12 d-flex justify-content-end">
                                    <Button className="mr-2" color="white" onClick={this.modalDeleteData}>{t('batal')}</Button>
                                    <Button color="danger" style={{ width: '70px' }} onClick={this.disableData}>
                                        {this.state.loadingButton ? <Spinner color="light" size="sm" /> : t('setuju')}
                                    </Button>
                                </div>
                            }
                            {this.state.personnel.active === 'nonaktif' &&
                                <div className="col-12">
                                    <CustomInput type="checkbox" id="areyousureSwitch" checked={this.state.checkSaved} onChange={this.toggleCheckSaved} name="switchSaved" label="Saya sudah mengunduh profil karyawan ini dan bersedia menghapus karyawan ini untuk selamanya" />
                                    <div className="d-flex justify-content-between mt-4 align-items-center">
                                        <Button color="link" size="sm" onClick={this.downloadProfile}>
                                            {this.state.downloadProfileLoading ? <Fragment><Spinner className="mr-1" size="sm" /> Loading...</Fragment> : <span><i className="fa fa-download mr-1"></i> {t('Unduh Profil')} (PDF)</span>}
                                        </Button>
                                        <Button color="danger" style={{ width: '70px', float: 'right' }} onClick={this.deleteData} disabled={!this.state.checkSaved}>
                                            {this.state.loadingButton ? <Spinner color="light" size="sm" /> : t('hapus')}
                                        </Button>
                                    </div>
                                </div>
                            }
                        </Row>
                    </ModalBody>
                </Modal>
            </div >
        );
    }
}

const DataVerificationButton = memo(({ personnel, onChanged }) => {
    const unverified = personnel.verif !== 'verified';
    const toastIdRef = useRef();

    const toggleVerify = () => {
        const isVerifying = personnel.verif !== 'verified';
        request.put('v1/personnels/' + personnel.id + '/verif')
            .then(res => {
                if (toastIdRef.current) {
                    toast.dismiss(toastIdRef.current);
                }
                if (isVerifying) {
                    toastIdRef.current = toast.success(t('data berhasil diverifikasi'))
                } else {
                    toastIdRef.current = toast.success(t('verifikasi data telah dibatalkan'))
                }
                onChanged(isVerifying ? 'verified' : 'unverified');
            })
    }

    return (
        <Button color="primary" onClick={toggleVerify}>
            {unverified ?
                <>
                    <i className='fa fa-check mr-2'></i>
                    {t('verifikasi')} {t('karyawan')}
                </>
                :
                <>
                    <i className='fa fa-close mr-2'></i>
                    {t('hapus')} {t('verifikasi')} {t('karyawan')}
                </>
            }
        </Button>
    );
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}
export default connect(mapStateToProps, { setUser })(withPrivileges(translate(PersonnelDetail)));
