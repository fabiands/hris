import React from 'react';
import PersonnelProfileInfo from './Profile/PersonnelProfileInfo';
import PersonnelProfileContact from './Profile/PersonnelProfileContact';
import PersonnelProfileIdentity from './Profile/PersonnelProfileIdentity';
import PersonnelProfileAddress from './Profile/PersonnelProfileAddress';
import useSWR from 'swr';
import LoadingAnimation from '../../../components/LoadingAnimation';

function PersonnelProfileNew({personnel}){
    const { data: countryResponse, error: countryError } = useSWR('v1/location/countries');
    const loading = !countryError && !countryResponse;

    if (loading) {
        return <LoadingAnimation/>
    }

    const country = (countryResponse?.data?.data ?? [])

    return(
        <div className="animated fadeIn">
            <div className="content-body my-2">
                <PersonnelProfileInfo info={personnel} />
            </div>
            <hr className="my-1" />
            <div className="content-body my-2">
                <PersonnelProfileContact contact={personnel} />
            </div>
            <hr className="my-1" />
            <div className="content-body my-2">
                <PersonnelProfileIdentity identity={personnel} country={country} />
            </div>
            <hr className="my-1" />
            <div className="content-body my-2">
                <PersonnelProfileAddress address={personnel} country={country} />
            </div>
        </div>
    )
}

export default PersonnelProfileNew;
