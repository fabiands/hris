import React, { useState, useEffect, useMemo, useCallback } from 'react'
import LoadingAnimation from '../../../components/LoadingAnimation';
import DataNotFound from '../../../components/DataNotFound';
import { translate, t } from 'react-switch-lang'
import { Button, Col, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Spinner } from 'reactstrap';
import Select from "react-select";
import { Link } from "react-router-dom";
import request, {requestDownload} from "../../../utils/request";
import moment from 'moment';
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from "../../../components/ResponsiveTable";
import { useUserPrivileges } from '../../../store';
import usePagination from '../../../hooks/usePagination';

const statusLabel = {
    permanen: t("pekerjatetap"),
    kontrak: t("pekerjakontrak"),
    magang: t("magang"),
}

function PersonnelActive() {
    moment.locale('id')
    const [isLoading, setIsLoading] = useState(false)
    const [isDownloading, setIsDownloading] = useState(null)
    const [notFound, setNotFound] = useState(false)
    const [personnels, setPersonnels] = useState([])
    const [units, setUnits] = useState([])
    const [jobs, setJobs] = useState([])
    const [search, setSearch] = useState(null)
    const { can } = useUserPrivileges();
    const [filterUnit, setFilterUnit] = useState([])
    const [filterJob, setFilterJob] = useState([])
    const [filters, setFilters] = useState([])

    const getPersonnels = () => {
        return request.get(`/v1/personnels/all/nonaktif`)
            .then((res) => setPersonnels(res.data.data))
            .catch((err) => setNotFound(true))
    }
    const getUnits = () => {
        return request.get(`/v1/master/units`)
            .then((res) => setUnits(res.data.data))
            .catch((err) => setNotFound(true))
    }
    const getJobs = () => {
        return request.get(`/v1/master/jobs`)
            .then((res) => setJobs(res.data.data))
            .catch((err) => setNotFound(true))
    }

    useEffect(() => {
        setIsLoading(true)
        getPersonnels()
        getUnits()
        getJobs()
        setIsLoading(false)
    }, [])

    const downloadProfile = (id) => {
        setIsDownloading(id)
        requestDownload(`v1/personnels/${id}/download-profile`)
        .finally(() => setIsDownloading(false))
    }

    const searchSpace = (e) => {
        setSearch(e.target.value)
    };
    const handleUnit = (e) => {
        let unit = e?.map(item => item.value) ?? []
        setFilterUnit(unit)
    }
    const handleJob = (e) => {
        let job = e?.map(item => item.value) ?? []
        setFilterJob(job)
    }

    const filtered = useMemo(() => {
        let filterData = personnels
            ?.filter(item =>
                search?.trim() ? item?.fullName?.toLowerCase().includes(search?.trim().toLowerCase()) : true
            )
            ?.filter(item =>
                filterUnit?.length > 0 ? filterUnit.includes(item.unit.id) : true
            )
            ?.filter(item =>
                filterJob?.length > 0 ? filterJob.includes(item.job.id) : true
            )
        return filterData;
    }, [personnels, search, filterUnit, filterJob])

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
    );

    const { data: groupFiltered, PaginationComponent, currentPage } = usePagination(
        filtered,
        8,
        filters.pagination,
        handleChangeCurrentPage
    );

    return (
        <div className="animated fadeIn">
            <h6 className="mt-2 font-weight-bold">{t('Sortir Berdasarkan')}</h6>
            <Row className="mb-4">
                <Col sm="6" md="4">
                    <Select
                        disabled={units.length < 1}
                        isMulti={true}
                        isSearchable={false}
                        name="unit"
                        options={units.map(item => ({ label: item.name, value: item.id }))}
                        placeholder={t("Unit")}
                        onChange={handleUnit}
                    />
                </Col>
                <Col sm="6" md="4">
                    <Select
                        disabled={jobs.length < 1}
                        isMulti={true}
                        isSearchable={false}
                        name="jabatan"
                        options={jobs.map(item => ({ label: item.name, value: item.id }))}
                        placeholder={t("Jabatan")}
                        onChange={handleJob}
                    />
                </Col>
                <Col sm="12" md="4">
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-search" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Input
                            type="text"
                            placeholder="Search"
                            className="input-search"
                            onChange={searchSpace}
                        />
                    </InputGroup>
                </Col>
            </Row>
            <Row>
                <Col xs="12" lg="12">
                    <Rtable size='sm'>
                        <Rthead>
                            <Rtr>
                                <Rth className="text-md-center w-5">No.</Rth>
                                <Rth className="w-30">{t("karyawan")}</Rth>
                                <Rth className="w-15">{t("telepon")}</Rth>
                                <Rth className="w-15">Email</Rth>
                                <Rth className="w-20">{t('Unit')}</Rth>
                                <Rth className="w-20">{t('Status Kontrak')}</Rth>
                                <Rth className="w-20">{t('Tanggal Nonaktif')}</Rth>
                                <Rth></Rth>
                            </Rtr>
                        </Rthead>
                        <Rtbody>
                            {isLoading ? <Rtr><Rtd colSpan="9999"><LoadingAnimation /></Rtd></Rtr>
                                : notFound ? <Rtr><Rtd colSpan="9999"><DataNotFound /></Rtd></Rtr>
                                    : groupFiltered?.map((data, idx) => (
                                        <Rtr key={idx}>
                                            <Rtd className="text-md-center">{idx + 1 + (8 * currentPage)}</Rtd>
                                            <Rtd>
                                                <Link
                                                    to={"/personnels/" + data.id}
                                                    className={`personnel-name ${can("read-employee") ? `` : `d-none`}`}
                                                >
                                                    {data.fullName}
                                                </Link><br />
                                                <i>{data.job.name}</i>
                                            </Rtd>
                                            <Rtd>{data.phone}</Rtd>
                                            <Rtd>{data.email}</Rtd>
                                            <Rtd>{data.unit.name}</Rtd>
                                            <Rtd>{statusLabel[data.status] ?? t("pekerjalepas")}</Rtd>
                                            <Rtd>{moment(data.endDate).format('DD MMM YYYY')}</Rtd>
                                            <Rtd>
                                                <Button color="link" className="text-nowrap" onClick={() => downloadProfile(data.id)}>
                                                    {(isDownloading === data.id) ? <Spinner size="sm" /> : <span><i className="fa fa-download" /> PDF</span>}
                                                </Button>
                                            </Rtd>
                                        </Rtr>
                                    ))
                            }
                        </Rtbody>
                    </Rtable>
                </Col>
            </Row>
            {filtered?.length > 8 && <PaginationComponent />}
        </div>
    )

}

export default translate(PersonnelActive)