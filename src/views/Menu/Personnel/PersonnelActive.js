import React, { useState, useEffect, Fragment, useMemo, useCallback } from 'react'
import LoadingAnimation from '../../../components/LoadingAnimation';
import DataNotFound from '../../../components/DataNotFound';
import { translate, t } from 'react-switch-lang'
import { Button, Col, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Modal, ModalBody, FormGroup, Label, Spinner } from 'reactstrap';
import Select from "react-select";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import { DatePickerInput } from "rc-datepicker";
import request from "../../../utils/request";
import moment from 'moment';
import { Formik } from "formik";
import * as Yup from 'yup';
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from "../../../components/ResponsiveTable";
import { useUserPrivileges } from '../../../store';
import usePagination from '../../../hooks/usePagination';

const verif = {
    verified: t("diverifikasi"),
    unverified: t("belumdiverifikasi")
}
const statusLabel = {
    permanen: t("pekerjatetap"),
    kontrak: t("pekerjakontrak"),
    magang: t("magang"),
}
const dataObject = {
    fullName: "",
    nickName: "",
    phone: "",
    email: "",
    placeOfBirth: "",
    dateOfBirth: "",
    parent: "",
    roleId: "",
    jobId: "",
    unitId: "",
}

function PersonnelActive() {
    const [modalAddData, setModalAddData] = useState(false)
    const [loading, setLoading] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const [notFound, setNotFound] = useState(false)
    const [personnels, setPersonnels] = useState([])
    const [units, setUnits] = useState([])
    const [jobs, setJobs] = useState([])
    const [search, setSearch] = useState(null)
    const [roles, setRoles] = useState([])
    const { can } = useUserPrivileges();
    const [modalUserOld, setModalUserOld] = useState(false)
    const [filterUnit, setFilterUnit] = useState([])
    const [filterJob, setFilterJob] = useState([])
    const [filters, setFilters] = useState([])

    const getPersonnels = () => {
        return request.get(`/v1/personnels/all/aktif`)
            .then((res) => setPersonnels(res.data.data))
            .catch((err) => setNotFound(true))
    }
    const getUnits = () => {
        return request.get(`/v1/master/units`)
            .then((res) => setUnits(res.data.data))
            .catch((err) => setNotFound(true))
    }
    const getJobs = () => {
        return request.get(`/v1/master/jobs`)
            .then((res) => setJobs(res.data.data))
            .catch((err) => setNotFound(true))
    }
    const getRoles = () => {
        return request.get(`/v1/master/roles`)
            .then((res) => setRoles(res.data.data))
            .catch((err) => setNotFound(true))
    }

    useEffect(() => {
        setIsLoading(true)
        getPersonnels()
        getUnits()
        getJobs()
        getRoles()
        setIsLoading(false)
    }, [])

    const searchSpace = (e) => {
        setSearch(e.target.value)
    };
    const handleUnit = (e) => {
        let unit = e?.map(item => item.value) ?? []
        setFilterUnit(unit)
    }
    const handleJob = (e) => {
        let job = e?.map(item => item.value) ?? []
        setFilterJob(job)
    }

    const filtered = useMemo(() => {
        let filterData = personnels
            ?.filter(item =>
                search?.trim() ? item?.fullName?.toLowerCase().includes(search?.trim().toLowerCase()) : true
            )
            ?.filter(item =>
                filterUnit?.length > 0 ? filterUnit.includes(item.unit.id) : true
            )
            ?.filter(item =>
                filterJob?.length > 0 ? filterJob.includes(item.job.id) : true
            )
        return filterData;
    }, [personnels, search, filterUnit, filterJob])

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
    );

    const { data: groupFiltered, PaginationComponent, currentPage } = usePagination(
        filtered,
        8,
        filters.pagination,
        handleChangeCurrentPage
    );

    return (
        <div className="animated fadeIn">
            <div className={can("add-employee") ? "d-flex justify-content-end mb-3" : " d-none"}>
                <Button
                    className={can("add-employee") ? undefined : " d-none"}
                    color="netis-color"
                    onClick={() => setModalAddData(!modalAddData)}
                >
                    <i className="fa fa-plus mr-2" />
                    {t("tambah")} Data
                </Button>
            </div>
            <h6 className="mt-2 font-weight-bold">{t('Sortir Berdasarkan')}</h6>
            <Row className="mb-4">
                <Col sm="6" md="4" className="my-1">
                    <Select
                        disabled={units.length < 1}
                        isMulti={true}
                        isSearchable={false}
                        name="unit"
                        options={units.map(item => ({ label: item.name, value: item.id }))}
                        placeholder={t("Unit")}
                        onChange={handleUnit}
                    />
                </Col>
                <Col sm="6" md="4" className="my-1">
                    <Select
                        disabled={jobs.length < 1}
                        isMulti={true}
                        isSearchable={false}
                        name="jabatan"
                        options={jobs.map(item => ({ label: item.name, value: item.id }))}
                        placeholder={t("Jabatan")}
                        onChange={handleJob}
                    />
                </Col>
                <Col sm="12" md="4" className="my-1">
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-search" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Input
                            type="text"
                            placeholder={t('Cari Nama')}
                            className="input-search"
                            onChange={searchSpace}
                        />
                    </InputGroup>
                </Col>
            </Row>
            <Row>
                <Col xs="12" lg="12">
                    <Rtable size='sm'>
                        <Rthead>
                            <Rtr>
                                <Rth className="text-md-center w-5">No.</Rth>
                                <Rth className="w-30">{t("karyawan")}</Rth>
                                <Rth className="w-15">{t("telepon")}</Rth>
                                <Rth className="w-15">Email</Rth>
                                <Rth className="w-20">{t('Unit')}</Rth>
                                <Rth className="w-20">{t("kontrakstatus")}</Rth>
                                <Rth className="w-20">Status</Rth>
                            </Rtr>
                        </Rthead>
                        <Rtbody>
                            {isLoading ? <Rtr><Rtd colSpan="9999"><LoadingAnimation /></Rtd></Rtr>
                                : notFound ? <Rtr><Rtd colSpan="9999"><DataNotFound /></Rtd></Rtr>
                                    : groupFiltered?.map((data, idx) => (
                                        <Rtr key={idx}>
                                            <Rtd className="text-md-center">{idx + 1 + (8 * currentPage)}</Rtd>
                                            <Rtd>
                                                <Link
                                                    to={"/personnels/" + data.id}
                                                    className={`personnel-name ${can("read-employee") ? `` : `d-none`}`}
                                                >
                                                    {data.fullName}
                                                </Link><br />
                                                <i>{data.job.name}</i>
                                            </Rtd>
                                            <Rtd>{data.phone}</Rtd>
                                            <Rtd>{data.email}</Rtd>
                                            <Rtd>{data.unit.name}</Rtd>
                                            <Rtd>{statusLabel[data.status] ?? t("pekerjalepas")}</Rtd>
                                            <Rtd>{verif[data.verif]}</Rtd>
                                        </Rtr>
                                    ))
                            }
                        </Rtbody>
                    </Rtable>
                </Col>
            </Row>
            {filtered?.length > 8 && <PaginationComponent />}
            <Modal
                isOpen={modalAddData}
                toggle={() => setModalAddData(!modalAddData)}
                className={"modal-lg "}
            >
                <ModalBody>
                    <h5 className="content-sub-title mb-4">
                        {t("Tambah Data Karyawan")}
                    </h5>
                    <Formik
                        initialValues={dataObject}
                        validationSchema={Yup.object().shape({
                            fullName: Yup.string().required().label(t('namalengkap')),
                            nickName: Yup.string().required().label(t('namapanggilan')),
                            phone: Yup.string().required().label(t('telepon')),
                            email: Yup.string().email().required().label(t('email')),
                            placeOfBirth: Yup.string().required().label(t('tempatlahir')),
                            dateOfBirth: Yup.string().required().label(t('Tanggal Lahir')),
                            // eslint-disable-next-line
                            jobId: Yup.number().oneOf(jobs.map(j => j.id), "${path} harus berupa salah satu dari pilihan yang ada").required().label(t('jabatan')),
                            // eslint-disable-next-line
                            unitId: Yup.number().oneOf(units.map(u => u.id), "${path} harus berupa salah satu dari pilihan yang ada").required().label(t('unit'))
                        })}
                        validateOnBlur
                        onSubmit={(values, { setErrors, resetForm }) => {
                            setLoading(true)
                            const data = values;
                            for (let keyName in data) {
                                if (typeof data[keyName] === 'string' || data[keyName] instanceof String) {
                                    data[keyName] = data[keyName].trim();
                                }
                            }
                            request.post("v1/personnels", data)
                                .then((res) => {
                                    setPersonnels(state => [...state, res.data.data])
                                    setModalAddData(!modalAddData)
                                    if (res?.status === 204) {
                                        setModalUserOld(true)
                                    }
                                    resetForm();
                                    toast.success("Success", { autoClose: 3000 });
                                })
                                .catch(err => {
                                    if (err?.response?.status === 403) {
                                        toast.error(t('User dengan email tersebut masih terdaftar di perusahaan lain'))
                                        return;
                                    }
                                    else if (err?.response?.status === 422) {
                                        setErrors(err.response.data.errors);
                                    }
                                })
                                .finally(() => setLoading(false))
                        }}
                    >{(formik) => {
                        return (<>
                            <Row>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label htmlFor="fullName" className="input-label">
                                            <b>{t("namalengkap")}</b>
                                        </Label>
                                        <Input
                                            type="text"
                                            name="fullName"
                                            id="fullName"
                                            placeholder={t("namalengkap")}
                                            onChange={formik.handleChange}
                                            value={formik.values.fullName}
                                            invalid={(formik.touched.fullName && !!formik.errors.fullName)}
                                        />
                                        {(formik.touched.fullName && !!formik.errors.fullName) && <span className="invalid-feedback">{formik.errors.fullName}</span>}
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label htmlFor="nickName" className="input-label">
                                            <b>{t("namapanggilan")}</b>
                                        </Label>
                                        <Input
                                            type="text"
                                            name="nickName"
                                            id="nickName"
                                            placeholder={t("namapanggilan")}
                                            onChange={formik.handleChange}
                                            value={formik.values.nickName}
                                            invalid={(formik.touched.nickName && !!formik.errors.nickName)}
                                        />
                                        {(formik.touched.nickName && !!formik.errors.nickName) && <span className="invalid-feedback">{formik.errors.nickName}</span>}
                                    </FormGroup>
                                </div>
                            </Row>
                            <Row>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label htmlFor="phone" className="input-label">
                                            <b>{t("telepon")}</b>
                                        </Label>
                                        <Input
                                            type="text"
                                            name="phone"
                                            id="phone"
                                            placeholder={t("telepon")}
                                            onChange={formik.handleChange}
                                            value={formik.values.phone}
                                            invalid={(formik.touched.phone && !!formik.errors.phone)}
                                        />
                                        {(formik.touched.phone && !!formik.errors.phone) && <span className="invalid-feedback">{formik.errors.phone}</span>}
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label htmlFor="email" className="input-label">
                                            <b>Email</b>
                                        </Label>
                                        <Input
                                            type="text"
                                            name="email"
                                            id="email"
                                            placeholder="Email"
                                            onChange={formik.handleChange}
                                            value={formik.values.email}
                                            invalid={(formik.touched.email && !!formik.errors.email)}
                                        />
                                        {(formik.touched.email && !!formik.errors.email) && <span className="invalid-feedback">{formik.errors.email}</span>}
                                    </FormGroup>
                                </div>
                            </Row>
                            <Row>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label htmlFor="placeOfBirth" className="input-label">
                                            <b>{t("tempatlahir")}</b>
                                        </Label>
                                        <Input
                                            type="text"
                                            name="placeOfBirth"
                                            id="placeOfBirth"
                                            placeholder={t("tempatlahir")}
                                            onChange={formik.handleChange}
                                            value={formik.values.placeOfBirth}
                                            invalid={(formik.touched.placeOfBirth && !!formik.errors.placeOfBirth)}
                                        />
                                        {(formik.touched.placeOfBirth && !!formik.errors.placeOfBirth) && <span className="invalid-feedback">{formik.errors.placeOfBirth}</span>}
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label htmlFor="dateOfBirth" className="input-label">
                                            <b>{t("tanggallahir")}</b>
                                        </Label>
                                        <DatePickerInput
                                            name="dateOfBirth"
                                            id="dateOfBirth"
                                            showOnInputClick={true}
                                            closeOnClickOutside={true}
                                            returnFormat={'YYYY-MM-DD'}
                                            onChange={(date, formattedDate) => { formik.setFieldValue('dateOfBirth', formattedDate); formik.setFieldError('dateOfBirth', null) }}
                                            startDate={moment().subtract(25, 'years')}
                                            startMode="year"
                                            value={formik.values.dateOfBirth}
                                            className={`my-custom-datepicker-component${(formik.touched.dateOfBirth && !!formik.errors.dateOfBirth) ? ' is-invalid' : ''}`}
                                            displayFormat="DD MMMM YYYY"
                                            readOnly
                                            maxDate={new Date()}
                                            placeholder={t('tanggallahir')}
                                        />
                                        {(formik.touched.dateOfBirth && !!formik.errors.dateOfBirth) && <small className="text-danger">{formik.errors.dateOfBirth}</small>}
                                    </FormGroup>
                                </div>
                            </Row>
                            <Row>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label htmlFor="roleId" className="input-label">
                                            <b>{t("Jenis User")}</b>
                                        </Label>
                                        <Select
                                            name="roleId"
                                            id="roleId"
                                            placeholder={t("Jenis User")}
                                            options={[{ id: null, name: 'Non Admin' }].concat(roles)}
                                            getOptionValue={p => p.id}
                                            getOptionLabel={p => p.name}
                                            onChange={(value, opt) => { formik.setFieldValue(opt.name, value?.id ?? null) }}
                                        />
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label htmlFor="parent" className="input-label">
                                            <b>{t("atasan")}</b>
                                        </Label>
                                        <Select
                                            name="parent"
                                            id="parent"
                                            options={personnels}
                                            getOptionValue={p => p.id}
                                            getOptionLabel={p => p.fullName}
                                            isClearable
                                            placeholder={t("atasan")}
                                            onChange={(value, opt) => { formik.setFieldValue(opt.name, value?.id ?? null) }}
                                        />
                                    </FormGroup>
                                </div>
                            </Row>
                            <Row>
                                <div className="col-md-6">
                                    <FormGroup>
                                        <Label htmlFor="jobId" className="input-label">
                                            <b>{t("jabatan")}</b>
                                        </Label>
                                        <Select
                                            name="jobId"
                                            id="jobId"
                                            options={jobs}
                                            getOptionValue={p => p.id}
                                            getOptionLabel={p => p.name}
                                            placeholder={t("jabatan")}
                                            onChange={(value, opt) => { formik.setFieldTouched(opt.name); formik.setFieldValue(opt.name, value?.id ?? null); }}
                                            styles={{ control: (base) => formik.touched.jobId && !!formik.errors.jobId ? ({ ...base, border: 'solid 1px #f86c6b !important' }) : base }}
                                        />
                                        {formik.touched.jobId && !!formik.errors.jobId && <small className="text-danger">{formik.errors.jobId}</small>}
                                    </FormGroup>
                                </div>
                                <div className="col-md-6">
                                    <FormGroup style={{ marginBottom: 5 }}>
                                        <Label htmlFor="unitId" className="input-label">
                                            <b>{t('Unit')}</b>
                                        </Label>
                                        <Select
                                            name="unitId"
                                            id="unitId"
                                            options={units}
                                            getOptionValue={p => p.id}
                                            getOptionLabel={p => p.name}
                                            placeholder={t('Unit')}
                                            onChange={(value, opt) => { formik.setFieldTouched(opt.name); formik.setFieldValue(opt.name, value?.id); }}
                                            styles={{ control: (base) => formik.touched.unitId && !!formik.errors.unitId ? ({ ...base, border: 'solid 1px #f86c6b !important' }) : base }}
                                        />
                                        {formik.touched.unitId && !!formik.errors.unitId && <small className="text-danger">{formik.errors.unitId}</small>}
                                    </FormGroup>
                                </div>
                            </Row>
                            <Row>
                                <div className="col-12 d-flex justify-content-end">
                                    {loading ? (
                                        <Spinner color="dark" size="sm" />
                                    ) : (
                                        <Fragment>
                                            <Button
                                                className="mr-2"
                                                color="white"
                                                onClick={() => setModalAddData(!modalAddData)}
                                            >
                                                {t("batal")}
                                            </Button>
                                            <Button
                                                type="submit"
                                                color="netis-color"
                                                style={{ width: "67px" }}
                                                onClick={formik.handleSubmit}
                                            >
                                                {t("simpan")}
                                            </Button>
                                        </Fragment>
                                    )}
                                </div>
                            </Row>
                        </>);
                    }}</Formik>
                </ModalBody>
            </Modal>
            <Modal isOpen={modalUserOld} toggle={() => setModalUserOld(false)} style={{ marginTop: "40vh" }}>
                <ModalBody className="text-center">
                    <h3 className="text-warning mb-3">{t('Perhatian')}</h3>
                    {t('daftarkaryawanlama')}
                    <div className="mt-3 text-center">
                        <Button color="netis-color" onClick={() => setModalUserOld(false)} disabled={loading}>
                            {loading ? <Spinner size="sm" color="light" /> : t('Oke')}
                        </Button>
                    </div>
                </ModalBody>
            </Modal>
        </div>
    )

}

export default translate(PersonnelActive)