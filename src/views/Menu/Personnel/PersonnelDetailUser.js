import React, { Component, Fragment } from 'react';
import { Row, Col, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import PersonnelDocument from './User/PersonnelDocument';
import PersonnelResume from './User/PersonnelResume';
import { connect } from 'react-redux';
import { translate } from 'react-switch-lang';
import PersonnelProfileNew from './PersonnelProfileNew';
import request from '../../../utils/request';
import LoadingAnimation from '../../../components/LoadingAnimation';

const TAB_PROFILE = 1;
const TAB_DOCUMENT = 2;
const TAB_RESUME = 3;

class PersonnelDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dropdownOpen: [false, false],
            activeTab: TAB_PROFILE,
            loading: true,
            loadingButton: false,
            personnel: null,
        };
    }

    setActiveTab(tab) {
        this.setState({ activeTab: tab });
    }

    toggle(tabPane, tab) {
        const newArray = this.state.activeTab.slice()
        newArray[tabPane] = tab
        this.setState({
            activeTab: newArray,
        });
    }

    componentDidMount = () => {
        request.get('v1/personnels?id=' + this.props.user.personnel.id)
            .then(res => {
                const personnel = res.data.data;
                this.setState({ personnel });
            })
            .finally(() => {
                this.setState({ loading: false });
            })
    }

    render() {
        const { t } = this.props;
        // let tabContent;
        // if (this.state.activeTab === TAB_PROFILE) {
        //     tabContent = <PersonnelProfile personnel={this.state.personnel.personnel} />
        // } else if (this.state.activeTab === TAB_DOCUMENT) {
        //     tabContent = <PersonnelDocument personnel={this.state.personnel.personnel} />
        // } else if (this.state.activeTab === TAB_RESUME) {
        //     tabContent = <PersonnelResume personnel={this.state.personnel.personnel} />
        // }
        // else if (this.state.activeTab[0] === '4') {
        //     tabContent = <PersonnelAssessmentResult isProfile={true} personnel={this.state.personnel.personnel} />
        // }

        if (this.state.loading) {
            return <LoadingAnimation />
        }
        return (
            <div className="">
                < Fragment >
                    <Row>
                        <Col sm="12" md="12">
                            <h4 className="content-title">{t('Detail Karyawan')}</h4>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" md="12" className="mb-12">
                            <Nav tabs>
                                <NavItem >
                                    <NavLink
                                        active={this.state.activeTab === TAB_PROFILE}
                                        onClick={() => { this.setActiveTab(TAB_PROFILE); }}
                                    >
                                        {t('Profil')}
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        active={this.state.activeTab === TAB_DOCUMENT}
                                        onClick={() => { this.setActiveTab(TAB_DOCUMENT); }}
                                    >
                                        {t('dokumen')}
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        active={this.state.activeTab === TAB_RESUME}
                                        onClick={() => { this.setActiveTab(TAB_RESUME); }}
                                    >
                                        Resume
                                            </NavLink>
                                </NavItem>
                                {/* <NavItem>
                                    <NavLink
                                        active={this.state.activeTab[0] === '4'}
                                        onClick={() => { this.toggle(0, '4'); }}
                                    >
                                        Hasil Asesmen
                                    </NavLink>
                                </NavItem> */}
                            </Nav>
                            <TabContent activeTab={this.state.activeTab}>
                                {/* <TabPane tabId={this.state.activeTab[0]}>
                                    {tabContent}
                                </TabPane> */}
                                <TabPane tabId={TAB_PROFILE}>
                                    <PersonnelProfileNew personnel={this.state.personnel} />
                                </TabPane>
                                <TabPane tabId={TAB_DOCUMENT}>
                                    <PersonnelDocument personnel={this.state.personnel} />
                                </TabPane>
                                <TabPane tabId={TAB_RESUME}>
                                    <PersonnelResume personnel={this.state.personnel} />
                                </TabPane>
                            </TabContent>
                        </Col>
                    </Row>
                </Fragment>
            </div >
        );
    }
}

export default connect(({ user }) => ({ user }))(translate(PersonnelDetail));
