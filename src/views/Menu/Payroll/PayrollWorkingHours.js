import React, { useCallback, useEffect, useMemo, useState } from 'react'
import Select from 'react-select'
import { t } from 'react-switch-lang'
import { Row, Col, Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap'
import DataNotFound from '../../../components/DataNotFound'
import LoadingAnimation from '../../../components/LoadingAnimation'
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../components/ResponsiveTable'
import request from '../../../utils/request'
import Datepicker from "react-datepicker";
import moment from 'moment';
import { useUserPrivileges } from '../../../store'
import ReactInputMask from 'react-input-mask'
import usePagination from '../../../hooks/usePagination';

function PayrollWorkingHours() {
    const { can } = useUserPrivileges();
    const [period, setPeriod] = useState(new Date(localStorage.getItem('period') ?? moment()) ?? new Date());
    const [data, setData] = useState([])
    const [finish, setFinish] = useState(false)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [search, setSearch] = useState(null)
    const [filterUnit, setFilterUnit] = useState([])
    const [filterJob, setFilterJob] = useState([])
    const [value, setValue] = useState(null)
    const [units, setUnits] = useState([]);
    const [jobs, setJobs] = useState([]);
    const [filters, setFilters] = useState([])

    const handleChangePeriod = useCallback((e) => {
        if(!e){
            let dateNow = new Date()
            setPeriod(dateNow)
            localStorage.setItem('period', dateNow)
        }
        if(e){
            setPeriod(e)
            localStorage.setItem('period', e)
        }
    }, [])

    useEffect(() => {
        setLoading(true)
        request.get(`v1/personnels/workinghours?period=${moment(period).format('MMYYYY')}`)
            .then((res) => {
                setData(res.data.data.personnels)
                setFinish(res.data.data.isFinish)
            })
            .catch((err) => {
                setError(true)
                console.log(err)
            })
            .finally(() => setLoading(false))
    }, [period])

    useEffect(() => {
        request.get('v1/master/units')
            .then((res) => setUnits(res.data.data.map((item) => {
                return { label: item.name, value: item.id };
            })))
            .catch((err) => { })
        request.get('v1/master/jobs')
            .then((res) => setJobs(res.data.data.map((item) => {
                return { label: item.name, value: item.id };
            })))
            .catch((err) => { })
    }, [])

    useEffect(() => {
        if (data) {
            const val = data?.map(item => ({
                [item.personnelId]: {
                    workinghours: item.working_hours ?? '',
                    overtimehours: item.overtime_hours ?? '',
                }
            }));
            const objVal = Object.assign({}, ...val)
            setValue(objVal)
        }
    }, [data])

    const handleSaveValue = (id) => {
        request.post(`v1/personnels/${id}/workinghours`, {
            working_hours: value[id].workinghours,
            overtime_hours: value[id].overtimehours,
            period: moment(period).format('MMYYYY')
        })
        // .then(() => toast.success('Sukses'))
        // .catch(() => toast.error('Error'))
    }

    const handleSearch = (e) => {
        setSearch(e.target.value);
    }
    const handleUnit = (e) => {
        let unit = e?.map(item => item.label) ?? []
        setFilterUnit(unit)
    }
    const handleJob = (e) => {
        let job = e?.map(item => item.label) ?? []
        setFilterJob(job)
    }
    const handleChangeValue = (id, val, type) => {
        let newVal;
        if (type === 'work') {
            newVal = { ...value, [id]: { ...value[id], workinghours: val } }
        } else {
            newVal = { ...value, [id]: { ...value[id], overtimehours: val } }
        }
        setValue(newVal)
    }

    const filtered = useMemo(() => {
        let filterData = data
            ?.filter(item =>
                search?.trim() ? item?.userFullName?.toLowerCase().includes(search?.trim().toLowerCase()) : true
            )
            ?.filter(item =>
                filterUnit?.length > 0 ? filterUnit.includes(item.unit) : true
            )
            ?.filter(item =>
                filterJob?.length > 0 ? filterJob.includes(item.job) : true
            )
        return filterData;
    }, [data, search, filterUnit, filterJob])

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
    );

    const { data: groupFiltered, PaginationComponent, currentPage } = usePagination(
        filtered,
        8,
        filters?.pagination,
        handleChangeCurrentPage
    );

    return (
        <div className="animated fadeIn">
            <Row>
                <Col xs="6">
                    <h4 className="content-title mb-4">{t('Perhitungan Jam Kerja')}</h4>
                </Col>
                <Col xs="5">
                    <InputGroup className="float-right period-group">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-calendar" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Datepicker
                            selected={period}
                            onChange={handleChangePeriod}
                            showMonthYearPicker
                            showFullMonthYearPicker
                            showFourColumnMonthYearPicker
                            dateFormatCalendar="MMMM"
                            calendarClassName="period-date"
                            dateFormat="MMMM yyyy"
                            maxDate={new Date()}
                            placeholderText="Select a date"
                            className="form-control"
                        />
                    </InputGroup>
                </Col>
            </Row>
            <Row>
                {/* <Col md="4">
                    <Button color="success" className="mr-2 mb-1">
                        <i className="fa fa-download mr-1" />
                        Download Template
                    </Button>
                    <Button color="success" className="mb-1">
                        <i className="fa fa-upload mr-1" />
                        Upload Data
                    </Button>
                </Col> */}
                <Col md="8" className="text-left">
                    <InputGroup className="w-50">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-search" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="search" onChange={handleSearch} name="search" placeholder={t('Search')} className="input-search" />
                    </InputGroup>
                </Col>
            </Row>
            <h6 className="mt-4 mb-1 font-weight-bold">{t('Sortir Berdasarkan')}</h6><br />
            <Row className="mb-4">
                <Col xs="6" md="3">
                    <Select
                        isMulti={true}
                        isSearchable={false}
                        name="unit"
                        options={units}
                        placeholder={t("Unit")}
                        onChange={handleUnit}
                    />
                </Col>
                <Col xs="6" md="3">
                    <Select
                        isMulti={true}
                        isSearchable={false}
                        name="jabatan"
                        options={jobs}
                        placeholder={t("Jabatan")}
                        onChange={handleJob}
                    />
                </Col>
            </Row>
            <Rtable size="sm">
                <Rthead>
                    <Rtr>
                        <Rth className="text-md-center w-5">No.</Rth>
                        <Rth>{t("Nama")}</Rth>
                        <Rth>{t('Jabatan')}</Rth>
                        <Rth>{t('Unit')}</Rth>
                        <Rth>{t('Jam Kerja')}</Rth>
                        <Rth>{t('Jam Lembur')}</Rth>
                    </Rtr>
                </Rthead>
                <Rtbody>
                    {
                        loading ?
                            <Rtr><Rtd colSpan="9999"><LoadingAnimation /></Rtd></Rtr>
                            : (error || filtered?.length < 1) ?
                                <Rtr><Rtd colSpan="9999"><DataNotFound /></Rtd></Rtr>
                                :
                                groupFiltered?.map((item, idx) =>
                                    <Rtr key={idx + 1 + (8*currentPage)}>
                                        <Rtd>{idx + 1 + (8*currentPage)}</Rtd>
                                        <Rtd>{item.userFullName}</Rtd>
                                        <Rtd>{item.job}</Rtd>
                                        <Rtd>{item.unit}</Rtd>
                                        <Rtd className="working-hours-input">
                                            <ReactInputMask
                                                className="form-control"
                                                name="workinghours"
                                                id="workinghours"
                                                mask="99:99"
                                                value={value[item.personnelId].workinghours ?? 0}
                                                onChange={(e) => handleChangeValue(item.personnelId, e.target.value, 'work')}
                                                onBlur={() => handleSaveValue(item.personnelId)}
                                                placeholder="__:__"
                                                disabled={finish || !can('write-workinghours-payroll')}
                                            >
                                                {(inputProps) => (
                                                    <Input
                                                        {...inputProps}
                                                        disabled={finish || !can('write-workinghours-payroll')}
                                                    />
                                                )}
                                            </ReactInputMask>
                                        </Rtd>
                                        <Rtd className="working-hours-input">
                                            <ReactInputMask
                                                className="form-control"
                                                name="overtimehours"
                                                id="overtimehours"
                                                mask="99:99"
                                                value={value[item.personnelId].overtimehours ?? 0}
                                                onChange={(e) => handleChangeValue(item.personnelId, e.target.value, 'overtime')}
                                                onBlur={() => handleSaveValue(item.personnelId)}
                                                placeholder="__:__"
                                                disabled={finish || !can('write-workinghours-payroll')}
                                            >
                                                {(inputProps) => (
                                                    <Input
                                                        {...inputProps}
                                                        disabled={finish || !can('write-workinghours-payroll')}
                                                    />
                                                )}
                                            </ReactInputMask>
                                        </Rtd>
                                    </Rtr>
                                )
                    }
                </Rtbody>
            </Rtable>
            {filtered.length > 8 && <PaginationComponent />}
        </div>
    )
}

export default PayrollWorkingHours