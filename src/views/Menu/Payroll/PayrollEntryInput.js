import moment from 'moment';
import React, { useCallback, useEffect, useState } from 'react'
import { t } from 'react-switch-lang';
import { Col, Input, Label, Row, Button, InputGroup, InputGroupAddon, InputGroupText, PopoverBody, Popover } from 'reactstrap'
import DataNotFound from '../../../components/DataNotFound';
import LoadingAnimation from '../../../components/LoadingAnimation';
import request from '../../../utils/request';
import TextInput from 'react-autocomplete-input';
import 'react-autocomplete-input/dist/bundle.css';
import { useHistory } from 'react-router';
import { toast } from 'react-toastify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Calculator from './Components/Calculator';
import Datepicker from "react-datepicker";
import { useUserPrivileges } from '../../../store';

function PayrollEntryInput({ match }) {
    const { can } = useUserPrivileges();
    const history = useHistory()
    const [period, setPeriod] = useState(new Date(localStorage.getItem('period') ?? moment()) ?? new Date());
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [isSubmitting, setIsSubmitting] = useState(false)
    const [allowance, setAllowance] = useState(null)
    const [cut, setCut] = useState(null)
    const [allCode, setAllCode] = useState(null)
    const [salary, setSalary] = useState("")
    const [haveSalary, setHaveSalary] = useState(false)
    const [valueAllow, setValueAllow] = useState(null)
    const [valueCut, setValueCut] = useState(null)
    const [finish, setFinish] = useState(false)
    const [errorFormula, setErrorFormula] = useState([{ code: '', error: false }]);
    const [popoverOpen, setPopoverOpen] = useState({ open: false, code: '' });

    const togglePopover = (code) => setPopoverOpen(state => ({ open: !state.open, code: code }));

    const handleChangePeriod = useCallback((e) => {
        if(!e){
            let dateNow = new Date()
            setPeriod(dateNow)
            localStorage.setItem('period', dateNow)
        }
        if(e){
            setPeriod(e)
            localStorage.setItem('period', e)
        }
    }, [])

    useEffect(() => {
        setLoading(true)
        request.get(`v1/personnels/${match.params.id}/payroll?period=${moment(period).format('MMYYYY')}`)
            .then((res) => {
                setSalary(res.data.data.salary)
                setAllowance(res.data.data.allowance)
                setCut(res.data.data.cuts)
                setFinish(res.data.data.isFinish)
            })
            .catch((err) => {
                console.log(err)
                setError(true)
            })
            .finally(() => setLoading(false))
    }, [match, period])

    useEffect(() => {
        if (salary) {
            setHaveSalary(true)
        }
        else if (!salary) {
            setHaveSalary(false)
        }
    }, [salary])

    useEffect(() => {
        if (allowance && cut) {
            const merge = allowance.concat(cut)
            setAllCode(merge)
            const valAllow = allowance?.map(item => ({ [item.code]: item.formula_value }));
            const valCut = cut?.map(item => ({ [item.code]: item.formula_value }));
            const newObjAllow = Object.assign({}, ...valAllow)
            const newObjCut = Object.assign({}, ...valCut)
            setValueAllow(newObjAllow)
            setValueCut(newObjCut)
        }
    }, [allowance, cut])

    const handleSubmitEntry = () => {
        setIsSubmitting(true)
        const submitAllowance = Object.keys(valueAllow).map(item =>
            valueAllow[item]
        )
        const submitCut = Object.keys(valueCut).map(item =>
            valueCut[item]
        )
        request.post(`v1/personnels/${match.params.id}/payroll`, {
            salary: salary,
            period: moment(period).format('MMYYYY'),
            allowance: submitAllowance,
            cuts: submitCut
        })
            .then(() => {
                toast.success(t('Berhasil Mengubah Data'))
                history.goBack()
            })
            .catch((err) => {
                toast.error(err?.response?.data?.message ?? t('Gagal Mengubah Data'))
                return;
            })
            .finally(() => setIsSubmitting(false))
    }

    const handleSalary = (e) => {
        const { value } = e.target
        if (!value) {
            setHaveSalary(false)
        }
        else {
            setHaveSalary(true)
        }
        setSalary(e.target.value)
    }

    const handleNumberOnly = (evt) => {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            evt.preventDefault()
        }
        return true;
    }

    const changeValueAllow = (value, code) => {
        let newVal = { ...valueAllow, [code]: value }
        setValueAllow(newVal)
    }
    const changeValueCut = (value, code) => {
        let newVal = { ...valueCut, [code]: value }
        setValueCut(newVal)
    }
    const codeOption = allCode?.map(item => { return item.code })

    const handleFormulaAllow = (value, code) => {
        const nextVal = value.replace('@', '')
        let newVal = { ...valueAllow, [code]: nextVal }
        setValueAllow(newVal)
    }
    const handleFormulaCut = (value, code) => {
        const nextVal = value.replace('@', '')
        let newVal = { ...valueCut, [code]: nextVal }
        setValueCut(newVal)
    }
    const handleWhiteSpaceAllow = (code) => {
        const handleValue = valueAllow[code]
        const replaceValue = handleValue.replace(/ /g, '');
        let newVal = { ...valueAllow, [code]: replaceValue }
        setValueAllow(newVal)
    }
    const handleWhiteSpaceCut = (code) => {
        const handleValue = valueCut[code]
        const replaceValue = handleValue.replace(/ /g, '+');
        let newVal = { ...valueCut, [code]: replaceValue }
        setValueCut(newVal)
    }

    const handleErrorFormula = useCallback((e, code) => {
        setErrorFormula(old => [...old].map(formula => {
            if (formula.code === code) {
                console.log('update', code, e);
                return { ...formula, error: e }
            }
            console.log('new', code, e)
            return { code: code, error: e };
        }))
    }, [setErrorFormula])

    if (loading) {
        return <LoadingAnimation />
    }
    if (error) {
        return <DataNotFound />
    }

    return (
        <div className="animated fadeIn mb-5 payroll-entry-input">
            <Row>
                <Col xs="6">
                    <h4 className="content-title mb-4">{t('Gaji Pokok')}</h4>
                </Col>
                <Col xs="5">
                    <InputGroup className="float-right period-group">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-calendar" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Datepicker
                            selected={period}
                            onChange={handleChangePeriod}
                            showMonthYearPicker
                            showFullMonthYearPicker
                            showFourColumnMonthYearPicker
                            dateFormatCalendar="MMMM"
                            calendarClassName="period-date"
                            dateFormat="MMMM yyyy"
                            maxDate={new Date()}
                            placeholderText="Select a date"
                            className="form-control"
                        />
                    </InputGroup>
                </Col>
            </Row>
            <Row>
                <Col sm="6" md="4" lg="3">
                    <Input
                        type="text"
                        id="salary"
                        name="salary"
                        onKeyPress={handleNumberOnly}
                        pattern="[0-9]*"
                        inputMode="numeric"
                        value={salary}
                        onChange={handleSalary}
                        disabled={finish || !can('write-entry-payroll')}
                    />
                </Col>
            </Row>
            <hr />
            <Row>
                <Col md="6">
                    <h4>{t('Tunjangan')}</h4>
                    <br />
                    <Row>
                        {allowance?.map((item, idx) =>
                            <Col xs="7" key={idx}>
                                <Label htmlFor={item.name} className="input-label mt-3">{item.name} [{item.code}]</Label>
                                {(item.formula_type === 'nominal' || item.formula_type === 'input') ?
                                    <Input
                                        pattern="[0-9]*"
                                        inputMode="numeric"
                                        type="text"
                                        id={item.name}
                                        name={item.name}
                                        onKeyPress={handleNumberOnly}
                                        disabled={!haveSalary || item.formula_type === 'nominal' || finish || !can('write-entry-payroll')}
                                        value={!haveSalary ? "" : valueAllow[item.code] ?? ""}
                                        onChange={(e) => changeValueAllow(e.target.value, item.code)}
                                    />
                                    :
                                    <>
                                        <InputGroup>
                                            <TextInput
                                                className="form-control"
                                                trigger="@"
                                                options={codeOption}
                                                id={item.name}
                                                name={item.name}
                                                disabled={!haveSalary || item.formula_type === 'rumus_hitungan' || finish || !can('write-entry-payroll')}
                                                value={!haveSalary ? "" : valueAllow[item.code] ?? ""}
                                                onChange={(e) => handleFormulaAllow(e, item.code)}
                                                onBlur={() => handleWhiteSpaceAllow(item.code)}
                                            />
                                            {(item.formula_type !== 'rumus_hitungan' && !finish && can('write-entry-payroll')) &&
                                                <InputGroupAddon addonType="prepend" className="position-absolute h-100" id={`popover-${item.code}`} style={{ right: 0, cursor: 'pointer' }}>
                                                    <InputGroupText>
                                                        <FontAwesomeIcon icon="calculator" />
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                            }
                                        </InputGroup>
                                        {(item.formula_type !== 'rumus_hitungan' && !finish && can('write-entry-payroll')) &&
                                            <small className="text-secondary"><i>{t("Pilih kode menggunakan '@'")}</i></small>
                                        }
                                        {(item.formula_type !== 'rumus_hitungan' && !finish && can('write-entry-payroll')) &&
                                            <Popover trigger="legacy" placement="bottom"
                                                isOpen={popoverOpen.open && popoverOpen.code === item.code}
                                                toggle={() => togglePopover(item.code)} target={`popover-${item.code}`}
                                                popperClassName="popover-payroll-entry-input"
                                                innerClassName="p-3"
                                            >
                                                <PopoverBody>
                                                    <Calculator allCode={allCode} onChangeFormulaValue={(e) => handleFormulaAllow(e, item.code)} onErrorFormula={(e) => handleErrorFormula(e, item.code)} defaultValue={valueAllow[item.code]} isOpen={true} showInput={false} />
                                                </PopoverBody>
                                            </Popover>
                                        }
                                    </>
                                }
                            </Col>
                        )}
                    </Row>
                </Col>
                <Col md="6">
                    <h4>{t('Potongan')}</h4>
                    <br />
                    <Row>
                        {cut?.map((item, idx) =>
                            <Col xs="7" key={idx}>
                                <Label htmlFor={item.name} className="input-label mt-3">{item.name} [{item.code}]</Label>
                                {(item.formula_type === 'nominal' || item.formula_type === 'input') ?
                                    <Input
                                        pattern="[0-9]*"
                                        inputMode="numeric"
                                        type="text"
                                        id={item.name}
                                        name={item.name}
                                        onKeyPress={handleNumberOnly}
                                        disabled={!haveSalary || item.formula_type === 'nominal' || finish || !can('write-entry-payroll')}
                                        value={!haveSalary ? "" : valueCut[item.code] ?? ""}
                                        onChange={(e) => changeValueCut(e.target.value, item.code)}
                                    />
                                    :
                                    <>
                                        <InputGroup>
                                            <TextInput
                                                className="form-control"
                                                trigger="@"
                                                options={codeOption}
                                                id={item.name}
                                                name={item.name}
                                                disabled={!haveSalary || item.formula_type === 'rumus_hitungan' || finish || !can('write-entry-payroll')}
                                                value={!haveSalary ? "" : valueCut[item.code] ?? ""}
                                                onChange={(e) => handleFormulaCut(e, item.code)}
                                                onBlur={() => handleWhiteSpaceCut(item.code)}
                                            />
                                            {(item.formula_type !== 'rumus_hitungan' && !finish && can('write-entry-payroll')) &&
                                                <InputGroupAddon addonType="prepend" className="position-absolute h-100" id={`popover-${item.code}`} style={{ right: 0, cursor: 'pointer' }}>
                                                    <InputGroupText>
                                                        <FontAwesomeIcon icon="calculator" />
                                                    </InputGroupText>
                                                </InputGroupAddon>
                                            }
                                        </InputGroup>
                                        {(item.formula_type !== 'rumus_hitungan' && !finish && can('write-entry-payroll')) &&
                                            <small className="text-secondary"><i>{t("Pilih kode menggunakan '@'")}</i></small>
                                        }
                                        {(item.formula_type !== 'rumus_hitungan' && !finish && can('write-entry-payroll')) &&
                                            <Popover trigger="legacy" placement="bottom"
                                                isOpen={popoverOpen.open && popoverOpen.code === item.code}
                                                toggle={() => togglePopover(item.code)} target={`popover-${item.code}`}
                                                popperClassName="popover-payroll-entry-input"
                                                innerClassName="p-3"
                                            >
                                                <PopoverBody>
                                                    <Calculator allCode={allCode} onChangeFormulaValue={(e) => handleFormulaCut(e, item.code)} onErrorFormula={(e) => handleErrorFormula(e, item.code)} defaultValue={valueAllow[item.code]} isOpen={true} showInput={false} />
                                                </PopoverBody>
                                            </Popover>
                                        }
                                    </>
                                }

                            </Col>
                        )}
                    </Row>
                </Col>
            </Row>
            <hr />
            {errorFormula.filter(err => err.error)[0]?.error &&
                <div className="mb-3">
                    <small className="text-muted">{t('Formula tidak valid pada komponen dengan kode')}: </small> <br />
                    <span className="text-danger">{errorFormula.filter(err => err.error)?.map(f => f.code)}</span>
                </div>
            }
            {!finish && can('write-entry-payroll') &&
                <>
                    <Button disabled={isSubmitting} onClick={() => history.goBack()} color="outline-netis-primary" className="mr-2">{t('Batal')}</Button>
                    <Button disabled={isSubmitting || (errorFormula.filter(err => err.error)[0]?.error ?? false)} color="netis-primary" className="ml-2" onClick={handleSubmitEntry}>{t('Simpan')}</Button>
                </>
            }
        </div>
    )

}

export default PayrollEntryInput