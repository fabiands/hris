// import { DatePickerInput } from 'rc-datepicker'
import React, { useEffect, useMemo, useRef, useState, useCallback } from 'react'
import { Link } from 'react-router-dom'
import Select from 'react-select'
import { t } from 'react-switch-lang'
import { Button, Row, Col, Input, InputGroup, InputGroupAddon, InputGroupText, Spinner } from 'reactstrap'
import DataNotFound from '../../../components/DataNotFound'
import LoadingAnimation from '../../../components/LoadingAnimation'
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../components/ResponsiveTable'
import request, { requestDownload } from '../../../utils/request'
import moment from 'moment';
import Datepicker from "react-datepicker";
import { toast } from 'react-toastify'
import usePagination from '../../../hooks/usePagination';

function PayrollEntry() {
    const inputFile = useRef(null)
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [search, setSearch] = useState(null)
    const [filterUnit, setFilterUnit] = useState([])
    const [filterJob, setFilterJob] = useState([])
    const [units, setUnits] = useState([]);
    const [jobs, setJobs] = useState([]);
    const [downloading, setDownloading] = useState(false)
    const [uploading, setUploading] = useState(false)
    const [period, setPeriod] = useState(new Date(localStorage.getItem('period') ?? moment()) ?? new Date());
    const [filters, setFilters] = useState([])

    const handleChangePeriod = useCallback((e) => {
        if(!e){
            let dateNow = new Date()
            setPeriod(dateNow)
            localStorage.setItem('period', dateNow)
        }
        if(e){
            setPeriod(e)
            localStorage.setItem('period', e)
        }
    }, [])

    useEffect(() => {
        setLoading(true)
        request.get('v1/personnels/all/aktif')
            .then((res) => {
                setData(res.data.data)
            })
            .catch((err) => {
                setError(true)
                console.log(err)
            })
            .finally(() => setLoading(false))
    }, [])

    useEffect(() => {
        request.get('v1/master/units')
            .then((res) => setUnits(res.data.data.map((item) => {
                return { label: item.name, value: item.id };
            })))
            .catch((err) => { })
        request.get('v1/master/jobs')
            .then((res) => setJobs(res.data.data.map((item) => {
                return { label: item.name, value: item.id };
            })))
            .catch((err) => { })
    }, [])

    const handleSearch = (e) => {
        setSearch(e.target.value);
    }
    const handleUnit = (e) => {
        let unit = e?.map(item => item.label) ?? []
        setFilterUnit(unit)
    }
    const handleJob = (e) => {
        let job = e?.map(item => item.label) ?? []
        setFilterJob(job)
    }

    const downloadTemplate = () => {
        setDownloading(true)
        requestDownload(`v1/personnels/payroll/template/${moment(period).format('MMYYYY')}`)
            .catch((err) => {
                toast.error(t(err?.response?.data?.message ?? t('Periode ini sudah selesai dibentuk, tidak bisa didownload')))
                return;
            })
            .finally(() => setDownloading(false))
    }

    const onButtonClick = () => {
        inputFile.current.click();
    };

    const onChangeFile = (e) => {
        setUploading(true)
        // console.log(e)
        // console.log(e.target.files[0])
        let form = new FormData();
        form.append('file', e.target.files[0], 'file_upload')
        request.post(`v1/personnels/payroll/import//${moment(period).format('MMYYYY')}`, form)
            .then((res) => {
                toast.success(t(res?.message ?? 'Success'))
            })
            .catch((err) => {
                toast.error(t(err?.response?.data?.message ?? 'Error'))
                return;
            })
            .finally(() => setUploading(false))
    }

    const filtered = useMemo(() => {
        let filterData = data
            ?.filter(item =>
                search?.trim() ? item?.fullName?.toLowerCase().includes(search?.trim().toLowerCase()) : true
            )
            ?.filter(item =>
                filterUnit?.length > 0 ? filterUnit.includes(item.unit.name) : true
            )
            ?.filter(item =>
                filterJob?.length > 0 ? filterJob.includes(item.job.name) : true
            )
        return filterData;
    }, [data, search, filterUnit, filterJob])

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
    );

    const { data: groupFiltered, PaginationComponent, currentPage } = usePagination(
        filtered,
        8,
        filters?.pagination,
        handleChangeCurrentPage
    );

    return (
        <div className="animated fadeIn">
            <Row>
                <Col xs="6">
                    <h4 className="content-title mb-4">{t('Entri Gaji')}</h4>
                </Col>
                <Col xs="5">
                    <InputGroup className="float-right period-group">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-calendar" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Datepicker
                            selected={period}
                            onChange={handleChangePeriod}
                            showMonthYearPicker
                            showFullMonthYearPicker
                            showFourColumnMonthYearPicker
                            dateFormatCalendar="MMMM"
                            calendarClassName="period-date"
                            dateFormat="MMMM yyyy"
                            maxDate={new Date()}
                            placeholderText="Select a date"
                            className="form-control"
                        />
                    </InputGroup>
                </Col>
            </Row>
            <Row>
                <Col md="8" className="text-left">
                    <InputGroup className="w-50">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-search" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="search" onChange={handleSearch} name="search" placeholder={t('Search')} className="input-search" />
                    </InputGroup>
                </Col>
            </Row>
            <h6 className="mt-4 mb-1 font-weight-bold">{t('Sortir Berdasarkan')}</h6><br />
            <Row className="mb-4">
                <Col xs="6" md="3">
                    <Select
                        isMulti={true}
                        isSearchable={false}
                        name="unit"
                        options={units}
                        placeholder={t("Unit")}
                        onChange={handleUnit}
                    />
                </Col>
                <Col xs="6" md="3">
                    <Select
                        isMulti={true}
                        isSearchable={false}
                        name="jabatan"
                        options={jobs}
                        placeholder={t("Jabatan")}
                        onChange={handleJob}
                    />
                </Col>
                <Col xs="6" className="d-flex justify-content-end">
                    <Button color="success" className="mr-2 mb-1" onClick={downloadTemplate} disabled={downloading || uploading}>
                        {downloading ?
                            <>
                                <Spinner color="light" size="sm" className="mr-1" />
                                Downloading...
                            </>
                            :
                            <>
                                <i className="fa fa-download mr-1" />
                                Download Template
                            </>
                        }
                    </Button>
                    <input type='file' id='file' ref={inputFile} style={{ display: 'none' }} onChange={(e) => onChangeFile(e)}
                        accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                    />
                    <Button color="success" className="mb-1" disabled={downloading || uploading} onClick={onButtonClick}>
                        {uploading ?
                            <>
                                <Spinner color="light" size="sm" className="mr-1" />
                                Uploading...
                            </>
                            :
                            <>
                                <i className="fa fa-upload mr-1" />
                                Upload Data
                            </>
                        }
                    </Button>
                </Col>
            </Row>
            <Rtable size="sm">
                <Rthead>
                    <Rtr>
                        <Rth className="text-md-center w-5">No.</Rth>
                        <Rth>{t("Nama")}</Rth>
                        <Rth>Email</Rth>
                        <Rth>{t('Jabatan')}</Rth>
                        <Rth>{t('Unit')}</Rth>
                    </Rtr>
                </Rthead>
                <Rtbody>
                    {
                        loading ?
                            <Rtr><Rtd colSpan="9999"><LoadingAnimation /></Rtd></Rtr>
                            : (error || filtered?.length < 1) ?
                                <Rtr><Rtd colSpan="9999"><DataNotFound /></Rtd></Rtr>
                                :
                                groupFiltered?.map((item, idx) =>
                                    <Rtr key={idx + 1 + (8*currentPage)}>
                                        <Rtd>{idx + 1 + (8*currentPage)}</Rtd>
                                        <Rtd>
                                            <Link to={`/payroll/entry/input/${item.id}`}>
                                                {item.fullName}
                                            </Link>
                                        </Rtd>
                                        <Rtd>{item.email}</Rtd>
                                        <Rtd>{item.job.name}</Rtd>
                                        <Rtd>{item.unit.name}</Rtd>
                                    </Rtr>
                                )
                    }
                </Rtbody>
            </Rtable>
            {filtered.length > 8 && <PaginationComponent />}
        </div>
    )
}

export default PayrollEntry