import React, { useCallback, useEffect, useMemo, useState } from 'react'
import Select from 'react-select'
import { t } from 'react-switch-lang'
import { Button, Row, Col, Input, InputGroup, InputGroupAddon, InputGroupText, CustomInput, Card, CardBody } from 'reactstrap'
import DataNotFound from '../../../components/DataNotFound'
import LoadingAnimation from '../../../components/LoadingAnimation'
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../components/ResponsiveTable'
import request, { requestDownload } from '../../../utils/request'
import Datepicker from "react-datepicker";
import moment from 'moment'
// import useSWR from 'swr'
import { useUserPrivileges } from '../../../store'
import usePagination from "../../../hooks/usePagination";
import useSWR from 'swr'
import { toast } from 'react-toastify'
import { Link } from 'react-router-dom'

function PayrollTax({ match }) {
    const { can } = useUserPrivileges();
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [search, setSearch] = useState(null)
    const [filterUnit, setFilterUnit] = useState([])
    const [filterJob, setFilterJob] = useState([])
    const [period, setPeriod] = useState(new Date(localStorage.getItem('period') ?? moment()) ?? new Date());
    const [units, setUnits] = useState([]);
    const [jobs, setJobs] = useState([]);
    const [checked, setChecked] = useState([])
    const { data: getDataPeriod } = useSWR(`v1/company/payroll?period=${moment(period).format('MMYYYY')}`, { refreshInterval: 10000 });
    // const loadingPeriod = !getDataPeriod && !errorPeriod;
    const [filters, setFilters] = useState([])
    const dataPeriod = useMemo(() => {
        if (getDataPeriod) {
            return getDataPeriod.data.data;
        }
        return [];
    }, [getDataPeriod]);

    useEffect(() => {
        setLoading(true)
        request.get('v1/personnels/all/aktif')
            .then((res) => {
                setData(res.data.data)
            })
            .catch((err) => {
                setError(true)
            })
            .finally(() => setLoading(false))
    }, [])

    useEffect(() => {
        request.get('v1/master/units')
            .then((res) => setUnits(res.data.data.map((item) => {
                return { label: item.name, value: item.id };
            })))
            .catch((err) => { })
        request.get('v1/master/jobs')
            .then((res) => setJobs(res.data.data.map((item) => {
                return { label: item.name, value: item.id };
            })))
            .catch((err) => { })
    }, [])

    const handleSearch = (e) => {
        setSearch(e.target.value);
    }
    const handleUnit = (e) => {
        let unit = e?.map(item => item.value) ?? []
        setFilterUnit(unit)
    }
    const handleJob = (e) => {
        let job = e?.map(item => item.value) ?? []
        setFilterJob(job)
    }

    const filtered = useMemo(() => {
        let filterData = data
            ?.filter(item =>
                search?.trim() ? item?.fullName?.toLowerCase().includes(search?.trim().toLowerCase()) : true
            )
            ?.filter(item =>
                filterUnit?.length > 0 ? filterUnit.includes(item.unit.id) : true
            )
            ?.filter(item =>
                filterJob?.length > 0 ? filterJob.includes(item.job.id) : true
            )
        return filterData;
    }, [data, search, filterUnit, filterJob])

    const downloadSlipGaji = (id) => {
        requestDownload(`v1/personnels/tax/download?period=${moment(period).format('MMYYYY')}&personnelId=${id}`)
    }

    const handleChangePeriod = useCallback((e) => {
        if(!e){
            let dateNow = new Date()
            setPeriod(dateNow)
            localStorage.setItem('period', dateNow)
        }
        if(e){
            setPeriod(e)
            localStorage.setItem('period', e)
        }
    }, [])

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
    );

    const { data: groupFiltered, PaginationComponent } = usePagination(
        filtered,
        8,
        filters.pagination,
        handleChangeCurrentPage
    );

    const handleCheckPage = (e) => {
        if (!e.target.checked) {
            setChecked([])
        }
        else {
            let arrayPage = groupFiltered.map(item => item.id)
            setChecked(arrayPage);
        }
    }

    const handleCheckList = (e) => {
        const { value, checked: isCheck } = e.target;
        if (!isCheck) {
            setChecked(state => state.filter(s => s !== parseInt(value)))
        }
        else {
            setChecked(state => [...state, parseInt(value)])
        }
    }

    const handleCheckAllData = () => {
        let arrayAll = filtered.map(item => item.id)
        setChecked(arrayAll);
    }

    const handleSendEmailCheckList = () => {
        request.post(`v1/personnels/tax/send`, {
            personnelId: checked,
            period: moment(period).format('MMYYYY')
        })
        .then(() => toast.success(t('Berhasil mengirim slip gaji')))
        .catch((err) => toast.error(err?.response?.data?.message ?? t('Gagal mengirim slip gaji')))
    }

    return (
        <div className="animated fadeIn">
            <h4 className="content-title mb-4">{t('Perhitungan Pajak')}</h4>
            <Row>
                <Col md="6" className="text-left">
                    <InputGroup className="w-50">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-search" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="search" onChange={handleSearch} name="search" placeholder={t('Search')} className="input-search" />
                    </InputGroup>
                </Col>
                <Col xs="5">
                    <InputGroup className="float-right period-group">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-calendar" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Datepicker
                            selected={period}
                            onChange={handleChangePeriod}
                            showMonthYearPicker
                            showFullMonthYearPicker
                            showFourColumnMonthYearPicker
                            dateFormatCalendar="MMMM"
                            calendarClassName="period-date"
                            dateFormat="MMMM yyyy"
                            maxDate={new Date()}
                            placeholderText="Select a date"
                            className="form-control"
                        />
                    </InputGroup>
                </Col>
            </Row>
            <h6 className="mt-4 mb-1 font-weight-bold">{t('Sortir Berdasarkan')}</h6><br />
            <Row className="mb-4">
                <Col xs="6" md="3">
                    <Select
                        isMulti={true}
                        isSearchable={false}
                        name="unit"
                        options={units}
                        placeholder={t("Unit")}
                        onChange={handleUnit}
                    />
                </Col>
                <Col xs="6" md="3">
                    <Select
                        isMulti={true}
                        isSearchable={false}
                        name="jabatan"
                        options={jobs}
                        placeholder={t("Jabatan")}
                        onChange={handleJob}
                    />
                </Col>
            </Row>
            <Row className="mb-4">
                <Col md="6" lg="4">
                    <Button disabled={checked.length <= 1 || dataPeriod?.isFinish !== 2} onClick={() => downloadSlipGaji(checked.toString())} color="netis-primary" className="mr-2">{t('Unduh Form')}</Button>
                    <Button onClick={handleSendEmailCheckList} disabled={checked.length <= 1 || dataPeriod?.isFinish !== 2} color="netis-primary" className="ml-2">{t('Kirim Form')}</Button>
                </Col>
            </Row>
            {checked?.length === groupFiltered?.length && dataPeriod.isFinish &&
                <Card className="border-0 w-100" style={{ borderRadius: '0px', backgroundColor: '#f5f6fa' }}>
                    <CardBody className="text-center py-3 font-weight-bold">
                        {groupFiltered?.length}&nbsp;
                        {t('datakaryawanterpilih')}. <span onClick={handleCheckAllData} className="link-text">{t('pilihsemuakaryawan')}</span>
                    </CardBody>
                </Card>
            }
            <Rtable size="sm">
                <Rthead>
                    <Rtr>
                        <Rth>
                            <CustomInput
                                className="ml-1"
                                id="checkAll"
                                name="checkAll"
                                type="checkbox"
                                disabled={!dataPeriod.isFinish}
                                checked={(checked?.length === groupFiltered?.length) || checked?.length === filtered?.length}
                                onChange={handleCheckPage}
                            />
                        </Rth>
                        <Rth>{t("Nama")}</Rth>
                        <Rth>{t('Jabatan')} {t('dan')} {t('Unit')}</Rth>
                        <Rth></Rth>
                    </Rtr>
                </Rthead>
                <Rtbody>
                    {
                        loading ?
                            <Rtr><Rtd colSpan="9999"><LoadingAnimation /></Rtd></Rtr>
                            : (error || filtered?.length < 1) ?
                                <Rtr><Rtd colSpan="9999"><DataNotFound /></Rtd></Rtr>
                                :
                                groupFiltered?.map((item, idx) =>
                                    <Rtr key={idx}>
                                        <Rtd>
                                            <CustomInput
                                                className="ml-1"
                                                id={`check-${idx}`}
                                                name={`check-${idx}`}
                                                type="checkbox"
                                                value={item.id}
                                                disabled={!dataPeriod.isFinish}
                                                checked={checked.includes(item.id)}
                                                onChange={handleCheckList}
                                            />
                                        </Rtd>
                                        <Rtd>
                                            <Link to={`/payroll/tax-count/${item.id}`}>
                                                <div className="font-weight-bold">
                                                    {item.fullName}
                                                </div>
                                                <small>{item.email}</small>
                                            </Link>
                                        </Rtd>
                                        <Rtd>
                                            <div className="font-weight-bold">{item.job.name}</div>
                                            <small>{item.unit.name}</small>
                                        </Rtd>
                                        <Rtd>
                                            <div className="d-flex">
                                                <Button color="netis-primary" disabled={dataPeriod?.isFinish !== 2 || !can('write-tax-payroll')} className="mr-2" onClick={() => downloadSlipGaji(item.id)}>{t('Cetak Form')}</Button>
                                            </div>
                                        </Rtd>
                                    </Rtr>
                                )
                    }
                </Rtbody>
            </Rtable>
            {filtered?.length > 8 && <PaginationComponent />}
        </div>
    )
}

export default PayrollTax