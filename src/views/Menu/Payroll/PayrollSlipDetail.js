import React, { useCallback, useEffect, useState, Fragment } from 'react'
import {translate, t} from 'react-switch-lang'
import { Modal, ModalBody, Row, Col, Button, Card, CardBody, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import LoadingAnimation from '../../../components/LoadingAnimation';
import request, { requestDownload } from '../../../utils/request'
import Datepicker from "react-datepicker";
import { useHistory } from 'react-router';
import DataNotFound from '../../../components/DataNotFound';
import moment from 'moment';
import { toast } from 'react-toastify'

function PayrollSlipDetail({match}){
    const history = useHistory()
    const [period, setPeriod] = useState(new Date(localStorage.getItem('period')) ?? new Date());
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)
    const sumAllowance = data?.payrolls?.allowance
        .filter(item => Boolean(parseInt(item.formula_value)))
        .map(item => parseInt(item.formula_value))
        .reduce((a, b) => a + b, 0)
    const sumCuts = data?.payrolls?.cuts
        .filter(item => Boolean(parseInt(item.formula_value)))
        .map(item => parseInt(item.formula_value))
        .reduce((a, b) => a + b, 0)
    const totalSalary = parseInt(data?.salary) + sumAllowance - sumCuts;

    useEffect(() => {
        setError(false)
        setLoading(true)
        request.get(`v1/personnels/${match.params.id}/payroll/slip?period=${moment(period).format('MMYYYY')}`)
        .then((res) => {
            setData(res.data.data)
        })
        .catch((err) => {
            setError(err?.response?.status)
        })
        .finally(() => setLoading(false))
    }, [period, match])

    const handleChangePeriod = useCallback((e) => {
        if(!e){
            let dateNow = new Date()
            setPeriod(dateNow)
            localStorage.setItem('period', dateNow)
        }
        if(e){
            setPeriod(e)
            localStorage.setItem('period', e)
        }
    }, [])

    const downloadSlipGaji = () => {
        requestDownload(`v1/personnels/slip/download?period=${moment(period).format('MMYYYY')}&personnelId=${match.params.id}`)
    }

    const handleSendEmail = () => {
        request.post(`v1/personnels/payroll/send`, {
            personnelId: [match.params.id],
            period: moment(period).format('MMYYYY')
        })
        .then(() => toast.success(t('Berhasil mengirim slip gaji')))
        .catch((err) => toast.error(err?.response?.data?.message ?? t('Gagal mengirim slip gaji')))
    }

    const formatter = new Intl.NumberFormat('ind', {
        style: 'currency',
        currency: 'IDR',
    });

    if(loading){
        return <LoadingAnimation />
    }
    if(error){
        if(error === 404){
            return(
                <Modal style={{marginTop:'25vh'}} isOpen={error ? true : false}>
                    <ModalBody className="text-center py-5">
                        <h5 className="mt-2">
                            {t('Payroll pada periode ini belum ada')}
                        </h5>
                        <Row className="mt-5">
                            <Col className="text-center">
                                {t('Ubah periode')} :<br />
                                <InputGroup className="float-right period-group">
                                    <Datepicker
                                        selected={period}
                                        onChange={handleChangePeriod}
                                        showMonthYearPicker
                                        showFullMonthYearPicker
                                        showFourColumnMonthYearPicker
                                        dateFormatCalendar="MMMM"
                                        calendarClassName="period-date"
                                        dateFormat="MMMM yyyy"
                                        maxDate={new Date()}
                                        placeholderText="Select a date"
                                        className="form-control"
                                    />
                                </InputGroup>
                            </Col>
                            <Col className="text-center">
                                {t('Kembali ke Beranda')}<br />
                                <Button color="netis-primary" onClick={() => history.push('/dashboard')}>
                                    <i className="fa fa-home mr-2" />{t('Beranda')}
                                </Button>
                            </Col>
                        </Row>
                    </ModalBody>
                </Modal>
            )
        }
        else {
            return <DataNotFound />
        }
    }

    return(
        <div className="animated fadeIn payroll-user">
            <h4 className="content-title mb-4">{t('Slip Gaji')}</h4>
            <div className="d-flex justify-content-end mt-1 mb-3">
                <InputGroup className="float-right period-group">
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText className="input-group-transparent">
                            <i className="fa fa-calendar" />
                        </InputGroupText>
                    </InputGroupAddon>
                    <Datepicker
                        selected={period}
                        onChange={handleChangePeriod}
                        showMonthYearPicker
                        showFullMonthYearPicker
                        showFourColumnMonthYearPicker
                        dateFormatCalendar="MMMM"
                        calendarClassName="period-date"
                        dateFormat="MMMM yyyy"
                        maxDate={new Date()}
                        placeholderText="Select a date"
                        className="form-control"
                    />
                </InputGroup>
            </div>
            <Card className="border-0 w-100" style={{borderRadius:'0px', backgroundColor:'#f5f6fa'}}>
                <CardBody className="py-1 font-weight-bold">
                    <Row className="text-left">
                        <Col md="6" className="py-1">
                            <Row>
                                <Col xs="5">{t('Nama')}</Col><Col xs="7"> :  {data?.personnel?.userFullName}</Col>
                                <Col xs="5">{t('Tanggal Gajian')}</Col><Col xs="7"> :  {moment(data?.payDay).format('DD MMMM YYYY') ?? '-'}</Col>
                            </Row>
                        </Col>
                        <Col md="6" className="py-1">
                            <Row>
                                <Col xs="5">{t('Jabatan')}</Col><Col xs="7"> :  {data?.personnel?.job?.name}</Col>
                                <Col xs="5">{t('Periode Gaji')}</Col>
                                <Col xs="7"> :  {moment(data?.periodStart).format('DD MMMM YYYY') ?? '-'} {t('sampai')} {moment(data?.periodEnd).format('DD MMMM YYYY') ?? '-'}</Col>
                            </Row>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
            <Card className="border-0 w-100">
                <CardBody className="py-1">
                    <Row className="text-left">
                        <Col md="6">
                            <Row>
                                <Col xs="5" className="font-weight-bold">{t('Gaji Pokok')}</Col><Col xs="7"> :  {formatter.format(data?.salary)}</Col>
                            </Row>
                            <div className="d-md-none"><hr /></div>
                        </Col>
                    </Row>
                    <Row className="text-left">
                        <Col md="6">
                            <Row>
                                <Col xs="12" className="font-weight-bold">{t('Tunjangan')}</Col>
                                {data?.payrolls?.allowance?.filter(item => Boolean(parseInt(item.formula_value))).map((item, idx) => (
                                    <Fragment key={idx}>
                                        <Col xs="5">{item.name}</Col><Col xs="7"> :  {formatter.format(item.formula_value)}</Col>
                                    </Fragment>
                                ))}
                            </Row>
                            <div className="d-md-none"><hr /></div>
                        </Col>
                        <Col md="6">
                            <Row>
                                <Col xs="12" className="font-weight-bold">{t('Potongan')}</Col>
                                {data?.payrolls?.cuts?.filter(item => Boolean(parseInt(item.formula_value))).map((item, idx) => (
                                    <Fragment key={idx}>
                                        <Col xs="5">{item.name}</Col><Col xs="7"> :  {formatter.format(item.formula_value)}</Col>
                                    </Fragment>
                                ))}
                            </Row>
                            <div className="d-md-none"><hr /></div>
                        </Col>
                        <Col md="6" className="mt-3">
                            <Row className="font-weight-bold">
                                <Col xs="5">{t('Gaji Pokok')}</Col><Col xs="7"> :  {formatter.format(data?.salary)}</Col>
                                <Col xs="5">{t('Tunjangan')}</Col><Col xs="7"> :  {formatter.format(sumAllowance)}</Col>
                                <Col xs="5">{t('Potongan')}</Col><Col xs="7"> :  {formatter.format(sumCuts)}</Col>
                                <Col xs="5">{t('Total Gaji')}</Col><Col xs="7"> :  {formatter.format(totalSalary)}</Col>
                            </Row>
                        </Col>
                    </Row>
                    <hr />
                    <Row className="my-4">
                        <Col md="8">
                            <Button disabled={data?.isFinish !== 2} onClick={downloadSlipGaji} color="netis-primary" className="mr-2">{t('Unduh Slip')}</Button>
                            <Button disabled={data?.isFinish !== 2} onClick={handleSendEmail} color="netis-primary" className="ml-2">{t('Kirim Slip Gaji')}</Button>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        </div>
    )
}

export default translate(PayrollSlipDetail)