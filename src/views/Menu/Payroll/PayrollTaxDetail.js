import React, { useCallback, useEffect, useState, Fragment } from 'react'
import { translate, t } from 'react-switch-lang'
import { Modal, ModalBody, Row, Col, Button, Card, CardBody, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import LoadingAnimation from '../../../components/LoadingAnimation';
import request, { requestDownload } from '../../../utils/request'
import Datepicker from "react-datepicker";
import { useHistory } from 'react-router';
import DataNotFound from '../../../components/DataNotFound';
import moment from 'moment';
import { toast } from 'react-toastify'

function PayrollTaxDetail({ match }) {
    const history = useHistory()
    const [period, setPeriod] = useState(new Date(localStorage.getItem('period')) ?? new Date());
    const [data, setData] = useState(null)
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(null)

    useEffect(() => {
        setError(false)
        setLoading(true)
        request.get(`v1/personnels/${match.params.id}/tax?period=${moment(period).format('MMYYYY')}`)
            .then((res) => {
                setData(res.data.data)
            })
            .catch((err) => {
                setError(err?.response?.status)
            })
            .finally(() => setLoading(false))
    }, [period, match])

    const handleChangePeriod = useCallback((e) => {
        if (!e) {
            let dateNow = new Date()
            setPeriod(dateNow)
            localStorage.setItem('period', dateNow)
        }
        if (e) {
            setPeriod(e)
            localStorage.setItem('period', e)
        }
    }, [])

    const downloadSlipGaji = () => {
        requestDownload(`v1/personnels/tax/download?period=${moment(period).format('MMYYYY')}&personnelId=${match.params.id}`)
    }

    const handleSendEmail = () => {
        request.post(`v1/personnels/tax/send`, {
            personnelId: [match.params.id],
            period: moment(period).format('MMYYYY')
        })
            .then(() => toast.success(t('Berhasil mengirim slip gaji')))
            .catch((err) => toast.error(err?.response?.data?.message ?? t('Gagal mengirim slip gaji')))
    }

    const formatter = new Intl.NumberFormat('ind', {
        style: 'currency',
        currency: 'IDR',
    });

    if (loading) {
        return <LoadingAnimation />
    }
    if (error) {
        if (error === 404) {
            return (
                <Modal style={{ marginTop: '25vh' }} isOpen={error ? true : false}>
                    <ModalBody className="text-center py-5">
                        <h5 className="mt-2">
                            {t('Payroll pada periode ini belum ada')}
                        </h5>
                        <Row className="mt-5">
                            <Col className="text-center period-group">
                                {t('Ubah periode')} :<br />
                                <Datepicker
                                    selected={period}
                                    onChange={handleChangePeriod}
                                    showMonthYearPicker
                                    showFullMonthYearPicker
                                    showFourColumnMonthYearPicker
                                    dateFormat="MMMM yyyy"
                                    maxDate={new Date()}
                                    placeholderText="Select a date"
                                    className="form-control float-right"
                                />
                            </Col>
                            <Col className="text-center">
                                {t('Kembali ke Beranda')}<br />
                                <Button color="netis-primary" onClick={() => history.push('/dashboard')}>
                                    <i className="fa fa-home mr-2" />{t('Beranda')}
                                </Button>
                            </Col>
                        </Row>
                    </ModalBody>
                </Modal>
            )
        }
        else {
            return <DataNotFound />
        }
    }

    return (
        <div className="animated fadeIn payroll-user">
            <h4 className="content-title mb-4">{t('Perhitungan Pajak')}</h4>
            <div className="d-flex justify-content-end my-3 period-group">
                <InputGroup className="float-right period-group">
                    <InputGroupAddon addonType="prepend">
                        <InputGroupText className="input-group-transparent">
                            <i className="fa fa-calendar" />
                        </InputGroupText>
                    </InputGroupAddon>
                    <Datepicker
                        selected={period}
                        onChange={handleChangePeriod}
                        showMonthYearPicker
                        showFullMonthYearPicker
                        showFourColumnMonthYearPicker
                        dateFormatCalendar="MMMM"
                        calendarClassName="period-date"
                        dateFormat="MMMM yyyy"
                        maxDate={new Date()}
                        placeholderText="Select a date"
                        className="form-control"
                    />
                </InputGroup>
            </div>
            <Card className="border-0 w-100" style={{ borderRadius: '0px', backgroundColor: '#f5f6fa' }}>
                <CardBody className="py-1 font-weight-bold">
                    <Row className="text-left">
                        <Col md="6" className="py-1">
                            <Row>
                                <Col xs="4">{t('Nama')}</Col><Col xs="1" className="d-flex align-items-center">:</Col><Col xs="7">{data?.personnel.userFullName}</Col>
                                <Col xs="4">NPWP</Col><Col xs="1" className="d-flex align-items-center">:</Col><Col xs="7">{data?.personnel.userNpwp}</Col>
                            </Row>
                        </Col>
                        <Col md="6" className="py-1">
                            <Row>
                                <Col xs="4">{t('Jabatan')}</Col><Col xs="1" className="d-flex align-items-center">:</Col><Col xs="7">{data?.personnel.job.name} in {data?.personnel.unit.name}</Col>
                                <Col xs="4">{t('Periode Pajak')}</Col><Col xs="1" className="d-flex align-items-center">:</Col><Col xs="7">{moment(period).format('MMMM YYYY')}</Col>
                            </Row>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
            <Card className="border-0 w-100">
                <CardBody className="py-1">
                    <Row className="text-left">
                        <Col md="12">
                            <div className="font-weight-bold">PENGHASILAN BRUTO</div>
                            <hr />
                        </Col>
                        <Col md="12">
                            {data?.tax.allowance.map((a, i) => (
                                <Fragment key={i}>
                                    <Row className="my-3">
                                        <Col xs="9">{a.id}. {a.name}</Col>
                                        <Col xs="3" className="text-right">{formatter.format(isNaN(a.amount) ? 0 : a.amount)}</Col>
                                    </Row>
                                    <hr />
                                </Fragment>
                            ))}
                            <Row className="my-3">
                                <Col xs="9" onClick={() => console.log(isNaN(data?.tax?.bruto))}>8. JUMLAH PENGHASILAN BRUTO (1 S.D.7)</Col>
                                <Col xs="3" className="text-right">{formatter.format(isNaN(data?.tax?.bruto) ? 0 : data?.tax?.bruto)}</Col>
                            </Row>
                            <hr />
                        </Col>
                        <Col md="12">
                            <div className="font-weight-bold">PENGURANGAN</div>
                            <hr />
                        </Col>
                        <Col md="12">
                            <Row className="my-3">
                                <Col xs="9">9. BIAYA JABATAN/BIAYA PENSIUN</Col>
                                <Col xs="3" className="text-right">{formatter.format(isNaN(data?.tax?.biayaJabatan) ? 0 : data?.tax?.biayaJabatan)}</Col>
                            </Row>
                            <hr />
                            {data?.tax.cuts.map((a, i) => (
                                <Fragment key={i}>
                                    <Row className="my-3">
                                        <Col xs="9">10. {a.name}</Col>
                                        <Col xs="3" className="text-right">{formatter.format(isNaN(a.amount) ? 0 : a.amount)}</Col>
                                    </Row>
                                    <hr />
                                </Fragment>
                            ))}
                            <Row className="my-3">
                                <Col xs="9">11. JUMLAH PENGURANGAN (9 S.D 10)</Col>
                                <Col xs="3" className="text-right">{formatter.format(isNaN(data?.tax?.pengurangan) ? 0 : data?.tax?.pengurangan)}</Col>
                            </Row>
                            <hr />
                        </Col>
                        <Col md="12">
                            <div className="font-weight-bold">PENGHITUNGAN PPh PASAL 21</div>
                            <hr />
                        </Col>
                        <Col md="12">
                            <Row className="my-3">
                                <Col xs="9">12. JUMLAH PENGHASILAN NETO (8-11)</Col>
                                <Col xs="3" className="text-right">{formatter.format(isNaN(data?.tax?.neto) ? 0 : data?.tax?.neto)}</Col>
                            </Row>
                            <hr />
                            <Row className="my-3">
                                <Col xs="9">13. PENGHASILAN NETO MASA SEBELUMNYA</Col>
                                <Col xs="3" className="text-right">{formatter.format(0)}</Col>
                            </Row>
                            <hr />
                            <Row className="my-3">
                                <Col xs="9">14. JUMLAH PENGHASILAN NETO UNTUK PENGHITUNGAN PPh PASAL 21 (SETAHUN / DISETAHUNKAN)</Col>
                                <Col xs="3" className="text-right">{formatter.format(isNaN(data?.tax?.netoSetahun) ? 0 : data?.tax?.netoSetahun)}</Col>
                            </Row>
                            <hr />
                            <Row className="my-3">
                                <Col xs="9">15. PENGHASILAN TIDAK KENA PAJAK (PTKP)</Col>
                                <Col xs="3" className="text-right">{formatter.format(isNaN(data?.tax?.ptkp) ? 0 : data?.tax?.ptkp)}</Col>
                            </Row>
                            <hr />
                            <Row className="my-3">
                                <Col xs="9">16. PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN (14-15)</Col>
                                <Col xs="3" className="text-right">{formatter.format(isNaN(data?.tax?.pkp) ? 0 : data?.tax?.pkp)}</Col>
                            </Row>
                            <hr />
                            <Row className="my-3">
                                <Col xs="9">17. PPh PASAL 21 ATAS PENGHASILAN KENA PAJAK SETAHUN/DISETAHUNKAN</Col>
                                <Col xs="3" className="text-right">{formatter.format(isNaN(data?.tax?.tarif) ? 0 : data?.tax?.tarif)}</Col>
                            </Row>
                            <hr />
                            <Row className="my-3">
                                <Col xs="9">18. PPh BULANAN</Col>
                                <Col xs="3" className="text-right">{formatter.format(isNaN(data?.tax?.pphBulanan) ? 0 : data?.tax?.pphBulanan)}</Col>
                            </Row>
                            <hr />
                        </Col>
                    </Row>
                    <Row className="my-4">
                        <Col md="6">
                            <Button disabled={data?.isFinish !== 2} onClick={downloadSlipGaji} color="netis-primary" className="mr-2">{t('Unduh Slip')}</Button>
                            <Button disabled={data?.isFinish !== 2} onClick={handleSendEmail} color="netis-primary" className="ml-2">{t('Kirim Slip Gaji')}</Button>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        </div>
    )
}

export default translate(PayrollTaxDetail)