import React, { useEffect, useState, useCallback } from 'react'
import { useFormik } from 'formik';
import moment from 'moment';
import { useLocation } from 'react-router';
import { Link } from 'react-router-dom';
import { t, translate } from 'react-switch-lang';
import { toast } from 'react-toastify';
import {
    Button, Col, Collapse, CustomInput, Input, Label, ListGroup, ListGroupItem, Modal, ModalBody,
    ModalFooter, ModalHeader, Nav, NavItem, NavLink, Row, TabContent, TabPane, Spinner, InputGroup, InputGroupAddon, InputGroupText
} from 'reactstrap';
import DataNotFound from '../../../components/DataNotFound';
import LoadingAnimation from '../../../components/LoadingAnimation';
import request from '../../../utils/request';
import Datepicker from "react-datepicker";
import Calculator from './Components/Calculator';
import { useUserPrivileges } from '../../../store';
import Select from 'react-select';

function PayrollSetting() {
    const tabs = {
        'master': t('Master Data Komponen Gaji'),
        'setting': t('Tanggal Penggajian')
    }
    const tabsArray = Object.keys(tabs);
    const { can } = useUserPrivileges();
    const [allowance, setAllowance] = useState(null);
    const [cut, setCut] = useState(null);
    const location = useLocation();
    const selectedTab = location.hash ? location.hash.substring(1) : tabsArray[0];
    const [addComponent, setAddComponent] = useState(false);
    const [loading, setLoading] = useState(false);
    const [finish, setFinish] = useState(false)
    const [error, setError] = useState(false);
    const [create, setCreate] = useState('new');
    const [allCode, setAllCode] = useState(null);
    const [modalDelete, setModalDelete] = useState(false);
    const [isDeleting, setIsDeleting] = useState(false);
    const [datePayroll, setDatePayroll] = useState({ start: '', pay: '' });
    const [dateData, setDateData] = useState({ start: '', pay: '' });
    const [isChangeStart, setIsChangeStart] = useState(false);
    const [loadingStart, setLoadingStart] = useState(false);
    const [errorStart, setErrorStart] = useState(false);
    const [period, setPeriod] = useState(new Date(localStorage.getItem('period') ?? moment()) ?? new Date());
    const [errorFormula, setErrorFormula] = useState(false);
    const [taxCategory, setTaxCategory] = useState(null)
    const [sameCode, setSameCode] = useState(false)

    const { values, isSubmitting, ...formik } = useFormik({
        initialValues: {
            code: "",
            name: "",
            formula_type: null,
            formula_value: "",
            isSalaryCalculation: false,
            isTaxed: false,
            categoryTax: null,
            isRegular: true,
            category: null
        },
        onSubmit: (values, { setSubmitting }) => {
            setSubmitting(true)
            if (create === 'new') {
                request.post(`v1/company/payroll/${moment(period).format('MMYYYY')}`, values)
                    .then((res) => {
                        getData();
                        toast.success(t('Berhasil Menambahkan data'));
                        formik.handleReset();
                        setAddComponent(false)
                    })
                    .catch((err) => {
                        toast.error(err?.response?.data?.message ?? t('Gagal Menambahkan Data Master, silahkan coba lagi'));
                        return;
                    })
                    .finally(() => setSubmitting(false))
            }
            else if (create === 'edit') {
                request.put(`v1/company/payroll/${moment(period).format('MMYYYY')}`, values)
                    .then((res) => {
                        getData();
                        toast.success(t('Berhasil Mengubah data'));
                        formik.handleReset();
                        setAddComponent(false)
                    })
                    .catch((err) => {
                        toast.error(err?.response?.data?.message ?? t('Gagal Mengubah Data Master, silahkan coba lagi'));
                        return;
                    })
                    .finally(() => setSubmitting(false))
            }
        }
    })
    const nullFormula = (values.formula_type === 'rumus_hitungan' || values.formula_type === 'nominal') && !values.formula_value
    const nullCategory = values.isTaxed && !values.categoryTax

    const getData = useCallback(() => {
        setLoading(true)
        return request.get(`v1/company/payroll?period=${moment(period).format('MMYYYY')}`)
            .then((res) => {
                setAllowance(res.data.data.allowance)
                setCut(res.data.data.cuts)
                setFinish(res.data.data.isFinish)
            })
            .catch((err) => {
                console.log(err)
                setError(true)
            })
            .finally(() => setLoading(false))
    }, [period])

    useEffect(() => {
        getData()
    }, [getData, period])

    useEffect(() => {
        if (values.code) {
            let val = values.code.toLowerCase()
            let hasCode = allCode.some(a => val === a.code.toLowerCase())
            if (hasCode || val === 'gjpkk' || val === 'jmkrj' || val === 'jmlmbr' || val === 'pph') {
                setSameCode(true)
            }
            else setSameCode(false)
        }
        else setSameCode(false)
    }, [values, allCode])

    useEffect(() => {
        if (allowance && cut) {
            const merge = allowance.concat(cut)
            setAllCode(merge)
        }
    }, [allowance, cut])

    useEffect(() => {
        setLoadingStart(true)
        request.get('v1/company/payroll/start')
            .then((res) => {
                setDatePayroll({ start: res.data.data.payrollStart, pay: res.data.data.payDay })
                setDateData({ start: res.data.data.payrollStart, pay: res.data.data.payDay })
            })
            .catch(() => {
                setErrorStart(true)
            })
            .finally(() => setLoadingStart(false))
    }, [])

    useEffect(() => {
        request.get('v1/master/tax')
            .then((res) => {
                setTaxCategory(res.data.data)
            })
    }, [])

    const handleEdit = (item, params) => {
        setCreate('edit')
        setAddComponent(true)
        formik.setFieldValue('code', item.code)
        formik.setFieldValue('name', item.name)
        formik.setFieldValue('formula_type', item.formula_type)
        formik.setFieldValue('formula_value', item.formula_value)
        formik.setFieldValue('isSalaryCalculation', item.isSalaryCalculation)
        formik.setFieldValue('isTaxed', item.isTaxed)
        formik.setFieldValue('categoryTax', item.categoryTax)
        formik.setFieldValue('category', params)
        formik.setFieldValue('isRegular', item.isRegular)
    }

    const handleDelete = () => {
        setIsDeleting(true)
        request.put(`v1/company/payroll/${moment(period).format('MMYYYY')}/delete`, {
            code: values.code,
            category: values.category
        })
            .then((res) => {
                if (values.category === 'allowance') {
                    setAllowance(allowance.filter(e => e.code !== values.code))
                }
                else if (values.category === 'cuts') {
                    setCut(cut.filter(e => e.code !== values.code))
                }
                toast.success(t('Berhasil Menghapus Data'))
                setModalDelete(false)
                formik.handleReset()
            })
            .catch((err) => {
                // console.log(err)
                toast.error(err?.response?.data?.message ?? t('Gagal menghapus Data'))
                return
            })
            .finally(() => setIsDeleting(false))
    }
    const confirmDelete = (item, params) => {
        formik.setFieldValue('code', item.code)
        formik.setFieldValue('name', item.name)
        formik.setFieldValue('category', params)
        setModalDelete(true)
    }
    const cancelDelete = () => {
        formik.handleReset()
        setModalDelete(false)
    }
    const handleCode = (e) => {
        const { value } = e.target
        const code = value?.toUpperCase()
        formik.setFieldValue('code', code)
    }
    const handleFormulaValue = (e) => {
        // let val = e;
        // if (e === 'x') {
        //     val = '*'
        // }
        // const tempValue = values.formula_value ?? ""
        // const nextValue = tempValue + val
        formik.setFieldValue('formula_value', e)
    }
    const handleCheck = (e) => {
        formik.setFieldValue('formula_value', '')
        formik.setFieldValue('formula_type', e)
    }
    const handleNumberOnly = (evt) => {
        let charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            evt.preventDefault()
        }
        return true;
    }
    const handleLetterOnly = (evt) => {
        let charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
            evt.preventDefault()
        }
        return true;
    }
    const addAllowance = () => {
        setCreate('new')
        setAddComponent(true)
        formik.setFieldValue('category', 'allowance')
    }
    const addCuts = () => {
        setCreate('new')
        setAddComponent(true)
        formik.setFieldValue('category', 'cuts')
    }
    const toggleAddComponent = () => {
        formik.handleReset();
        setAddComponent(false)
    }
    const saveStartPayRoll = () => {
        setIsChangeStart(true)
        request.post('v1/company/payroll/start', { startPayroll: datePayroll.start, payDay: datePayroll.pay })
            .then(() => {
                toast.success(t('Berhasil Merubah Tanggal Cut-off Gaji'))
                setDateData({ start: datePayroll.start, pay: datePayroll.pay })
            })
            .catch((err) => {
                toast.error(err?.response?.data?.message ?? t('Gagal Merubah Tanggal Cut-off Gaji'))
                return;
            })
            .finally(() => setIsChangeStart(false))
    }
    const cancelStartPayRoll = () => {
        setDatePayroll({ start: dateData.start, pay: dateData.pay })
    }

    const handleChangePeriod = useCallback((e) => {
        if(!e){
            let dateNow = new Date()
            setPeriod(dateNow)
            localStorage.setItem('period', dateNow)
        }
        if(e){
            setPeriod(e)
            localStorage.setItem('period', e)
        }
    }, [])

    const handleChangePayrollStart = useCallback((e) => {
        const { value } = e.target;
        let date = value;
        if (value <= 0) date = 0;
        if (value > 31) date = 31;
        setDatePayroll(state => ({ ...state, start: parseInt(date) }))
    }, [])

    const handleChangePayDay = useCallback((e) => {
        const { value } = e.target;
        let date = value;
        if (value <= 0) date = 0;
        if (value > 31) date = 31;
        setDatePayroll(state => ({ ...state, pay: parseInt(date) }))
    }, [])

    return (
        can('read-settings-payroll') &&
        <div className="animated fadeIn">
            <Row>
                <Col xs="6">
                    <h4 className="content-title mb-4">{t('Pengaturan Gaji')}</h4>
                </Col>
                <Col xs="5">
                    <InputGroup className="float-right period-group">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-calendar" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Datepicker
                            selected={period}
                            onChange={handleChangePeriod}
                            showMonthYearPicker
                            showFullMonthYearPicker
                            showFourColumnMonthYearPicker
                            dateFormatCalendar="MMMM"
                            calendarClassName="period-date"
                            dateFormat="MMMM yyyy"
                            maxDate={new Date()}
                            placeholderText="Select a date"
                            className="form-control"
                        />
                    </InputGroup>
                </Col>
            </Row>
            <Row className="mb-4">
                <Col xs="12" md="12" className="mb-12">
                    <Nav tabs>
                        {tabsArray.map(tab => (
                            <NavItem key={tab}>
                                <NavLink tag={Link} className="pt-2/5" active={selectedTab === tab} replace to={{ hash: "#" + tab }}>
                                    {tabs[tab]}
                                </NavLink>
                            </NavItem>
                        ))}
                    </Nav>
                    <TabContent activeTab={selectedTab}>
                        <TabPane tabId="master">
                            {loading ? <LoadingAnimation />
                                : error ? <DataNotFound />
                                    : <Row>
                                        <Col md="6">
                                            <h4>{t('Tunjangan')}</h4>
                                            <ListGroup>
                                                {allowance?.map((item, idx) =>
                                                    <ListGroupItem className="d-flex justify-content-between border-0 pl-3 hover-bg-secondary" key={idx}>
                                                        <span>{item.name}</span>
                                                        {!finish && can('write-settings-payroll') ?
                                                            <div className="ml-auto">
                                                                <span className="link-button text-netis-primary mr-1" onClick={() => handleEdit(item, 'allowance')}>
                                                                    {t('Ubah')}
                                                                </span>
                                                                <span className="link-button text-netis-primary ml-1" onClick={() => confirmDelete(item, 'allowance')}>
                                                                    {t('Hapus')}
                                                                </span>
                                                            </div>
                                                            :
                                                            <div className="ml-auto">
                                                                <span className="link-button text-netis-primary mr-1" onClick={() => handleEdit(item, 'allowance')}>
                                                                    Detail
                                                                </span>
                                                            </div>
                                                        }
                                                    </ListGroupItem>
                                                )}
                                            </ListGroup>
                                            <br />
                                            {!finish && can('write-settings-payroll') &&
                                                <span className="link-button text-info pl-3" onClick={addAllowance}>
                                                    {t('Tambah Item Tunjangan')}
                                                </span>
                                            }
                                        </Col>
                                        <Col md="6">
                                            <h4>{t('Potongan')}</h4>
                                            <ListGroup>
                                                {cut?.map((item, idx) =>
                                                    <ListGroupItem className="d-flex justify-content-between border-0 pl-3 hover-bg-secondary" key={idx}>
                                                        <span>{item.name}</span>
                                                        {!finish && can('write-settings-payroll') ?
                                                            <div className="ml-auto">
                                                                <span className="link-button text-netis-primary mr-1" onClick={() => handleEdit(item, 'cuts')}>
                                                                    {t('Ubah')}
                                                                </span>
                                                                <span className="link-button text-netis-primary ml-1" onClick={() => confirmDelete(item, 'cuts')}>
                                                                    {t('Hapus')}
                                                                </span>
                                                            </div>
                                                            :
                                                            <div className="ml-auto">
                                                                <span className="link-button text-netis-primary mr-1" onClick={() => handleEdit(item, 'cuts')}>
                                                                    Detail
                                                                </span>
                                                            </div>
                                                        }
                                                    </ListGroupItem>
                                                )}
                                            </ListGroup>
                                            <br />
                                            {!finish && can('write-settings-payroll') &&
                                                <span className="link-button text-info pl-3" onClick={addCuts}>
                                                    {t('Tambah Item Potongan')}
                                                </span>
                                            }
                                        </Col>
                                    </Row>
                            }
                        </TabPane>
                        <TabPane tabId="setting">
                            <Row>
                                <Col xs="6" md="4" lg="3">
                                    <Label>{t('Tanggal mulai perhitungan gaji')}</Label>
                                    {loadingStart ? <><Spinner color="light" /> Loading....</>
                                        : errorStart ? <span className="text-danger">{t('Terjadi kesalahan')}</span>
                                            :
                                            <Input
                                                type="text"
                                                id="startDate"
                                                name="startDate"
                                                placeholder={t('Masukkan Angka Tanggal')}
                                                onKeyPress={handleNumberOnly}
                                                pattern="[0-9]*"
                                                inputMode="numeric"
                                                maxLength="2"
                                                value={datePayroll.start}
                                                onChange={handleChangePayrollStart}
                                                disabled={loadingStart || errorStart || isChangeStart || !can('write-settings-payroll')}
                                            />
                                    }
                                </Col>
                                <Col xs="6" md="4" lg="3">
                                    <Label>{t('Tanggal gajian')}</Label>
                                    {loadingStart ? <><Spinner color="light" /> Loading....</>
                                        : errorStart ? <span className="text-danger">{t('Terjadi kesalahan')}</span>
                                            :
                                            <Input
                                                type="text"
                                                id="payDay"
                                                name="payDay"
                                                placeholder={t('Masukkan Angka Tanggal')}
                                                onKeyPress={handleNumberOnly}
                                                pattern="[0-9]*"
                                                inputMode="numeric"
                                                maxLength="2"
                                                value={datePayroll.pay}
                                                onChange={handleChangePayDay}
                                                disabled={loadingStart || errorStart || isChangeStart || !can('write-settings-payroll')}
                                            />
                                    }
                                </Col>
                            </Row>
                            <hr />
                            <Button
                                disabled={loadingStart || errorStart || isChangeStart || !can('write-settings-payroll') || datePayroll.pay === 0 || datePayroll.start === 0}
                                onClick={cancelStartPayRoll}
                                color="outline-netis-primary"
                                className="mr-2"
                            >
                                {t('Batal')}
                            </Button>
                            <Button
                                disabled={loadingStart || errorStart || isChangeStart || !can('write-settings-payroll') || datePayroll.pay === 0 || datePayroll.start === 0}
                                onClick={saveStartPayRoll}
                                color="netis-primary"
                                className="ml-2"
                            >
                                {t('Simpan')}
                            </Button>
                        </TabPane>
                    </TabContent>
                </Col>
            </Row>
            <Modal isOpen={addComponent} size="lg">
                <ModalHeader className="border-bottom-0">
                    {create === 'new' ? t('tambah') : (finish ? t('Detail') : t('Ubah'))}&nbsp;
                    {values.category === 'allowance' ? t('Komponen Tunjangan') : t('Komponen Potongan')}
                </ModalHeader>
                <ModalBody className="pt-1">
                    <Row>
                        <Col xs="6">
                            <Label htmlFor="code" className="input-label">
                                {values.category === 'allowance' ? t('Kode Komponen Tunjangan') : t('Kode Komponen Potongan')}
                            </Label>
                            <Input type="text" maxLength="5" name="code" id="code" disabled={create !== 'new'} value={values.code} onKeyPress={handleLetterOnly} onChange={handleCode} onBlur={formik.handleBlur} />
                            {create === 'new' && sameCode && <small className="text-danger">{t('Kode Komponen sudah ada')}</small>}
                        </Col>
                        <Col xs="6">
                            <Label htmlFor="name" className="input-label">
                                {values.category === 'allowance' ? t('Nama Komponen Tunjangan') : t('Nama Komponen Potongan')}
                            </Label>
                            <Input type="text" name="name" id="name" disabled={finish || !can('write-settings-payroll')} value={values.name} onChange={formik.handleChange} onBlur={formik.handleBlur} />
                        </Col>
                    </Row>
                    <Row className="my-4">
                        <Col><CustomInput type="radio" disabled={finish || !can('write-settings-payroll')} checked={values.formula_type === 'rumus_hitungan'} onChange={() => handleCheck('rumus_hitungan')} id="rumus_hitungan" name="rumus_hitungan" label="Formula" /></Col>
                        <Col><CustomInput type="radio" disabled={finish || !can('write-settings-payroll')} checked={values.formula_type === 'nominal'} onChange={() => handleCheck('nominal')} id="nominal" name="nominal" label="Nominal" /></Col>
                        <Col><CustomInput type="radio" disabled={finish || !can('write-settings-payroll')} checked={values.formula_type === 'input'} onChange={() => handleCheck('input')} id="input" name="input" label="Input Nominal" /></Col>
                        <Col><CustomInput type="radio" disabled={finish || !can('write-settings-payroll')} checked={values.formula_type === 'input_rumus'} onChange={() => handleCheck('input_rumus')} id="input_rumus" name="input_rumus" label="Input Formula" /></Col>
                    </Row>
                    {finish || !can('write-settings-payroll')
                        ? <Input type="text" disabled value={values.formula_value ?? ""} />
                        : (values.formula_type === 'nominal' ?
                            <Input
                                type="text"
                                name="formula_value"
                                id="formula_value"
                                disabled={values.formula_type === 'input' || values.formula_type === 'input_rumus' || !values.formula_type}
                                value={values.formula_value ?? ""}
                                onChange={formik.handleChange}
                                onKeyPress={handleNumberOnly}
                                pattern="[0-9]*"
                            />
                            :
                            values.formula_type === 'rumus_hitungan'
                                ? null
                                : <Input type="text" disabled />
                        )}
                    {!finish && can('write-settings-payroll') &&
                        <Collapse isOpen={values.formula_type === 'rumus_hitungan'} className="mt-4">
                            <Calculator allCode={allCode} onChangeFormulaValue={handleFormulaValue} onErrorFormula={setErrorFormula} defaultValue={values.formula_value} isOpen={values.formula_type === 'rumus_hitungan' && !finish} showInput={true} />
                        </Collapse>
                    }
                    <Row className="mt-3">
                        <Col xs="6" md="4" lg="3">
                            <CustomInput type="checkbox" id="isSalaryCalculation" name="isSalaryCalculation"
                                label={t('Perhitungan Gaji')}
                                onChange={() => formik.setFieldValue('isSalaryCalculation', !values.isSalaryCalculation)}
                                checked={values.isSalaryCalculation}
                                disabled={finish || !can('write-settings-payroll')}
                            />
                        </Col>
                        <Col xs="6" md="4" lg="3">
                            <CustomInput type="checkbox" id="hasTax" name="hasTax"
                                label={t('Perhitungan Pajak')}
                                onChange={() => formik.setFieldValue('isTaxed', !values.isTaxed)}
                                checked={values.isTaxed}
                                disabled={finish || !can('write-settings-payroll')}
                            />
                        </Col>
                    </Row>
                    <Collapse isOpen={values.isTaxed}>
                        <Select
                            className="mt-3"
                            isMulti={false}
                            isSearchable={false}
                            name="taxCategory"
                            options={
                                taxCategory?.filter(e => e.type === values.category)
                                    .map(item => ({
                                        value: item.id,
                                        label: item.name
                                    }))
                            }
                            value={
                                taxCategory?.filter(e => e.id === values.categoryTax)
                                    .map(item => ({
                                        value: item.id,
                                        label: item.name
                                    }))
                            }
                            placeholder={t("Kategori")}
                            onChange={(e) => formik.setFieldValue('categoryTax', e.value)}
                        />
                        <Row className="mt-3">
                            <Col xs="6" md="4" lg="3">
                                <CustomInput type="radio" disabled={finish || !can('write-settings-payroll')}
                                    checked={values.isRegular === true}
                                    onChange={() => formik.setFieldValue('isRegular', true)}
                                    id="isRegularTrue"
                                    name="isRegularTrue"
                                    label={t('Pendapatan Rutin')}
                                />
                            </Col>
                            <Col xs="6" md="4" lg="3">
                                <CustomInput type="radio" disabled={finish || !can('write-settings-payroll')}
                                    checked={values.isRegular === false}
                                    onChange={() => formik.setFieldValue('isRegular', false)}
                                    id="isRegularFalse"
                                    name="isRegularFalse"
                                    label={t('Pendapatan Tidak Rutin')}
                                />
                            </Col>
                        </Row>
                    </Collapse>
                </ModalBody>
                <ModalFooter className="border-top-0 d-flex justify-content-start pb-4" style={{ paddingLeft: '2rem' }}>
                    <Button disabled={isSubmitting} onClick={toggleAddComponent} color="outline-netis-primary" className="mr-2">{t('Batal')}</Button>
                    {!finish && can('write-settings-payroll') &&
                        <Button disabled={isSubmitting || errorFormula || nullFormula || nullCategory} type="submit" onClick={formik.handleSubmit} color="netis-primary" className="ml-2">
                            {isSubmitting ? <Spinner color="light" /> : t('Simpan')}
                        </Button>
                    }
                </ModalFooter>
            </Modal>
            <Modal isOpen={modalDelete} toggle={cancelDelete}>
                <ModalBody className="text-center py-4">
                    {t('Apakah anda yakin ingin menghapus')} {values.category === 'allowance' ? t('Tunjangan') : t('Potongan')}&nbsp;
                    <b>{values.name}</b> ?
                    <br />
                    <div className="mt-3 w-50 mx-auto text-center d-flex justify-content-around">
                        <Button disabled={isDeleting} color="outline-netis-primary" className="mr-2" onClick={cancelDelete}>{t('Batal')}</Button>
                        <Button disabled={isDeleting} color="netis-primary" className="ml-2" onClick={handleDelete}>
                            {isDeleting ? 'loading...' : t('Hapus')}
                        </Button>
                    </div>
                </ModalBody>
            </Modal>
        </div>
    )
}

export default translate(PayrollSetting)