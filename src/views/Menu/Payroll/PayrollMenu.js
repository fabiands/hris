import React, { useCallback, useState } from 'react'
import Datepicker from "react-datepicker";
import { Link } from 'react-router-dom'
import { t, translate } from 'react-switch-lang'
import { Row, Col, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap'
import { useUserPrivileges } from '../../../store';
import moment from 'moment';

function PayrollMenu({ match }) {
    const [period, setPeriod] = useState(new Date(localStorage.getItem('period') ?? moment()) ?? new Date());
    const { can } = useUserPrivileges();
    const menus = [
        {
            link: match.path + '/setting',
            title: t('Pengaturan Gaji'),
            image: require('../../../assets/img/payroll/setting.png'),
            can: 'read-settings-payroll'
        },
        {
            link: match.path + '/entry',
            title: t('Entri Gaji'),
            image: require('../../../assets/img/payroll/entry.png'),
            can: 'read-entry-payroll'
        },
        {
            link: match.path + '/workinghours',
            title: t('Perhitungan Jam Kerja'),
            image: require('../../../assets/img/payroll/work-count.png'),
            can: 'read-workinghours-payroll'
        },
        {
            link: match.path + '/tax-count',
            title: t('Perhitungan Pajak'),
            image: require('../../../assets/img/payroll/tax-count.png'),
            can: 'read-tax-payroll'
        },
        {
            link: match.path + '/slip',
            title: t('Slip Gaji'),
            image: require('../../../assets/img/payroll/slip.png'),
            can: 'read-salaryslip-payroll'
        },
    ]

    const handleChangePeriod = useCallback((e) => {
        if(!e){
            let dateNow = new Date()
            setPeriod(dateNow)
            localStorage.setItem('period', dateNow)
        }
        if(e){
            setPeriod(e)
            localStorage.setItem('period', e)
        }
    }, [])

    return (
        <div className="animated fadeIn">
            <Row>
                <Col xs="6">
                    <h4 className="content-title mb-4">{t('Penggajian')}</h4>
                </Col>
                <Col xs="5">
                    <InputGroup className="float-right period-group">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-calendar" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Datepicker
                            selected={period}
                            onChange={handleChangePeriod}
                            showMonthYearPicker
                            showFullMonthYearPicker
                            showFourColumnMonthYearPicker
                            dateFormatCalendar="MMMM"
                            calendarClassName="period-date"
                            dateFormat="MMMM yyyy"
                            maxDate={new Date()}
                            placeholderText="Select a date"
                            className="form-control"
                        />
                    </InputGroup>
                </Col>
            </Row>
            <div className="content-body">
                <Row>
                    {menus.map((item, idx) =>
                        can(item.can) &&
                        <Col xs="6" md="4" lg="3" key={idx} className="link-card scale-div-small">
                            <Link to={item.link}>
                                <div className="card menu-item" style={{ borderRadius: 15 }}>
                                    <div className="card-body">
                                        <div className="mt-1 mb-3 text-center">
                                            <img src={item.image} width="100%" alt="" style={{ objectFit: 'cover' }} />
                                        </div>
                                        <div className="menu-title text-center mb-2">
                                            <p className="mb-0 title-menu-company">{item.title}</p>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </Col>
                    )}
                </Row>
            </div>
        </div >
    )
}

export default translate(PayrollMenu)