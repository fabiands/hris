import React, { useCallback, useEffect, useState } from 'react'
import { t } from 'react-switch-lang'
import { Button, Col, Input, Row, Tooltip, UncontrolledTooltip } from 'reactstrap'

export default ({ allCode, onChangeFormulaValue, onErrorFormula, isOpen, defaultValue, showInput }) => {
    const operator = ['.', '+', '-', 'x', '/', '(', ')']
    const number = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    const [disableLetter, setDisableLetter] = useState(false)
    const [disableNumber, setDisableNumber] = useState(false)
    const [errorFormula, setErrorFormula] = useState(false);
    const [disableButtonFormula, setDisableButtonFormula] = useState({
        code: false,
        operator: false,
        number: false,
    });

    const handleInput = useCallback((evt) => {
        let charCode = (evt.which) ? evt.which : evt.keyCode;
        if(disableNumber){
            if (charCode > 31 && (charCode > 47 && charCode < 58)) {
                evt.preventDefault()
            }
            else {
                return true;
            }
        }
        else if (disableLetter){
            if (charCode > 31 && (charCode > 96 && charCode < 123)) {
                evt.preventDefault()
            }
            else {
                return true;
            }
        }
        else {
            if (charCode > 31 && charCode === 46 && (charCode < 40 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
                evt.preventDefault()
            }
            return true;
        }
    }, [disableNumber, disableLetter])

    // const handleLetterAndNumericOnly = (evt) => {
    //     let charCode = (evt.which) ? evt.which : evt.keyCode;

    //     if (charCode > 31 && charCode === 46 && (charCode < 40 || charCode > 57) && (charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
    //         evt.preventDefault()
    //     }
    //     return true;
    // }

    // const handleDisableNumber = (evt) => {
    //     let charCode = (evt.which) ? evt.which : evt.keyCode;
    //     if (charCode > 31 && (charCode > 47 || charCode < 58)) {
    //         evt.preventDefault()
    //     }
    //     return true;
    // }

    const handleBlurFormula = useCallback(() => {
        const value = defaultValue;
        try {
            // eslint-disable-next-line
            eval(value.replace(/[^0-9 ()+\-*/]/g, 1))
            setErrorFormula(false)
            onErrorFormula(false)
        } catch (e) {
            setErrorFormula(true)
            onErrorFormula(true)
        }
    }, [defaultValue, onErrorFormula])

    const handleFormulaValue = (e) => {
        let val = e;
        if (e === 'x') {
            val = '*'
        }
        const tempValue = defaultValue ?? ""
        const nextValue = tempValue + val
        // setFormulaValue(nextValue)
        onChangeFormulaValue(nextValue)
    }

    useEffect(() => {
        // eslint-disable-next-line
        const split = defaultValue?.split(/([-+*\/]|[^-+*\/]+)/).filter(x => x !== '');
        if (split?.length > 0) {
            if (split[split?.length - 1].match(/^[a-zA-Z]*$/)) {
                setDisableNumber(true)
                setDisableButtonFormula(state => ({ ...state, code: true, number: true }))
            } else {
                setDisableNumber(false)
                setDisableButtonFormula(state => ({ ...state, code: true, number: false }))
            }

            if (split[split?.length - 1].match(/^[()+\-*/]*$/)) {
                setDisableNumber(false)
                setDisableButtonFormula(state => ({ ...state, code: false, number: false }))
            }
            // else {
            // setDisableButtonFormula(state => ({ ...state, code: false, number: false }))
            // }
        } else {
            setDisableNumber(false)
            setDisableButtonFormula({ operator: false, code: false, number: false })
        }
        handleBlurFormula()
        // eslint-disable-next-line
    }, [defaultValue])

    useEffect(() => {
        if(defaultValue?.length > 0){
            if (defaultValue[defaultValue?.length - 1].match(/^[a-zA-Z]*$/)) {
                setDisableNumber(true)
                setDisableLetter(false)
            }
            else if (defaultValue[defaultValue?.length - 1].match(/^\d+$/)){
                setDisableNumber(false)
                setDisableLetter(true)
            }
            else {
                setDisableLetter(false)
                setDisableNumber(false)
            }
        }
    }, [defaultValue])

    return (
        <Row>
            {showInput &&
                <Col xs="12" className="mb-3">
                    <Input
                        type="text"
                        name="formula_value"
                        id="formula_value"
                        value={defaultValue ?? ""}
                        onChange={(e) => onChangeFormulaValue(e.target.value)}
                        onKeyPress={handleInput}
                        onBlur={handleBlurFormula}
                        pattern="[A-Z0-9]*"
                    />
                    {errorFormula && <small className="text-danger">{t('Formula tidak valid')}</small>}
                </Col>
            }
            {isOpen &&
                <>
                    <Col xs="6" className="pr-4">
                        <Row>
                            {number.map((item, idx) =>
                                <Col xs="4" key={idx} className="text-center px-1">
                                    <Button color='light' disabled={disableButtonFormula.number} onClick={() => handleFormulaValue(item)} className="btn-sm w-100 my-1">{item}</Button>
                                </Col>
                            )}
                            {operator.map((item, idx) =>
                                <Col xs="4" key={idx} className="text-center px-1">
                                    <Button color='secondary' disabled={disableButtonFormula.operator} onClick={() => handleFormulaValue(item)} className="btn-sm w-100 my-1"><b>{item}</b></Button>
                                </Col>
                            )}
                            <Col xs="4" className="text-center px-1">
                                <Button color="secondary" onClick={() => onChangeFormulaValue('')} className="btn-sm w-100 my-1 text-danger"><b>AC</b></Button>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs="6" className="pl-4">
                        <Row>
                            <Col xs="6" sm="4" className="text-center px-1">
                                <Button color='secondary' disabled={disableButtonFormula.code} id="Tooltip-GJPKK" onClick={() => handleFormulaValue('GJPKK')} className="btn-sm w-100 my-1">GJPKK</Button>
                                <UncontrolledTooltip
                                    placement="bottom"
                                    target={"Tooltip-GJPKK"}
                                >
                                    {t('Gaji Pokok')}
                                </UncontrolledTooltip>
                            </Col>
                            <Col xs="6" sm="4" className="text-center px-1">
                                <Button color='secondary' disabled={disableButtonFormula.code} id="Tooltip-JMKRJ" onClick={() => handleFormulaValue('JMKRJ')} className="btn-sm w-100 my-1">JMKRJ</Button>
                                <UncontrolledTooltip
                                    placement="bottom"
                                    target={"Tooltip-JMKRJ"}
                                >
                                    {t('Jam Kerja')}
                                </UncontrolledTooltip>
                            </Col>
                            <Col xs="6" sm="4" className="text-center px-1">
                                <Button color='secondary' disabled={disableButtonFormula.code} id="Tooltip-JMLMB" onClick={() => handleFormulaValue('JMLMB')} className="btn-sm w-100 my-1">JMLMB</Button>
                                <UncontrolledTooltip
                                    placement="bottom"
                                    target={"Tooltip-JMLMB"}
                                >
                                    {t('Jam Lembur')}
                                </UncontrolledTooltip>
                            </Col>
                            <Col xs="6" sm="4" className="text-center px-1">
                                <Button color='secondary' disabled={disableButtonFormula.code} id="Tooltip-PPH21" onClick={() => handleFormulaValue('PPH21')} className="btn-sm w-100 my-1">PPH21</Button>
                                <UncontrolledTooltip
                                    placement="bottom"
                                    target={"Tooltip-PPH21"}
                                >
                                    {t('Pajak Penghasilan')}
                                </UncontrolledTooltip>
                            </Col>
                        </Row>
                        <hr />
                        <Row>
                            {allCode && allCode?.map((item, idx) =>
                                <Col xs="6" sm="4" key={idx} className="text-center px-1">
                                    <ButtonComponent item={item} disableButtonFormula={disableButtonFormula} handleFormulaValue={handleFormulaValue} />
                                </Col>
                            )}
                        </Row>
                    </Col>
                    {!showInput &&
                        <Col xs="12">
                            {errorFormula && <small className="text-danger">{t('Formula tidak valid')}</small>}
                        </Col>
                    }
                </>
            }
        </Row>
    )
}

const ButtonComponent = ({ item, disableButtonFormula, handleFormulaValue }) => {
    const [tooltipOpen, setTooltipOpen] = useState(false);

    const toggleTooltip = () => setTooltipOpen(!tooltipOpen);

    return (
        <div>
            <Button color='secondary' disabled={disableButtonFormula.code} id={"Tooltip-" + item.code} onClick={() => handleFormulaValue(item.code)} className="btn-sm w-100 my-1">{item.code}</Button>
            <Tooltip
                placement="bottom"
                isOpen={tooltipOpen}
                target={"Tooltip-" + item.code}
                toggle={toggleTooltip}
            >
                {item.name}
            </Tooltip>
        </div>
    )
}