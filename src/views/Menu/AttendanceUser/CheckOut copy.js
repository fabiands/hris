import React, { useState, Fragment } from 'react';
import Webcam from "react-webcam";
import Notification from "./components/Notification";
import TimeFormat from '../../../utils/TimeFormat';
import { toast } from 'react-toastify';
import Color from '../../../utils/Colors';
import captcha from '../../../utils/captcha';
import CaptureLoading from './components/CaptureLoading'
import AfirmationModal from './components/AfirmationModal'
import { translate } from 'react-switch-lang';
import { useToken } from '../../../store';
import request from '../../../utils/request';


var dataPhoto = null;
var dataLatitude = null;
var dataLongitude = null;
var dataTime = null;

const CheckOut = (props) => {
    const token = useToken();
    const { t } = props;
    const [state, setState] = useState({
        isLoading: false,
        isGetLocation: false,
        isSuccess: false,
        isFaceNotMatch: false,
        isLocNotMatch: false,
        isLocNotAccurate: false,
        isTimeNotMatch: false,
        isShiftNotMatch: false,
        isAlready: false,
        isFailed: false,
        attendanceHistoryId: 0,
        isDetect: true,
        isNotSupport: false
    });
    const webcamRef = React.useRef(null);
    // eslint-disable-next-line
    const [imgSrc, setImgSrc] = React.useState(null);

    // eslint-disable-next-line
    const findCoordinates = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                dataLatitude = position.coords.latitude;
                dataLongitude = position.coords.longitude;
            },
            error => console.log(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
        );
    };

    const findCoordinatesLowAccuracy = () => {
        navigator.geolocation.getCurrentPosition(
            position => {
                dataLatitude = position.coords.latitude;
                dataLongitude = position.coords.longitude;
            },
            error => console.log(error.message),
        );
    };


    const takePicture = React.useCallback(() => {

        setState({
            isLoading: true,
        })
        dataPhoto = webcamRef.current.getScreenshot();
        sendData();
        // eslint-disable-next-line
    }, [webcamRef, setImgSrc]);
    const showLoading = () => {

        if (state.isLoading) {
            return <CaptureLoading title="Loading" visible={true} />;
        }
    };

    const showNotification = () => {
        if (state.isSuccess) {
            return (
                <AfirmationModal
                    visible={true}
                    title="Sukses"
                    text={`${t('selamatcheckoutdicatat')} ${dataTime}`}
                    onPressButton={() => {
                        setState({ isSuccess: false });
                        window.location.replace("/home")
                    }}
                    titleButton="Oke"
                    titleColor={Color.white}
                    bgColorButton={Color.primaryColor}
                />
            );
        }
        if (state.isFaceNotMatch) {
            return (
                <AfirmationModal
                    visible={true}
                    title="Perhatian!"
                    text={`${t('berhasilabsenkeluar')} ${dataTime} ${t('tetapiwajahtdksesuai')}`}
                    onPressButton={() => {
                        setState({ isFaceNotMatch: false });
                        window.location.replace("/home")
                    }}
                    titleButton="Oke"
                    titleColor={Color.white}
                    bgColorButton={Color.primaryColor}
                />
            );
        }
        if (state.isShiftNotMatch) {
            return (
                <AfirmationModal
                    visible={true}
                    title="Gagal"
                    text={t('kerjatdkada')}
                    onPressButton={() => {
                        setState({ isShiftNotMatch: false });
                        window.location.replace("/home")
                    }}
                    titleButton="Oke"
                    titleColor={Color.white}
                    bgColorButton={Color.primaryColor}
                />
            );
        }
        if (state.isAlready) {
            return (
                <AfirmationModal
                    visible={true}
                    title="Gagal"
                    text={t('pernahcheckout')}
                    onPressButton={() => {
                        setState({ isAlready: false });
                        window.location.replace("/home")
                    }}
                    titleButton="Oke"
                    titleColor={Color.white}
                    bgColorButton={Color.primaryColor}
                />
            );
        }
        if (state.isFailed) {
            return (
                <AfirmationModal
                    animationType="none"
                    visible={true}
                    title="Failed"
                    text={t('checkingagal')}
                    onPressButton={() => {
                        setState({ isFailed: false });
                        window.location.replace("/home")
                    }}
                    titleButton="Back"
                    titleColor={Color.white}
                    bgColorButton={Color.primaryColor}
                />
            );
        }
    };
    const DataURIToBlob = (dataURI) => {
        const splitDataURI = dataURI.split(',')
        const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
        const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

        const ia = new Uint8Array(byteString.length)
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i)
        }
        return new Blob([ia], { type: mimeString })
    }
    const sendData = async () => {
        setState({ isLoading: true })

        let formData = new FormData();
        let file = DataURIToBlob(dataPhoto);
        formData.append('latitude', dataLatitude);
        formData.append('longitude', dataLongitude);
        formData.append('faceFile', file, 'faceClockIn.jpg');

        let answer = await captcha.get();
        if (answer === '') return setState({ isLoading: false })
        else formData.append('captcha', answer);

        const url = 'v1/attendance/clockout';
        const session = token;
        await request.post(url, formData)
            .then(({ status: res, data }) => {
                if (res === 200) {
                    if (data.data.faceIsMatch === '1') {
                        dataTime = TimeFormat(data.data.time);
                        setState({
                            isLoading: false,
                            isSuccess: true,
                        });
                    } else {
                        dataTime = TimeFormat(data.data.time);
                        setState({
                            isLoading: false,
                            isFaceNotMatch: true,
                        });
                    }
                } else if (res === 404) {
                    setState({
                        isLoading: false,
                        isShiftNotMatch: true,
                    });
                } else if (res === 302) {
                    setState({
                        isLoading: false,
                        isAlready: true,
                    });
                } else {
                    setState({
                        isLoading: false,
                        isFailed: true,
                    });
                }
            })
            .catch(error => {
                console.log('Error:', error);
            });
    };

    const checkGeo = async () => {
        if (navigator.geolocation) {
            await navigator.geolocation.getCurrentPosition(
                position => {
                    dataLatitude = position.coords.latitude;
                    dataLongitude = position.coords.longitude;
                    setState({
                        isDetect: false,
                        isNotSupport: false,


                    })
                },
                error => setState({
                    isDetect: false,
                    isNotSupport: true
                }),
                { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 });

        } else {
            setState({
                isDetect: false,
                isNotSupport: true
            })
        }
    }
    // eslint-disable-next-line
    const detectDevice = () => {
        if (state.isDetect) {
            navigator.getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

            if (navigator.getUserMedia) {
                // Request the camera.
                navigator.getUserMedia(
                    // Constraints
                    {
                        video: true,
                    },

                    // Success Callback
                    function (localMediaStream) {
                        checkGeo();
                    },

                    // Error Callback
                    function (err) {

                        setState({
                            isDetect: false,
                            isNotSupport: true,
                            isAllowed: false,
                        })

                    }
                );

            } else if (navigator.mediaDevices.getUserMedia) {
                // Request the camera.
                navigator.mediaDevices.getUserMedia(
                    // Constraints
                    {
                        video: true,
                    },

                    // Success Callback
                    function (localMediaStream) {
                        checkGeo();
                    },

                    // Error Callback
                    function (err) {

                        setState({
                            isDetect: false,
                            isNotSupport: true,
                            isAllowed: false,
                        })

                    }
                );
            } else {
                console.log('Sorry, your browser does not support getUserMedia');
                setState({
                    isDetect: false,
                    isNotSupport: true,
                    isAllowed: false,
                })
            }
        }

    }
    const showNotSupport = () => {
        if (state.isNotSupport) {
            return <Notification
                onClick={() => { setState({ isNotSupport: false }, window.location.replace("/home")) }}
                visible={true}
                title="Error"
                desc={t('akseskameradilarang')}
                actionTitle="Back"
            />
        }
    };
    const videoConstraints = {
        facingMode: "user"
    };
    return (

        <Fragment>
            {detectDevice()}
            {showNotSupport()}
            {showLoading()}
            {showNotification()}

            <div className="margin-30" style={{ height: window.outerHeight }}>
                <div className="text-center flexbox-wrapper">
                    <div className="flexboxi-1 text-center">

                        <Webcam
                            audio={false}
                            ref={webcamRef}
                            screenshotFormat="image/jpeg"
                            forceScreenshotSourceSize={true}
                            mirrored={true}
                            style={{ height: '100%', width: '100vw' }}
                            videoConstraints={videoConstraints}
                        />

                        <button className="button-capture" onClick={() => {

                            if (dataLatitude !== null) {
                                setState({
                                    isLoading: true,
                                })
                                takePicture();
                            } else {
                                setState({
                                    isLoading: true,
                                })
                                findCoordinatesLowAccuracy();
                                setTimeout(() => {
                                    takePicture();
                                }, 5000);
                            }

                        }} />

                    </div>
                </div>
            </div>

        </Fragment>
    );

};


export default translate(CheckOut);
