import React, { Component, Fragment } from 'react';
import { Col, Row, Input, Button, Modal, ModalBody, FormGroup, Label, Spinner, Badge } from 'reactstrap';
import MapMarker from '../Personnel/component/atom/MapMarker';
import GoogleMapReact from 'google-map-react';
import Axios from 'axios';
import { connect } from 'react-redux';
import { DatePickerInput } from 'rc-datepicker';
import { toast } from 'react-toastify';
import 'rc-datepicker/lib/style.css';
import moment from '../../../utils/moment';
import { formatDate } from '../../../utils/formatter';
import { translate } from 'react-switch-lang';
import LoadingAnimation from '../../../components/LoadingAnimation';
import DataNotFound from '../../../components/DataNotFound';
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../components/ResponsiveTable';
toast.configure()
class AttendanceUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dropdownOpen: [false, false],
            session: props.token,
            loading: true,
            modalDetail: false,
            editMessage: false,
            idEditData: '',
            attendanceDetail: {},
            message: {
                message: ''
            },
            pathFaceFile: '',
            latLng: {},
            date: new Date(Date.now() - 7 * 24 * 60 * 60 * 1000),
            dateEnd: new Date(),
            start: formatDate(new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)),
            end: formatDate(new Date()),
            resMessage: '',
            attendanceHistory: [],
            dataNotFound: false,
        };

        this.modalDetail = this.modalDetail.bind(this);
    }

    componentDidMount = () => {
        this.getDataFromAPI(this.state.start, this.state.end)
    }

    modalDetail = (data) => {
        const attendanceDetail = { ...this.state.attendanceDetail, type: data.type, time: data.time, FaceIsMatch: data.FaceIsMatch }
        const message = { ...this.state.message, message: data.message }
        const latLng = { ...this.state.latLng, lat: data.latitude, lng: data.longitude }
        this.setState({
            modalDetail: !this.state.modalDetail,
            attendanceDetail,
            message,
            latLng,
            idEditData: data.id,
            pathFaceFile: process.env.REACT_APP_DOMAIN + data.pahFaceFile,
            editMessage: false
        })
    }

    editMessage = () => this.setState({ editMessage: !this.state.editMessage })

    handleChange = (e) => {
        const message = { ...this.state.message, message: e.target.value }
        this.setState({ message })
    }

    handleChangeDateStart = date => {
        if (date instanceof Date && date.getTime() <= this.state.dateEnd.getTime()) {
            this.setState({
                date,
                start: formatDate(date)
            });
        }
        else {
            let dateStart;
            if (this.state.dateEnd) {
                dateStart = new Date(this.state.dateEnd.getTime() - 7 * 24 * 60 * 60 * 1000)
            } else {
                dateStart = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
            }
            this.setState({
                date: dateStart,
                start: formatDate(dateStart)
            })
        }
    }

    handleChangeDateEnd = date => {
        if (date instanceof Date && date.getTime() >= this.state.date.getTime()) {
            this.setState({
                dateEnd: date,
                end: formatDate(date)
            });
        } else {
            let dateEnd;
            if (this.state.date) {
                dateEnd = new Date(this.state.date.getTime() + 7 * 24 * 60 * 60 * 1000)
            } else {
                dateEnd = new Date();
            }
            this.setState({
                dateEnd: dateEnd,
                end: formatDate(dateEnd)
            })
        }
    }

    filterData = () => {
        this.setState({ loading: true })
        this.getDataFromAPI(this.state.start, this.state.end)
    }

    getDataFromAPI = (start, end) => {
        this.setState({ loading: true });
        Axios.get(process.env.REACT_APP_DOMAIN + `/api/v1/personnels/attendance/history?start=${start}&end=${end}`, { headers: { "Authorization": `Bearer ${this.state.session}` } })
            .then((res) => {
                const attendanceHistory = res.data.data;
                setTimeout(() => {
                    this.setState({ attendanceHistory }, () => this.setState({ loading: false }))
                }, 500)
            })
            .catch(error => {
                if(error.response.status === 404){
                    this.setState({ dataNotFound: true });
                    return;
                }
            })
            .finally(() => {
                this.setState({
                    loading: false
                })
            })
    }

    editMessageToAPI = (id) => {
        const { t } = this.props;
        this.setState({ loading: true })
        Axios.post(process.env.REACT_APP_DOMAIN + `/api/v1/personnels/attendance/history/${id}`, this.state.message, { headers: { "Authorization": `Bearer ${this.state.session}` } })
            .then((res) => {
                this.getDataFromAPI(this.state.start, this.state.end);
                this.setState({
                    loading: false,
                    modalDetail: !this.state.modalDetail,
                    editMessage: !this.state.editMessage,
                    resMessage: t('pesandiubah'),
                }, () => toast.success(this.state.resMessage, { autoClose: 3000 }))
            })
            .catch(error => {
                // console.log(error.response.data.errors.message)
                this.setState({ loading: false })
                toast.error(error.response.data.errors.message, { autoClose: 3000 })
            });
    }

    render() {
        const { t } = this.props;
        moment.locale(t('id'))
        return (
            <div className="animated fadeIn">
                <h4 className="content-title mb-4">{t('riwayatabsensi')}</h4>
                <div className="content">
                    <Row className="md-company-header mb-3">
                        <Col md="2" >
                            <FormGroup>
                                <Label htmlFor="startDate" className="input-label">{t('dari')}</Label>
                                <DatePickerInput
                                    name="startDate"
                                    id="startDate"
                                    onChange={this.handleChangeDateStart}
                                    value={this.state.date}
                                    maxDate={this.state.end}
                                    className='my-custom-datepicker-component'
                                    showOnInputClick={true}
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col md="2" >
                            <FormGroup>
                                <Label htmlFor="endDate" className="input-label">{t('hingga')}</Label>
                                <DatePickerInput
                                    name="endDate"
                                    id="endDate"
                                    onChange={this.handleChangeDateEnd}
                                    value={this.state.dateEnd}
                                    minDate={this.state.start}
                                    className='my-custom-datepicker-component'
                                    showOnInputClick={true}
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                    readOnly
                                    maxDate={new Date()}
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="6" md="2" >
                            <div className="pt-sm-2">
                                <Button type="submit" className="mt-sm-4" color="netis-color" style={{ width: '60px' }} onClick={this.filterData}>
                                    {this.state.loading ? <Spinner color="light" size="sm" /> : 'Filter'}
                                </Button>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" lg="12">
                            <Rtable size="sm">
                                <Rthead>
                                    <Rtr>
                                        <Rth className="">{t('jenis')}</Rth>
                                        <Rth className="">{t('tanggal')}</Rth>
                                        <Rth className="text-center w-20">{t('jam')}</Rth>
                                        <Rth className="text-center w-20"></Rth>
                                    </Rtr>
                                </Rthead>
                                <Rtbody>
                                    {
                                    this.state.loading ?
                                        <Rtr>
                                            <Rtd colSpan="5">
                                                <LoadingAnimation/>
                                            </Rtd>
                                        </Rtr>
                                    :
                                    (!this.state.dataNotFound && this.state.attendanceHistory.length > 0) ?
                                        this.state.attendanceHistory.map((data, idx) => (
                                            <Rtr key={idx}>
                                                <Rtd className="">{t(data.type)}</Rtd>
                                                <Rtd className="">{moment(data.date).format('DD MMMM YYYY')}</Rtd>
                                                <Rtd className="text-md-center">{moment(data.time, "HH:mm:ss").format("HH:mm")}</Rtd>

                                                <Rtd className="text-md-center">
                                                    <p className="action-text" onClick={() => this.modalDetail(data)}>{t('lihatdetail')}</p>
                                                </Rtd>
                                            </Rtr>
                                        ))
                                    :
                                        <Rtr key={"no_data"}>
                                            <Rtd className="text-center no-data-message" colSpan="5">
                                                <DataNotFound/>
                                            </Rtd>
                                        </Rtr>
                                    }
                                </Rtbody>
                            </Rtable>
                        </Col>
                    </Row>

                    {/* Modal Box Detail Data */}
                    <Modal isOpen={this.state.modalDetail} toggle={this.modalDetail} className={'modal-lg ' + this.props.className}>
                        <ModalBody>
                            <div className="d-flex justify-content-between align-items-center mb-4">
                                <h5 className="content-sub-title mb-0">{t('Detail Riwayat Presensi')}</h5>
                                <Button color="link" size="lg" className="text-danger" onClick={this.modalDetail}><strong>&times;</strong></Button>
                            </div>
                            <Row>
                                <div className="col-6">
                                    <FormGroup>
                                        <Label htmlFor="jenis" className="input-label">{t('jenis')}</Label>
                                        <Input type="text" name="jenis" id="jenis" placeholder={t('jenis')}
                                            disabled={true}
                                            defaultValue={t(this.state.attendanceDetail.type || 'clockIn')} />
                                    </FormGroup>
                                </div>
                                <div className="col-6">
                                    <FormGroup>
                                        <Label htmlFor="waktu" className="input-label">{t('jam')}</Label>
                                        <Input type="text" name="waktu" id="waktu" placeholder={t('jam')}
                                            disabled={true}
                                            defaultValue={moment(this.state.attendanceDetail.time, "HH:mm:ss").format("HH:mm")} />
                                    </FormGroup>
                                </div>
                            </Row>
                            <Row className="mb-2">
                                <div className="col-md-6">
                                    <div className="text-center">
                                        <img src={this.state.pathFaceFile} style={{ maxHeight: '350px' }} className="img-fluid" alt="personnel-face" />
                                        <br />
                                        {this.state.attendanceDetail.FaceIsMatch ? (
                                            <Badge className="mr-1" color="success">
                                                <p style={{ margin: 0 }}>{t('wajahsesuai')}</p>
                                            </Badge>
                                        ) : (
                                                <Badge className="mr-1" color="danger">
                                                    <p style={{ margin: 0 }}>{t('wajahbelumsesuai')}</p>
                                                </Badge>
                                            )
                                        }
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div style={{ height: '350px', width: '100%' }}>
                                        <GoogleMapReact
                                            bootstrapURLKeys={{ key: "AIzaSyDQsNCd2Trmf4MLwcB7k1oqpWZPpTeCkc0" }}
                                            center={this.state.latLng}
                                            defaultZoom={17}
                                        >
                                            <MapMarker
                                                lat={this.state.latLng.lat}
                                                lng={this.state.latLng.lng}
                                                text={""}
                                            />
                                        </GoogleMapReact>
                                    </div>
                                </div>
                            </Row>
                            <Row>
                                <div className="col-md-12">
                                    <FormGroup>
                                        <Label htmlFor="message" className="input-label">{t('Pesan Karyawan')}</Label>
                                        <Input type="textarea" name="message" id="message" placeholder={t('pesan')} maxLength="255"
                                            disabled={!this.state.editMessage}
                                            value={this.state.message.message || ''}
                                            onChange={this.handleChange} />
                                        <div className="d-flex justify-content-end align-items-center">
                                            {this.state.editMessage ?
                                                <Fragment>
                                                    <Button className="mr-2 mt-3" color="white" onClick={this.editMessage}>{t('batal')}</Button>
                                                    <Button type="submit" className="mr-2 mt-3" color="netis-color" style={{ width: '70px' }} onClick={() => this.editMessageToAPI(this.state.idEditData)}>
                                                        {this.state.loading ? <Spinner color="light" size="sm" /> : t('simpan')}
                                                    </Button>
                                                </Fragment>
                                                :
                                                <Button className="mt-3" color="netis-color" onClick={this.editMessage}><i className="fa fa-pencil" style={{ marginRight: 5 }}></i>Edit</Button>
                                            }
                                        </div>
                                    </FormGroup>
                                </div>
                            </Row>
                        </ModalBody>
                    </Modal>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.token
    }
}
export default connect(mapStateToProps)(translate(AttendanceUser));
