import React, { Suspense } from 'react';
import { lazyComponent as lazy } from '../../../components/lazyComponent';
import { Switch, Redirect } from 'react-router';
import LoadingAnimation from '../../../components/LoadingAnimation'
import { Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import { Link } from 'react-router-dom';
import {
    t,
    translate,
} from 'react-switch-lang';
import { connect } from 'react-redux';
import AuthRoute from '../../../components/AuthRoute';
const OvertimeDetail = lazy(() => import('./OvertimeDetail'));
const OvertimeHistory = lazy(() => import('./OvertimeHistory'));
const OvertimeApproval = lazy(() => import('./OvertimeApproval'));

const PANEL_USER = '3';
const PANEL_ADMIN = '2';

function matchWildcard(str, wildcard) {
    var escapeRegex = (str) => str.replace(/([.*+?^=!:${}()|[\]/\\])/g, "\\$1");
    return new RegExp("^" + wildcard.split("*").map(escapeRegex).join(".*") + "$").test(str);
}

function getRoutes(props) {
    const { t, match, user, menu } = props;
    const routes = [];

    if (menu === PANEL_ADMIN) {
        routes.push({
            path: match.path + '/manage', privileges: ['browse-overtime'], exact: true, component: OvertimeApproval,
            tab: t('kelolalembur'), active: match.path + '/manage'
        });
        routes.push({ path: match.path + '/manage/:id', privileges: ['read-overtime'], exact: true, component: OvertimeDetail });
    } else if (menu === PANEL_USER) {
        routes.push({
            path: match.path + '/history', exact: true, component: OvertimeHistory,
            tab: t('riwayatlembur'), active: match.path + '/history'
        })
        routes.push({ path: match.path + '/history/:id', exact: true, component: OvertimeDetail });

        if (user.hasBawahan) {
            routes.push({
                path: match.path + '/manage', exact: true, component: OvertimeApproval,
                tab: t('kelolalemburbawahan'), active: match.path + '/manage'
            });
            routes.push({ path: match.path + '/manage/:id', privileges: ['read-overtime'], exact: true, component: OvertimeDetail });
        }
    }

    return routes;
}

function OvertimeWrapper(props) {
    const routes = getRoutes(props);
    const { match, location, menu } = props;
    return (
        <div className="animated fadeIn">
            {menu === PANEL_USER && <div className="row mb-4 d-sm-none">
                <div className="col-6">
                    <WebcamButton to="/overtimes/webcam/start" type="in" imgSrc={require('../../../assets/assets_ari/check_in.png')} label={t('Mulai Lembur')} />
                </div>
                <div className="col-6">
                    <WebcamButton to="/overtimes/webcam/end" type="out" imgSrc={require('../../../assets/assets_ari/check_out.png')} label={t('Akhiri Lembur')} />
                </div>
            </div>}
            <Nav tabs className="flex-nowrap">
                {routes.filter(route => !!route.tab).map(route => (
                    <NavItem key={route.path} className="text-nowrap">
                        <NavLink tag={Link} to={route.path} active={route.active && matchWildcard(location.pathname, route.active)}>{route.tab}</NavLink>
                    </NavItem>
                ))}
            </Nav>
            <TabContent>
                <TabPane className="px-0">
                    <Suspense fallback={<LoadingAnimation />}>
                        <Switch>
                            {routes.map(route => (
                                <AuthRoute key={route.path} {...route} />
                            ))}
                            {routes[0] && <Redirect exact from={match.path} to={routes[0].path} />}
                        </Switch>
                    </Suspense>
                </TabPane>
            </TabContent>
        </div>
    )
}

function WebcamButton({ imgSrc, label, type, ...rest }) {
    return (<Link {...rest}>
        <div className="card menu-item">
            <div className={`card-body check-${type}`}>
                <div className="menu-img mt-2 mb-3 text-center">
                    <img src={imgSrc} alt="" />
                </div>
                <div className="menu-title mb-2 text-center">
                    <h5 className="mb-0 title-menu-company"><b>{label}</b></h5>
                </div>
            </div>
        </div>
    </Link>)
}


const mapStateToProps = ({ user, token, menu }) => ({ user, token, menu });
export default connect(mapStateToProps)(translate(OvertimeWrapper));
