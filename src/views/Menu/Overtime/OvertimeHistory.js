import React, { useState, useEffect, useMemo } from "react";
import { formatDate } from "../../../utils/formatter";
import request from "../../../utils/request";
import { Button, Form, Row, Col, Label, Spinner, ModalBody, FormGroup, Modal, Input, FormFeedback, UncontrolledPopover, PopoverBody } from "reactstrap";
import { DatePickerInput } from "rc-datepicker";
import { t, translate } from 'react-switch-lang';
import LoadingAnimation from "../Personnel/component/atom/LoadingAnimation";
import DataNotFound from "../../../components/DataNotFound";
import * as moment from 'moment';
import { useFormik } from 'formik';
import ReactInputMask from "react-input-mask";
import * as Yup from 'yup';
import StatusBadge from "./StatusBadge";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";
import useFilterDate from "./filterDateHooks";
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from "../../../components/ResponsiveTable";


function OvertimeHistory(props) {
    const [filters, { DateStartPickerComponent, DateEndPickerComponent }] = useFilterDate({
        start: null,
        end: null
    })

    const [loading, setLoading] = useState(true);
    const [overtimes, setOvertimes] = useState([]);
    const [showCreateModal, setShowCreateModal] = useState(false);
    const [deleteData, setDeleteData] = useState(null);
    const [deleteLoading, setDeleteLoading] = useState(false);

    // const toggleDelete = () => setDeleteModal(!deleteModal);

    function fetchOvertimes() {
        const params = Object.keys(filters)
            .filter(k => filters[k] !== null)
            .reduce((initial, k) => ({ ...initial, [k]: formatDate(filters[k])}), {});

        setLoading(true);
        request.get('v1/overtimes/history', { params })
            .then(res => {
                setOvertimes(res.data.data);
            })
            .finally(() => setLoading(false));
    }

    useEffect(fetchOvertimes, []);

    function doDelete() {
        if (deleteLoading) return;
        setDeleteLoading(true);
        request.delete('/v1/overtimes/cancel/'+ deleteData).then(() => {
            setOvertimes(overtimes.filter(data => data.id !== deleteData));
            setDeleteData(null);
            toast.success(t('Permintaan lembur berhasil dibatalkan'));
        })
        .catch(err => {
            toast.error(t('Permintaan lembur gagal dibatalkan'));
            throw err;
        })
        .finally(() => {
            setDeleteLoading(false);
        });
    }

    return (
        <div className="animated fadeIn">
            <div className="d-flex align-items-center mb-4">
                <Button color="netis-color" onClick={() => setShowCreateModal(true)}>
                    <i className="fa fa-plus mr-2"></i> {t('buatpermintaanlembur')}
                </Button>
            </div>

            <Form onSubmit={(e) => { e.preventDefault(); fetchOvertimes() }}>
                <Row className="mb-4">
                    <Col sm="5" md="3">
                        <Label tag="small" htmlFor="start" className="text-muted">{t('dari')}</Label>
                        <br />
                        <DateStartPickerComponent/>
                    </Col>
                    <Col sm="5" md="3">
                        <Label tag="small" htmlFor="end" className="text-muted">{t('hingga')}</Label>
                        <br />
                        <DateEndPickerComponent/>
                    </Col>
                    <Col sm="2">
                        <div className="pt-sm-0">
                            <Button type="submit" className="mt-sm-4" color="netis-color" style={{ width: '60px' }} disabled={loading}>
                                {loading ? <Spinner color="light" size="sm" /> : t('cari')}
                            </Button>
                        </div>
                    </Col>
                </Row>
            </Form>

            <Row className="md-company-header mb-3">
                <Col xs="12">
                {
                    loading ? <LoadingAnimation/> :
                    (overtimes.length === 0 ? <DataNotFound/> :
                        <Rtable size="sm">
                            <Rthead>
                                <Rtr>
                                    <Rth className="text-center w-5">No.</Rth>
                                    <Rth className="text-left w-15">{t('tanggal')}</Rth>
                                    <Rth className="text-center w-10">{t('mulai')}</Rth>
                                    <Rth className="text-center w-10">{t('hingga')}</Rth>
                                    <Rth className="text-left w-20">Agenda</Rth>
                                    <Rth>Status</Rth>
                                    <Rth ></Rth>
                                </Rtr>
                            </Rthead>
                            <Rtbody>
                                {overtimes.map((data, idx) => (
                                    <Rtr key={idx}>
                                        <Rtd className="text-md-center">{idx + 1}</Rtd>
                                        <Rtd>{moment(data.date).format('DD MMMM YYYY')}</Rtd>
                                        <Rtd className="text-md-center">{data.start.substr(0, 5)}</Rtd>
                                        <Rtd className="text-md-center">{data.end ? data.end.substr(0,5) : <i className="text-muted">NOT SET</i>}</Rtd>
                                        <Rtd>
                                            <span id={`overtimePurpose${idx}`}>{data.message.substr(0, 20) + (data.message.length > 20 ? '...' : '')}</span>
                                            <UncontrolledPopover placement="top" target={`overtimePurpose${idx}`}>
                                                <PopoverBody style={{whiteSpace: 'pre-wrap'}}>{data.message}</PopoverBody>
                                            </UncontrolledPopover>
                                        </Rtd>
                                        <Rtd className="text-left text-nowrap"><StatusBadge status={data.status}/></Rtd>
                                        <Rtd className="text-left text-nowrap">
                                            <Link className="btn btn-netis-color" to={`/overtimes/history/${data.id}`}>{t('detail')}</Link>{' '}
                                            {
                                            (function(){
                                                if (data.status === 'pending'){
                                                    return(
                                                        <button className="btn btn-netis-danger"
                                                        onClick={() => setDeleteData(data.id)}>{t('batal')}
                                                        </button>
                                                    );
                                                }
                                            })()
                                            }
                                        </Rtd>
                                    </Rtr>
                                ))}
                            </Rtbody>
                        </Rtable>

                    )
                }
                </Col>
            </Row>
            <Modal isOpen={!!deleteData} toggle={() => {
                if (!deleteLoading) {
                    setDeleteData(null)
                }
            }}>
            <ModalBody>
                <h6>{t('yakinmenghapus')}</h6>
                <div className="d-flex justify-content-end">
                {!deleteLoading && <Button className="mr-2" color="light" onClick={() => setDeleteData(null)}>{t('batal')}</Button>}
                <Button color="netis-primary" onClick={doDelete} disabled={deleteLoading}>
                    { deleteLoading ? <React.Fragment><Spinner size="sm" color="light" /> Deleting...</React.Fragment> : t('hapus')}
                </Button>
                </div>
            </ModalBody>
            </Modal>
            <OvertimeCreateModal isOpen={showCreateModal} toggle={() => setShowCreateModal(! showCreateModal)} onCreated={(newObj) => setOvertimes([newObj, ...overtimes])} />
        </div>
    );
}


const OvertimeCreateModal = translate(({ isOpen, toggle, onCreated, t }) => {
    const [modalLoading, setModalLoading] = useState(false)
    const CreateOvertimeSchema = useMemo(() => {
        const today = new Date();
        today.setHours(0,0,0,0);
        return Yup.object().shape({
            date: Yup.date().test('minimum-date', t('nilai paling lambat adalah hari ini'), function (value) {
                return moment(value).isSameOrAfter(today);
            }).required().label(t('tanggal')),
            start: Yup.string()
                .required()
                .matches(/([0-1][0-9]|2[0-3]):[0-5][0-9]/, t('invalid time'))
                .test('after-now', t('time must be after now'), function (value) {
                    const isToday = this.parent.date.getDate() === today.getDate() && this.parent.date.getMonth() === today.getMonth() && this.parent.date.getFullYear() === today.getFullYear();
                    if (isToday) {
                        return moment(value, "HH:mm").isSameOrAfter(moment());
                    }
                    return true;
                })
                .label(t('waktu mulai')),
            end: Yup.string()
                .required()
                .matches(/([0-1][0-9]|2[0-3]):[0-5][0-9]/, {message: t('invalid time'), excludeEmptyString: true})
                .test('is-greater', t('time should be greater than start time'), function (value) {
                    const momentVal = moment(value, "HH:mm");
                    const momentStart = moment(this.parent.start, "HH:mm");
                    return momentVal.isSameOrAfter(momentStart);
                })
                .label(t('waktu berakhir')),
            message: Yup.string().required().label(t('pesan'))
        })
    }, [t]);

    const { values, touched, errors, isSubmitting, ...formik} = useFormik({
        initialValues: {
            date: new Date(),
            start: '',
            end: '',
            message: ''
        },
        validationSchema: CreateOvertimeSchema,
        onSubmit: (values, {setSubmitting, setErrors}) => {
            setSubmitting(true)
            setModalLoading(true)
            request.post('v1/overtimes/history', {...values, date: formatDate(values.date)})
                .then(res => {
                    onCreated(res.data.data);
                    toggle();
                    toast.success(t('Permintaan lembur berhasil dibuat'));
                })
                .catch(err => {
                    if (err.response?.status === 422) {
                        setErrors(err.response.data.errors);
                        return;
                    }
                    Promise.reject(err);
                })
                .finally(() => {
                    setSubmitting(false)
                    setModalLoading(false)
                });
        }
    })

    const onDatepickerChange = function (val) {
        formik.setFieldTouched('date', true);
        formik.setFieldValue('date', val instanceof Date && !isNaN(val) ? val : undefined, true);
    };
    const onDatepickerClear = React.useCallback(function () {
        formik.setFieldTouched('date', true);
        formik.setFieldValue('date', undefined, true);
    }, [formik]);

    return <Modal isOpen={isOpen} toggle={toggle}>
        <Form onSubmit={formik.handleSubmit}>
            <ModalBody>
                <Row form>
                    <Col xs="12">
                        <FormGroup>
                            <Label htmlFor="overtimeDate" className="input-label">{t('tanggal')} <span className="required">*</span></Label>
                            <DatePickerInput
                                onClear={onDatepickerClear}
                                showOnInputClick={true}
                                closeOnClickOutside={true}
                                onChange={onDatepickerChange}
                                minDate={new Date()}
                                value={values.date}
                                className={'my-custom-datepicker-component' + (touched.date && errors.date ? ' is-invalid' : '')}
                                name="date"
                                id="overtimeDate"
                                displayFormat="DD MMMM YYYY"
                                readOnly
                            />
                            {/* {console.log(touched.date, errors.date)} */}
                            {touched.date && errors.date && <small className="text-danger">{t(errors.date)}</small>}
                        </FormGroup>
                    </Col>
                    <Col xs="6">
                        <FormGroup>
                            <Label htmlFor="overtimeStart" className="input-label">{t('Jam mulai')} <span className="required">*</span></Label>
                            <ReactInputMask className="form-control" name="start" id="overtimeStart" mask="99:99" value={values.start} onChange={formik.handleChange} onBlur={formik.handleBlur} placeholder="__:__">
                                {(inputProps) => <Input {...inputProps} invalid={Boolean(touched.start && errors.start)} />}
                            </ReactInputMask>
                            {touched.start && errors.start && <FormFeedback>{errors.start}</FormFeedback>}
                        </FormGroup>
                    </Col>
                    <Col xs="6">
                        <FormGroup>
                            <Label htmlFor="overtimeEnd" className="input-label">{t('Jam berakhir')} <span className="required">*</span></Label>
                            <ReactInputMask mask="99:99" value={values.end} name="end" onChange={formik.handleChange} onBlur={formik.handleBlur}>
                                {(inputProps) => <Input {...inputProps} id="overtimeEnd"  className="form-control" invalid={Boolean(touched.end && errors.end)} placeholder="__:__" />}
                            </ReactInputMask>
                            {touched.end && errors.end && <FormFeedback>{errors.end}</FormFeedback>}
                        </FormGroup>
                    </Col>
                    <Col xs="12">
                        <FormGroup>
                            <Label htmlFor="overtimeMessage" className="input-label">{t('pesan')} <span className="required">*</span></Label>
                            <Input invalid={Boolean(touched.message && errors.message)} type="textarea" value={values.message} onChange={formik.handleChange} onBlur={formik.handleBlur} name="message" placeholder={t('Tulis tujuan lembur Anda')} />
                            {touched.message && errors.message && <FormFeedback>{errors.message}</FormFeedback>}
                        </FormGroup>
                    </Col>
                </Row>
                <div className="d-flex mt-4 justify-content-end">
                    <Button type="submit" color="netis-color" disabled={modalLoading}>
                        {modalLoading ? <><Spinner color="light" size="sm" /> {t('Loading...')}</> : t('simpan') }
                    </Button>
                </div>
            </ModalBody>
        </Form>
    </Modal>
})

export default OvertimeHistory;
