import React from 'react';

interface CapturedDataPosition {
    latitude?: number,
    longitude?: number
}

interface CustomWebcamCapturedData {
    imageUri: string,
    position: CapturedDataPosition
}

interface CustomWebcamSetState<T> {
    (state: T)
}

interface ResultState {
    isSuccess: boolean,
    title: string,
    message: string,
    onDone: Function,
    previewImage: string
}
interface ErrorState {
    message: string,
    previewImage: string
}

interface CustomWebcamFunc {
    setLoading: CustomWebcamSetState<boolean>,
    setResult: CustomWebcamSetState<ResultState>,
    setError: CustomWebcamSetState<ErrorState>,
    reset: Function,
}

interface CustomWebcamOnCapturedProps {
    (data: CustomWebcamCapturedData, functions: CustomWebcamFunc)
}

export interface CustomWebcamProps {
    onCaptured: CustomWebcamOnCapturedProps,
    trackLocation?: boolean
}

declare const CustomWebcam: React.FunctionComponent<CustomWebcamProps>

export default CustomWebcam;
