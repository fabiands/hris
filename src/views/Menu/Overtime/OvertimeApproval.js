import React, { useState, useEffect, Fragment, useCallback, useMemo } from "react";
import { formatDate } from "../../../utils/formatter";
import request from "../../../utils/request";
import { Form, Row, Col, Label, Button, Spinner, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, FormGroup, ModalFooter, ModalHeader, Modal, ModalBody, Input, UncontrolledPopover, PopoverBody } from "reactstrap";
import {InputGroup, InputGroupAddon, InputGroupText} from 'reactstrap'
import { t } from 'react-switch-lang';
import LoadingAnimation from "../../../components/LoadingAnimation";
import DataNotFound from "../../../components/DataNotFound";
import StatusBadge from "./StatusBadge";
import * as moment from 'moment';
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import useFilterDate from "./filterDateHooks";
import { toast } from "react-toastify";
import { useUserPrivileges } from "../../../store";
import usePagination from '../../../hooks/usePagination'; 
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from "../../../components/ResponsiveTable";
import Select from 'react-select'

const defaultDateFilterStart = new Date();
const twoWeekAgo = defaultDateFilterStart.getDate() - 14;
defaultDateFilterStart.setDate(twoWeekAgo);

const defaultDateFilterEnd = new Date();
const twoWeekLater = defaultDateFilterEnd.getDate() + 14;
defaultDateFilterEnd.setDate(twoWeekLater)

function OvertimeApproval(props) {
    const {can} = useUserPrivileges();
    const { isAdminPanel } = props;
    const searchParams = new URLSearchParams(props.location.search);
    const [filters, { DateStartPickerComponent, DateEndPickerComponent }] = useFilterDate({
        start: searchParams.get('start') || defaultDateFilterStart,
        end: searchParams.get('end') || defaultDateFilterEnd
    })

    const [loading, setLoading] = useState(true);
    const [overtimes, setOvertimes] = useState([]);
    const [updateModal, setUpdateModal] = useState(null);
    const [downloadingExcel, setdownloadingExcel] = useState(false);
    const [units, setUnits] = useState([])
    const [search, setSearch] = useState(null)
    const [filterUnit, setFilterUnit] = useState([])
    const [filterPage, setFilterPage] = useState([])

    const fetchOvertimes = () => {
        const params = Object.keys(filters)
            .filter(k => filters[k] !== null)
            .reduce((initial, k) => ({ ...initial, [k]: formatDate(filters[k]) }), {});
        setLoading(true);
        const url = isAdminPanel ? 'v1/overtimes/admin/requests' : 'v1/overtimes/requests';
        return request.get(url, { params })
            .then(res => setOvertimes(res.data.data))
            .finally(() => setLoading(false));
    }

    const getUnits = () => {
        return request.get(`/v1/master/units`)
        .then((res) => setUnits(res.data.data))
        // .catch((err) => setNotFound(true))
    }

    useEffect(() => {
        getUnits()
        fetchOvertimes()
        setLoading(false)
        // eslint-disable-next-line
    },[])

    // useEffect(fetchOvertimes, []);

    function updateStatus(overtimeId, status) {
        setUpdateModal({ overtimeId, status });
    }

    function onUpdatedStatus(data) {
        const idx = overtimes.findIndex(overtime => overtime.id === data.id);
        overtimes[idx] = data;
        setOvertimes(overtimes);
    }

    function downloadExcel() {
        setdownloadingExcel(true)
        const params = Object.keys(filters)
            .filter(k => filters[k] !== null)
            .reduce((initial, k) => ({ ...initial, [k]: formatDate(filters[k]) }), {});

        request.get(`v1/overtimes/recaps/excel`, {
            params,
            responseType: 'arraybuffer',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(res => {
                const type = res.headers['content-type']
                const blob = new Blob([res.data], { type: type, encoding: 'UTF-8' })
                let filename = "rekap.xlsx";
                const disposition = res.headers['content-disposition'];
                if (disposition && disposition.indexOf('inline') !== -1) {
                    const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                    const matches = filenameRegex.exec(disposition);
                    if (matches != null && matches[1]) {
                        filename = matches[1].replace(/['"]/g, '');
                    }
                }

                const URL = window.URL || window.webkitURL;
                const downloadUrl = URL.createObjectURL(blob);
                let newWindow = null;

                const iOS = window.navigator.platform && /iPad|iPhone|iPod/.test(window.navigator.platform)
                if (iOS) {
                    const reader = new FileReader();
                    reader.onload = function (e) {
                        newWindow = window.open(reader.result);
                        newWindow.onload = function () {
                            newWindow.document.getElementsByTagName('html')[0]
                                .appendChild(document.createElement('head'))
                                .appendChild(document.createElement('title'))
                                .appendChild(document.createTextNode(filename));
                        }
                        setTimeout(() => {
                            newWindow.document.title = filename;
                        }, 100)
                    }
                    reader.readAsDataURL(blob);
                } else {
                    const link = document.createElement('a')
                    link.href = downloadUrl
                    link.download = filename;
                    link.click();
                    setTimeout(() => {
                        link.remove();
                    }, 1500);
                }
            })
            .catch(err => {
                console.error(err)
            }).finally(() => {
                setdownloadingExcel(false)
            })
    }
    const searchSpace = (e) => {
        setSearch(e.target.value)
    };
    const handleUnit = (e) => {
        let unit = e?.map(item => item.value) ?? []
        setFilterUnit(unit)
    }
    const filtered = useMemo(() => {
        let filterData = overtimes
            ?.filter(item =>
                search?.trim() ? item?.personnel?.user?.fullName?.toLowerCase().includes(search?.trim().toLowerCase()) : true
            )
            ?.filter(item =>
                filterUnit?.length > 0 ? filterUnit.includes(item?.personnel?.unit?.id) : true
            )
        return filterData;
    }, [overtimes, search, filterUnit])

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilterPage((state) => ({ ...state, pagination: page }));
        },
        [setFilterPage]
    );

    const { data: groupFiltered, PaginationComponent, currentPage } = usePagination(
        filtered,
        8,
        filterPage.pagination,
        handleChangeCurrentPage
    );

    return (
    <div className="animated fadeIn md-company-header mb-3">
        <h4 className="content-title mb-3">{t('Riwayat Lembur')}</h4>
        <Form onSubmit={(e) => { e.preventDefault(); fetchOvertimes() }}>
            <Row className="md-company-header mb-3">
                <Col xs="12" md="9">
                    <Row>
                        <Col xs="6">
                            <FormGroup>
                                <Label htmlFor="job" className="input-label">
                                {t("jabatan")}
                                </Label>
                                <Select
                                    disabled={units.length < 1}
                                    isMulti={true}
                                    isSearchable={false}
                                    name="job"
                                    options={units.map(item => ({ label: item.name, value: item.id }))}
                                    placeholder={t("Jabatan")}
                                    onChange={handleUnit}
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="6">
                            <FormGroup style={{ marginBottom: 5 }}>
                                <Label htmlFor="unit" className="input-label">
                                {t('Unit')}
                                </Label>
                                <Select
                                    disabled={units.length < 1}
                                    isMulti={true}
                                    isSearchable={false}
                                    name="unit"
                                    options={units.map(item => ({ label: item.name, value: item.id }))}
                                    placeholder={t("Unit")}
                                    onChange={handleUnit}
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="6" xl="4" >
                            <FormGroup>
                                <Label htmlFor="start" className="input-label">{t('dari')}</Label>
                                <DateStartPickerComponent />
                            </FormGroup>
                        </Col>
                        <Col xs="6" xl="4" >
                            <FormGroup>
                                <Label htmlFor="end" className="input-label">{t('hingga')}</Label>
                                <DateEndPickerComponent />
                            </FormGroup>
                        </Col>
                        <Col xs="12" xl="4" className="mt-xl-1 pt-xl-4 mb-2" >
                            <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText className="input-group-transparent">
                                        <i className="fa fa-search"></i>
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input type="text" placeholder={t('cari') + ' ' + t('nama')} className="input-search"
                                onChange={searchSpace}
                                />
                            </InputGroup>
                        </Col>
                    </Row>
                </Col>
                <Col xs="12" md="3">
                    <Row>
                        <Col sm="6" md="12">
                            <div className="pt-sm-2 my-2 mt-md-0">
                                <Button type="submit" className="mt-sm-4 w-100 py-2" color="netis-color" disabled={loading}>
                                    {loading ? <Spinner color="light" size="sm" /> : 'Filter'}
                                </Button>
                            </div>
                        </Col>
                        {can('download-attendance') && <Col sm="6" md="12" className="text-right">
                            <div className="pt-sm-2 my-2">
                                <Button onClick={downloadExcel} className="mt-sm-4 w-100" color="success">
                                    {downloadingExcel ? <Fragment><Spinner size="sm" /> Downloading...</Fragment> : <Fragment><i className="fa fa-download mr-2"></i> {t('unduhlaporan')}</Fragment>}
                                </Button>
                            </div>
                        </Col>}
                    </Row>
                </Col>
            </Row>
        </Form>
        <Row className="md-company-header mb-3">
            <Col xs="12">
                {
                    loading ? <LoadingAnimation /> :
                        (groupFiltered?.length === 0 ? <DataNotFound /> :
                            <Rtable size="sm">
                                <Rthead>
                                    <Rtr>
                                        <Rth className="text-center w-5">No.</Rth>
                                        <Rth className="text-left w-15">{t('nama')}</Rth>
                                        <Rth className="text-left w-15">{t('tanggal')}</Rth>
                                        <Rth className="text-center w-5">{t('mulai')}</Rth>
                                        <Rth className="text-center w-5">{t('hingga')}</Rth>
                                        <Rth className="text-left w-20">Agenda</Rth>
                                        <Rth>Status</Rth>
                                        <Rth></Rth>
                                    </Rtr>
                                </Rthead>
                                <Rtbody>
                                    {groupFiltered?.map((data, index) => (
                                        <Rtr key={index + 1 + (8*currentPage)}>
                                            <Rtd className="text-md-center">{index + 1 + (8*currentPage)}</Rtd>
                                            <Rtd>
                                                {data.personnel.user.fullName}<br />
                                                <small><i>{data.personnel.unit.name}</i></small>
                                            </Rtd>
                                            <Rtd className="text-nowrap">{moment(data.date).format('DD MMMM YYYY')}</Rtd>
                                            <Rtd className="text-md-center">{data.start.substr(0, 5)}</Rtd>
                                            <Rtd className="text-md-center">{data.end ? data.end.substr(0, 5) : <i className="text-muted">NOT SET</i>}</Rtd>
                                            <Rtd style={{ cursor: 'pointer' }}>
                                                <span id={`overtimePurpose${index}`}>{data.message.substr(0, 20) + (data.message.length > 20 ? '...' : '')}</span>
                                                <UncontrolledPopover placement="top" target={`overtimePurpose${index}`}>
                                                    <PopoverBody style={{ whiteSpace: 'pre-wrap' }}>{data.message}</PopoverBody>
                                                </UncontrolledPopover>
                                            </Rtd>
                                            <Rtd className="text-left text-nowrap"><StatusBadge status={data.status} /></Rtd>
                                            <Rtd className="text-md-right text-nowrap">
                                                {!['done', 'approved2', 'rejected'].includes(data.status) && (<UncontrolledDropdown className="d-inline-block">
                                                    <DropdownToggle color="netis-color" caret>{t('ubah')} Status</DropdownToggle>
                                                    <DropdownMenu>
                                                        {(data.status === 'pending' || (can('verify-overtime') && data.status === 'approved1')) && <DropdownItem onClick={() => updateStatus(data.id, 'approved')}>
                                                            <i className="fa fa-circle text-success mr-1"></i> {t('disetujui')}
                                                        </DropdownItem>}
                                                        <DropdownItem onClick={() => updateStatus(data.id, 'rejected')}>
                                                            <i className="fa fa-circle text-danger mr-1"></i> {t('ditolak')}
                                                        </DropdownItem>
                                                    </DropdownMenu>
                                                </UncontrolledDropdown>)}
                                                <Link className="btn btn-netis-color mx-1" to={`/overtimes/manage/${data.id}`}>{t('detail')}</Link>
                                            </Rtd>
                                        </Rtr>
                                    ))}
                                </Rtbody>
                            </Rtable>
                        )
                }
                {filtered?.length > 8 && <PaginationComponent />}
            </Col>
        </Row>

        <UpdateStatusModalForm toggle={() => setUpdateModal(null)} payload={updateModal} onUpdated={onUpdatedStatus} />
    </div>)
}

function UpdateStatusModalForm({ toggle, payload, onUpdated }) {
    const [message, setMessage] = useState('');
    const [loading, setLoading] = useState(false);
    useEffect(() => {
        setMessage('')
    }, [payload])

    function submitForm(e) {
        e.preventDefault();
        setLoading(true);
        request.post(`v1/overtimes/requests/${payload.overtimeId}`, { status: payload.status, message })
            .then(res => {
                const { data } = res.data
                onUpdated(data);
                if (data.status === 'approved2') {
                    toast.success(t("Permintaan lembur berhasil disetujui"));
                } else {
                    toast.success(t("Permintaan lembur berhasil ditolak"));
                }
                toggle();
            })
            .catch((err) => {
                if(err.response?.status === 403){
                    toast.error(t('Menunggu Persetujuan Atasan'))
                }
                else{
                    toast.error(err.response?.data.message ?? 'Terjadi kesalahan');
                }
                setLoading(false);
                toggle();
            })
            .finally(() => setLoading(false))
    }

    return (
        <Modal isOpen={!!payload} toggle={() => !loading && toggle()}>
            {!!payload && <Form onSubmit={submitForm}>
                <ModalHeader toggle={() => !loading && toggle()}>Update Status</ModalHeader>
                <ModalBody>
                    <FormGroup row>
                        <Label htmlFor="status" className="text-md-right" md="3">Status</Label>
                        <Col md="9">
                            <Input type="select" name="status" id="status" value={payload.status} disabled>
                                <option value="approved">{t('disetujui')}</option>
                                <option value="rejected">{t('ditolak')}</option>
                            </Input>
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label htmlFor="message" className="text-md-right" md="3">{t('pesan')}</Label>
                        <Col md="9">
                            <Input type="textarea" name="message" id="message" rows="2" value={message} onChange={(e) => setMessage(e.target.value)} />
                        </Col>
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                    <Button type="submit" color="netis-color" disabled={loading}>
                        {
                            loading ?
                                <><Spinner size="sm" color="light" /> Loading...</> :
                                <><i className="fa fa-check mr-2"></i> Update Status</>
                        }
                    </Button>
                </ModalFooter>
            </Form>}
        </Modal>
    );
}

const mapStateToProps = ({ isAdminPanel, user }) => ({ isAdminPanel, user });
export default connect(mapStateToProps)(OvertimeApproval);
