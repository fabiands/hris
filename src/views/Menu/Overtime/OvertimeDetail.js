import React, { useState, useEffect, useMemo, useCallback } from "react";
import request from "../../../utils/request";

import {
  Button, Card, CardBody, Table, Row, Col, CardHeader, Badge, FormFeedback,
  UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem,
  Form, FormGroup, Label, Spinner, Input, Modal, ModalHeader, ModalBody, ModalFooter,
} from "reactstrap";
import { formatDate } from "../../../utils/formatter";
import { useFormik } from "formik";
import { DatePickerInput } from "rc-datepicker";
import ReactInputMask from "react-input-mask";
import * as moment from "moment";
import * as Yup from "yup";

// import moment from '../../../utils/moment'
import StatusBadge from "./StatusBadge";
import Loading from "../../../components/LoadingAnimation";
import DataNotFound from "../../../components/DataNotFound";
import { t } from "react-switch-lang";
import GoogleMapReact from "google-map-react";
import MapMarker from "../Personnel/component/atom/MapMarker";
import { useRouteMatch } from "react-router-dom";
import { toast } from "react-toastify";
import { useUserPrivileges } from "../../../store";
import { connect } from "react-redux";

const logMessage = {
  pending: t('Permintaan dibuat'),
  approved1: t('Disetujui oleh Atasan'),
  approved2: t('Disetujui oleh Admin'),
  rejected: t('Permintaan ditolak')
}

function OvertimeDetail(props) {
  const { isAdminPanel } = props;
  const [loading, setLoading] = useState(true);
  const [notFound, setNotFound] = useState(false);
  const [data, setData] = useState({});
  const [showFaceImage, setShowFaceImage] = useState(null);
  const [showMapLocation, setShowMapLocation] = useState(null);
  const matchRoute = useRouteMatch();
  const [editDetail, setEditDetail] = useState(false);
  const [updateModal, setUpdateModal] = useState(null);
  const [editLoading, setEditLoading] = useState(false);
  const { can } = useUserPrivileges();
  let canEdit = can('edit-overtime');

  if (!isAdminPanel) {
    canEdit = true;
  }

  const CreateOvertimeSchema = useMemo(() => {
    const today = new Date();
    return Yup.object().shape({
      date: Yup.date()
        .test(
          "minimum-date",
          t("nilai paling lambat adalah hari ini"),
          function (value) {
            return (
              value.getDate() >= today.getDate() &&
              value.getMonth() >= today.getMonth() &&
              value.getFullYear() >= today.getFullYear()
            );
          }
        )
        .required()
        .label(t("tanggal")),
      start: Yup.string()
        .required()
        .matches(/([0-1][0-9]|2[0-3]):[0-5][0-9]/, t("invalid time"))
        .test("after-now", t("time must be after now"), function (value) {
          const isToday =
            this.parent.date.getDate() === today.getDate() &&
            this.parent.date.getMonth() === today.getMonth() &&
            this.parent.date.getFullYear() === today.getFullYear();
          if (isToday) {
            return moment(value, "HH:mm").isSameOrAfter(moment());
          }
          return true;
        })
        .label(t("waktu mulai")),
      end: Yup.string()
        .required()
        .matches(/([0-1][0-9]|2[0-3]):[0-5][0-9]/, {
          message: t("invalid time"),
          excludeEmptyString: true,
        })
        .test(
          "is-greater",
          t("time should be greater than start time"),
          function (value) {
            const momentVal = moment(value, "HH:mm");
            const momentStart = moment(this.parent.start, "HH:mm");
            return momentVal.isSameOrAfter(momentStart);
          }
        )
        .label(t("waktu berakhir")),
      message: Yup.string().required().label(t("pesan")),
    });
  }, []);

  const {
    values,
    touched,
    errors,
    isSubmitting,
    setValues,
    ...formik
  } = useFormik({
    initialValues: {
      date: new Date(),
      start: "",
      end: "",
      message: "",
    },
    validationSchema: CreateOvertimeSchema,
    onSubmit: (values, { setSubmitting, setErrors }) => {
      if (!canEdit) {
        toast.error(t('Maaf, Anda tidak boleh merubah data'));
        return
      }
      setSubmitting(true);
      setEditLoading(true);
      request
        .put(`v1/overtimes/requests/${matchRoute.params.id}`, {
          start: values.start.substr(0, 5),
          end: values.end.substr(0, 5),
          message: values.message,
          date: formatDate(values.date),
        })
        .then((res) => {
          setEditLoading(false);
          setEditDetail(!editDetail);
          toast.success(t('Permintaan lembur berhasil diubah'));
        })
        .catch((err) => {
          setEditLoading(false);
          if (err.response?.status === 422) {
            setErrors(err.response.data.errors);
            return;
          }
          Promise.reject(err);
        })
        .finally(() => setSubmitting(false));
    },
  });

  const onDatepickerChange = function (val) {
    formik.setFieldValue(
      "date",
      val instanceof Date && !isNaN(val) ? val : undefined
    );
    formik.setFieldTouched("date", true);
  };
  const onDatepickerClear = React.useCallback(
    function () {
      formik.setFieldValue("date", undefined);
      formik.setFieldTouched("date", true);
    },
    [formik]
  );

  useEffect(() => {
    request
      .get(`v1/overtimes/requests/${matchRoute.params.id}`)
      .then((response) => {
        setData(response.data.data);
        var data = response.data.data;
        setValues({
          date: data.date,
          start: data.start,
          end: data.end,
          message: data.message,
        });
      })
      .catch((err) => {
        if (err.response && err.response.status === 404) {
          setNotFound(true);
        } else {
          toast.error(t('terjadikesalahan'));
          setNotFound(true);
        }
      })
      .finally(() => setLoading(false));
  }, [matchRoute.params.id, setValues]);

  const cancelEditDetail = () => {
    setValues({
      date: data.date,
      start: data.start,
      end: data.end,
      message: data.message,
    });
    setEditDetail(!editDetail);
  };

  const updateStatus = (dataId, status) => {
    setUpdateModal({ dataId, status });
  };

  const onUpdatedStatus = (data) => {
    setData(data);
  };

  const handleBackButton = useCallback(() => {
    const url = props.location.pathname;

    return props.history.push(url.slice(0, url.lastIndexOf('/')));
  }, [props.location, props.history]);

  if (loading) {
    return <Loading />;
  }

  if (notFound) {
    return <DataNotFound />;
  }

  return (
    <div className="animated fadeIn">
      <div className="d-flex bd-highlight mb-3">
        <div className="mr-auto bd-highlight mt-2">
          <button className="btn btn-netis-color" onClick={handleBackButton}>
            <i className="fa fa-chevron-left mr-2"></i> {t("kembali")}
          </button>
        </div>
        {(function () {
          if (data.status === "pending") {
            return (
              <div className="d-flex bd-highlight mb-3">
                <div className="p-2 bd-highlight">
                  <UncontrolledDropdown className="d-inline-block">
                    <DropdownToggle className={`${can('verify-overtime') ? '' : ' d-none'}`} color="netis-color" caret>
                      {t("ubah")} Status
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem
                        onClick={() => updateStatus(data.id, "approved")}
                      >
                        <i className="fa fa-circle text-success mr-1"></i>{" "}
                        {t("disetujui")}
                      </DropdownItem>
                      <DropdownItem
                        onClick={() => updateStatus(data.id, "rejected")}
                      >
                        <i className="fa fa-circle text-danger mr-1"></i>{" "}
                        {t("ditolak")}
                      </DropdownItem>
                    </DropdownMenu>
                  </UncontrolledDropdown>
                </div>
                <div className={`p-2 bd-highlight ${canEdit ? '' : ' d-none'}`}>
                  {!editDetail ? (
                    <Button
                      className="btn-netis-color"
                      onClick={() => setEditDetail(!editDetail)}
                    >
                      <i className="fa fa-pencil mr-2"></i>Edit
                    </Button>
                  ) : (
                      <Button
                        className="btn-netis-danger"
                        onClick={cancelEditDetail}
                      >
                        <i className="fa fa-times"></i> {t('batal')}
                      </Button>
                    )}
                </div>
              </div>
            );
          }
        })()}
      </div>
      <UpdateStatusModalForm
        toggle={() => setUpdateModal(null)}
        payload={updateModal}
        onUpdated={onUpdatedStatus}
      />
      <Card>
        <CardBody>
          <Form onSubmit={formik.handleSubmit}>
            <Row>
              <Col md="8">
                <Table className="table table-hover">
                  <tbody>
                    <tr>
                      <th scope="row">{t("tanggal")}</th>
                      <td colSpan="3">
                        <DatePickerInput
                          onClear={onDatepickerClear}
                          showOnInputClick={true}
                          closeOnClickOutside={true}
                          onChange={onDatepickerChange}
                          minDate={new Date()}
                          value={values.date}
                          className={'my-custom-datepicker-component' + (touched.date && errors.date ? ' is-invalid' : '')}
                          name="date"
                          id="overtimeDate"
                          displayFormat="DD MMMM YYYY"
                          readOnly
                          disabled={!editDetail}
                        />
                        {touched.date && errors.date && (
                          <small className="text-danger">{errors.date}</small>
                        )}
                      </td>
                    </tr>

                    <tr>
                      <th scope="row">{t("Pukul")}</th>
                      <td>
                        <ReactInputMask
                          className="form-control"
                          name="start"
                          id="overtimeStart"
                          mask="99:99"
                          value={values.start}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          placeholder="__:__"
                          disabled={!editDetail}
                        >
                          {(inputProps) => (
                            <Input
                              {...inputProps}
                              invalid={Boolean(touched.start && errors.start)}
                              disabled={!editDetail}
                            />
                          )}
                        </ReactInputMask>
                        {touched.start && errors.start && (
                          <FormFeedback>{errors.start}</FormFeedback>
                        )}
                      </td>
                      <td>
                        <b>-</b>
                      </td>
                      <td>
                        <ReactInputMask
                          className="form-control"
                          name="end"
                          id="overtimeEnd"
                          mask="99:99"
                          value={values.end}
                          onChange={formik.handleChange}
                          onBlur={formik.handleBlur}
                          placeholder="__:__"
                          disabled={!editDetail}
                        >
                          {(inputProps) => (
                            <Input
                              {...inputProps}
                              invalid={Boolean(touched.end && errors.end)}
                              disabled={!editDetail}
                            />
                          )}
                        </ReactInputMask>
                        {touched.end && errors.end && (
                          <FormFeedback>{errors.end}</FormFeedback>
                        )}
                      </td>
                    </tr>

                    <tr>
                      <th scope="row">Status</th>
                      <td>
                        <StatusBadge status={data.status} />
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </Col>
              <Col md="4">
                <strong className="mb-4">{t('deskripsi')}:</strong>
                <br />
                <Input
                  type="textarea"
                  value={values.message}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  name="message"
                  rows="5"
                  disabled={!editDetail}
                  invalid={Boolean(touched.message && errors.message)}
                />
                {touched.message && errors.message && (
                  <FormFeedback>{errors.message}</FormFeedback>
                )}
              </Col>
              <div className="ml-4">
                {editDetail ? (
                  <>
                    <Button
                      type="submit"
                      color="netis-color"
                      disabled={editLoading}
                      className={`${canEdit ? '' : ' d-none'}`}
                    >
                      {editLoading ? (
                        <>
                          <Spinner size="sm" color="light" /> Loading...
                        </>
                      ) : (
                          <>
                            <i className="fa fa-check mr-2"></i> {t('simpan')}
                        </>
                        )}
                    </Button>
                  </>
                ) : null}
              </div>
            </Row>
          </Form>
        </CardBody>
      </Card>

      {(data.startPah || data.endPah) && (
        <Row>
          <Col sm="6">
            {data.startPah && (
              <Card>
                <CardHeader>
                  <i className="fa fa-sign-in mr-2"></i> {t("overtimeIn")}
                </CardHeader>
                <CardBody>
                  <table className="table">
                    <tbody>
                      <tr>
                        <th style={{ width: 70 }}>{t('Pukul')}</th>
                        <td>{data.startPah.time?.substr(0, 5)}</td>
                      </tr>
                      <tr>
                        <th style={{ width: 70 }}>{t('wajah')}</th>
                        <td className="d-flex">
                          <div className="mr-auto">
                            {data.startPah.FaceIsMatch ? (
                              <Badge color="success">{t("sesuai")}</Badge>
                            ) : (
                                <Badge color="danger">{t("tidaksesuai")}</Badge>
                              )}
                          </div>
                          <div className="ml-auto">
                            <Button
                              size="sm"
                              color="link"
                              className="text-netis-primary"
                              onClick={() =>
                                setShowFaceImage(data.startPah.faceFile)
                              }
                            >
                              <i className="fa fa-eye mr-1"></i>{" "}
                              {t("Tampilkan")}
                            </Button>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th style={{ width: 70 }}>{t('lokasi')}</th>
                        <td className="d-flex">
                          {data.startPah.loc.name || (
                            <i className="text-muted">{t("tidak ditemukan")}</i>
                          )}
                          <div className="ml-auto">
                            <Button
                              size="sm"
                              color="link"
                              className="text-netis-primary"
                              onClick={() =>
                                setShowMapLocation({
                                  name: data.startPah?.loc?.Name,
                                  latitude: data.startPah?.latitude,
                                  longitude: data.startPah?.longitude,
                                })
                              }
                            >
                              <i className="fa fa-eye mr-1"></i>{" "}
                              {t("Tampilkan")}
                            </Button>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </CardBody>
              </Card>
            )}
          </Col>
          <Col sm="6">
            {data.endPah && (
              <Card>
                <CardHeader>
                  <i className="fa fa-sign-out mr-2"></i> {t("overtimeOut")}
                </CardHeader>
                <CardBody>
                  <table className="table">
                    <tbody>
                      <tr>
                        <th style={{ width: 70 }}>{t('Pukul')}</th>
                        <td>{data.endPah.time?.substr(0, 5)}</td>
                      </tr>
                      <tr>
                        <th style={{ width: 70 }}>{t('wajah')}</th>
                        <td className="d-flex">
                          <div className="mr-auto">
                            {data.endPah.FaceIsMatch ? (
                              <Badge color="success">{t("sesuai")}</Badge>
                            ) : (
                                <Badge color="danger">{t("tidaksesuai")}</Badge>
                              )}
                          </div>
                          <div className="ml-auto">
                            <Button
                              size="sm"
                              color="link"
                              className="text-netis-primary"
                              onClick={() =>
                                setShowFaceImage(data.endPah.faceFile)
                              }
                            >
                              <i className="fa fa-eye mr-1"></i>{" "}
                              {t("Tampilkan")}
                            </Button>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <th style={{ width: 70 }}>{t('lokasi')}</th>
                        <td className="d-flex">
                          {data.endPah.loc.name || (
                            <i className="text-muted">{t("tidak ditemukan")}</i>
                          )}
                          <div className="ml-auto">
                            <Button
                              size="sm"
                              color="link"
                              className="text-netis-primary"
                              onClick={() =>
                                setShowMapLocation({
                                  name: data.endPah?.loc?.Name,
                                  latitude: data.endPah?.latitude,
                                  longitude: data.endPah?.longitude,
                                })
                              }
                            >
                              <i className="fa fa-eye mr-1"></i>{" "}
                              {t("Tampilkan")}
                            </Button>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </CardBody>
              </Card>
            )}
          </Col>
        </Row>
      )}
      <Card>
        <CardHeader>{t('Riwayat Status')}</CardHeader>
        <CardBody>
          <Table>
            <tbody>
              {data.status_log &&
                data.status_log.map((statusLog, index) => (
                  <tr key={index}>
                    <td style={{ width: 170 }}>
                      <strong>
                        {(statusLog.subject && statusLog.subject.nickName) ||
                          "system"}
                      </strong>
                      <br />
                      <span className="text-muted">{statusLog.created}</span>
                    </td>
                    <td>
                      <strong>
                        <StatusBadge status={statusLog.status} />
                      </strong>
                      <br /> {logMessage[statusLog.status]}
                    </td>
                  </tr>
                ))}
            </tbody>
          </Table>
        </CardBody>
      </Card>
      <Modal
        isOpen={Boolean(showFaceImage)}
        toggle={() => setShowFaceImage(null)}
      >
        <ModalBody>
          {Boolean(showFaceImage) && (
            <img src={showFaceImage} alt="face" width="100%" />
          )}
        </ModalBody>
      </Modal>
      <Modal
        isOpen={Boolean(showMapLocation)}
        toggle={() => setShowMapLocation(null)}
      >
        <ModalBody>
          <div style={{ height: 350, width: "100%" }}>
            {Boolean(showMapLocation) && (
              <GoogleMapReact
                bootstrapURLKeys={{
                  key: "AIzaSyDQsNCd2Trmf4MLwcB7k1oqpWZPpTeCkc0",
                }}
                center={{
                  lat: parseFloat(showMapLocation.latitude),
                  lng: parseFloat(showMapLocation.longitude),
                }}
                defaultZoom={17}
              >
                <MapMarker
                  lat={showMapLocation.latitude}
                  lng={showMapLocation.longitude}
                  text={showMapLocation.name}
                />
              </GoogleMapReact>
            )}
          </div>
        </ModalBody>
      </Modal>
    </div>
  );
}

function UpdateStatusModalForm({ toggle, payload, onUpdated }) {
  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setMessage("");
  }, [payload]);

  function submitForm(e) {
    e.preventDefault();
    setLoading(true);
    request.post(`v1/overtimes/requests/${payload.dataId}`, { status: payload.status, message })
      .then(res => {
        const { data } = res.data
        if (data.status === 'approved2') {
          toast.success(t("Permintaan lembur berhasil disetujui"));
        } else {
          toast.success(t("Permintaan lembur berhasil ditolak"));
        }
        onUpdated(data);
        toggle();
      })
      .finally(() => setLoading(false));
  }

  return (
    <Modal isOpen={!!payload} toggle={() => !loading && toggle()}>
      {!!payload && (
        <Form onSubmit={submitForm}>
          <ModalHeader toggle={() => !loading && toggle()}>
            Update Status
          </ModalHeader>
          <ModalBody>
            <FormGroup row>
              <Label htmlFor="status" className="text-md-right" md="3">
                Status
              </Label>
              <Col md="9">
                <Input readOnly type="text" value={payload.status} />
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label htmlFor="message" className="text-md-right" md="3">
                {t('pesan')}
              </Label>
              <Col md="9">
                <Input
                  type="textarea"
                  name="message"
                  id="message"
                  rows="2"
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                />
              </Col>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button type="submit" color="netis-color" disabled={loading}>
              {loading ? (
                <>
                  <Spinner size="sm" color="light" /> Loading...
                </>
              ) : (
                  <>
                    <i className="fa fa-check mr-2"></i> Update Status
                </>
                )}
            </Button>
          </ModalFooter>
        </Form>
      )}
    </Modal>
  );
}

const mapStateToProps = ({ isAdminPanel }) => ({ isAdminPanel });
export default connect(mapStateToProps)(OvertimeDetail);
