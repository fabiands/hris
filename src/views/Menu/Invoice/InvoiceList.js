import React, { useMemo, useCallback, useState } from 'react'
import {Card, CardHeader, CardBody, Badge} from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import moment from 'moment'
import { translate, t } from 'react-switch-lang'
import useSWR from 'swr'
import LoadingAnimation from '../../../components/LoadingAnimation'
import langUtils from "../../../utils/language/index";
import { convertNumber } from "../../../utils/formatter";
import usePagination from '../../../hooks/usePagination';

const statusBadge = {
    pending: 'warning',
    expired: 'danger',
    paid: 'success'
}

function InvoiceList() {
    const [filters, setFilters] = useState([])
    const { data: response, error } = useSWR('v1/invoice')
    const loading = !response && !error;
    const statusText = {
        pending: t('Belum Terbayar'),
        expired: t('Kadaluarsa'),
        paid: t('Lunas')
    }
    const invoiceData = useMemo(() => {
        if (response) {
            return response.data.data;
        }
        return [];
    }, [response]);

    const handleClick = (url, status) => {
        if(status === 'pending'){
            window.open(url)
        }
    }
    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
    );

    const { data: groupFiltered, PaginationComponent } = usePagination(
        invoiceData,
        8,
        filters?.pagination,
        handleChangeCurrentPage
    );

    return (
        <div className="animated fadeIn" >
            <h4 className="content-title mb-4">{t('Daftar Tagihan')}</h4>
            <div className="content">
                {loading ? <LoadingAnimation /> : groupFiltered?.map((item) =>
                    <div key={item.paymentId} className={item.status === 'pending' ? 'card-link-invoice' : ''} onClick={() => handleClick(item.url, item.status)}>
                        <Card className="card-invoice">
                            <CardHeader className="d-flex justify-content-between bg-transparent">
                                <div>
                                    {moment(item.createdAt).locale(langUtils.getLanguage()).format("DD MMMM YYYY LT")}
                                </div>
                                <div>
                                    {item.status === "pending" ? (
                                        <small className="text-muted">
                                            {t("Bayar sebelum")}{" "}
                                            {moment(item.expiredAt).locale(langUtils.getLanguage()).format("DD MMMM YYYY LT")}
                                        </small>
                                    ) : null}
                                </div>
                            </CardHeader>
                            <CardBody>
                                <div className="d-flex align-items-center">
                                    <div className="flex-fill small ml-2">
                                        <div className="d-flex">
                                            <div>
                                                <span
                                                    style={{ fontSize: 15 }}
                                                    className="font-weight-bold mr-3 text-netis-primary"
                                                >
                                                    {item.description}
                                                </span>
                                                <br />
                                                <span style={{ fontSize: 14 }} className="mr-auto">
                                                    <FontAwesomeIcon
                                                        icon="money-bill-wave"
                                                        color="#137500"
                                                        className="mr-1"
                                                    />{" "}
                                                    {convertNumber(item.amount, "$ 0,0[.]00")}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='ml-auto d-flex align-items-center justify-content-center'>
                                        <Badge color={statusBadge[item.status]}>{statusText[item.status]}</Badge>
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </div>
                )}
                {invoiceData?.length > 8 && <PaginationComponent />}
            </div>
        </div>
    )
}

export default translate(InvoiceList)