import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import banner from '../../../assets/assets_ari/333.png';
// import leaf from '../../../assets/assets_ari/leaf.svg';
// import { FormGroup, Label, Input } from 'reactstrap';
// import axios from 'axios';

class AttendanceMenu extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dropdownOpen: [false, false],
        };
    }

    render() {
        return (
            <div className="animated fadeIn">
                <h4 className="content-title mb-4">Informasi Karyawan</h4>
                <div className="content-body">
                    <div className="row">
                        <div className="menu col-md-8">
                            <div className="row">
                                <div className="col-12 col-sm-4 col-lg-4">
                                    <Link to="/attendance/history">
                                        <div className="card menu-item">
                                            <div className="card-body">
                                                <div className="menu-img mt-2 mb-3">
                                                    <img src={require("../../../assets/assets_ari/his.png")} className="img-width" alt="" />
                                                </div>
                                                <div className="menu-title mb-2">
                                                    <p className="mb-0 title-menu-company">Histori<br />Presensi</p>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </div>
                                <div className="col-12 col-sm-4 col-lg-4">
                                    <Link to="/attendance/recap">
                                        <div className="card menu-item">
                                            <div className="card-body">
                                                <div className="menu-img mt-2 mb-3">
                                                    <img src={require("../../../assets/assets_ari/rek.png")} className="img-width" alt="" />
                                                </div>
                                                <div className="menu-title mb-2">
                                                    <p className="mb-0 title-menu-company">Rekapitulasi<br />Presensi</p>
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default AttendanceMenu;