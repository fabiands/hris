import React, { memo, useMemo, useState, useCallback } from 'react'
import { Button } from 'reactstrap';
import useSWR from 'swr'
import LoadingAnimation from '../../../../components/LoadingAnimation';
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../../components/ResponsiveTable';
import { arrayGroupBy, objectGet } from '../../../../utils/array';
import {translate, t} from 'react-switch-lang';
import usePagination from '../../../../hooks/usePagination';

function AttendanceHistoryToday({ attendances: attendanceHistories, day, search = '', seeDetail }) {
    const [filters, setFilters] = useState([])
    const { data: personnelsResponse, error: personnelsError } = useSWR('v1/personnels/all/aktif');
    const loading = !personnelsResponse && !personnelsError;
    const personnels = useMemo(() => {
        let data = personnelsResponse?.data?.data ?? []
        if (search.trim()) {
            data = data.filter(p => (p?.fullName ?? '').toLowerCase().includes(search.trim().toLowerCase()));
        }
        return data;
    }, [personnelsResponse, search]);
    
    const attendances = useMemo(() => {
        const attendanceGroupByType = arrayGroupBy(attendanceHistories, 'type');
        const attendanceGroupByTypeAndPersonnel = {};
        for (let type in attendanceGroupByType) {
            attendanceGroupByTypeAndPersonnel[type] = arrayGroupBy(attendanceGroupByType[type], 'personnelId');
        }
        return attendanceGroupByTypeAndPersonnel;
    }, [attendanceHistories]);

    const { data: personnelsWorkingShiftResponse } = useSWR(`v2/personnels/working-shifts?day=${day}`);
    const personnelsWorkingsShift = useMemo(() => {
        const data = (personnelsWorkingShiftResponse?.data?.data ?? []);
        return arrayGroupBy(data, (item) => item.personnelId);
    }, [personnelsWorkingShiftResponse]);

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
    );

    const { data: groupFiltered, PaginationComponent } = usePagination(
        personnels,
        8,
        filters?.pagination,
        handleChangeCurrentPage
    );

    if (loading) {
        return <LoadingAnimation/>
    }

    return (
        <>
        <Rtable style={{display:'table'}}>
            <Rthead>
                <Rtr>
                    <Rth>{t('nama')}</Rth>
                    <Rth>{t('Jadwal')}</Rth>
                    <Rth className="text-md-center">{t('Masuk')}</Rth>
                    <Rth className="text-md-center">{t('Keluar')}</Rth>
                </Rtr>
            </Rthead>
            <Rtbody>
                {groupFiltered?.map(personnel => (
                    <PersonnelsRow
                        key={personnel.id}
                        personnel={personnel} 
                        shift={personnelsWorkingsShift[personnel.id]}
                        clockIn={objectGet(attendances, `clockIn.${personnel.id}.0`)}
                        clockOut={objectGet(attendances, `clockOut.${personnel.id}.0`)}
                        seeDetail={seeDetail}
                    />
                ))}
            </Rtbody>
        </Rtable>
        {personnels?.length > 8 && <PaginationComponent />}
        </>
    )
}

const PersonnelsRow = memo(({ personnel, shift, clockIn, clockOut, seeDetail }) => {
    return <Rtr>
        <Rtd>{personnel.fullName}</Rtd>
        <Rtd>{shift && shift.map(s => s.shiftName).join(', ')}</Rtd>
        <Rtd className="text-md-center">
            {clockIn ? 
                <Button color="link" onClick={() => seeDetail(clockIn)}>{clockIn.time.substr(0, 5)}<br/>{clockIn.loc.name}</Button>
                :
                '-'
            }
        </Rtd>
        <Rtd className="text-md-center">
            {clockOut ?
                <Button color="link" onClick={() => seeDetail(clockOut)}>{clockOut.time.substr(0, 5)}<br/>{clockOut.loc.name}</Button>
                :
                '-'
            }
        </Rtd>
    </Rtr>
});

export default translate(AttendanceHistoryToday)
