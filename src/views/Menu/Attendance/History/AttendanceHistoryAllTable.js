import React, {useState, useCallback} from 'react'
import { Badge, UncontrolledTooltip } from 'reactstrap';
import { t } from 'react-switch-lang';
import { useUserPrivileges } from '../../../../store';
import moment from 'moment';
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../../components/ResponsiveTable';
import usePagination from '../../../../hooks/usePagination';

function AttendanceHistoryAllTable({ attendances, seeDetail }) {
    const { can } = useUserPrivileges();
    const [filters, setFilters] = useState([])

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
    );

    const { data: groupFiltered, PaginationComponent } = usePagination(
        attendances,
        8,
        filters?.pagination,
        handleChangeCurrentPage
    );

    return (
        <>
        <Rtable>
            <Rthead>
                <Rtr>
                    <Rth className="w-30">{t('nama')}</Rth>
                    <Rth className="w-10">{t('jenis')}</Rth>
                    <Rth className="text-center w-20">{t('tanggal')}</Rth>
                    <Rth className="w-10">{t('jam')}</Rth>
                    <Rth className="text-center w-10">{t('wajah')}</Rth>
                    <Rth className="text-center w-20">{t('lokasi')}</Rth>
                    {can('read-attendance') && <Rth className="text-center w-10"></Rth>}
                </Rtr>
            </Rthead>
            <Rtbody>
               {groupFiltered?.map(data => (
                   <Rtr key={data.id}>
                        <Rtd className="w-30">{data.name}</Rtd>
                        <Rtd>{t(data.type)}</Rtd>
                        <Rtd className="text-center">{moment(data.date).format('DD MMMM YYYY')}</Rtd>
                        <Rtd className="">
                            {data.time?.substr(0, 5)} {data.type === 'clockIn' && !data.timeIsMatch ?
                                <>
                                    <i id="late" className="fa fa-exclamation-circle text-danger"></i>
                                    <UncontrolledTooltip placement="top" target="late">{t('terlambat')}</UncontrolledTooltip>
                                </>
                                : null
                            }
                        </Rtd>
                        <Rtd className="text-center">{data.FaceIsMatch ? <Badge color="success">{t('sesuai')}</Badge> : <Badge color="danger">{t('tidaksesuai')}</Badge>}</Rtd>
                        <Rtd className="text-center">{data.loc.name ?? '-'}</Rtd>
                        {can('read-attendance') && <Rtd className="text-center">
                            <p className="action-text" onClick={() => seeDetail(data)}>{t('detail')}</p>
                        </Rtd>}
                    </Rtr>
                ))}
            </Rtbody>
        </Rtable>
        {attendances?.length > 8 && <PaginationComponent />}
        </>
    );
}

export default AttendanceHistoryAllTable
