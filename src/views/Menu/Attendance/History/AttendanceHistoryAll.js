import React, { useCallback, useState, Fragment, useEffect, useRef, useMemo } from 'react';
import { Row, Col, FormGroup, Label, ModalHeader, InputGroupText, InputGroupAddon, Input, Button, Spinner, InputGroup, Modal, ModalBody } from 'reactstrap';
import { DatePickerInput } from 'rc-datepicker';
import moment from 'moment';
import useSWR from 'swr';
import { t, translate } from 'react-switch-lang';
import request, { requestDownload } from '../../../../utils/request';
import { useUserPrivileges } from '../../../../store';
import LoadingAnimation from '../../../../components/LoadingAnimation';
import AttendanceHistoryToday from './AttendanceHistoryToday';
import AttendanceHistoryAllTable from './AttendanceHistoryAllTable';
import { toast } from 'react-toastify';
import DataNotFound from '../../../../components/DataNotFound';
import GoogleMapReact from 'google-map-react';
import MapMarker from '../../Personnel/component/atom/MapMarker';
import Select from "react-select";

function AttendanceHistoryAll() {
    const { can } = useUserPrivileges();

    const today = moment().format('YYYY-MM-DD');
    const [filters, setFilters] = useState({ start: today, end: today, search: '' , unit: '', job: '' });
    const { data: attendancesResponse, error: attendancesError, isValidating,  mutate } = useSWR(() => `v1/attendance/company?start=${filters.start}&end=${filters.end}&unit=${filters.unit}&job=${filters.job}`);
    const loading = !attendancesResponse && !attendancesError && isValidating;
    const attendances = useMemo(() => {
        let data = (attendancesResponse?.data?.data ?? []);
        if (filters.search) {
            data = data.filter(item => item.name.toLowerCase().includes(filters.search.trim().toLowerCase()));
        }
        return data;
    }, [filters.search, attendancesResponse])
    const refreshIntervalRef = useRef(null);
    
    useEffect(() => {
        if (filters.start === today && filters.end === today) {
            refreshIntervalRef.current = setInterval(() => {
                mutate();
            }, 10000);
        } else {
            if (refreshIntervalRef.current !== null) {
                clearInterval(refreshIntervalRef.current)
            }
        }

        return () => {
            if (refreshIntervalRef.current !== null) {
                clearInterval(refreshIntervalRef.current);
            }
        }
    }, [filters, mutate, today])

    const [ units, setUnits ] = useState([]);
    const [ jobs, setJobs ] = useState([]);
    
    useEffect(() => {
        if (units.length) return;
        request.get('v1/master/units')
        .then((res) => setUnits(res.data.data.map((item)=>{
            return {label:item.name,value:item.id};
        })))
        .catch((err)=>{})
        request.get('v1/master/jobs')
        .then((res) => setJobs(res.data.data.map((item)=>{
            return {label:item.name,value:item.id};
        })))
        .catch((err)=>{})
    },[units]);
    
    const [currentFilters, setCurrentFilters] = useState({ start: today, end: today, search: '' , unit: '', job: '' });
    const handleFilterChange = useCallback((event) => {
        const { name, value } = event.target;
        setCurrentFilters((states) => {
            const updates = { [name]: value };
            if (name === 'start') {
                const momentStart = moment(value);
                const momentEnd = moment(states.end);
                if (momentStart.isAfter(momentEnd, 'day')) {
                    updates.end = value;
                }
            } else if (name === 'end') {
                const momentEnd = moment(value);
                const momentStart = moment(states.start);
                if (momentEnd.isBefore(momentStart, 'day')) {
                    updates.start = value;
                }
            }

            return { ...states, ...updates };
        });
    }, [])
    const handleChangeDateStart = useCallback((_, formattedDate) => {
        handleFilterChange({ target: { name: 'start', value: formattedDate }});
    }, [handleFilterChange]);
    const handleChangeDateEnd = useCallback((_, formattedDate) => {
        handleFilterChange({ target: { name: 'end', value: formattedDate }});
    }, [handleFilterChange]);

    const [downloadingExcel, setDownloadingExcel] = useState(false);
    const handleDownloadExcel = useCallback(() => {
        setDownloadingExcel(true);
        requestDownload(`v1/attendance/recaps/excel?start=${currentFilters.start}&end=${currentFilters.end}&unit=${currentFilters.unit}&job=${currentFilters.job}`)
            .finally(() => setDownloadingExcel(false));
    }, [currentFilters.start, currentFilters.end, currentFilters.job, currentFilters.unit]);

    const [changingFaceIsMatch, setChangingFaceIsMatch] = useState(false);
    const toggleChangingFaceIsMatch = useCallback(() => {
        setChangingFaceIsMatch(state => !state);
    }, []);
    const [loadingFaceMatching, setLoadingFaceMatching] = useState(false);
    const [modalDetail, setModalDetail] = useState();
    const toggleModal = useCallback(() => {
        if (! loadingFaceMatching) {
            setModalDetail(null);
        }
    }, [loadingFaceMatching]);

    const setFaceIsMatch = useCallback((isMatch) => {
        if (!modalDetail) {
            return;
        }
        setLoadingFaceMatching(true);
        request.post(`v1/personnels/attendance/history/${modalDetail.id}/face`, { faceIsMatch: isMatch ? '1' : '0' })
            .then(() => {
                toast.success(t('Berhasil Mengubah Kesesuaian Wajah'));
                mutate((response) => {
                    const updatedResponse = { ...response };
                    const updatedData = [ ...updatedResponse.data.data ]
                    const updatedIndex = updatedData.findIndex(data => data.id === modalDetail.id);
                    updatedData[updatedIndex] = { ...modalDetail, FaceIsMatch: isMatch };
                    updatedResponse.data.data = updatedData;
                    return updatedResponse;
                }, false);
            })
            .catch(err => {
                if (err.response && err.response.data) {
                    toast.error(err.response.data.message || t('Gagal Mengubah Kesesuaian Wajah'))
                }
            })
            .finally(() => {
                setChangingFaceIsMatch(false);
                setLoadingFaceMatching(false);
            });
    }, [modalDetail, mutate])

    // console.log(currentFilters);
    return (
        <div className="animated fadeIn" >
            <h4 className="content-title mb-3">{t('Riwayat Presensi')}</h4>
            <div className="content">
                <Row className="md-company-header mb-3">
                    <Col xs="12" md="9">
                        <Row>
                            <Col xs="6">
                                <FormGroup>
                                  <Label htmlFor="jobId" className="input-label">
                                    {t("jabatan")}
                                  </Label>
                                  <Select
                                    name="jobId"
                                    id="jobId"
                                    options={jobs}
                                    isClearable={true}
                                    onChange={(e) => setCurrentFilters({...currentFilters,job: e?.value || ""})}
                                  />
                                </FormGroup>
                            </Col>
                            <Col xs="6">
                                <FormGroup style={{ marginBottom: 5 }}>
                                  <Label htmlFor="unitId" className="input-label">
                                    {t('Unit')}
                                  </Label>
                                  <Select
                                    name="unitId"
                                    id="unitId"
                                    options={units}
                                    isClearable={true}
                                    onChange={(e) => setCurrentFilters({...currentFilters,unit: e?.value || ""})}
                                  />
                                </FormGroup>
                            </Col>
                            <Col xs="6" xl="4" >
                                <FormGroup>
                                    <Label htmlFor="start" className="input-label">{t('dari')}</Label>
                                    <DatePickerInput
                                        name="start"
                                        id="start"
                                        onChange={handleChangeDateStart}
                                        value={currentFilters.start}
                                        className='my-custom-datepicker-component'
                                        showOnInputClick={true}
                                        closeOnClickOutside={true}
                                        displayFormat="DD MMMM YYYY"
                                        returnFormat="YYYY-MM-DD"
                                        readOnly
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs="6" xl="4" >
                                <FormGroup>
                                    <Label htmlFor="end" className="input-label">{t('hingga')}</Label>
                                    <DatePickerInput
                                        name="end"
                                        id="end"
                                        onChange={handleChangeDateEnd}
                                        value={currentFilters.end}
                                        className='my-custom-datepicker-component'
                                        showOnInputClick={true}
                                        closeOnClickOutside={true}
                                        displayFormat="DD MMMM YYYY"
                                        returnFormat="YYYY-MM-DD"
                                        readOnly
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs="12" xl="4" className="mt-xl-1 pt-xl-4 mb-2">
                                <InputGroup className="">
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText className="input-group-transparent">
                                            <i className="fa fa-search"></i>
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="text" name="search" placeholder={t('cari') + ' ' + t('nama')} className="input-search" value={currentFilters.change} onChange={handleFilterChange} />
                                </InputGroup>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs="12" md="3">
                        <Row>
                            <Col sm="6" md="12">
                                <div className="pt-sm-2 my-2 mt-md-0">
                                    <Button type="submit" className="mt-sm-4" color="netis-color" style={{ width: '100%' }} disabled={loading} onClick={() => setFilters(currentFilters)}>
                                        {loading ? <Spinner color="light" size="sm" /> : 'Filter'}
                                    </Button>
                                </div>
                            </Col>
                            {can('download-attendance') && <Col sm="6" md="12" className="text-right">
                                <div className="pt-sm-2 my-2">
                                    <Button type="submit" className="mt-sm-4" color="netis-success" style={{ width: '100%' }} onClick={handleDownloadExcel} disabled={downloadingExcel}>
                                        {downloadingExcel ? <Fragment><Spinner size="sm" /> Downloading...</Fragment> : <Fragment><i className="fa fa-download mr-2"></i> {t('unduhlaporan')}</Fragment>}
                                    </Button>
                                </div>
                            </Col>}
                        </Row>
                    </Col>
                </Row>
                {
                    loading ?
                        <LoadingAnimation/>
                    : filters.start === filters.end ?
                        <AttendanceHistoryToday attendances={attendances} day={moment(filters.start).isoWeekday() % 7} seeDetail={setModalDetail} search={filters.search} />
                    : attendances.length > 0 ?
                        <AttendanceHistoryAllTable attendances={attendances} seeDetail={setModalDetail} />
                    :
                        <DataNotFound/>
                }
                {/* Modal Box Detail Data */}
                <Modal isOpen={!!modalDetail} toggle={toggleModal} size="lg">
                    <ModalHeader toggle={toggleModal}>
                        {t('Detail Riwayat Presensi')}
                    </ModalHeader>
                    <ModalBody>
                        {/* <div className="d-flex justify-content-between align-items-center mb-4">
                            <h5 className="content-sub-title mb-0">Details {t('riwayat')} {t('Presensi')}</h5>
                            <Button color="link" size="lg" className="text-danger" onClick={toggleM}><strong>&times;</strong></Button>
                        </div> */}
                        <Row>
                            <div className="col-6">
                                <FormGroup>
                                    <Label htmlFor="jenis" className="input-label">{t('Jenis Presensi')}</Label>
                                    <Input type="text" name="jenis" id="jenis" placeholder={t('Jenis Presensi')}
                                        disabled={true}
                                        defaultValue={t(modalDetail?.type || 'clockIn')} />
                                </FormGroup>
                            </div>
                            <div className="col-6">
                                <FormGroup>
                                    <Label htmlFor="waktu" className="input-label">{t('Waktu Presensi')}</Label>
                                    <Input type="text" name="waktu" id="waktu" placeholder={t('Waktu Presensi')}
                                        disabled={true}
                                        defaultValue={moment(modalDetail?.time, 'HH:mm:ss').format('HH:mm')} />
                                </FormGroup>
                            </div>
                        </Row>
                        <Row className="mb-2">
                            <div className="col-md-6">
                                <div className="d-flex justify-content-center">
                                    <img src={modalDetail?.pahFaceFile} style={{ maxHeight: '325px' }} className="img-fluid" alt="personnel-face" />
                                </div>
                                <div className="d-flex justify-content-center">
                                    <div className="mb-3">
                                        {
                                            modalDetail?.FaceIsMatch ?
                                                <h6 className="mt-2 mb-0 text-success text-center">{ t('wajahsesuai') }<i className="fa fa-check"></i></h6>
                                                :
                                                <h6 className="mt-2 mb-0 text-warning text-center">{ t('wajahbelumsesuai') }<i className="fa fa-times"></i></h6>
                                        }
                                        {
                                            can('edit-attendance') && (
                                                !changingFaceIsMatch ?
                                                    <div className="text-center">
                                                        <Button color="netis-color" className="pt-0 pb-0 pl-2 pr-2" onClick={toggleChangingFaceIsMatch}>{t('ubah')}</Button>
                                                    </div>
                                                    :
                                                    <div className="d-flex items-align-center justify-content-between">
                                                        <div>
                                                            <Button color="success" className="mr-1 pt-0 pb-0 pl-2 pr-2" disabled={loadingFaceMatching} onClick={() => setFaceIsMatch(true)}>
                                                                {loadingFaceMatching ? <Spinner color="light" size="sm" /> : t('sesuai')}
                                                            </Button>
                                                            <Button color="warning" className="ml-1 pt-0 pb-0 pl-2 pr-2" disabled={loadingFaceMatching} onClick={() => setFaceIsMatch(false)}>
                                                                {loadingFaceMatching ? <Spinner color="light" size="sm" /> : t('tidaksesuai')}
                                                            </Button>
                                                        </div>
                                                        <Button color="link" size="lg" className="text-danger p-0 ml-3" disabled={loadingFaceMatching} onClick={toggleChangingFaceIsMatch}><strong>&times;</strong></Button>
                                                    </div>
                                            )
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div style={{ height: '350px', width: '100%' }}>
                                    <GoogleMapReact
                                        bootstrapURLKeys={{ key: "AIzaSyDQsNCd2Trmf4MLwcB7k1oqpWZPpTeCkc0" }}
                                        center={{ lat: modalDetail?.latitude, lng: modalDetail?.longitude }}
                                        defaultZoom={17}
                                    >
                                        <MapMarker
                                            lat={modalDetail?.latitude}
                                            lng={modalDetail?.longitude}
                                        />
                                    </GoogleMapReact>
                                </div>
                            </div>
                        </Row>
                        <Row>
                            <div className="col-12">
                                <FormGroup>
                                    <Label className="input-label">{t('Pesan Karyawan')}</Label>
                                    <Input type="text" placeholder={t('Pesan Karyawan')} disabled={true} value={modalDetail?.message || ''}/>
                                </FormGroup>
                            </div>
                        </Row>
                    </ModalBody>
                </Modal>
            </div>
        </div>
    )
}

export default translate(AttendanceHistoryAll)
