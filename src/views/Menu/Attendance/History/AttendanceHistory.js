import React, { Component, Fragment } from 'react';
import { Table, Col, Row, Input, Button, Modal, ModalBody, FormGroup, Label } from 'reactstrap';
import MapMarker from '../../Personnel/component/atom/MapMarker';
import GoogleMapReact from 'google-map-react';
import Axios from 'axios';
import { connect } from 'react-redux';
import { DatePickerInput } from 'rc-datepicker';
import { toast } from 'react-toastify';
import LoadingAnimation from '../../components/LoadingAnimation';
import { t } from 'react-switch-lang';
class AttendanceHistory extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dropdownOpen: [false, false],
            session: props.token,
            modalDetail: false,
            editMessage: false,
            idEditData: '',
            attendanceDetail: {},
            message: {
                message: ''
            },
            latLng: {},
            date: new Date(Date.now() - 7 * 24 * 60 * 60 * 1000),
            dateEnd: new Date(),
            start: this.formatDate(new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)),
            end: this.formatDate(new Date()),
            resMessage: '',
            attendanceHistory: [],
            loading: true,
        };

        this.modalDetail = this.modalDetail.bind(this);
    }

    componentDidMount = () => {
        this.getDataFromAPI(this.state.start, this.state.end)
    }

    modalDetail = (data) => {
        const attendanceDetail = { ...this.state.attendanceDetail, type: data.type, time: data.time }
        const message = { ...this.state.message, message: data.message }
        const latLng = { ...this.state.latLng, lat: data.latitude, lng: data.longitude }
        this.setState({
            modalDetail: !this.state.modalDetail,
            attendanceDetail,
            message,
            latLng,
            idEditData: data.id
        })
    }

    editMessage = () => this.setState({ editMessage: !this.state.editMessage })

    handleChange = (e) => {
        const message = { ...this.state.message, message: e.target.value }
        this.setState({ message })
    }

    handleChangeDateStart = date => {
        if (date.getTime() < this.state.dateEnd.getTime()) {
            this.setState({
                date,
                start: this.formatDate(date)
            });
        }
        else {
            this.setState({
                date: new Date(this.state.dateEnd.getTime() - 1 * 24 * 60 * 60 * 1000),
                start: this.formatDate(new Date(this.state.dateEnd.getTime() - 1 * 24 * 60 * 60 * 1000))
            })
        }
    }

    handleChangeDateEnd = date => {
        if (date.getTime() > this.state.date.getTime()) {
            this.setState({
                dateEnd: date,
                end: this.formatDate(date)
            });
        }
        else {
            this.setState({
                dateEnd: new Date(this.state.date.getTime() + 1 * 24 * 60 * 60 * 1000),
                end: this.formatDate(new Date(this.state.date.getTime() + 1 * 24 * 60 * 60 * 1000))
            })
        }
    }

    formatDate = (date) => {
        let d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    filterData = () => {
        this.getDataFromAPI(this.state.start, this.state.end)
    }

    getDataFromAPI = (start, end) => {
        this.setState({loading: true})
        Axios.get(process.env.REACT_APP_DOMAIN + `/api/v1/personnels/attendance/history?start=${start}&end=${end}`, { headers: { "Authorization": `Bearer ${this.state.session}` } })
            .then((res) => {
                const attendanceHistory = res.data.data;
                this.setState({ attendanceHistory })
            })
            .catch(error => {
                console.log(error.response)
            })
            .finally(() => this.setState({loading: false}));
    }

    editMessageToAPI = (id) => {
        Axios.post(process.env.REACT_APP_DOMAIN + `/api/v1/personnels/attendance/history/${id}`, this.state.message, { headers: { "Authorization": `Bearer ${this.state.session}` } })
            .then((res) => {
                this.getDataFromAPI(this.state.start, this.state.end);
                this.setState({
                    modalDetail: !this.state.modalDetail,
                    editMessage: !this.state.editMessage,
                    resMessage: t('Pesan Berhasil Diubah')
                }, () => toast.success(this.state.resMessage, { autoClose: 3000 }))
            })
            .catch(error => {
                toast.error(error.response.data.errors.message, { autoClose: 3000 })
            });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <h4 className="content-title mb-4">{('Riwayat Presensi')}</h4>
                <div className="content">
                    <Row className="md-company-header mb-3">
                        <Col xs="6" md="2" >
                            <FormGroup>
                                <Label htmlFor="startDate" className="input-label">{t('dari')}</Label>
                                <DatePickerInput
                                    name="startDate"
                                    id="startDate"
                                    onChange={this.handleChangeDateStart}
                                    value={this.state.date}
                                    className='my-custom-datepicker-component'
                                    showOnInputClick={true}
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="6" md="2" >
                            <FormGroup>
                                <Label htmlFor="endDate" className="input-label">{t('hingga')}</Label>
                                <DatePickerInput
                                    name="endDate"
                                    id="endDate"
                                    onChange={this.handleChangeDateEnd}
                                    value={this.state.dateEnd}
                                    className='my-custom-datepicker-component'
                                    showOnInputClick={true}
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="6" md="2" >
                            <div className="pt-sm-2">
                                <Button type="submit" className="mt-sm-4" color="netis-color" onClick={this.filterData}>Filter</Button>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" lg="12">
                            {
                                this.state.loading ?
                                <div className="text-center py-5">
                                    <LoadingAnimation />
                                </div>
                                :
                                <Table responsive>
                                <thead>
                                    <tr>
                                        <th className="">{t('jenis')}</th>
                                        <th className="text-center w-20">{t('tanggal')}</th>
                                        <th className="text-center w-20">{t('jam')}</th>
                                        <th className="text-center w-20">{t('lokasi')}</th>
                                        <th className="text-center w-20">{t('aksi')}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.attendanceHistory.length > 0 ?
                                        this.state.attendanceHistory.map((data, idx) => {
                                            return (
                                                <tr key={idx}>
                                                    <td>{t(data.type)}</td>
                                                    <td className="text-center">{data.date}</td>
                                                    <td className="text-center">{data.time}</td>
                                                    <td className="text-center">{data.loc.name}</td>
                                                    <td className="text-center">
                                                        <p className="action-text" onClick={() => this.modalDetail(data)}>{t('detail')}</p>
                                                    </td>
                                                </tr>
                                            );
                                        })
                                        :
                                        <tr key={"no_data"}>
                                            <td className="text-center no-data-message" colSpan="5">No Data</td>
                                        </tr>
                                    }
                                </tbody>
                            </Table>}
                        </Col>
                    </Row>

                    {/* Modal Box Detail Data */}
                    <Modal isOpen={this.state.modalDetail} toggle={this.modalDetail} className={this.props.className}>
                        <ModalBody>
                            <h5 className="content-sub-title mb-4">{t('Detail Riwayat Presensi')}</h5>
                            <Row>
                                <div className="col-6">
                                    <FormGroup>
                                        <Label htmlFor="jenis" className="input-label">{t('Jenis Presensi')}</Label>
                                        <Input type="text" name="jenis" id="jenis" placeholder={t('Jenis Presensi')}
                                            disabled={true}
                                            defaultValue={t(this.state.attendanceDetail.type || 'clockIn')} />
                                    </FormGroup>
                                </div>
                                <div className="col-6">
                                    <FormGroup>
                                        <Label htmlFor="waktu" className="input-label">{t('Waktu Presensi')}</Label>
                                        <Input type="text" name="waktu" id="waktu" placeholder={t('Waktu Presensi')}
                                            disabled={true}
                                            defaultValue={this.state.attendanceDetail.time} />
                                    </FormGroup>
                                </div>
                            </Row>
                            <Row className="mb-2">
                                <div className="col-12">
                                    <div style={{ height: '350px', width: '100%' }}>
                                        <GoogleMapReact
                                            bootstrapURLKeys={{ key: "AIzaSyDQsNCd2Trmf4MLwcB7k1oqpWZPpTeCkc0" }}
                                            center={this.state.latLng}
                                            defaultZoom={17}
                                        >
                                            <MapMarker
                                                lat={this.state.latLng.lat}
                                                lng={this.state.latLng.lng}
                                                text={""}
                                            />
                                        </GoogleMapReact>
                                    </div>
                                </div>
                            </Row>
                            <Row>
                                <div className={this.state.editMessage ? "col-md-8" : "col-md-10"}>
                                    <FormGroup>
                                        <Label htmlFor="message" className="input-label">{t('pesan')}</Label>
                                        <Input type="text" name="message" id="message" placeholder={t('pesan')}
                                            disabled={!this.state.editMessage}
                                            value={this.state.message.message || ''}
                                            onChange={this.handleChange} />
                                    </FormGroup>
                                </div>
                                <div className={this.state.editMessage ? "col-md-4 d-flex justify-content-end align-items-center" : "col-md-2 d-flex justify-content-end align-items-center"}>
                                    {this.state.editMessage ?
                                        <Fragment>
                                            <Button className="mr-2 mt-3" color="white" onClick={this.editMessage}>{t('batal')}</Button>
                                            <Button type="submit" className="mt-3" color="netis-color" onClick={() => this.editMessageToAPI(this.state.idEditData)}>{t('simpan')}</Button>
                                        </Fragment>
                                        :
                                        <Button type="submit" className="mt-3" color="netis-color" onClick={this.editMessage}><i className="fa fa-pencil" style={{ marginRight: 5 }}></i>Edit</Button>
                                    }
                                </div>
                            </Row>
                        </ModalBody>
                    </Modal>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.token
    }
}
export default connect(mapStateToProps)(AttendanceHistory);
