import React, { Component, Fragment } from 'react';
import {
    Table,
    Col,
    Row,
    Input,
    Button,
    Modal,
    ModalBody,
    FormGroup,
    Label,
    Spinner
} from 'reactstrap';
import MapMarker from '../../Personnel/component/atom/MapMarker';
import GoogleMapReact from 'google-map-react';
import Axios from 'axios';
import { connect } from 'react-redux';
import { DatePickerInput } from 'rc-datepicker';
import { toast } from 'react-toastify';
import {t} from 'react-switch-lang';
class AttendanceRecapDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dropdownOpen: [false, false],
            session: props.token,
            loading: false,
            modalDetail: false,
            editMessage: false,
            idEditData: '',
            attendanceDetail: {},
            message: {
                message: ''
            },
            latLng: {},
            date: new Date(Date.now() - 7 * 24 * 60 * 60 * 1000),
            dateEnd: new Date(),
            start: this.formatDate(new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)),
            end: this.formatDate(new Date()),
            resMessage: '',
            idPersonnel: this.props.match.params.id,
            personnel: {},
            attendanceHistory: []
        };

        this.modalDetail = this.modalDetail.bind(this);
    }

    componentDidMount = () => {
        this.getDataHistoryById(this.state.idPersonnel, this.state.start, this.state.end)
        Axios.get(process.env.REACT_APP_DOMAIN + `/api/v1/personnels?id=${this.state.idPersonnel}`, { headers: { "Authorization": `Bearer ${this.state.session}` } })
            .then(res => {
                const personnel = res.data.data[0];
                this.setState({ personnel });
            })
            .catch(error => console.log(error.response));
    }

    modalDetail = (data) => {
        const attendanceDetail = { ...this.state.attendanceDetail, type: data.type, time: data.time }
        const message = { ...this.state.message, message: data.message }
        const latLng = { ...this.state.latLng, lat: data.latitude, lng: data.longitude }
        this.setState({
            modalDetail: !this.state.modalDetail,
            attendanceDetail,
            message,
            latLng,
            idEditData: data.id
        })
    }

    editMessage = () => this.setState({ editMessage: !this.state.editMessage })

    handleChange = (e) => {
        const message = { ...this.state.message, message: e.target.value }
        this.setState({ message })
    }

    handleChangeDateStart = date => {
        if (date.getTime() < this.state.dateEnd.getTime()) {
            this.setState({
                date,
                start: this.formatDate(date)
            });
        }
        else {
            this.setState({
                date: new Date(this.state.dateEnd.getTime() - 1 * 24 * 60 * 60 * 1000),
                start: this.formatDate(new Date(this.state.dateEnd.getTime() - 1 * 24 * 60 * 60 * 1000))
            })
        }
    }

    handleChangeDateEnd = date => {
        if (date.getTime() > this.state.date.getTime()) {
            this.setState({
                dateEnd: date,
                end: this.formatDate(date)
            });
        }
        else {
            this.setState({
                dateEnd: new Date(this.state.date.getTime() + 1 * 24 * 60 * 60 * 1000),
                end: this.formatDate(new Date(this.state.date.getTime() + 1 * 24 * 60 * 60 * 1000))
            })
        }
    }

    formatDate = (date) => {
        let d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    filterData = () => {
        this.setState({ loading: true })
        this.getDataHistoryById(this.state.idPersonnel, this.state.start, this.state.end)
    }

    getDataHistoryById = (id, start, end) => {
        Axios.get(process.env.REACT_APP_DOMAIN + `/api/v1/personnels/${id}/attendance/history?start=${start}&end=${end}`, { headers: { "Authorization": `Bearer ${this.state.session}` } })
            .then((res) => {
                const attendanceHistory = res.data.data;
                setTimeout(() => {
                    this.setState({ attendanceHistory }, () => this.setState({ loading: false }))
                }, 500)
            })
            .catch(error => {
                console.log(error.response)
            });
    }

    editMessageToAPI = (id) => {
        this.setState({ loading: true })
        Axios.post(process.env.REACT_APP_DOMAIN + `/api/v1/personnels/attendance/history/${id}`, this.state.message, { headers: { "Authorization": `Bearer ${this.state.session}` } })
            .then((res) => {
                this.getDataHistoryById(this.state.idPersonnel, this.state.start, this.state.end);
                this.setState({
                    modalDetail: !this.state.modalDetail,
                    editMessage: !this.state.editMessage,
                    resMessage: 'Pesan Berhasil Diubah!',
                    loading: false
                }, () => toast.success(this.state.resMessage, { autoClose: 3000 }))
            })
            .catch(error => {
                // console.log(error.response)
                toast.error(error.response.data.errors.message, { autoClose: 3000 })
            });
    }

    render() {
        return (
            <div className="animated fadeIn" >
                <h4 className="content-title mb-4">Rekapitulasi Presensi {this.state.personnel !== undefined ? this.state.personnel.fullName : ''}</h4>
                <div className="content">
                    <Row className="md-company-header mb-3">
                        {this.state.personnel !== undefined ?
                            <Fragment>
                                <Col xs="6" md="2" >
                                    <FormGroup>
                                        <Label htmlFor="startDate" className="input-label">Dari</Label>
                                        <DatePickerInput
                                            name="startDate"
                                            id="startDate"
                                            onChange={this.handleChangeDateStart}
                                            value={this.state.date}
                                            className='my-custom-datepicker-component'
                                            showOnInputClick={true}
                                            closeOnClickOutside={true}
                                            displayFormat="DD MMMM YYYY"
                                            readOnly
                                        />
                                    </FormGroup>
                                </Col>
                                <Col xs="6" md="2" >
                                    <FormGroup>
                                        <Label htmlFor="endDate" className="input-label">Hingga</Label>
                                        <DatePickerInput
                                            name="endDate"
                                            id="endDate"
                                            onChange={this.handleChangeDateEnd}
                                            value={this.state.dateEnd}
                                            className='my-custom-datepicker-component'
                                            showOnInputClick={true}
                                            closeOnClickOutside={true}
                                            displayFormat="DD MMMM YYYY"
                                            readOnly
                                        />
                                    </FormGroup>
                                </Col>
                                <Col xs="6" md="2" >
                                    <div className="pt-sm-2">
                                        <Button type="submit" className="mt-sm-4" color="netis-color" style={{ width: '60px' }} onClick={this.filterData}>
                                            {this.state.loading ? <Spinner color="light" size="sm" /> : 'Filter'}
                                        </Button>
                                    </div>
                                </Col>
                            </Fragment>
                            :
                            <Col col="12">
                                <h5 className="text-center content-sub-title no-data-message mb-0">Data Tidak Ditemukan :(</h5>
                            </Col>
                        }
                    </Row>
                    <Row>
                        {this.state.personnel !== undefined ?
                            <Col xs="12" lg="12">
                                <Table responsive>
                                    <thead>
                                        <tr>
                                            <th className="">Jenis</th>
                                            <th className="text-center w-20">Tanggal</th>
                                            <th className="text-center w-20">Jam</th>
                                            <th className="text-center w-20">Lokasi</th>
                                            <th className="text-center w-20">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.attendanceHistory.length > 0 ?
                                            this.state.attendanceHistory.map((data, idx) => {
                                                return (
                                                    <tr key={idx}>
                                                        <td className="">{t(data.type)}</td>
                                                        <td className="text-center">{data.date}</td>
                                                        <td className="text-center">{data.time}</td>
                                                        <td className="text-center">{data.loc.name}</td>
                                                        <td className="text-center">
                                                            <p className="action-text" onClick={() => this.modalDetail(data)}>Detail</p>
                                                        </td>
                                                    </tr>
                                                );
                                            })
                                            :
                                            <tr key={"no_data"}>
                                                <td className="text-center no-data-message" colSpan="5">No Data :(</td>
                                            </tr>
                                        }
                                    </tbody>
                                </Table>
                            </Col>
                            : null
                        }
                    </Row>

                    {/* Modal Box Detail Data */}
                    <Modal isOpen={this.state.modalDetail} toggle={this.modalDetail} className={this.props.className}>
                        <ModalBody>
                            <h5 className="content-sub-title mb-4">Detail Riwayat Presensi</h5>
                            <Row>
                                <div className="col-6">
                                    <FormGroup>
                                        <Label htmlFor="jenis" className="input-label">Jenis Presensi</Label>
                                        <Input type="text" name="jenis" id="jenis" placeholder="Jenis Presensi"
                                            disabled={true}
                                            defaultValue={t(this.state.attendanceDetail.type || 'clockIn')} />
                                    </FormGroup>
                                </div>
                                <div className="col-6">
                                    <FormGroup>
                                        <Label htmlFor="waktu" className="input-label">Waktu Presensi</Label>
                                        <Input type="text" name="waktu" id="waktu" placeholder="Waktu Presensi"
                                            disabled={true}
                                            defaultValue={this.state.attendanceDetail.time} />
                                    </FormGroup>
                                </div>
                            </Row>
                            <Row className="mb-2">
                                <div className="col-12">
                                    <div style={{ height: '350px', width: '100%' }}>
                                        <GoogleMapReact
                                            bootstrapURLKeys={{ key: "AIzaSyDQsNCd2Trmf4MLwcB7k1oqpWZPpTeCkc0" }}
                                            center={this.state.latLng}
                                            defaultZoom={17}
                                        >
                                            <MapMarker
                                                lat={this.state.latLng.lat}
                                                lng={this.state.latLng.lng}
                                                text={""}
                                            />
                                        </GoogleMapReact>
                                    </div>
                                </div>
                            </Row>
                            <Row>
                                <div className={this.state.editMessage ? "col-md-8" : "col-md-10"}>
                                    <FormGroup>
                                        <Label htmlFor="message" className="input-label">Pesan</Label>
                                        <Input type="text" name="message" id="message" placeholder="Pesan"
                                            disabled={!this.state.editMessage}
                                            value={this.state.message.message || ''}
                                            onChange={this.handleChange} />
                                    </FormGroup>
                                </div>
                                <div className={this.state.editMessage ? "col-md-4 d-flex justify-content-end align-items-center" : "col-md-2 d-flex justify-content-end align-items-center"}>
                                    {this.state.editMessage ?
                                        <Fragment>
                                            <Button className="mr-2 mt-sm-3" color="white" onClick={this.editMessage}>Batal</Button>
                                            <Button type="submit" className="mt-sm-3" color="netis-color" style={{ width: '70px' }} onClick={() => this.editMessageToAPI(this.state.idEditData)}>
                                                {this.state.loading ? <Spinner color="light" size="sm" /> : 'Simpan'}
                                            </Button>
                                        </Fragment>
                                        :
                                        <Button type="submit" className="mt-sm-3" color="netis-color" onClick={this.editMessage}><i className="fa fa-pencil" style={{ marginRight: 5 }}></i>Edit</Button>
                                    }
                                </div>
                            </Row>
                        </ModalBody>
                    </Modal>
                </div>
            </div >
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.token
    }
}
export default connect(mapStateToProps)(AttendanceRecapDetail);
