import React, { Component } from 'react';
import {
    Table,
    Col,
    Row,
    FormGroup,
    Label,
    Input,
    Button,
    Spinner
} from 'reactstrap';
import { Link } from 'react-router-dom';
import Axios from 'axios';
import { connect } from 'react-redux';
import { DatePickerInput } from 'rc-datepicker';
import 'rc-datepicker/lib/style.css';

class AttendanceRecap extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dropdownOpen: [false, false],
            session: props.token,
            loading: false,
            date: new Date(Date.now() - 7 * 24 * 60 * 60 * 1000),
            dateEnd: new Date(),
            start: this.formatDate(new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)),
            end: this.formatDate(new Date()),
            units: [],
            jobs: [],
            unit: null,
            job: null,
            attendanceRecap: []
        };
    }

    componentDidMount = () => {
        Axios.all([
            Axios.get(process.env.REACT_APP_DOMAIN + `/api/v1/master/units`, { headers: { "Authorization": `Bearer ${this.state.session}` } }),
            Axios.get(process.env.REACT_APP_DOMAIN + `/api/v1/master/jobs`, { headers: { "Authorization": `Bearer ${this.state.session}` } })
        ])
            .then(Axios.spread((res1, res2) => {
                const units = res1.data.data;
                const jobs = res2.data.data;
                this.setState({ units, jobs });
            }))
            .catch(error => console.log(error));
        this.getDataFromAPI(this.state.start, this.state.end, this.state.unit, this.state.job)
    }

    getDataFromAPI = (start, end, unit, job) => {
        Axios.get(process.env.REACT_APP_DOMAIN + `/api/v1/attendance/recaps?start=${start}&end=${end}&job=${job || ''}&unit=${unit || ''}`, { headers: { "Authorization": `Bearer ${this.state.session}` } })
            .then((res) => {
                const attendanceRecap = res.data.data;
                setTimeout(() => {
                    this.setState({ attendanceRecap }, () => this.setState({ loading: false }))
                }, 500)
            })
            .catch(error => {
                console.log(error.response)
            });
    }

    handleChangeDateStart = date => {
        if (date.getTime() < this.state.dateEnd.getTime()) {
            this.setState({
                date,
                start: this.formatDate(date)
            });
        }
        else {
            this.setState({
                date: new Date(this.state.dateEnd.getTime() - 1 * 24 * 60 * 60 * 1000),
                start: this.formatDate(new Date(this.state.dateEnd.getTime() - 1 * 24 * 60 * 60 * 1000))
            })
        }
    }

    handleChangeDateEnd = date => {
        if (date.getTime() > this.state.date.getTime()) {
            this.setState({
                dateEnd: date,
                end: this.formatDate(date)
            });
        }
        else {
            this.setState({
                dateEnd: new Date(this.state.date.getTime() + 1 * 24 * 60 * 60 * 1000),
                end: this.formatDate(new Date(this.state.date.getTime() + 1 * 24 * 60 * 60 * 1000))
            })
        }
    }

    onChangeUnit = (e) => {
        this.setState({
            unit: e.target.value
        });
    }

    onChangeJob = (e) => {
        this.setState({
            job: e.target.value
        });
    }

    filterData = () => {
        this.setState({ loading: true })
        this.getDataFromAPI(this.state.start, this.state.end, this.state.unit, this.state.job)
    }

    formatDate = (date) => {
        let d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    render() {
        return (
            <div className="animated fadeIn">
                <h4 className="content-title mb-4">Rekapitulasi Presensi</h4>
                <div className="content">
                    <Row className="justify-content-start align-items-center md-company-header mb-3">
                        <Col xs="6" md="2" >
                            {/* <h5 className="content-sub-title mb-0">Daftar Karyawan</h5> */}
                            <FormGroup>
                                <Label htmlFor="startDate" className="input-label">Dari</Label>
                                <DatePickerInput
                                    name="startDate"
                                    id="startDate"
                                    onChange={this.handleChangeDateStart}
                                    value={this.state.date}
                                    className='my-custom-datepicker-component'
                                    showOnInputClick={true}
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="6" md="2" >
                            <FormGroup>
                                <Label htmlFor="endDate" className="input-label">Hingga</Label>
                                <DatePickerInput
                                    name="endDate"
                                    id="endDate"
                                    onChange={this.handleChangeDateEnd}
                                    value={this.state.dateEnd}
                                    className='my-custom-datepicker-component'
                                    showOnInputClick={true}
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                    readOnly
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="6" md="2" >
                            <FormGroup>
                                <Label htmlFor="unit" className="input-label">Unit</Label>
                                <Input type="select" id="unit" name="unit"
                                    value={this.state.unit}
                                    onChange={this.onChangeUnit} >
                                    <option value={0}>{"Semua"}</option>
                                    {this.state.units.map((data, idx) => {
                                        return (
                                            <option key={idx} value={data.id}>{data.name}</option>
                                        );
                                    })}
                                </Input>
                            </FormGroup>
                        </Col>
                        <Col xs="6" md="2" >
                            <FormGroup>
                                <Label htmlFor="job" className="input-label">Jabatan</Label>
                                <Input type="select" id="unit" name="unit"
                                    value={this.state.job}
                                    onChange={this.onChangeJob} >
                                    <option value={0}>{"Semua"}</option>
                                    {this.state.jobs.map((data, idx) => {
                                        return (
                                            <option key={idx} value={data.id}>{data.name}</option>
                                        );
                                    })}
                                </Input>
                            </FormGroup>
                        </Col>
                        <Col xs="6" md="2" >
                            <div className="">
                                <Button type="submit" className="mt-sm-3" color="netis-color" style={{ width: '60px' }} onClick={this.filterData}>
                                    {this.state.loading ? <Spinner color="light" size="sm" /> : 'Filter'}
                                </Button>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" lg="12">
                            <Table responsive className="table-multilevel">
                                <thead>
                                    <tr>
                                        <th className="text-center w-10">No.</th>
                                        <th className="w-20">Nama</th>
                                        <th className="text-center">Hari Kerja</th>
                                        <th className="text-center">Masuk On Time</th>
                                        <th className="text-center">Pulang On Time</th>
                                        <th className="text-center w-20">Aksi</th>
                                    </tr>
                                    {/* <tr>
                                        <th className="text-center">On Time</th>
                                        <th className="text-center">Lokasi</th>
                                        <th className="text-center">On Time</th>
                                        <th className="text-center">Lokasi</th>
                                    </tr> */}
                                </thead>
                                <tbody>
                                    {this.state.attendanceRecap.length > 0 ?
                                        this.state.attendanceRecap.map((data, idx) => {
                                            return (
                                                <tr key={idx}>
                                                    <td className="text-center">{idx + 1}</td>
                                                    <td className="">{data.fullName}</td>
                                                    <td className="text-center">{data.countAttendance}</td>
                                                    <td className="text-center">{`${data.clockIn.onTime}`}</td>
                                                    <td className="text-center">{`${data.clockOut.onTime}`}</td>
                                                    <td className="text-center">
                                                        <Link to={`/attendance/recap/${data.personnelId}`} className="personnel-name semi-bold">
                                                            Detail
                                                        </Link>
                                                    </td>
                                                </tr>
                                            );
                                        })
                                        :
                                        <tr key={"no_data"}>
                                            <td className="text-center no-data-message" colSpan="7">No Data :(</td>
                                        </tr>
                                    }
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.token
    }
}
export default connect(mapStateToProps)(AttendanceRecap);
