import React, { memo, useCallback, useEffect, useMemo, useState } from 'react'
import moment from 'moment';
import request from '../../../../utils/request';
import { translate, t } from 'react-switch-lang';
import { Badge, Button, Col, Form, FormGroup, Input, Label, Modal, ModalBody, Row, Spinner, Table } from 'reactstrap';
import { DatePickerInput } from 'rc-datepicker';
import LoadingAnimation from '../../../../components/LoadingAnimation';
import ReactInputMask from 'react-input-mask';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import * as Yup from "yup";

const today = moment().format('YYYY-MM-DD');
const weekAgo = moment().subtract(10, 'd').format('YYYY-MM-DD');

function AttendanceRecapNew() {
    const [loading, setLoading] = useState(false)
    const [date, setDate] = useState({ start: weekAgo, end: today });
    const [filterName, setFilterName] = useState('');
    const [dataDate, setDataDate] = useState(null);
    const [holiday, setHoliday] = useState(null);
    const [dataAbsensi, setDataAbsensi] = useState([]);
    const [currentFilters, setCurrentFilters] = useState({ start: weekAgo, end: today });

    const getAPI = useCallback(() => {
        setLoading(true)
        return request
            .get(`v1/attendance/admin?start=${date.start}&end=${date.end}`)
            .then((res) => {
                setDataAbsensi(res.data.data.absensi)
                setHoliday(res.data.data.holidays)
                setDataDate(res.data.data.dates)
            })
            .catch((err) => {
                console.log(err)
            })
            .finally(() => setLoading(false))
        // eslint-disable-next-line
    }, [date])

    useEffect(() => {
        getAPI()
    }, [getAPI])

    const handleFilterChange = useCallback((event) => {
        const { name, value } = event.target;
        setCurrentFilters((states) => {
            const updates = { [name]: value };
            if (name === 'start') {
                const momentStart = moment(value);
                const momentEnd = moment(states.end);
                if (momentStart.isAfter(momentEnd, 'day')) {
                    updates.end = value;
                }
            } else if (name === 'end') {
                const momentEnd = moment(value);
                const momentStart = moment(states.start);
                if (momentEnd.isBefore(momentStart, 'day')) {
                    updates.start = value;
                }
            }

            return { ...states, ...updates };
        });
    }, [])

    const handleChangeDateStart = useCallback((_, formattedDate) => {
        handleFilterChange({ target: { name: 'start', value: formattedDate } });
    }, [handleFilterChange]);
    const handleChangeDateEnd = useCallback((_, formattedDate) => {
        handleFilterChange({ target: { name: 'end', value: formattedDate } });
    }, [handleFilterChange]);
    const filterByNamaKaryawan = (keyword) => {
        setFilterName(keyword)
    }

    // console.log(dataAbsensi)
    const filteredEmployee = useMemo(() => {
        return dataAbsensi.filter(item => item.userFullName?.toLowerCase().indexOf(filterName.toLowerCase()) >= 0);
    }, [dataAbsensi, filterName])

    if (loading) {
        return <LoadingAnimation />
    }


    return (
        <div className="animated fadeIn" >
            <h4 className="content-title mb-4">{t('Rekapitulasi Presensi')}</h4>
            <div className="content">
                <Row className="md-company-header mb-4">
                    <Col xs="6" md="3" >
                        <Label htmlFor="start" className="input-label">{t('dari')}</Label>
                        <DatePickerInput
                            name="start"
                            id="start"
                            onChange={handleChangeDateStart}
                            value={currentFilters.start}
                            className='my-custom-datepicker-component'
                            showOnInputClick={true}
                            closeOnClickOutside={true}
                            displayFormat="DD MMMM YYYY"
                            returnFormat="YYYY-MM-DD"
                            readOnly
                            maxDate={today}
                        />
                    </Col>
                    <Col xs="6" md="3" >
                        <Label htmlFor="end" className="input-label">{t('hingga')}</Label>
                        <DatePickerInput
                            name="end"
                            id="end"
                            onChange={handleChangeDateEnd}
                            value={currentFilters.end}
                            className='my-custom-datepicker-component'
                            showOnInputClick={true}
                            closeOnClickOutside={true}
                            displayFormat="DD MMMM YYYY"
                            returnFormat="YYYY-MM-DD"
                            readOnly
                            maxDate={today}
                        />
                    </Col>
                    <Col xs="4" md="3" >
                        <div className="pt-2">
                            <Button type="submit" className="mt-sm-4" color="netis-color" style={{ width: '100%' }} disabled={loading} onClick={() => setDate(currentFilters)}>
                                {loading ? <Spinner color="light" size="sm" /> : 'Filter'}
                            </Button>
                        </div>
                    </Col>
                </Row>
            </div>
            <Table size="sm" className="table-responsive mt-3 table-recap">
                <thead className="text-center">
                    <tr>
                        <th className="column-header table-recap-corner" style={{ minWidth: 170, backgroundColor: "#f5f6fa" }} rowSpan="2">
                            {t('nama')}
                            <Input type="text" className="mt-1" id="nama-karyawan" onKeyUp={(e) => filterByNamaKaryawan(e.target.value)} placeholder={t('Cari nama') + '...'} />
                        </th>
                        <th className="column-header table-recap-top" colSpan={dataDate?.length ?? 15}>{t('tanggal')}</th>
                    </tr>
                    <tr>
                        {dataDate && dataDate?.map((day, idx) => (
                            <React.Fragment key={idx}>
                                <th key={idx} style={{ minWidth: 235 }} className={`column-header table-recap-head ${holiday?.find(item => item.date === day) ? 'border-bottom-red' : ''}`}>
                                    {moment(day).format('LL')}
                                    {holiday?.find(item => item.date === day) ?
                                        <Badge color="danger">{holiday?.find(item => item.date === day).description}</Badge>
                                        : <div>&nbsp;</div>
                                    }
                                </th>
                                {/* <th key={idx} style={{ minWidth: 235 }} className='border-bottom-red table-recap-head'>

                                </th> */}
                            </React.Fragment>
                        ))}
                    </tr>
                    {/* <tr>
                        {dataDate && dataDate?.map((day, idx) => {
                            const isHoliday = holiday?.find(item => item.date === day)
                            return(
                                <th key={idx} style={{ minWidth: 235 }} className='border-bottom-red table-recap-head'>
                                    {isHoliday && <Badge color="danger">{isHoliday.description}</Badge>}
                                </th>
                            )
                        })}
                    </tr> */}
                </thead>
                <tbody>
                    {filteredEmployee && filteredEmployee.map((absen, index) => (
                        <tr key={index}>
                            <td className="px-2 py-4 text-center table-recap-first-td" style={{ backgroundColor: "#f5f6fa" }}>
                                <b>{absen.userFullName}</b><br />
                                - {absen.job} -<br />
                                ({absen.unit})
                            </td>
                            {dataDate && dataDate?.map((day, idx) => {
                                const recapData = absen.data.find(item => item.date === day)
                                const isHoliday = holiday?.find(item => item.date === day)
                                return (
                                    <td key={idx}
                                        className={`text-center
                                            ${isHoliday ? `red-border` : ``}
                                            ${(isHoliday && index === 0) ? `border-top-red` : ``}
                                            ${(isHoliday && index === filteredEmployee.length - 1) ? `border-bottom-red` : ``}
                                        `}
                                    >
                                        <RecapModal isHoliday={isHoliday} data={recapData} personnelId={absen.personnelId} getAPI={getAPI} date={day} />
                                    </td>
                                )
                            })}
                        </tr>
                    ))}
                </tbody>
            </Table>
        </div >
    )
}

const RecapModal = memo(({ data, personnelId, getAPI, date, isHoliday }) => {
    const [editType, setEditType] = useState(null);
    const [manageIn, setManageIn] = useState(null);
    const [manageOut, setManageOut] = useState(null);
    const [manageIdx, setManageIdx] = useState(null);
    const [action, setAction] = useState(null);
    const [loadingModal, setLoadingModal] = useState(false)
    const inTime = useMemo(() => data?.in.split("\n") ?? [], [data])
    const inId = useMemo(() => data?.in_id.split("\n") ?? [], [data])
    const inNote = useMemo(() => data?.in_note.split("\n") ?? [], [data])
    const outTime = useMemo(() => data?.out.split("\n") ?? [], [data])
    const outId = useMemo(() => data?.out_id.split("\n") ?? [], [data])
    const outNote = useMemo(() => data?.out_note.split("\n") ?? [], [data])

    const ValidationFormSchema = useMemo(() => {
        return Yup.object().shape({
            in: Yup.string()
            .test('mustLower', t('Waktu presensi masuk tidak boleh lebih atau sama dengan waktu presensi keluar'), function(value){
                if(value){
                    if(this.parent.out && !outTime[manageIdx]?.includes("+")){
                        const inValue = value.replace(':', '')
                        const outValue = this.parent.out.replace(':', '')
                        return inValue < outValue;
                    }
                }
                return true;
            })
            .test('multiDataMustLower', t('Presensi keluar sudah dilakukan setelah jam tersebut'), function(value){
                const trueCondition = value && (inTime.length > 1) && (manageIdx > 0)
                if(trueCondition){
                    const inValue = value.replace(':', '')
                    const outValue = outTime[manageIdx-1].replace(':', '')
                    return inValue > outValue;
                }
                return true;
            }),
            out: Yup.string()
            .test('mustHigher', t('Waktu presensi keluar tidak boleh kurang atau sama dengan waktu presensi masuk'), function(value){
                if(value){
                    if(this.parent.in && this.parent.date === date){
                        const outValue = value.replace(':', '')
                        const inValue = this.parent.in.replace(':', '')
                        return inValue < outValue;
                    }
                }
                return true;
            })
            .test('multiDataMustHigher', t('Presensi masuk sudah dilakukan sebelum jam tersebut'), function(value){
                const trueCondition = value && (outTime.length > 1) && (manageIdx > 0) && inTime[manageIdx+1]
                if(trueCondition){
                    const outValue = value.replace(':', '')
                    const inValue = inTime[manageIdx+1]?.replace(':', '')
                    return inValue > outValue;
                }
                return true;
            })
        })
    },[outTime, manageIdx, date, inTime])

    const { values, touched, errors, ...formik } = useFormik({
        initialValues: {
            date: '',
            in: '',
            out: '',
            note: ''
        },
        validationSchema: ValidationFormSchema,
        onSubmit: (values, { setSubmitting }) => {
            let type;
            let id;
            let clock;
            setSubmitting(true)
            setLoadingModal(true)
            if (editType === "in") {
                type = values.in
                id = inId[manageIdx]
                clock = "clockin"
            }
            else {
                type = values.out
                id = outId[manageIdx]
                clock = "clockout"
            };
            if (action === "put") {
                request.put(`v1/attendance/recaps/${id}`, {
                    note: values.note,
                    date: moment(values.date).format("YYYY-MM-DD"),
                    time: type + ":00"
                })
                    .then(() => {
                        toast.success(t('Berhasil mengubah data rekap absensi'))
                        setAction(null)
                        setManageIdx(null)
                        setManageIn(null)
                        setManageOut(null)
                        setEditType(null)
                        formik.handleReset()
                        getAPI();
                    })
                    .catch((err) => {
                        console.log(err)
                        toast.error(t('terjadikesalahan'))
                        return;
                    })
                    .finally(() => {
                        setLoadingModal(false)
                        setSubmitting(false)
                    })
            }
            else if (action === "post") {
                request.post(`v1/attendance/recaps/${clock}`, {
                    personnelId: personnelId,
                    note: values.note,
                    date: moment(values.date).format("YYYY-MM-DD"),
                    time: type + ":00"
                })
                    .then(() => {
                        toast.success(t('Berhasil mengubah data rekap absensi'))
                        setAction(null)
                        setManageIdx(null)
                        setManageIn(null)
                        setManageOut(null)
                        setEditType(null)
                        formik.handleReset()
                        getAPI();
                    })
                    .catch((err) => {
                        console.log(err)
                        toast.error(t('terjadikesalahan'))
                        return;
                    })
                    .finally(() => {
                        setLoadingModal(false)
                        setSubmitting(false)
                    })
            }
        }
    })

    useEffect(() => {
        if (editType) {
            if (outTime && outTime[manageIdx]?.includes("+")) {
                const outSplit = outTime[manageIdx].split("+")
                formik.setFieldValue('out', outSplit[0])
            }
            else {
                formik.setFieldValue('out', outTime[manageIdx])
            }
            if (editType === "in") {
                formik.setFieldValue('note', inNote[manageIdx])
                formik.setFieldValue('date', date)
            }
            else if (editType === "out") {
                formik.setFieldValue('note', outNote[manageIdx])
                if (outTime && outTime[manageIdx]?.includes("+")) {
                    const outSplit = outTime[manageIdx].split("+")
                    const dateNew = moment(date).add(outSplit[1], 'day')
                    formik.setFieldValue('date', dateNew)
                }
                else {
                    formik.setFieldValue('date', date)
                }
            }
        }
        // eslint-disable-next-line
    }, [editType, manageIdx])

    const onDatepickerChange = function (val) {
        formik.setFieldTouched('date', true);
        formik.setFieldValue('date', val instanceof Date && !isNaN(val) ? val : undefined, true);
    };
    // const onDatepickerClear = React.useCallback(function () {
    //     formik.setFieldTouched('date', true);
    //     formik.setFieldValue('date', undefined, true);
    // }, [formik]);

    return (
        <>
            {!data ?
                <div className="d-flex justify-content-center align-items-center" style={{ position: "relative", width: '100%', height: '100px', verticalAlign: "middle" }}>
                    <button type="button" className="btn btn-link"
                        onClick={() => {
                            setManageIdx(1)
                            setEditType("in")
                            setManageIn(1)
                            // formik.setFieldValue('in', inTime[idx])
                            formik.setFieldValue('date', date)
                            setAction('post')
                        }}
                    >
                        <small className="text-muted">{t('Tidak ada riwayat absensi')}</small>
                    </button>
                </div> :
                <Table hover borderless>
                    <thead>
                        <tr>
                            <th>{t('Masuk')}</th>
                            <th>{t('Keluar')}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {inTime.map((data, idx) => (
                            <tr key={idx} style={{ width: "100%" }}>
                                <td>
                                    <button type="button" className="btn btn-link"
                                        onClick={() => {
                                            setManageIdx(idx)
                                            setEditType("in")
                                            setManageIn(idx + 1)
                                            formik.setFieldValue('in', inTime[idx])
                                            formik.setFieldValue('date', date)
                                            if (inTime[idx].length === 0 || !inTime[idx]) {
                                                setAction('post')
                                            }
                                            else if (inTime[idx].length > 0) {
                                                setAction('put')
                                            }
                                        }}
                                    >
                                        {(inTime[idx].length === 0 || !inTime[idx]) ? <><small><i>({t('Belum diisi')})</i></small><span className="required"> *</span></> : inTime[idx]}
                                    </button>
                                </td>
                                <td>
                                    <button type="button" className="btn btn-link"
                                        onClick={() => {
                                            setManageIdx(idx)
                                            setEditType("out")
                                            setManageOut(idx + 1)
                                            formik.setFieldValue('in', inTime[idx])
                                            // formik.setFieldValue('out', outTime[idx])
                                            if (outTime[idx].length === 0 || !outTime[idx]) {
                                                setAction('post')
                                            }
                                            else if (outTime[idx].length > 0) {
                                                setAction('put')
                                            }
                                        }}
                                    >
                                        {(outTime[idx].length === 0 || !outTime[idx]) ? <><small><i>({t('Belum diisi')})</i></small><span className="required"> *</span></> : outTime[idx]}
                                    </button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            }
            {/* Modal absen masuk */}
            <Modal
                className="right"
                isOpen={Boolean(manageIn)}
                toggle={() => {
                    setAction(null)
                    setManageIdx(null)
                    setManageIn(null)
                    setEditType(null)
                    formik.handleReset();
                }}
            >
                <ModalBody>
                    <Form onSubmit={formik.handleSubmit}>
                        <Row className="mb-5">
                            <Col col="12" sm="12" md="12">
                                <h5>{t('Kelola Rekapitulasi Presensi Masuk')}</h5>
                                {/* <h6>Tanggal : {moment(data.date).format('LL')}</h6> */}
                                <hr />
                                <FormGroup>
                                    <Label htmlFor="date" className="input-label">{t('Tanggal Presensi Masuk')}</Label>
                                    <DatePickerInput
                                        // onClear={onDatepickerClear}
                                        showOnInputClick={true}
                                        closeOnClickOutside={true}
                                        onChange={onDatepickerChange}
                                        value={values.date}
                                        className='my-custom-datepicker-component'
                                        displayFormat="LL"
                                        disabled={true}
                                        readOnly
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="in" className="input-label">{t('Waktu Masuk')}</Label>
                                    <ReactInputMask id="in" name="in" className="form-control pr-2/5" mask="99:99"
                                        placeholder={t('Masukkan waktu masuk')}
                                        // disabled={editType === 'out'}
                                        value={values.in}
                                        onChange={formik.handleChange}
                                    />
                                    {touched.in && errors.in && (
                                        <small className="text-danger">{errors.in}</small>
                                    )}
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="note" className="input-label">{t('Catatan')}</Label>
                                    <Input type="textarea" value={values.note} name="note" id="note" rows="5" onChange={formik.handleChange} placeholder={t('deskripsiketerangan')} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Button
                            disabled={loadingModal}
                            className="mr-2"
                            color="light"
                            onClick={() => {
                                setAction(null)
                                setManageIdx(null)
                                setManageIn(null)
                                setEditType(null)
                                formik.handleReset();
                            }}
                        >
                            {t("batal")}
                        </Button>
                        <Button
                            disabled={loadingModal}
                            type="submit"
                            color="netis-primary"
                            // onClick={() => {
                            //     console.log(outTime[manageIdx])
                            //     console.log(values.date)
                            // }}
                        >
                            {loadingModal ? <><Spinner size="sm" color="light" /> Loading...</> : t("simpan")}
                        </Button>
                    </Form>
                </ModalBody>
            </Modal>
            {/* Modal absen keluar */}
            <Modal
                className="right"
                isOpen={Boolean(manageOut)}
                toggle={() => {
                    setAction(null)
                    setManageIdx(null)
                    setManageOut(null)
                    setEditType(null)
                    formik.handleReset();
                }}
            >
                <ModalBody>
                    <Form onSubmit={formik.handleSubmit}>
                        <Row className="mb-5">
                            <Col col="12" sm="12" md="12">
                                <h5>{t('Kelola Rekapitulasi Presensi Keluar')}</h5>
                                {/* <h6>Tanggal : {moment(data.date).format('LL')}</h6> */}
                                <hr />
                                <FormGroup>
                                    <Label htmlFor="date" className="input-label">{t('Tanggal Presensi Keluar')}</Label>
                                    <DatePickerInput
                                        // onClear={onDatepickerClear}
                                        showOnInputClick={true}
                                        closeOnClickOutside={true}
                                        onChange={onDatepickerChange}
                                        value={values.date}
                                        className='my-custom-datepicker-component'
                                        displayFormat="LL"
                                        readOnly
                                        minDate={values.date}
                                        maxDate={moment(values?.date).add(1, 'd')}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="out" className="input-label">{t('Waktu Keluar')}</Label>
                                    <ReactInputMask id="out" name="out" className="form-control pr-2/5" mask="99:99"
                                        placeholder={t('Masukkan waktu keluar')}
                                        // disabled={editType === 'in'}
                                        value={values.out}
                                        onChange={formik.handleChange}
                                    />
                                    {touched.out && errors.out && (
                                        <small className="text-danger">{errors.out}</small>
                                    )}
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="note" className="input-label">{t('Catatan')}</Label>
                                    <Input type="textarea" value={values.note} name="note" id="note" rows="5" onChange={formik.handleChange} placeholder={t('deskripsiketerangan')} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Button
                            disabled={loadingModal}
                            className="mr-2"
                            color="light"
                            onClick={() => {
                                setAction(null)
                                setManageIdx(null)
                                setManageOut(null)
                                setEditType(null)
                                formik.handleReset();
                            }}
                        >
                            {t("batal")}
                        </Button>
                        <Button
                            disabled={loadingModal}
                            type="submit"
                            color="netis-primary"
                            // onClick={() => {
                            //     console.log(outTime[manageIdx])
                            //     console.log(values.date)
                            // }}
                        >
                            {loadingModal ? <><Spinner size="sm" color="light" /> Loading...</> : t("simpan")}
                        </Button>
                    </Form>
                </ModalBody>
            </Modal>
        </>
    )
})

export default translate(AttendanceRecapNew);