import React from 'react'
import { Link, useLocation } from 'react-router-dom'
import {translate, t} from 'react-switch-lang'
import { Col, Nav, NavItem, NavLink, Row, TabContent, TabPane } from 'reactstrap'
import AttendanceHistoryAll from './History/AttendanceHistoryAll'
import AttendanceRecapNew from './Recapitulation/AttendanceRecapNew'

const tabs = {
    history: t('Riwayat Presensi'),
    recap: t('Rekapitulasi Presensi')
}
const tabsArray = Object.keys(tabs)

function AttendanceWrapper(){
    const location = useLocation();
    const selectedTab = location.hash ? location.hash.substring(1) : tabsArray[0];

    return (
        <div>
            <div className="d-flex bd-highlight mb-3">
                <div className="mr-auto bd-highlight">
                </div>
            </div>
            <Nav tabs className="mx-3 tour-jobtab">
                {tabsArray.map(tab => (
                    <NavItem key={tab}>
                        <NavLink tag={Link} className="pt-2/5" active={selectedTab === tab} replace to={{ hash: "#" + tab }}>
                            {tabs[tab]}
                        </NavLink>
                    </NavItem>
                ))}
            </Nav>
            <TabContent activeTab={selectedTab}>
                <TabPane tabId="history">
                    <Row>
                        <Col sm="12">
                            <AttendanceHistoryAll />
                        </Col>
                    </Row>
                </TabPane>
                <TabPane tabId="recap">
                    <Row>
                        <Col sm="12">
                            <AttendanceRecapNew />
                        </Col>
                    </Row>
                </TabPane>
            </TabContent>
        </div>
    )
}

export default translate(AttendanceWrapper);