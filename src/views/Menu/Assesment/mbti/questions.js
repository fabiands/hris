
export default [
    {
      "a": "Saya lebih menyukai hal spontan, fleksibel, dan tidak terikat waktu",
      "b": "Saya lebih menyukai hal terencana dan memiliki deadline yang jelas",
      "lang": "id",
      "code": "q1"
    },
    {
      "a": "Saya lebih suka menyampaikan ide atau pendapat dengan menulis",
      "b": "Saya lebih suka menyampaikan ide atau pendapat dengan berbicara langsung",
      "lang": "id",
      "code": "q2"
    },
    {
      "a": "Saya tidak menyukai hal-hal yang bersifat mendadak dan di luar perencanaan",
      "b": "Saya tidak masalah dengan hal yang terjadi secara mendadak",
      "lang": "id",
      "code": "q3"
    },
    {
      "a": "Saya lebih mengutamakan kepentingan umum",
      "b": "Saya lebih mengutamakan kepentingan pribadi",
      "lang": "id",
      "code": "q4"
    },
    {
      "a": "Saya senang menemukan dan mengembangkan ide dengan mendiskusikannya",
      "b": "Saya menemukan dan mengembangkan ide dengan merenungkan",
      "lang": "id",
      "code": "q5"
    },
    {
      "a": "Saya memulai dari gambaran umum baru ke detail",
      "b": "Saya memulai dari detail ke gambaran umum",
      "lang": "id",
      "code": "q6"
    },
    {
      "a": "Saya berorientasi pada dunia eksternal (kegiatan, orang)",
      "b": "Saya berorientasi pada dunia internal (memori, pemikiran, ide)",
      "lang": "id",
      "code": "q7"
    },
    {
      "a": "Saya senang membicarakan permasalahan hari ini dan langkah-langkah praktis mengatasinya",
      "b": "Saya senang membicarakan visi masa depan dan konsep-konsep mengenai visi tersebut",
      "lang": "id",
      "code": "q8"
    },
    {
      "a": "Saya mudah diyakinkan dengan penjelasan yang menyentuh perasaan",
      "b": "Saya mudah diyakinkan dengan penjelasan yang masuk akal",
      "lang": "id",
      "code": "q9"
    },
    {
      "a": "Saya fokus pada sedikit hobi namun mendalam",
      "b": "Saya fokus pada banyak hobi secara luas dan umum",
      "lang": "id",
      "code": "q10"
    },
    {
      "a": "Saya adalah pribadi yang tertutup dan mandiri",
      "b": "Saya senang bersosial dan ekspresif",
      "lang": "id",
      "code": "q11"
    },
    {
      "a": "Bagi saya, aturan, jadwal dan target sangat mengikat dan membebani",
      "b": "Bagi saya, aturan, jadwal dan target sangat membantu saya dalam mengambil tindakan",
      "lang": "id",
      "code": "q12"
    },
    {
      "a": "Saya menggunakan pengalaman sebagai pedoman",
      "b": "Saya menggunakan imajinasi dan perenungan sebagai pedoman",
      "lang": "id",
      "code": "q13"
    },
    {
      "a": "Saya lebih banyak berfokus pada tugas dan uraian tugasnya",
      "b": "Saya lebih menyukai untuk membangun relasi dengan orang lain",
      "lang": "id",
      "code": "q14"
    },
    {
      "a": "Bagi saya, bertemu orang lain dan aktivitas sosial adalah hal yang melelahkan",
      "b": "Bagi saya, bertemu orang dan aktivitas sosial adalah hal yang menyenangkan",
      "lang": "id",
      "code": "q15"
    },
    {
      "a": "SOP sangat membantu saya",
      "b": "Bagi saya, SOP sangat membosankan",
      "lang": "id",
      "code": "q16"
    },
    {
      "a": "Saya mengambil keputusan berdasarkan logika dan aturan main",
      "b": "Saya mengambil keputusan berdasarkan perasaan pribadi dan kondisi orang lain",
      "lang": "id",
      "code": "q17"
    },
    {
      "a": "Saya pribadi yang bebas dan terbuka terhadap perubahan",
      "b": "Saya pribadi yang terikat dan berdasarkan ketentuan",
      "lang": "id",
      "code": "q18"
    },
    {
      "a": "Saya berorientasi pada hasil",
      "b": "Saya berorientasi pada proses",
      "lang": "id",
      "code": "q19"
    },
    {
      "a": "Menurut saya, beraktifitas sendirian di rumah menyenangkan",
      "b": "Menurut saya, beraktifitas sendirian di rumah membosankan",
      "lang": "id",
      "code": "q20"
    },
    {
      "a": "Saya membiarkan orang lain bertindak bebas asalkan tujuan tercapai",
      "b": "Saya suka mengatur orang lain dengan tata tertib agar tujuan tercapai",
      "lang": "id",
      "code": "q21"
    },
    {
      "a": "Saya memilih ide inspiratif lebih penting daripada fakta",
      "b": "Saya memilih fakta lebih penting daripada ide inspiratif",
      "lang": "id",
      "code": "q22"
    },
    {
      "a": "Saya mengemukakan tujuan dan sasaran terlebih dahulu",
      "b": "Saya mengemukakan kesepakatan terlebih dahulu",
      "lang": "id",
      "code": "q23"
    },
    {
      "a": "Saya fokus pada target dan mengabaikan hal-hal baru",
      "b": "Saya memperhatikan hal-hal baru dan siap menyesuaikan diri serta mengubah target",
      "lang": "id",
      "code": "q24"
    },
    {
      "a": "Saya mengutamakan keberlanjutan dan kestabilan",
      "b": "Saya mengutamakan perubahan dan variasi",
      "lang": "id",
      "code": "q25"
    },
    {
      "a": "Pendirian saya masih bisa berubah tergantung situasi nantinya",
      "b": "Saya berpegang teguh pada pendirian",
      "lang": "id",
      "code": "q26"
    },
    {
      "a": "Saya bertindak step by step dengan perencanaan yang jelas",
      "b": "Saya bertindak dengan semangat tanpa menggunakan perencanaan yang jelas",
      "lang": "id",
      "code": "q27"
    },
    {
      "a": "Saya berinisiatif tinggi hampir dalam berbagai hal meskipun tidak berhubungan dengan pribadi",
      "b": "Saya berinisiatif bila situasi memaksa atau berhubungan dengan kepentingan pribadi",
      "lang": "id",
      "code": "q28"
    },
    {
      "a": "Saya lebih menyukai tempat yang tenang dan pribadi untuk berkonsentrasi",
      "b": "Saya lebih menyukai tempat yang ramai dan banyak interaksi / aktifitas",
      "lang": "id",
      "code": "q29"
    },
    {
      "a": "Saya mampu menganalisa dengan baik",
      "b": "Saya berempati",
      "lang": "id",
      "code": "q30"
    },
    {
      "a": "Saya berpikir secara matang sebelum bertindak",
      "b": "Saya berani bertindak tanpa terlalu lama berfikir",
      "lang": "id",
      "code": "q31"
    },
    {
      "a": "Saya menghargai seseorang karena sifat dan perilakunya",
      "b": "Saya menghargai seseorang karena skill dan faktor teknis",
      "lang": "id",
      "code": "q32"
    },
    {
      "a": "Saya merasa nyaman bila situasi tetap terbuka terhadap pilihan-pilihan lain",
      "b": "Saya merasa tenang bila semua sudah ditentukan",
      "lang": "id",
      "code": "q33"
    },
    {
      "a": "Saya menarik kesimpulan dengan lama dan hati-hati",
      "b": "Saya menarik kesimpulan dengan cepat sesuai naluri",
      "lang": "id",
      "code": "q34"
    },
    {
      "a": "Saya senang mengekspresikan semangat",
      "b": "Saya menyimpan semangat dalam hati",
      "lang": "id",
      "code": "q35"
    },
    {
      "a": "Saya memahami teori sebelum mempraktekkannya",
      "b": "Saya lebih menyukai learning by doing",
      "lang": "id",
      "code": "q36"
    },
    {
      "a": "Saya menganggap melibatkan perasaan itu tidak profesional",
      "b": "Saya tidak menyukai pekerjaan yang kaku dan terlalu terikat pada peraturan",
      "lang": "id",
      "code": "q37"
    },
    {
      "a": "Saya mencari kesempatan untuk berkomunikasi secara perorangan",
      "b": "Saya nyaman berkomunikasi pada sekelompok orang",
      "lang": "id",
      "code": "q38"
    },
    {
      "a": "Saya mementingkan situasi yang harmonis",
      "b": "Saya mementingkan tujuan tercapai",
      "lang": "id",
      "code": "q39"
    },
    {
      "a": "Saya menganggap ketidakpastian itu seru, menegangkan dan membuat hati lebih senang",
      "b": "Saya menganggap ketidakpastian membuat bingung dan meresahkan",
      "lang": "id",
      "code": "q40"
    },
    {
      "a": "Saya berfokus pada masa kini (apa yang bisa diperbaiki sekarang)",
      "b": "Saya berfokus pada masa depan (apa yang mungkin dicapai di masa depan)",
      "lang": "id",
      "code": "q41"
    },
    {
      "a": "Saya lebih sering mempertanyakan segala sesuatu",
      "b": "Saya senang menyediakan sesuatu untuk memenuhi kebutuhan",
      "lang": "id",
      "code": "q42"
    },
    {
      "a": "Saya mengamati dan mengingat detail secara konsisten ",
      "b": "Saya mengamati dan mengingat detail hanya bila berhubungan dengan pola",
      "lang": "id",
      "code": "q43"
    },
    {
      "a": "Saya lebih bersemangat saat tugas yang saya kerjakan sudah mendekati deadline",
      "b": "Saya menjadi mudah tertekan saat tugas yang saya kerjakan sudah mendekati deadline",
      "lang": "id",
      "code": "q44"
    },
    {
      "a": "Saya lebih suka komunikasi tidak langsung (telp, surat, e-mail)",
      "b": "Saya lebih suka komunikasi langsung (tatap muka)",
      "lang": "id",
      "code": "q45"
    },
    {
      "a": "Saya menyukai praktik langsung",
      "b": "Saya memegang erat nilai teoritis",
      "lang": "id",
      "code": "q46"
    },
    {
      "a": "Saya tidak menyukai perubahan",
      "b": "Saya menyukai perubahan yang dinamis",
      "lang": "id",
      "code": "q47"
    },
    {
      "a": "Saya sering dianggap keras kepala",
      "b": "Saya sering dianggap terlalu memihak",
      "lang": "id",
      "code": "q48"
    },
    {
      "a": "Saya bersemangat saat menolong orang keluar dari kesalahan dan meluruskan",
      "b": "Saya bersemangat saat mengkritik dan menemukan kesalahan",
      "lang": "id",
      "code": "q49"
    },
    {
      "a": "Saya bertindak sesuai situasi dan kondisi yang terjadi saat itu",
      "b": "Saya bertindak sesuai apa yang sudah direncanakan",
      "lang": "id",
      "code": "q50"
    },
    {
      "a": "Saya menggunakan keterampilan yang sudah dikuasai",
      "b": "Saya menyukai tantangan untuk menguasai keterampilan baru",
      "lang": "id",
      "code": "q51"
    },
    {
      "a": "Saya memikirkan ide saat berbicara",
      "b": "Saya memikirkan ide secara matang lalu membicarakannya",
      "lang": "id",
      "code": "q52"
    },
    {
      "a": "Saya memilih cara yang sudah ada dan sudah terbukti",
      "b": "Saya memilih cara yang unik dan belum dipraktekkan orang lain",
      "lang": "id",
      "code": "q53"
    },
    {
      "a": "Saya merencanakan segala sesuatu dengan baik",
      "b": "Saya mengalir sesuai kondisi",
      "lang": "id",
      "code": "q54"
    },
    {
      "a": "Menurut saya, standar harus ditegakkan di atas segalanya (itu menunjukkan kehormatan dan harga diri)",
      "b": "Menurut saya, perasaan manusia lebih penting dari sekadar standar (yang adalah benda mati)",
      "lang": "id",
      "code": "q55"
    },
    {
      "a": "Saya menjadikan checklist sebagai panduan",
      "b": "Saya tidak menyukai daftar dan checklist",
      "lang": "id",
      "code": "q56"
    },
    {
      "a": "Saya menuntut perlakuan yang adil dan sama pada semua orang",
      "b": "Saya menuntut perlakuan khusus sesuai karakteristik masing-masing orang",
      "lang": "id",
      "code": "q57"
    },
    {
      "a": "Sya mementingkan sebab-akibat",
      "b": "Saya mementingkan nilai-nilai personal",
      "lang": "id",
      "code": "q58"
    },
    {
      "a": "Saya puas ketika mampu beradaptasi dengan momentum yang terjadi",
      "b": "Saya puas ketika mampu menjalankan semuanya sesuai rencana",
      "lang": "id",
      "code": "q59"
    },
    {
      "a": "Saya adalah pribadi yang spontan, Easy Going, fleksibel",
      "b": "Saya pribadi yang berhati-hati, penuh pertimbangan, kaku",
      "lang": "id",
      "code": "q60"
    }
  ]