import React, { useMemo, useState } from "react";
import {
    Button,
    Card,
    CardBody,
    Row,
    Col,
    Modal,
    ModalBody,
    ModalHeader
} from "reactstrap";
import { t } from "react-switch-lang";
import { arrayShuffle } from "../../../../utils/array";

const AssessmentPalmistry = ({ result }) => {
    const data = result?.answers?.data;
    const [modalPalmistry, setModalPalmistry] = useState(false);
    const togglePalmistry = () => setModalPalmistry(!modalPalmistry);
  
    const karakterList = useMemo(() => {
      const arrKarakter = Object.values(data.result);
      return arrayShuffle(arrKarakter);
    }, [data.result]);
  
    return (
      <Card>
        <CardBody>
          <Row className="md-company-header mb-3 mt-2">
            <Col>
              <h5 className="text-uppercase content-sub-title mb-0">
                {t("Hasil Palmistry")}
              </h5>
            </Col>
          </Row>
          <Row>
            <Col md="6" lg="6" className="mb-2">
              <img
                src={Object.values(data.result).length > 0 ? data?.processed : data?.raw}
                width="100%"
                alt="palmistry"
              />
            </Col>
            <Col md="6" lg="6">
              {Object.values(data.result).length > 0 ? (
                <>
                  <div className="mb-3">
                    {t("Tipe Data Palmistry")} : <br />
                    <br />
                    {Object.keys(data?.result ?? {}).map((data, idx) => (
                      <ul key={idx}>
                        <li>{data}</li>
                      </ul>
                    ))}
                  </div>
                  <div className="d-flex flex-row-reverse">
                    <Button
                      className="btn btn-netis-color"
                      onClick={togglePalmistry}
                      style={{ borderRadius: "8px" }}
                    >
                        {t("lihatkarakteristik")}
                    </Button>
                  </div>
                </>
              ) : (
                  <div className="alert alert-dark">
                    {t("Belum ada hasil Palmistry yang tersedia")}
                  </div>
                )}
            </Col>
          </Row>
        </CardBody>
  
        <Modal isOpen={modalPalmistry} toggle={togglePalmistry} className="modal-lg">
          <ModalHeader toggle={togglePalmistry}>{t("Detail Palmistry")}</ModalHeader>
          <ModalBody>
            {t("Bentuk Garis Tangan")} : <br />
            <br />
            {Object.keys(data?.result ?? {}).map((data, idx) => (
              <ul key={idx}>
                <li>{data}</li>
              </ul>
            ))}
            {t("Karakteristik")} : <br />
            <br />
            {karakterList.map((ciriDetail, idx) => (
              <ul key={idx}>
                <li>{ciriDetail}</li>
              </ul>
            ))}
          </ModalBody>
        </Modal>
      </Card>
    );
  };

export default AssessmentPalmistry;
