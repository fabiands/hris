
import React, { useState, useEffect } from 'react'
import { Card, CardBody, Row, Col } from 'reactstrap';
import moment from 'moment';
import ReactMarkdown from 'react-markdown';
import { t } from 'react-switch-lang';

const ZodiacResult = ({ result }) => {
    const zodiac = result.type_description;
    const [key, setKey] = useState(null);

    useEffect(() => {
        const zodiacKey = zodiac?.key.split(", ");
        if (zodiacKey.length >= 8) {
          const chunkLength = Math.max(zodiacKey.length / 2, 1);
          let chunks = [];
          for (var i = 0; i < 2; i++) {
            if (chunkLength * (i + 1) <= zodiacKey.length)
              chunks.push(zodiacKey.slice(chunkLength * i, chunkLength * (i + 1)));
          }
          setKey(chunks);
        }
        else if (zodiacKey.length < 8) {
          setKey(zodiacKey);
        }
      // eslint-disable-next-line
    }, [zodiac])
    return (
        <Card>
          <CardBody>
                <h5 className="text-uppercase content-sub-title mb-2">
                  ZODIAC - {zodiac.title}
                </h5>
                <div className="mb-2">
                  {t("harilahir")} : &nbsp;
                  {moment(result.answers).format("dddd")}
                </div>
                <div>
              <h3>Deskripsi</h3>
              <ReactMarkdown source={zodiac.description} />
              <hr />
              <h3>Ciri Khas</h3>
            {key && zodiac?.key.split(", ").length >= 8 ?
              <Row>
                <Col>
                  <ul>
                    {key && key[0]?.map(keyFirst => {
                      return (
                        <li className="text-capitalize">{keyFirst}</li>
                      )
                    })}
                  </ul>
                </Col>
                <Col>
                  <ul>
                    {key && key[1]?.map(keySecond => {
                      return (
                        <li className="text-capitalize">{keySecond}</li>
                      )
                    })}
                  </ul>
                </Col>
              </Row>
              :
              <ul>
                {key && key?.map(item => {
                  return (
                    <li className="text-capitalize">{item}</li>
                  )
                })}
              </ul>
            }
            <hr />
            <h3 className="text-capitalize">Sugesti Diri</h3>
            <ReactMarkdown source={zodiac?.sugestion} />
            <hr />
            <h3>Partner</h3>
            <ul>
              {zodiac.purchased && zodiac?.partner.map(item => {
                return (
                  <li>{item}</li>
                )
              })}
            </ul>
            <hr />
            <div className="row">
              <div className="col-6">
                <h5>{t("Sifat Positif")}</h5>
                <ReactMarkdown source={zodiac.positive_traits} />
              </div>
              <div className="col-6">
                <h5>{t("Sifat Negatif")}</h5>
                <ReactMarkdown source={zodiac.negative_traits} />
              </div>
            </div>
            </div>
          </CardBody>
        </Card>
    );
  };

export default ZodiacResult

