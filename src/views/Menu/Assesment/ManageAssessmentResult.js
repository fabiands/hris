import React, { memo, useCallback, useMemo } from "react";
import { Button, Modal, ModalBody, Spinner, Nav, NavItem, NavLink, TabContent, TabPane } from "reactstrap";
import { useEffect, useState } from "react";
import request from "../../../utils/request";
import { Link, useLocation, useRouteMatch } from "react-router-dom";
import LoadingAnimation from "../../../components/LoadingAnimation";
import * as moment from 'moment';
import { toast } from 'react-toastify';
import { t, translate } from 'react-switch-lang';
import { useUserPrivileges } from "../../../store";
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../components/ResponsiveTable';
import DataNotFound from "../../../components/DataNotFound";
import useSWR from "swr";
import { arrayGroupBy } from "../../../utils/array";

const testNames = {
    'mbti': 'MBTI',
    'papikostick': 'PAPI Kostick',
    'disc': 'DISC',
    'fisiognomi': 'Fisiognomi',
    'palmistry': 'Palmistry',
    'bazi': 'Bazi',
    'shio': 'Shio',
    'zodiac': 'Zodiac',
}

const testNamesArray = Object.keys(testNames);

function ManageAssessmentResult(props) {
    const routeMatch = useRouteMatch();

    const [deletingId, setDeletingId] = useState(null);
    const [deleteLoading, setDeleteLoading] = useState(false);
    const { can } = useUserPrivileges();
    const location = useLocation();
    const selectedTestName = location.hash ? location.hash.substring(1) : testNamesArray[0];
    const [filters, setFilters] = useState({ name: '' });
    const { data: assessmentResultsResponse, error: assessmentResultsError, mutate } = useSWR('v1/assessment/results');
    const loading = !assessmentResultsResponse && !assessmentResultsError;
    const resultsPerTestName = useMemo(() => arrayGroupBy(assessmentResultsResponse?.data?.data ?? [], 'testName'), [assessmentResultsResponse]);

    const handleFilterChange = useCallback((newFilters) => {
        setFilters(newFilters);
    }, [])

    const handleDeleteClick = useCallback((id) => {
        setDeletingId(id)
    }, [])

    if (loading) {
        return <div className="animated fadeIn">
            <LoadingAnimation />
        </div>
    }

    async function doDelete() {
        if (deleteLoading) return;

        try {
            setDeleteLoading(true);
            await request.delete('v1/assessment/results/' + deletingId);
            await mutate((response) => {
                return { 
                    ...response,
                    data: {
                        ...response.data,
                        data: response.data.data.filter(r => r.id !== deletingId)
                    }
                };
            }, false);
            setDeletingId(null);
            toast.success(t('Berhasil dihapus'));
        } catch (err) {
            toast.error(t('Terjadi kesalahan'));
        } finally {
            setDeleteLoading(false);
        }
    }

    // const personnelFilter = personnelResults
    //     .filter((result) => filters.name ? result.userFullName.toUpperCase().includes(filters.name.toUpperCase()) : true)
    //     .filter((result) => filters.testName === result.testName)
    //     ;

    return <div>
        <AssesmentFilter onChange={handleFilterChange} defaultFilter={filters} />
        <div className="card">
            <Nav tabs>
                {testNamesArray.map(testName => (
                    <NavItem key={testName}>
                        <NavLink tag={Link} className="pt-2/5" active={selectedTestName === testName} to={{ pathname: routeMatch.path ,hash: "#" + testName}} 
                            // onClick={() => setFilters(state => ({...state, testName }))}
                            // onClick={() => setFilters(state => ({...state, testName }))}
                        >
                            {testNames[testName]}
                        </NavLink>
                    </NavItem>
                ))}
            </Nav>
            <TabContent activeTab={selectedTestName} className="card-body p-0">
                {testNamesArray.map(testName => (
                    <TabPane className="animated fadeIn" key={testName} tabId={testName}>
                        <Rtable className="mb-0" size="sm">
                            <Rthead>
                                <Rtr>
                                    <Rth className="text-center w-5">No.</Rth>
                                    <Rth className="text-left">{t('nama')}</Rth>
                                    <Rth className="text-center">{t('jenistes')}</Rth>
                                    <Rth className="text-center">{t('hasil')}</Rth>
                                    <Rth className="text-left">{t('tanggaltes')}</Rth>
                                    <Rth className="text-center"></Rth>
                                </Rtr>
                            </Rthead>
                            <Rtbody>
                                <AssessmentTableRow data={resultsPerTestName[testName]} filters={filters} can={can} onDeleteClick={handleDeleteClick} />
                            </Rtbody>
                        </Rtable>
                    </TabPane>
                ))}
            </TabContent>
        </div>
        <Modal isOpen={!!deletingId} toggle={() => {
            if (!deleteLoading) {
                setDeletingId(null)
            }
        }}>
            <ModalBody>
                <h6>{t('yakinmenghapus')}</h6>
                <div className="d-flex justify-content-end">
                    {!deleteLoading && <Button className="mr-2" color="light" onClick={() => setDeletingId(null)}>{t('batal')}</Button>}
                    <Button color="netis-primary" onClick={doDelete} disabled={deleteLoading}>
                        {deleteLoading ? <React.Fragment><Spinner size="sm" color="light" /> menghapus...</React.Fragment> : t('hapus')}
                    </Button>
                </div>
            </ModalBody>
        </Modal>
    </div>
}


const AssesmentFilter = memo(({ onChange, defaultFilter })  => {
    const [searchName, setSearchName] = useState(defaultFilter?.name ?? '');

    useEffect(() => {
        onChange((state) => ({ ...state, name: searchName }));
    }, [onChange, searchName])

    const handleSearchNameChange = useCallback((event) => {
        setSearchName(event.target.value);
    }, [])


    return <div className="form-row">
        <div className="form-group col-sm-4">
            <label htmlFor="searchName" className="input-label mb-1">{ t('Cari nama') }</label>
            <input className="form-control" id="searchName" value={searchName} onChange={handleSearchNameChange} placeholder={ t('Cari nama') } />
        </div>
    </div>
});

const AssessmentTableRow = memo(({ data = [], filters, can, onDeleteClick }) => {
    let filtered = data;
    if (filters.name.trim()) {
        filtered = filtered.filter((result) => result.userFullName.toUpperCase().includes(filters.name.toUpperCase()));
    }
    if (filtered.length <= 0) {
        return (
            <Rtr>
                <Rtd colSpan="9999"><DataNotFound/></Rtd>
            </Rtr>
        );
    }
    return <>
    {filtered.map((r, idx) => (
        <Rtr key={idx}>
            <Rtd className="text-center">{idx + 1}</Rtd>
            <Rtd>
                <Link to={"/personnels/" + r.personnelId} className={`personnel-name${can('read-employee') ? '' : ' disabled'}`}>
                    {r.userFullName}
                </Link>
            </Rtd>
            <Rtd className="text-center">{testNames[r.testName] ?? r.testName}</Rtd>
            {/* <td className="text-center">{r.result?.toUpperCase()}</td> */}
            {  r.testName === 'disc' ?
                (() => {
                    const parse = JSON.parse(r.result)
                    const arrResult = Object.values(parse).flat()
                    const setResult = Array.from(new Set(arrResult)).join(", ")
                    return(
                        <Rtd>
                            {setResult}
                        </Rtd>
                    )
                })()
                :
                ['fisiognomi', 'palmistry'].includes(r.testName) ?
                <Rtd className="text-center">-</Rtd>
                :
                <Rtd className="text-center">{r.result?.toUpperCase()}</Rtd>
            }
            <Rtd className="text-left text-nowrap">
                {moment(r.created_at).format('DD MMM YYYY')}<br />
                ({moment(r.created_at).format('HH:mm')})
            </Rtd>
            <Rtd className="text-nowrap">
                <Link to={'/assessment/results/' + r.id}>
                    <Button color="netis-color" className={`${can('read-assessment-result') ? '' : ' d-none'}`}>{t('detail')}</Button>
                </Link>
                <Button className={`${can('delete-assessment-result') ? '' : ' d-none'}`} color="netis-danger ml-2" onClick={() => onDeleteClick(r.id)}><i className="fa fa-trash-o"></i></Button>
            </Rtd>
        </Rtr>))
        }
    </>
})


export default translate(ManageAssessmentResult);
