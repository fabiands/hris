import React from 'react'
import { Card, CardBody } from 'reactstrap';
import moment from 'moment';
import ReactMarkdown from 'react-markdown';
import { t } from 'react-switch-lang';

const BaziResult = ({ result }) => {
    const bazi = result.type_description
    return (
        <Card>
          <CardBody>
                <h5 className="text-uppercase content-sub-title mb-2">
                  BAZI - {bazi.title}
                </h5>
                <div className="mb-2">
                  {t("harilahir")} : &nbsp;
                  {moment(result.answers).format("dddd")}
                </div>
                <div>
              <h3>Deskripsi</h3>
              <ReactMarkdown source={bazi.description} />
              <hr />
              <h3>Karakter Ketika Bekerja</h3>
              <ReactMarkdown source={bazi.working_character} />
              <hr />
              <h3>Saran Pekerjaan</h3>
              <ReactMarkdown source={bazi.job_sugestion} />
              <hr />
              <div className="row">
                <div className="col-6">
                  <h5>{t("Sifat Positif")}</h5>
                  <ReactMarkdown source={bazi.positive_traits} />
                </div>
                <div className="col-6">
                  <h5>{t("Sifat Negatif")}</h5>
                  <ReactMarkdown source={bazi.negative_traits} />
                </div>
              </div>
            </div>
          </CardBody>
        </Card>
    );
  };

export default BaziResult

