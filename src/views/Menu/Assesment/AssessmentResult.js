import React, { useEffect, useState } from "react";
import LoadingAnimation from "../../../components/LoadingAnimation";
import MbtiResult from "./mbti/MbtiResult";
import PapikostickResult from "./papikostick/PapikostickResult";
import DiscResultAdmin from "./disc/DiscResultAdmin";
import { Button } from "reactstrap";
import request from "../../../utils/request";
import * as moment from 'moment';
import BaziResult from "./bazi/BaziResult";
import ShioResult from "./shio/ShioResult";
import ZodiacResult from "./zodiac/ZodiacResult";
import AssessmentFisiognomi from "./fisiognomi/AssessmentFisiognomi";
import AssessmentPalmistry from "./palmistry/AssessmentPalmistry";

function AssessmentResult(props) {
    const { id: resultId } = props.match.params
    const [loading, setLoading] = useState(true);
    const [result, setResult] = useState(null);

    useEffect(() => {
        request.get('v1/assessment/results/' + resultId).then(res => {
            setResult(res.data.data);
        }).finally(() => setLoading(false))
    }, [resultId]);

    if (loading) {
        return <LoadingAnimation />
    }
    if (result && ['mbti', 'papikostick', 'disc', 'fisiognomi', 'palmistry', 'bazi', 'shio', 'zodiac'].includes(result.testName)) {
        return <div>
            <div className="mb-3 d-flex align-items-center">
                <Button size="sm" onClick={props.history.goBack} color="outline-netis-primary"><i className="fa fa-chevron-left mr-1"></i></Button>
                <h3 className="h4 mb-0 ml-2">{result.userFullName}</h3>
                <span class="text-muted ml-auto">{moment(result.created_at).format('DD MMMM YYYY HH:mm')}</span>
            </div>
            <hr className="mb-3" />
            {
                result.testName === 'mbti' ? <MbtiResult result={result} />
                : result.testName === 'papikostick' ? <PapikostickResult result={result} isAdmin={true} />
                : result.testName === 'disc' ? <DiscResultAdmin result={result} isAdmin={true} />
                : result.testName === 'fisiognomi' ? <AssessmentFisiognomi result={result} isAdmin={true} />
                : result.testName === 'palmistry' ? <AssessmentPalmistry result={result} isAdmin={true} />
                : result.testName === 'bazi' ? <BaziResult result={result}/>
                : result.testName === 'shio' ? <ShioResult result={result}/>
                : result.testName === 'zodiac' ? <ZodiacResult result={result}/>
                : undefined
            }
        </div>
    }

    return <div className="text-center">
        <h2>Page Not Found</h2>
        <Button onClick={props.history.goBack} color="netis-primary"><i className="fa fa-backward mr-2"></i> Kembali</Button>
    </div>
}

export default AssessmentResult;
