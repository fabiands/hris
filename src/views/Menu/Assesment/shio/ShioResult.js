
import React from 'react'
import { Card, CardBody } from 'reactstrap';
import moment from 'moment';
import ReactMarkdown from 'react-markdown';
import { t } from 'react-switch-lang';

const ShioResult = ({ result }) => {
    const shio = result.type_description;
    const combinationShio = Object.keys(shio.combination);
    return (
        <Card>
          <CardBody>
                <h5 className="text-uppercase content-sub-title mb-2">
                  SHIO - {shio.title}
                </h5>
                <div className="mb-2">
                  {t("harilahir")} : &nbsp;
                  {moment(result.answers).format("dddd")}
                </div>
                <div>
              <h3>Deskripsi</h3>
              <ReactMarkdown source={shio.description} />
              <hr />
              <div>
                <h3>Partner <small className="text-capitalize">({combinationShio[0]} & {combinationShio[1]})</small></h3><br />
                <h4 className="text-capitalize"><u>{combinationShio[0]}</u></h4>
                <ReactMarkdown source={shio.combination[combinationShio[0]]} />
                <h4 className="text-capitalize"><u>{combinationShio[1]}</u></h4>
                <ReactMarkdown source={shio.combination[combinationShio[1]]} />
                <hr />
                <div className="row">
                <div className="col-6">
                    <h5>{t("Sifat Positif")}</h5>
                    <ReactMarkdown source={shio.positive_traits} />
                </div>
                <div className="col-6">
                    <h5>{t("Sifat Negatif")}</h5>
                    <ReactMarkdown source={shio.negative_traits} />
                </div>
                </div>
            </div>
            </div>
          </CardBody>
        </Card>
    );
  };

export default ShioResult

