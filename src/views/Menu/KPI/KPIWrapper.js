import React from 'react';
import { lazyComponent as lazy } from '../../../components/lazyComponent';
import { Switch, Redirect, withRouter } from 'react-router-dom';
import {
    translate,
} from 'react-switch-lang';
import { connect } from 'react-redux';
import AuthRoute from '../../../components/AuthRoute';
const KPIUserWrapper = lazy(() => import('./KPIUserWrapper'));
const KPIMenu = lazy(() => import('./KPIMenu'));
const KPISetting = lazy(() => import('./KPISetting'));
const KPISettingDetail = lazy(() => import('./Setting/KPIDetail'));
const KPIEntry = lazy(() => import('./KPIEntry'));
const KPIEntryDetail = lazy(() => import('./Entry/KPIEntryDetail'));

const PANEL_USER = '3';
const PANEL_ADMIN = '2';

function getRoutes(props) {
    const { match, user, menu } = props;
    const routes = [];

    if (menu === PANEL_ADMIN) {
        routes.push({
            path: match.path,
            component: KPIMenu,
            // privileges: [""],
            exact: true,
        })
        routes.push({
            path: match.path + "/setting",
            component: KPISetting,
            exact: true,
        })
        routes.push({
            path: match.path + "/setting/:id",
            component: KPISettingDetail,
            exact: true,
        })

        routes.push({
            path: match.path + "/entry",
            component: KPIEntry,
            exact: true,
        })

        routes.push({
            path: match.path + "/entry/:id",
            component: KPIEntryDetail,
            exact: true,
        })
    } else if (menu === PANEL_USER) {
        routes.push({
            path: match.path,
            component: KPIUserWrapper,
            exact: true,
        })
        if (user.hasBawahan) {
            routes.push({
                path: match.path + "/setting",
                component: KPISetting,
                exact: true,
            })
            routes.push({
                path: match.path + "/setting/:id",
                component: KPISettingDetail,
                exact: true,
            })

            routes.push({
                path: match.path + "/entry",
                component: KPIEntry,
                exact: true,
            })

            routes.push({
                path: match.path + "/entry/:id",
                component: KPIEntryDetail,
                exact: true,
            })
        }
    }

    return routes;
}

function KPIWrapper(props) {
    const routes = getRoutes(props);
    const { match } = props;

    return (
        <Switch>
            {routes.map(route => (
                <AuthRoute key={route.path} {...route} />
            ))}
            {routes[0] && <Redirect exact from={match.path} to={routes[0].path} />}
        </Switch>
    )
}


const mapStateToProps = ({ user, token, menu }) => ({ user, token, menu });
export default connect(mapStateToProps)(translate(withRouter(KPIWrapper)));
