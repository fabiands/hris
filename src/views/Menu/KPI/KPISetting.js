import React from 'react'
import { useLocation } from 'react-router';
import { Button, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap'
import { Link } from 'react-router-dom';
import { translate, t } from 'react-switch-lang'
// import KPIDepartment from "./Setting/KPIDepartment"
import KPIType from "./Setting/KPIType"
import KPIPeriodic from "./Setting/KPIPeriodic"
// import KPIStatus from "./Setting/KPIStatus"

function KPISetting(props) {
    const tabs = {
        // 'department': t('Departemen'),
        'type': t('Tipe KPI'),
        'periodic': t('Frekuensi KPI'),
        // 'status': t('Status KPI')
    }
    const tabsArray = Object.keys(tabs);
    const location = useLocation();
    const selectedTab = location.hash ? location.hash.substring(1) : tabsArray[0];

    return (
        <div className='animated fadeIn'>
            <div className="mr-auto bd-highlight my-3">
                <Button color="netis-color" onClick={() => props.history.goBack()}><i className="fa fa-chevron-left mr-2"></i> {t('kembali')} </Button>
            </div>
            <Nav tabs>
                {tabsArray.map(tab => (
                    <NavItem key={tab}>
                        <NavLink tag={Link} className="pt-2/5" active={selectedTab === tab} replace to={{ hash: "#" + tab }}>
                            {tabs[tab]}
                        </NavLink>
                    </NavItem>
                ))}
            </Nav>
            <TabContent activeTab={selectedTab}>
                {/* <TabPane tabId="department"><KPIDepartment /></TabPane> */}
                <TabPane tabId="type"><KPIType /></TabPane>
                <TabPane tabId="periodic"><KPIPeriodic /></TabPane>
                {/* <TabPane tabId="status"><KPIStatus /></TabPane> */}
            </TabContent>
        </div>
    )
}

export default translate(KPISetting)