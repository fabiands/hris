import React from 'react'
import { translate, t } from 'react-switch-lang'
import { Row, Col } from 'reactstrap'
import { Link } from 'react-router-dom'

function KPIMenu({ match, isUserPanel = false }) {

    const menus = [
        {
            link: match.path + '/setting',
            title: t('Pengaturan KPI'),
            image: require('../../../assets/img/payroll/setting.png'),
            // can: 'read-settings-payroll'
        },
        {
            link: match.path + '/entry',
            title: t('Entri KPI'),
            image: require('../../../assets/img/payroll/entry.png'),
            // can: 'read-entry-payroll'
        },
    ]

    return (
        <div className="animated fadeIn">
            <Row className={isUserPanel ? 'd-none' : 'd-block'}>
                <Col xs="6">
                    <h4 className="content-title mb-4">Key Performance Indicator (KPI)</h4>
                </Col>
            </Row>
            <div className="content-body">
                <Row>
                    {menus.map((item, idx) =>
                        // can(item.can) &&
                        <Col xs="6" md="4" lg="3" key={idx} className="link-card scale-div-small">
                            <Link to={item.link}>
                                <div className="card menu-item" style={{ borderRadius: 15 }}>
                                    <div className="card-body">
                                        <div className="mt-1 mb-3 text-center">
                                            <img src={item.image} width="100%" alt="" style={{ objectFit: 'cover' }} />
                                        </div>
                                        <div className="menu-title text-center mb-2">
                                            <p className="mb-0 title-menu-company">{item.title}</p>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </Col>
                    )}
                </Row>
            </div>
        </div >
    )
}

export default translate(KPIMenu)