import React, { useState } from 'react'
import { translate, t } from 'react-switch-lang'
import { Modal, ModalHeader, ModalBody, Card, CardBody, Row, Col, Label, CustomInput, Spinner } from 'reactstrap'
import request from '../../../../utils/request';

function ModalDetail({ isOpen, toggle, data, mutate }) {
    return (
        <Modal isOpen={isOpen} toggle={toggle} size="xl">
            <ModalHeader toggle={toggle} className="border-bottom-0" style={{ position: 'relative', backgroundColor: '#F5F6FA' }}>
                {t('Detail Task KPI')}
            </ModalHeader>
            <ModalBody style={{ backgroundColor: '#F5F6FA' }}>
                {data.map((item, idx) =>
                    <TaskComponent
                        key={idx}
                        idx={idx}
                        item={item}
                        mutate={mutate}
                    />
                )}
            </ModalBody>
        </Modal>
    )
}


function TaskComponent({ item, idx, mutate }) {
    const [checkbox, setCheckbox] = useState(item.task.status.status === 'Done' ? true : false)
    const [loading, setLoading] = useState(false)

    const handleChangeStatus = (id, status) => {
        setLoading(true)
        request.put(`v1/kpi/task/${id}/status`, { status })
            .then(() => mutate())
            .finally(() => setLoading(false))
    }

    const handleChangeCheckBox = (id, e) => {
        const { checked } = e.target;
        handleChangeStatus(id, checked)
        setCheckbox(checked)
    }

    return (
        <Card key={idx} className="mt-2 mb-3 border-0">
            <CardBody className="p-4 position-relative">
                {loading &&
                    <div className="w-100 h-100" style={{ position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, background: 'rgba(0,0,0, 0.5)', display: 'flex', justifyContent: 'center', alignItems: 'center', zIndex: 99 }}>
                        <Spinner style={{ width: 48, height: 48 }} />
                    </div>
                }
                <Row>
                    <Col xs="12" md="6">
                        <Label className="input-label">Task</Label>
                        <div>{item?.task?.name}</div>
                    </Col>
                    <Col xs="6" md="3">
                        <Label className="input-label">{t('tipe')}</Label>
                        <div>{item.task.type.name}</div>
                    </Col>
                    <Col xs="6" md="3">
                        <Label className="input-label">{t('Frekuensi')}</Label>
                        <div>{item.task.frequency.name}</div>
                    </Col>
                    <Col xs="12" md="6" className="mt-3">
                        <Label className="input-label">Notes</Label>
                        <div>{item.task.notes ?? '-'}</div>
                    </Col>
                    <Col xs="12" md="6" className="mt-3 d-flex align-items-center justify-content-end">
                        <div className="d-flex align-items-center">
                            <div className="mr-2" htmlFor={'status-' + item.id}>{item.task.status.status === 'Done' ? 'Done' : 'Due Date'}</div>
                            <CustomInput
                                id={'status-' + item.id}
                                className="ml-1"
                                type="checkbox"
                                checked={checkbox}
                                onChange={(e) => handleChangeCheckBox(item.id, e)}
                            />
                        </div>
                        <div>{item.task.dueDate}</div>
                    </Col>
                </Row>
            </CardBody>
        </Card>
    )
}

export default translate(ModalDetail)