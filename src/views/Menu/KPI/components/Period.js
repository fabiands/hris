import React, { useCallback, useMemo, useState } from 'react'
import { InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import Datepicker from "react-datepicker";
// import moment from 'moment';

function Period({ period, handleChangePeriod }) {
    return (
        <div className="d-flex justify-content-end mt-1 mb-3">
            <InputGroup className="float-right period-group">
                <InputGroupAddon addonType="prepend">
                    <InputGroupText className="input-group-transparent">
                        <i className="fa fa-calendar" />
                    </InputGroupText>
                </InputGroupAddon>
                <Datepicker
                    selected={period}
                    onChange={handleChangePeriod}
                    showMonthYearPicker
                    showFullMonthYearPicker
                    showFourColumnMonthYearPicker
                    dateFormatCalendar="MMMM"
                    calendarClassName="period-date"
                    dateFormat="MMMM yyyy"
                    placeholderText="Select a date"
                    className="form-control"
                />
            </InputGroup>
        </div>
    );
}

export default function usePeriod() {
    const [period, setPeriod] = useState(localStorage.getItem('period') ? new Date(localStorage.getItem('period')) : new Date());
    const handleChangePeriod = useCallback((e) => {
        if (!e) {
            let dateNow = new Date()
            setPeriod(dateNow)
            localStorage.setItem('period', dateNow)
        }
        if (e) {
            setPeriod(e)
            localStorage.setItem('period', e)
        }
    }, [])

    const PeriodComponent = useMemo(() => {
        return (props) => <Period
            period={period}
            handleChangePeriod={handleChangePeriod}
        />;
    }, [period, handleChangePeriod]);

    return { period, PeriodComponent };
}