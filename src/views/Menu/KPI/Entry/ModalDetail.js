import { useFormik } from 'formik'
import React, { useState, useEffect } from 'react'
import { translate, t } from 'react-switch-lang'
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Collapse, Spinner } from 'reactstrap'
import CardComponent from './CardComponent'
import CardCreate from './CardCreate'
import * as Yup from 'yup';
import { toast } from 'react-toastify'
import request from '../../../../utils/request'
import moment from 'moment'

function ModalDetail({ isOpen, toggle, data, dataFreq, dataType, id, mutate, period }) {
    const [isCreate, setIsCreate] = useState(false)
    const [isEdit, setIsEdit] = useState(false)
    const [type, setType] = useState(null)
    const [deletingData, setDeletingData] = useState(null)
    const [isDeleting, setIsDeleting] = useState(false)
    const setDelete = (value) => setDeletingData(value)

    const handleCreate = () => {
        setType('create')
        setIsCreate(true)
    }
    const handleEdit = () => {
        setType('edit')
        setIsEdit(true)
    }
    const handleCancel = () => {
        setIsCreate(false)
        setIsEdit(false)
        setType(null)
    }

    const ValidationFormSchema = () => {
        return Yup.array().of(
            Yup.object().shape({
                task: Yup.string().required().label(t("Task")),
                type: Yup.string().required().label(t("tipe")),
                freq: Yup.string().required().label(t("Frekuensi")),
                date: Yup.string().required().label(t("Waktu")),
                time: Yup.string().required().label(t("Jam")),
            })
        )
    }

    const { values, touched, errors, setValues, isSubmitting, ...formik } = useFormik({
        initialValues: [{
            id: '',
            task: '',
            type: '',
            freq: '',
            date: '',
            time: '',
            notes: ''
        }],
        validationSchema: ValidationFormSchema,
        onSubmit: (values, { setSubmitting }) => {
            setSubmitting(true)
            request.put(`v1/personnels/${id}/kpi/task`, values.map((v) => ({
                id: v.id,
                period: moment(period).format('MMYYYY'),
                name: v.task,
                typeId: v.type.value,
                frequencyId: v.freq.value,
                dueDate: v.date + " " + v.time,
                notes: v.notes
            })))
                .then(() => {
                    toast.success(t('Berhasil Mengubah Task'))
                    setIsEdit(false)
                    setType(null)
                    // toggle()
                    mutate()
                })
                .catch(() => {
                    toast.error(t('Gagal Mengubah Task'))
                    return;
                })
                .finally(() => setSubmitting(false))
        }
    })

    useEffect(() => {
        setValues(data.map((v) => (
            {
                id: v.id,
                task: v.task.name,
                type: { value: v.task.type.id, label: v.task.type.name },
                freq: { value: v.task.frequency.id, label: v.task.frequency.name },
                date: moment(v.task.dueDate).format('YYYY-MM-DD'),
                time: moment(v.task.dueDate).format('HH:mm'),
                notes: v.task.notes
            }
        )))
    }, [data, setValues])

    const handleDelete = () => {
        setIsDeleting(true)
        request.delete(`v1/kpi/task/${deletingData.id}`)
            .then(() => {
                toast.success(t('Berhasil menghapus komponen'))
                setDeletingData(null)
                setIsEdit(false)
                setType(null)
                // toggle()
                mutate()
            })
            .catch(() => {
                toast.error(t('Gagal menghapus komponen'))
            })
            .finally(() => setIsDeleting(false))
    }

    return (
        <div className="modal-extra-large">
            <Modal isOpen={isOpen} toggle={toggle} size="xl">
                <ModalHeader toggle={toggle} className="border-bottom-0" style={{ position: 'relative', backgroundColor: '#F5F6FA' }}>
                    {t('Detail Task KPI')}
                </ModalHeader>
                <ModalBody className="pt-0" style={{ backgroundColor: '#F5F6FA' }}>
                    {!type &&
                        <div className="d-flex justify-content-end">
                            <Button color="outline-netis-primary" className="mr-1 btn-sm" onClick={handleEdit}>
                                <i className='fa fa-pencil mr-1' />Edit
                            </Button>
                            <Button color="netis-primary" className="ml-1 btn-sm" onClick={handleCreate}>
                                <i className='fa fa-plus mr-1' />{t('tambah')}
                            </Button>
                        </div>
                    }
                    <Collapse isOpen={isCreate} className="mb-3">
                        {isCreate &&
                            <CardCreate
                                dataType={dataType}
                                dataFreq={dataFreq}
                                handleCancel={handleCancel}
                                id={id}
                                mutate={mutate}
                                toggle={toggle}
                                period={period}
                            />
                        }
                    </Collapse>
                    {values.map((item, idx) =>
                        <CardComponent
                            key={idx}
                            idx={idx}
                            dataType={dataType}
                            dataFreq={dataFreq}
                            values={item}
                            setValues={setValues}
                            isEdit={isEdit}
                            type={type}
                            touched={touched}
                            errors={errors}
                            isSubmitting={isSubmitting}
                            setDelete={setDelete}
                        />
                    )}
                </ModalBody>
                <ModalFooter style={{ backgroundColor: '#F5F6FA' }} className="border-top-0">
                    {isEdit &&
                        <>
                            <Button disabled={isSubmitting} color="outline-netis-primary" className="mr-1 btn-sm" onClick={handleCancel}>
                                {t('Batal')}
                            </Button>
                            <Button disabled={isSubmitting} color="netis-primary" className="ml-1 btn-sm" onClick={formik.handleSubmit}>
                                {isSubmitting ? <Spinner color="light" /> : 'Submit'}
                            </Button>
                        </>
                    }
                </ModalFooter>
            </Modal>
            <Modal isOpen={deletingData ? true : false} toggle={() => setDeletingData(null)} style={{ marginTop: '20vh' }}>
                <ModalHeader toggle={() => setDeletingData(null)}>
                    {t('Konfirmasi Penghapusan Task')}
                </ModalHeader>
                <ModalBody className="py-3">
                    {t('Apakah anda yakin ingin menghapus task')}&nbsp;
                    <b>{deletingData?.task}</b>&nbsp;?
                </ModalBody>
                <ModalFooter>
                    <Button disabled={isDeleting} color="outline-netis-primary" className="mr-1 btn-sm" onClick={() => setDeletingData(null)}>
                        {t('Batal')}
                    </Button>
                    <Button disabled={isDeleting} color="netis-primary" className="ml-1 btn-sm" onClick={handleDelete}>
                        {isDeleting ? <Spinner color="light" /> : t('Hapus')}
                    </Button>
                </ModalFooter>
            </Modal>
        </div>
    )
}

export default translate(ModalDetail)