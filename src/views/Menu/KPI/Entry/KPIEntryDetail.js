import React, { useEffect, useState, useMemo } from 'react'
import { translate, t } from 'react-switch-lang'
import { Card, CardBody, Row, Col, Button, Badge } from 'reactstrap'
import { useAuthUser } from '../../../../store'
import request from '../../../../utils/request'
import Select from 'react-select'
import { DatePickerInput } from 'rc-datepicker';
import useSWR from 'swr'
import LoadingAnimation from '../../../../components/LoadingAnimation'
import DataNotFound from '../../../../components/DataNotFound'
import moment from 'moment'
import ModalDetail from './ModalDetail'
import usePeriod from '../components/Period'
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../../components/ResponsiveTable'

function KPIEntryDetail({ match }) {
    const [openModal, setOpenModal] = useState(false)
    const { period, PeriodComponent } = usePeriod();
    const { data: dataResponse, error, mutate } = useSWR(`v1/personnels/${match.params.id}/kpi/task?period=${moment(period).format('MMYYYY')}`)
    const loading = !dataResponse && !error
    const data = useMemo(() => {
        if (dataResponse) {
            return dataResponse.data.data;
        }
        return [];
    }, [dataResponse])
    const [filterFreq, setFilterFreq] = useState(null)
    const [filterStart, setFilterStart] = useState(null)
    const [filterEnd, setFilterEnd] = useState(null)
    const [dataFreq, setDataFreq] = useState([])
    const [dataType, setDataType] = useState([])
    const user = useAuthUser()

    const handleDetailTask = () => {
        setOpenModal(!openModal)
    }

    useEffect(() => {
        request.get('v1/kpi/frequency')
            .then((res) => {
                let response = res.data.data.map(item =>
                    ({ value: item.id, label: item.name })
                )
                setDataFreq(response)
            })
    }, [])

    useEffect(() => {
        request.get('v1/kpi/type')
            .then((res) => {
                let response = res.data.data.map(item =>
                    ({ value: item.id, label: item.name })
                )
                setDataType(response)
            })
    }, [])

    const filtered = useMemo(() => {
        let filterData = data
            ?.filter(item =>
                filterFreq ? item.task.frequency.id === filterFreq : true
            )
            ?.filter(item =>
                filterStart ? moment(item.task.dueDate).isSameOrAfter(moment(filterStart)) : true
            )
            ?.filter(item =>
                filterEnd ? moment(item.task.dueDate).isSameOrBefore(moment(filterEnd)) : true
            )
        return filterData;
    }, [data, filterFreq, filterStart, filterEnd])

    return (
        <div className='animated fadeIn kpi-entry'>
            <PeriodComponent />
            <Card className="my-3 border-0 w-100" style={{ backgroundColor: '#f5f6fa' }}>
                <CardBody className="p-4">
                    <Row className="text-left">
                        <Col md="6">
                            <Row>
                                <Col xs="3" className="my-1">{t('Nama')}</Col><Col xs="9"> : {user.fullName}</Col>
                                <Col xs="3" className="my-1">Email</Col><Col xs="9"> : {user.email}</Col>
                            </Row>
                        </Col>
                        <Col md="6">
                            <Row>
                                <Col xs="3" className="my-1">{t('Jabatan')}</Col><Col xs="9"> : {user.jabatan}</Col>
                                <Col xs="3" className="my-1">{t('Periode KPI')}</Col><Col xs="9"> : {moment(period).format('MMMM YYYY')} </Col>
                            </Row>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
            <h5>{t('Sortir berdasarkan')}</h5>
            <Row>
                <Col xs="6" md="4" className="mt-1 mb-3">
                    <Select
                        name="freq"
                        id="freq"
                        className="div-freq"
                        options={dataFreq}
                        isDisabled={dataFreq.length < 1}
                        onChange={(e) => setFilterFreq(e?.value ?? null)}
                        isClearable={true}
                    />
                </Col>
                <Col xs="12" className="my-1">
                    <Row>
                        <Col md="4" className="my-1">
                            <DatePickerInput
                                readOnly
                                showOnInputClick={true}
                                name="start"
                                id="start"
                                placeholder={t('Tanggal Mulai')}
                                onChange={(value) => setFilterStart(value)}
                                value={filterStart}
                                className='my-custom-datepicker-component'
                                closeOnClickOutside={true}
                                displayFormat="DD MMMM YYYY"
                                maxDate={filterEnd}
                            />
                        </Col>
                        <Col md="1" className="my-1 date-wrapper">
                            <small>{t('Sampai dengan')}</small>
                        </Col>
                        <Col md="4" className="my-1">
                            <DatePickerInput
                                readOnly
                                showOnInputClick={true}
                                name="end"
                                id="end"
                                placeholder={t('Tanggal Selesai')}
                                onChange={(value) => setFilterEnd(value)}
                                value={filterEnd}
                                className='my-custom-datepicker-component'
                                closeOnClickOutside={true}
                                displayFormat="DD MMMM YYYY"
                                minDate={filterStart}
                            // maxDate={new Date()}
                            />
                        </Col>
                        <Col md="3" className="button-detail">
                            <Button color="netis-color" onClick={handleDetailTask}>
                                {t('Detail Task')}
                            </Button>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Rtable size="sm">
                <Rthead>
                    <Rtr>
                        <Rth>No.</Rth>
                        <Rth>{t('tipe')}</Rth>
                        <Rth>{t('Frekuensi')}</Rth>
                        <Rth>Task</Rth>
                        <Rth>{t('Batas pengerjaan Task')}</Rth>
                        <Rth>Status</Rth>
                        <Rth>{t('Catatan')}</Rth>
                    </Rtr>
                </Rthead>
                <Rtbody>
                    {loading ? <Rtr><Rtd colSpan={7}><LoadingAnimation /></Rtd></Rtr> :
                        (error || filtered.length) < 1 ? <Rtr><Rtd colSpan={7}><DataNotFound /></Rtd></Rtr> :
                            filtered?.map((item, idx) =>
                                <Rtr key={idx}>
                                    <Rtd>{idx + 1}</Rtd>
                                    <Rtd>{item?.task?.type?.name}</Rtd>
                                    <Rtd>{item?.task?.frequency?.name}</Rtd>
                                    <Rtd>{item?.task?.name}</Rtd>
                                    <Rtd>{moment(item.task?.dueDate).format('DD MMMM YYYY')}</Rtd>
                                    <Rtd>
                                        <Badge color={item?.task?.status.status === 'Done'
                                            ? 'success'
                                            : (item?.task?.status.status === 'Failed' ? 'danger' : 'info')
                                        }>
                                            {item?.task?.status.status}
                                            {item?.task?.status.status === 'Done' && item?.task?.status.date
                                                ? ' at ' + moment(item?.task?.status.date).format('DD MMMM YYYY')
                                                : null
                                            }
                                        </Badge>
                                    </Rtd>
                                    <Rtd>{item.task.notes === '' ? '-' : item.task.notes}</Rtd>
                                </Rtr>
                            )}
                </Rtbody>
            </Rtable>
            <ModalDetail id={match.params.id} mutate={mutate} isOpen={openModal} toggle={handleDetailTask} data={filtered} dataFreq={dataFreq} dataType={dataType} period={period} />
        </div>
    )
}

export default translate(KPIEntryDetail)