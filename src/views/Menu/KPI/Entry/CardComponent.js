import React from 'react'
import { translate, t } from 'react-switch-lang'
import { Card, CardBody, Row, Col, Label, Input, Button } from 'reactstrap'
import Select from 'react-select'
import { DatePickerInput } from 'rc-datepicker';
import ReactInputMask from 'react-input-mask';
import moment from 'moment';

function CardComponent({ values, idx, dataType, dataFreq, isEdit, setValues, setDelete, touched, errors, isSubmitting }) {

    const changeTask = (e, i) => {
        setValues(old => [...old].map(v => {
            if (v.id === i) return { ...v, task: e.target.value }
            return { ...v };
        }))
    }

    const changeNotes = (e, i) => {
        setValues(old => [...old].map(v => {
            if (v.id === i) return { ...v, notes: e.target.value }
            return { ...v };
        }))
    }
    const changeType = (e, i) => {
        setValues(old => [...old].map(v => {
            if (v.id === i) return { ...v, type: e }
            return { ...v };
        }))
    }
    const changeFreq = (e, i) => {
        setValues(old => [...old].map(v => {
            if (v.id === i) return { ...v, freq: e }
            return { ...v };
        }))
    }
    const changeDate = (e, i) => {
        let date = moment(e).format('YYYY-MM-DD')
        setValues(old => [...old].map(v => {
            if (v.id === i) return { ...v, date }
            return { ...v };
        }))
    }

    const changeTime = (e, i) => {
        setValues(old => [...old].map(v => {
            if (v.id === i) return { ...v, time: e.target.value }
            return { ...v };
        }))
    }

    return (
        <Card key={idx} className="mt-2 mb-3 border-0">
            <CardBody style={{position: 'relative'}}>
                {isEdit &&
                <Button onClick={() => setDelete(values)} style={{position:'absolute', top:'5px', right:'5px'}} className="bg-transparent border-0 btn-sm">
                    <i className='fa fa-trash text-danger' />
                </Button>
                }
                <Row>
                    <Col xs="12" lg="6">
                        <Label htmlFor="task" className="input-label">Task</Label>
                        <Input
                            type="text"
                            value={values.task}
                            disabled={isSubmitting || !isEdit}
                            required
                            onChange={(e) => changeTask(e, values.id)}
                        />
                        {touched.task && errors.task && <small className="text-danger">{errors.task}</small>}
                    </Col>
                    <Col xs="12" lg="3">
                        <Label htmlFor='type' className="input-label">{t('tipe')}</Label>
                        <Select
                            options={dataType}
                            value={values.type}
                            isDisabled={isSubmitting || !isEdit}
                            onChange={(e) => changeType(e, values.id)}
                        />
                        {touched.type && errors.type && <small className="text-danger">{errors.type}</small>}
                    </Col>
                    <Col xs="12" lg="3">
                        <Label htmlFor='freq' className="input-label">{t('Frekuensi')}</Label>
                        <Select
                            options={dataFreq}
                            value={values.freq}
                            isDisabled={isSubmitting || !isEdit}
                            onChange={(e) => changeFreq(e, values.id)}
                        />
                        {touched.freq && errors.freq && <small className="text-danger">{errors.freq}</small>}
                    </Col>
                    <Col xs="12" lg="6" className="mt-2">
                        <Label htmlFor='notes' className="input-label">Notes</Label>
                        <Input
                            type="text"
                            value={values.notes}
                            disabled={isSubmitting || !isEdit}
                            onChange={(e) => changeNotes(e, values.id)}
                        />
                    </Col>
                    <Col xs="6" lg="3" className="mt-2">
                        <Label htmlFor='date' className="input-label">Due Date</Label>
                        <DatePickerInput
                            readOnly
                            showOnInputClick={true}
                            onChange={(e) => changeDate(e, values.id)}
                            value={values.date}
                            className='my-custom-datepicker-component'
                            closeOnClickOutside={true}
                            displayFormat="YYYY-MM-DD"
                            disabled={isSubmitting || !isEdit}
                        />
                        {touched.date && errors.date && <small className="text-danger">{errors.date}</small>}
                    </Col>
                    <Col xs="6" lg="3" className="mt-2">
                        <Label htmlFor="time" className="input-label">Due Time</Label>
                        <ReactInputMask
                            className="form-control pr-2/5 w-100"
                            mask="99:99"
                            placeholder='00:00'
                            disabled={isSubmitting || !isEdit}
                            value={values.time}
                            onChange={(e) => changeTime(e, values.id)}
                        />
                        {touched.time && errors.time && <small className="text-danger">{errors.time}</small>}
                    </Col>
                </Row>
            </CardBody>
        </Card>
    )
}

export default translate(CardComponent)