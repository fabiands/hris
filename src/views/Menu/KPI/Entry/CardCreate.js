import React, { useMemo, useState, useEffect } from 'react'
import { translate, t } from 'react-switch-lang'
import {Card, CardBody, Row, Col, Label, Input, Button, Spinner} from 'reactstrap'
import Select from 'react-select'
import { DatePickerInput } from 'rc-datepicker';
import { useFormik } from 'formik';
import request from '../../../../utils/request'
import { toast } from 'react-toastify'
import ReactInputMask from 'react-input-mask';
import * as Yup from 'yup';
import moment from 'moment'

function CardCreate({dataType, dataFreq, handleCancel, id, mutate, toggle, period}){
    const [clicked, setClicked] = useState(false)
    const [errorTime, setErrorTime] = useState(false)
    const ValidationFormSchema = useMemo(() => {
        return Yup.object().shape({
            task: Yup.string().required().label(t("Task")),
            type: Yup.string().required().label(t("tipe")),
            freq: Yup.string().required().label(t("Frekuensi")),
            date: Yup.string().required().label(t("Waktu")),
            time: Yup.string().required().label(t("Jam")),
        })
    },[])
    const {values, touched, errors, isSubmitting, ...formik} = useFormik({
        initialValues:{
            task:'',
            type:'',
            freq:'',
            date:'',
            time:'',
            notes:''
        },
        validationSchema: ValidationFormSchema,
        onSubmit: (values, { setSubmitting }) => {
            setClicked(true)
            setSubmitting(true)
            let time = values.time.split(':')
            if(parseFloat(time[0]) >= 24 || parseFloat(time[1]) >= 60){
                setErrorTime(true)
                setSubmitting(false)
                return;
            }
            request.post(`v1/personnels/${id}/kpi/task`,{
                period: moment(period).format('MMYYYY'),
                name: values.task,
                typeId: values.type.value,
                frequencyId: values.freq.value,
                dueDate: values.date + " " + values.time,
                notes: values.notes
            })
            .then(() => {
                toast.success(t('Berhasil Menambahkan Task'))
                formik.handleReset()
                handleCancel()
                mutate()
                // toggle()
            })
            .catch(() => {
                toast.error(t('Gagal menambahkan Task'))
                return;
            })
            .finally(() => setSubmitting(false))
        }
    })

    useEffect(() => {
        if(clicked){
            let time = values.time.split(':')
            if(parseFloat(time[0]) >= 24 || parseFloat(time[1]) >= 60){
                setErrorTime(true)
            }
            else {
                setErrorTime(false)
            }
        }
    }, [clicked, values])

    const changeType = (e) => {
        console.log(e)
        formik.setFieldTouched('type', true)
        formik.setFieldValue('type', e)
    }
    const changeFreq = (e) => {
        console.log(e)
        formik.setFieldTouched('freq', true)
        formik.setFieldValue('freq', e)
    }
    const changeDate = (e) => {
        let date = moment(e).format('YYYY-MM-DD')
        formik.setFieldValue('date', date)
    }

    return(
        <>
        <Card className="mt-2 mb-3 border-0">
            <CardBody>
                <Row>
                    <Col xs="12" lg="6">
                        <Label htmlFor="task" className="input-label">Task</Label>
                        <Input
                            name="task"
                            id="task"
                            type="text"
                            value={values.task}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            disabled={isSubmitting}
                            required
                        />
                        {touched.task && errors.task && <small className="text-danger">{errors.task}</small>}
                    </Col>
                    <Col xs="12" lg="3">
                        <Label htmlFor='type' className="input-label">{t('tipe')}</Label>
                        <Select
                            name='type'
                            id='type'
                            options={dataType}
                            value={values.type}
                            isDisabled={isSubmitting}
                            onChange={changeType}
                        />
                        {touched.type && errors.type && <small className="text-danger">{errors.type}</small>}
                    </Col>
                    <Col xs="12" lg="3">
                        <Label htmlFor='freq' className="input-label">{t('Frekuensi')}</Label>
                        <Select
                            name='freq'
                            id='freq'
                            options={dataFreq}
                            value={values.freq}
                            isDisabled={isSubmitting}
                            onChange={changeFreq}
                        />
                        {touched.freq && errors.freq && <small className="text-danger">{errors.freq}</small>}
                    </Col>
                    <Col xs="12" lg="6" className="mt-2">
                        <Label htmlFor='notes' className="input-label">Notes</Label>
                        <Input
                            name='notes'
                            id='notes'
                            type="text"
                            value={values.notes}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            disabled={isSubmitting}
                        />
                    </Col>
                    <Col xs="6" lg="3" className="mt-2">
                        <Label htmlFor='date' className="input-label">Due Date</Label>
                        <DatePickerInput
                            readOnly
                            showOnInputClick={true}
                            name="date"
                            id="date"
                            onChange={changeDate}
                            value={values.date}
                            className='my-custom-datepicker-component'
                            closeOnClickOutside={true}
                            displayFormat="YYYY-MM-DD"
                            disabled={isSubmitting}
                        />
                        {touched.date && errors.date && <small className="text-danger">{errors.date}</small>}
                    </Col>
                    <Col xs="6" lg="3" className="mt-2">
                        <Label htmlFor="time" className="input-label">Due Time</Label>
                        <ReactInputMask
                            className="form-control pr-2/5 w-100"
                            id="time"
                            name="time"
                            mask="99:99"
                            placeholder='00:00'
                            disabled={isSubmitting}
                            value={values.time}
                            onChange={formik.handleChange}
                        />
                        {touched.time && errors.time && <small className="text-danger">{errors.time}</small>}
                        {errorTime && <small className="text-danger">{t('Isikan jam dengan  benar')}</small>}
                    </Col>
                </Row>
            </CardBody>
        </Card>
        <div className='mb-3 d-flex justify-content-end'>
            <Button disabled={isSubmitting} color="outline-netis-primary" className="mr-1 btn-sm" onClick={handleCancel}>
                {t('Batal')}
            </Button>
            <Button disabled={isSubmitting} color="netis-primary" className="ml-1 btn-sm" onClick={formik.handleSubmit}>
                {isSubmitting ? <Spinner color="light" /> : 'Submit'}
            </Button>
        </div>
        </>
    )
}

export default translate(CardCreate)