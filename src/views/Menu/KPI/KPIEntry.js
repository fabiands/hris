// import { DatePickerInput } from 'rc-datepicker'
import React, { useEffect, useMemo, useState, useCallback } from 'react'
import { Link } from 'react-router-dom'
import Select from 'react-select'
import { t } from 'react-switch-lang'
import { Row, Col, Input, InputGroup, InputGroupAddon, InputGroupText, Button } from 'reactstrap'
import DataNotFound from '../../../components/DataNotFound'
import LoadingAnimation from '../../../components/LoadingAnimation'
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../components/ResponsiveTable'
import request from '../../../utils/request'
import usePagination from '../../../hooks/usePagination';

function KPIEntry(props) {
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState(false)
    const [search, setSearch] = useState(null)
    const [filterUnit, setFilterUnit] = useState([])
    const [filterJob, setFilterJob] = useState([])
    const [units, setUnits] = useState([]);
    const [jobs, setJobs] = useState([]);
    const [filters, setFilters] = useState([])

    useEffect(() => {
        setLoading(true)
        request.get('v1/personnels/all/aktif')
            .then((res) => {
                setData(res.data.data)
            })
            .catch((err) => {
                setError(true)
                console.log(err)
            })
            .finally(() => setLoading(false))
    }, [])

    useEffect(() => {
        request.get('v1/master/units')
            .then((res) => setUnits(res.data.data.map((item) => {
                return { label: item.name, value: item.id };
            })))
            .catch((err) => { })
        request.get('v1/master/jobs')
            .then((res) => setJobs(res.data.data.map((item) => {
                return { label: item.name, value: item.id };
            })))
            .catch((err) => { })
    }, [])

    const handleSearch = (e) => {
        setSearch(e.target.value);
    }
    const handleUnit = (e) => {
        let unit = e?.map(item => item.label) ?? []
        setFilterUnit(unit)
    }
    const handleJob = (e) => {
        let job = e?.map(item => item.label) ?? []
        setFilterJob(job)
    }

    const filtered = useMemo(() => {
        let filterData = data
            ?.filter(item =>
                search?.trim() ? item?.fullName?.toLowerCase().includes(search?.trim().toLowerCase()) : true
            )
            ?.filter(item =>
                filterUnit?.length > 0 ? filterUnit.includes(item.unit.name) : true
            )
            ?.filter(item =>
                filterJob?.length > 0 ? filterJob.includes(item.job.name) : true
            )
        return filterData;
    }, [data, search, filterUnit, filterJob])

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
    );

    const { data: groupFiltered, PaginationComponent, currentPage } = usePagination(
        filtered,
        12,
        filters?.pagination,
        handleChangeCurrentPage
    );

    return (
        <div className="animated fadeIn">
            <div className="mr-auto bd-highlight my-3">
                <Button color="netis-color" onClick={() => props.history.goBack()}><i className="fa fa-chevron-left mr-2"></i> {t('kembali')} </Button>
            </div>
            <Row>
                <Col xs="6">
                    <h4 className="content-title mb-4">{t('Entri KPI')}</h4>
                </Col>
            </Row>
            <Row>
                <Col md="8" className="text-left">
                    <InputGroup className="w-50">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-search" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="search" onChange={handleSearch} name="search" placeholder={t('Search')} className="input-search" />
                    </InputGroup>
                </Col>
            </Row>
            <h6 className="mt-4 mb-1 font-weight-bold">{t('Sortir Berdasarkan')}</h6><br />
            <Row className="mb-4">
                <Col xs="6" md="3">
                    <Select
                        isMulti={true}
                        isSearchable={false}
                        name="unit"
                        options={units}
                        placeholder={t("Unit")}
                        onChange={handleUnit}
                    />
                </Col>
                <Col xs="6" md="3">
                    <Select
                        isMulti={true}
                        isSearchable={false}
                        name="jabatan"
                        options={jobs}
                        placeholder={t("Jabatan")}
                        onChange={handleJob}
                    />
                </Col>
            </Row>
            <Rtable size="sm">
                <Rthead>
                    <Rtr>
                        <Rth className="text-md-center w-5">No.</Rth>
                        <Rth>{t("Nama")}</Rth>
                        <Rth>Email</Rth>
                        <Rth>{t('Jabatan')}</Rth>
                        <Rth>{t('Unit')}</Rth>
                    </Rtr>
                </Rthead>
                <Rtbody>
                    {
                        loading ?
                            <Rtr><Rtd colSpan="9999"><LoadingAnimation /></Rtd></Rtr>
                            : (error || filtered?.length < 1) ?
                                <Rtr><Rtd colSpan="9999"><DataNotFound /></Rtd></Rtr>
                                :
                                groupFiltered?.map((item, idx) =>
                                    <Rtr key={idx + 1 + (8 * currentPage)}>
                                        <Rtd>{idx + 1 + (8 * currentPage)}</Rtd>
                                        <Rtd>
                                            <Link to={`/kpi/entry/${item.id}`}>
                                                {item.fullName}
                                            </Link>
                                        </Rtd>
                                        <Rtd>{item.email}</Rtd>
                                        <Rtd>{item.job.name}</Rtd>
                                        <Rtd>{item.unit.name}</Rtd>
                                    </Rtr>
                                )
                    }
                </Rtbody>
            </Rtable>
            {filtered.length > 8 && <PaginationComponent />}
        </div>
    )
}

export default KPIEntry