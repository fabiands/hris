import React, {useMemo, useState} from 'react'
import { Table, Collapse} from 'reactstrap';
import useSWR from 'swr'
import {translate, t} from 'react-switch-lang'
import DataNotFound from '../../../../components/DataNotFound';
import LoadingAnimation from '../../../../components/LoadingAnimation';
import KPIDetail from "./KPIDetail"

function KPIDepartment(){
    const [view, setView] = useState('unit')
    const {data: response, error} = useSWR('/v1/master/units')
    const loading = !response && !error;
    const handleUnit = () => {
        setDataUnit(null)
        setView('unit')
    }
    const handleDetail = () => setView('detail')
    const [dataUnit, setDataUnit] = useState(null)

    const setDetail = (item) => {
        setDataUnit(item)
        handleDetail()
    }
    const data = useMemo(() => {
        if (response) {
            return response.data.data;
        }
        else{
            return []
        }
    }, [response]);

    if(loading){
        return <LoadingAnimation />
    }
    if(error){
        return <DataNotFound />
    }
    return(
        <div className='animated fadeIn'>
            <Collapse isOpen={view === 'unit'}>
            <Table responsive hover borderless>
                <tbody>
                    {data.map((item, idx) =>
                        <tr key={idx}>
                            <td className='w-75'>{item.name}</td>
                            <td className='w-25 text-center'>
                                <span className="link-button text-netis-primary mr-1" onClick={() => setDetail(item)}>
                                    {t('lihatdetail')}
                                </span>
                            </td>
                        </tr>
                    )}
                </tbody>
            </Table>
            </Collapse>
            <Collapse isOpen={view === 'detail'}>
                {view === 'detail' && <KPIDetail dataUnit={dataUnit} handleUnit={handleUnit} />}
            </Collapse>
        </div>
    )
}

export default translate(KPIDepartment)