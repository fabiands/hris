import React, {useMemo, useState} from 'react'
import {ListGroup, ListGroupItem, Spinner, Modal, ModalBody, ModalHeader, ModalFooter, Button, Form, FormGroup, Label, Input} from 'reactstrap'
import useSWR from 'swr'
import {translate, t} from 'react-switch-lang'
import { useFormik } from 'formik'
import DataNotFound from '../../../../components/DataNotFound';
import request from '../../../../utils/request'
import { toast } from 'react-toastify'

function KPIPeriodic(){
    const {data: response, error, mutate} = useSWR('/v1/kpi/frequency')
    const loading = !response && !error;
    const [create, setCreate] = useState('new')
    const [modalComponent, setModalComponent] = useState(false)
    const [modalDelete, setModalDelete] = useState(false)
    const [deletingData, setDeletingData] = useState(null)
    const [isDeleting, setIsDeleting] = useState(false)

    const data = useMemo(() => {
        if (response) {
            return response.data.data;
        }
        else{
            return []
        }
    }, [response]);

    const { values, isSubmitting, ...formik } = useFormik({
        initialValues: {
            id:'',
            name:''
        },
        onSubmit: (values, { setSubmitting }) => {
            setSubmitting(true)
            if (create === 'new') {
                request.post('v1/kpi/frequency', { name: values.name })
                    .then(() => {
                        toast.success(t('Berhasil menambah Data'))
                        mutate()
                        toggleCancel()
                    })
                    .catch(() => {
                        toast.error(t('Gagal menambah Data'))
                        return;
                    })
                    .finally(() => setSubmitting(false))
            }
            else if (create === 'edit') {
                request.put(`v1/kpi/frequency/${values.id}`, { name: values.name })
                    .then(() => {
                        toast.success(t('Berhasil mengubah Data'))
                        mutate()
                        toggleCancel()
                    })
                    .catch(() => {
                        toast.error(t('Gagal mengubah Data'))
                        return;
                    })
                    .finally(() => setSubmitting(false))
            }
            setSubmitting(false)
        }
    })
    const addComponent = () => {
        setCreate('new')
        formik.handleReset()
        setModalComponent(true)
    }
    const toggleCancel = () => {
        setModalComponent(false)
        formik.handleReset()
    }
    const handleEdit = (data) => {
        setCreate('edit')
        formik.setFieldValue('id', data.id)
        formik.setFieldValue('name', data.name)
        setModalComponent(true)
    }
    const confirmDelete = (data) => {
        setDeletingData(data)
        setModalDelete(true)
    }
    const cancelDelete = () => {
        setDeletingData(null)
        setModalDelete(false)
    }

    const handleDelete = (e) => {
        setIsDeleting(true)
        request.delete(`v1/kpi/frequency/${deletingData.id}`, {name: deletingData?.name})
            .then(() => {
                mutate()
                toast.success(t('Berhasil menghapus Data'))
                cancelDelete()
            })
            .catch(() => {
                toast.error(t('Gagal menghapus Data'))
                return;
            })
            .finally(() => setIsDeleting(false))
    }

    return(
        <div className="animated fadeIn">
            <h4 className="content-title mb-4">{t('Frekuensi KPI')}</h4>
            {loading ? <div className="mx-auto py-5 py-5"><Spinner color="dark" size="lg" /></div>
                : error ? <DataNotFound /> :
                    <ListGroup className="kpi-class mb-4">
                        {data.length < 1 ? <DataNotFound /> :
                            data.map((item, idx) =>
                                <ListGroupItem className="d-flex justify-content-between border-0 pl-3 hover-bg-secondary" key={idx}>
                                    <span>{item.name}</span>
                                    <div className="ml-auto">
                                        <span className="link-button text-netis-primary mr-1" onClick={() => handleEdit(item)}>
                                            Edit
                                        </span>
                                        <span className="link-button text-netis-primary ml-1" onClick={() => confirmDelete(item)}>
                                            {t('Hapus')}
                                        </span>
                                    </div>
                                </ListGroupItem>
                            )}
                    </ListGroup>
            }
            {!loading && !error &&
                <span className="link-button text-info pl-3 mt-4" onClick={addComponent}>
                    {t('Tambah Item')}
                </span>
            }
            <Modal isOpen={modalComponent} toggle={toggleCancel}>
                <Form onSubmit={formik.handleSubmit}>
                    <ModalHeader className="bg-transparent border-bottom-0">
                        {create === 'new' ? t('tambah') : 'Edit'} &nbsp; {t('Frekuensi KPI')}
                    </ModalHeader>
                    <ModalBody>
                            <FormGroup>
                                <Label htmlFor="name" className="input-label">{t('Frekuensi KPI')}</Label>
                                <Input id="name" name="name" value={values.name} placeholder={t('Frekuensi KPI')} onChange={formik.handleChange} />
                            </FormGroup>
                    </ModalBody>
                    <ModalFooter className="border-top-0 d-flex justify-content-start pb-4" style={{ paddingLeft: '2rem' }}>
                        <Button disabled={isSubmitting} onClick={toggleCancel} color="outline-netis-primary" className="mr-2">{t('Batal')}</Button>
                        <Button disabled={isSubmitting || !values.name} type="submit" color="netis-primary" className="ml-2">
                            {isSubmitting ? <Spinner color="light" /> : t('Simpan')}
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
            <Modal isOpen={modalDelete} toggle={cancelDelete}>
                <ModalBody className="text-center py-4">
                    {t('Apakah anda yakin ingin menghapus')}&nbsp;
                    <b>{deletingData?.name}</b> ?
                    <br />
                    <div className="mt-3 w-50 mx-auto text-center d-flex justify-content-around">
                        <Button disabled={isDeleting} color="outline-netis-primary" className="mr-2" onClick={cancelDelete}>{t('Batal')}</Button>
                        <Button disabled={isDeleting} color="netis-primary" className="ml-2" onClick={handleDelete}>
                            {isDeleting ? 'loading...' : t('Hapus')}
                        </Button>
                    </div>
                </ModalBody>
            </Modal>
        </div>

    )
}

export default translate(KPIPeriodic)