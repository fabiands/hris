import React, { useEffect, useState, useMemo, useCallback } from 'react'
import Select from 'react-select'
import { Button, Form, FormGroup, Input, Label, ListGroup, ListGroupItem, Modal, ModalBody, ModalHeader, ModalFooter, Spinner } from 'reactstrap'
import DataNotFound from '../../../../components/DataNotFound'
import request from '../../../../utils/request'
import { translate, t } from 'react-switch-lang'
import { useFormik } from 'formik'
import useSWR from 'swr'
import { toast } from 'react-toastify'

function KPIDetail({ dataUnit, handleUnit }) {
    const [data, setData] = useState([])
    const [formulaData, setFormulaData] = useState(null)
    const [formulaValue, setFormulaValue] = useState("")
    const [modalFormula, setModalFormula] = useState(false)
    const [error, setError] = useState(false)
    const [loading, setLoading] = useState(false)
    const [modalComponent, setModalComponent] = useState(false)
    const [modalDelete, setModalDelete] = useState(false)
    const [isDeleting, setIsDeleting] = useState(false)
    const [isChangeFormula, setIsChangeFormula] = useState(false)
    const [deletingData, setDeletingData] = useState(null)
    const [create, setCreate] = useState('new')
    const { data: response, error: errorType } = useSWR('v1/kpi/type')
    const [sameCode, setSameCode] = useState(false)
    const dataType = useMemo(() => {
        if (response) {
            return response.data.data;
        }
        else {
            return []
        }
    }, [response]);

    const optionsType = dataType?.map(item =>
        ({ value: item.id, label: item.name })
    )

    const { values, isSubmitting, ...formik } = useFormik({
        initialValues: {
            id: '',
            name: '',
            code: '',
            type: ''
        },
        onSubmit: (values, { setSubmitting }) => {
            setSubmitting(true)
            const hasCode = data?.find(i => i.code === values.code)
            if(hasCode){
                setSameCode(true)
                setSubmitting(false)
                return;
            }
            else {
                setSameCode(false)
            }
            if (create === 'new') {
                request.post('v1/kpi', {
                    name: values.name,
                    code: values.code,
                    unitId: dataUnit?.id,
                    activityTypeId: values.type.value
                })
                    .then(() => {
                        toast.success(t('Berhasil menambah Data'))
                        getDataUnit()
                        toggleCancel()
                    })
                    .catch(() => {
                        toast.error(t('Gagal menambah Data'))
                        return;
                    })
                    .finally(() => setSubmitting(false))
            }
            else if (create === 'edit') {
                request.post(`v1/kpi/${values.id}`, {
                    name: values.name,
                    activityTypeId: values.type.value
                })
                    .then(() => {
                        toast.success(t('Berhasil mengubah Data'))
                        getDataUnit()
                        toggleCancel()
                    })
                    .catch(() => {
                        toast.error(t('Gagal mengubah Data'))
                        return;
                    })
                    .finally(() => setSubmitting(false))
            }
        }
    })
    const nullForm = !values.name || !values.code || !values.type

    const addComponent = () => {
        setCreate('new')
        formik.handleReset()
        setModalComponent(true)
    }
    const toggleCancel = () => {
        setModalComponent(false)
        formik.handleReset()
    }
    const toggleFormula = () => {
        setModalFormula(!modalFormula)
        setFormulaValue(formulaData?.kpiFormula)
    }

    const handleEdit = (data) => {
        setCreate('edit')
        formik.setFieldValue('id', data.id)
        formik.setFieldValue('name', data.name)
        formik.setFieldValue('code', data.code)
        formik.setFieldValue('type', { value: data.type.id, label: data.type.name })
        setModalComponent(true)
    }

    const confirmDelete = (data) => {
        setDeletingData(data)
        setModalDelete(true)
    }

    const cancelDelete = () => {
        setDeletingData(null)
        setModalDelete(false)
    }

    const handleDelete = (e) => {
        setIsDeleting(true)
        request.delete(`v1/kpi/${dataUnit?.id}`, {
            name: deletingData?.name,
            activityTypeId: deletingData?.type.value
        })
            .then(() => {
                toast.success(t('Berhasil menghapus Data'))
                let a = data.filter(i => i.name !== deletingData?.name)
                setData(a)
                cancelDelete()
            })
            .catch(() => {
                toast.error(t('Gagal menghapus Data'))
                return;
            })
            .finally(() => setIsDeleting(false))
    }

    const handleEditFormula = () => {
        setIsChangeFormula(true)
        request.post(`v1/kpi/${dataUnit?.id}/formula`, { formula: formulaValue })
            .then(() => {
                toast.success(t('Berhasil mengubah Formula'))
                getFormulaUnit()
                toggleFormula()
            })
            .catch(() => {
                toast.error(t('Gagal mengubah Formula'))
                return;
            })
            .finally(() => setIsChangeFormula(false))
    }

    const getDataUnit = useCallback(() => {
        setLoading(true)
        return request.get(`v1/kpi?unitId=${dataUnit?.id}`)
            .then((res) => {
                setData(res.data.data)
            })
            .catch(() => {
                setError(true)
            })
            .finally(() => setLoading(false))
    }, [dataUnit])

    const getFormulaUnit = useCallback(() => {
        return request.get(`v1/master/units/${dataUnit?.id}`)
            .then((res) => {
                setFormulaData(res.data.data)
            })
            .catch(() => {
                setError(true)
            })
    }, [dataUnit])

    useEffect(() => {
        if (dataUnit?.id) {
            getDataUnit()
        }
        getFormulaUnit()
    }, [getDataUnit, getFormulaUnit, dataUnit])

    useEffect(() => {
        if (formulaData) {
            setFormulaValue(formulaData?.kpiFormula ?? "")
        }
    }, [formulaData])

    useEffect(() => {
        if(sameCode){
            const interval = setInterval(() => {
                setSameCode(false)
            }, 3000);
            return () => clearInterval(interval);
        }
    }, [sameCode]);

    const handleCode = (e) => {
        let val = e.target.value
        formik.setFieldValue('code', val?.toUpperCase() ?? '')
    }
    const handleType = (e) => {
        formik.setFieldValue('type', e)
    }

    return (
        <div>
            <div className="mr-auto bd-highlight mt-2 mb-3">
                <button className="btn btn-netis-color" disabled={loading} onClick={handleUnit}>
                    <i className="fa fa-chevron-left mr-2"></i> {t("kembali")}
                </button>
            </div>
            {loading ? <div className="mx-auto py-5 py-5"><Spinner color="dark" size="lg" /></div>
                : error ? <DataNotFound /> :
                    <div>
                        <h5>{t('Komponent Departemen')} {dataUnit?.name}</h5>
                        <ListGroup>
                            {data.length < 1 ? <DataNotFound /> :
                                data.map((item, idx) =>
                                    <ListGroupItem className="d-flex justify-content-between border-0 pl-3 hover-bg-secondary" key={idx}>
                                        <span>{item.name}</span>
                                        <div className="ml-auto">
                                            <span className="link-button text-netis-primary mr-1" onClick={() => handleEdit(item)}>
                                                {t('Ubah')}
                                            </span>
                                            <span className="link-button text-netis-primary ml-1" onClick={() => confirmDelete(item)}>
                                                {t('Hapus')}
                                            </span>
                                        </div>
                                    </ListGroupItem>
                                )}
                        </ListGroup>
                    </div>
            }
            <br />
            <span className="link-button text-info pl-3" onClick={addComponent}>
                {t('Tambah Item')}
            </span>
            <hr />
            <ListGroup>
                <ListGroupItem className="d-flex justify-content-between border-0 pl-3">
                    <h5>{t('Rumus Departemen')} {dataUnit?.name}</h5>
                    <div className="ml-auto">
                        <span className="link-button text-netis-primary mr-1" onClick={toggleFormula}>
                            {t('lihatdetail')}
                        </span>
                        <span className="link-button text-netis-primary ml-1 d-none">
                            {t('')}
                        </span>
                    </div>
                </ListGroupItem>
            </ListGroup>
            <Modal isOpen={modalComponent} toggle={isSubmitting ? null : toggleCancel}>
                <Form onSubmit={formik.handleSubmit}>
                    <ModalHeader className="bg-transparent border-bottom-0">
                        {create === 'new' ? t('tambah') : 'Edit'}&nbsp; {t('Komponen KPI')}
                    </ModalHeader>
                    <ModalBody>
                        <FormGroup>
                            <Label htmlFor="name" className="input-label">{t('Nama Komponen')}</Label>
                            <Input id="name" name="name" value={values.name} onChange={formik.handleChange} onBlur={formik.handleBlur} placeholder={t('Nama Komponen')} />
                        </FormGroup>
                        <FormGroup>
                            <Label htmlFor="code" className="input-label">{t('Kode Komponen')}</Label>
                            <Input id="code" name="code"
                                value={values.code}
                                maxLength={5}
                                onChange={handleCode}
                                onBlur={formik.handleBlur}
                                placeholder={t('Kode Komponen')}
                                disabled={create === 'edit'}
                            />
                            {sameCode && <small className="text-danger">{t('Kode ini sudah digunakan oleh Komponen lain')}</small>}
                        </FormGroup>
                        <FormGroup>
                            <Label htmlFor="type" className="input-label">{t('Tipe Komponen')}</Label>
                            <Select
                                disabled={errorType || optionsType.length < 1}
                                id="type"
                                name="type"
                                options={optionsType}
                                value={values.type}
                                onChange={handleType}
                            />
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter className="border-top-0 d-flex justify-content-start pb-4" style={{ paddingLeft: '2rem' }}>
                        <Button disabled={isSubmitting} onClick={toggleCancel} color="outline-netis-primary" className="mr-2">{t('Batal')}</Button>
                        <Button disabled={isSubmitting || nullForm} type="submit" color="netis-primary" className="ml-2">
                            {isSubmitting ? <Spinner color="light" /> : t('Simpan')}
                        </Button>
                    </ModalFooter>
                </Form>
            </Modal>
            <Modal isOpen={modalFormula} toggle={toggleFormula}>
                <ModalHeader className="bg-transparent border-bottom-0">
                    {t('Edit Rumus KPI Departemen')}&nbsp;{formulaData?.name}
                </ModalHeader>
                <ModalBody>
                    <Label htmlFor="formula" className="input-label">Formula</Label>
                    <Input
                        id="formula"
                        name="formula"
                        value={formulaValue ?? ""}
                        onChange={(e) => setFormulaValue(e.target.value)}
                    />
                </ModalBody>
                <ModalFooter className="border-top-0 d-flex justify-content-start pb-4" style={{ paddingLeft: '2rem' }}>
                    <Button disabled={isChangeFormula} onClick={toggleFormula} color="outline-netis-primary" className="mr-2">{t('Batal')}</Button>
                    <Button disabled={isChangeFormula || !formulaValue} onClick={handleEditFormula} color="netis-primary" className="ml-2">
                        {isChangeFormula ? <Spinner color="light" /> : t('Simpan')}
                    </Button>
                </ModalFooter>
            </Modal>
            <Modal isOpen={modalDelete} toggle={cancelDelete}>
                <ModalBody className="text-center py-4">
                    {t('Apakah anda yakin ingin menghapus')}&nbsp;
                    <b>{deletingData?.name}</b> ?
                    <br />
                    <div className="mt-3 w-50 mx-auto text-center d-flex justify-content-around">
                        <Button disabled={isDeleting} color="outline-netis-primary" className="mr-2" onClick={cancelDelete}>{t('Batal')}</Button>
                        <Button disabled={isDeleting} color="netis-primary" className="ml-2" onClick={handleDelete}>
                            {isDeleting ? 'loading...' : t('Hapus')}
                        </Button>
                    </div>
                </ModalBody>
            </Modal>
        </div>

    )
}

export default translate(KPIDetail)