import React from 'react'
import { useLocation, withRouter } from 'react-router-dom';
import { Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap'
import { Link } from 'react-router-dom';
import { translate, t } from 'react-switch-lang';
import KPIUser from './KPIUser';
import KPIMenu from './KPIMenu';
import { connect } from 'react-redux';

function KPIUserWrapper(props) {
    const { match, user } = props;
    const tabs = {
        // 'department': t('Departemen'),
        'history': t('Riwayat KPI'),
        'manage': t('Kelola KPI'),
        // 'status': t('Status KPI')
    }
    const tabsArray = Object.keys(tabs);
    const location = useLocation();
    const selectedTab = location.hash ? location.hash.substring(1) : tabsArray[0];

    return (
        <div className='animated fadeIn'>
            <h4 className="content-title mb-4">Key Performance Indicator (KPI)</h4>
            <Nav tabs className={!user.hasBawahan ? 'd-none' : ''}>
                <NavItem>
                    <NavLink tag={Link} className="pt-2/5" active={selectedTab === 'history'} replace to={{ hash: '#history' }}>
                        {t('Riwayat KPI')}
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink tag={Link} className="pt-2/5" active={selectedTab === 'manage'} replace to={{ hash: '#manage' }}>
                        {t('Kelola KPI')}
                    </NavLink>
                </NavItem>
            </Nav>
            <TabContent activeTab={selectedTab}>
                <TabPane tabId="history"><KPIUser /></TabPane>
                <TabPane tabId="manage"><KPIMenu match={match} isUserPanel={true} /></TabPane>
            </TabContent>
        </div>
    )
}

const mapStateToProps = ({ user, token, menu }) => ({ user, token, menu });
export default connect(mapStateToProps)(translate(withRouter(KPIUserWrapper)));