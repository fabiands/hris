import React, { useEffect, useMemo } from 'react'
import { translate, t } from 'react-switch-lang'
import { Badge, Button, Card, CardBody, Col, Row } from 'reactstrap'
import useSWR from 'swr'
import DataNotFound from '../../../components/DataNotFound'
import { useAuthUser } from '../../../store'
import LoadingAnimation from '../../../components/LoadingAnimation'
import moment from 'moment'
import usePeriod from './components/Period'
import { DatePickerInput } from 'rc-datepicker';
import { useState } from 'react'
import request from '../../../utils/request'
import Select from 'react-select'
import ModalDetail from './components/ModalDetail'
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../components/ResponsiveTable'

function KPIUser() {
    const user = useAuthUser()
    const [openModal, setOpenModal] = useState(false)
    const { period, PeriodComponent } = usePeriod();
    const { data: dataResponse, error, mutate } = useSWR(`v1/kpi/task/history?period=${moment(period).format('MMYYYY')}`)
    const loading = !dataResponse && !error
    const data = useMemo(() => {
        if (dataResponse) {
            return dataResponse.data.data;
        }
        return [];
    }, [dataResponse])

    const handleChangeFilter = (name, date) => {
        let fil = filter
        fil[name] = date
        setFilter(fil)
    }
    const handleDetailTask = () => {
        setOpenModal(!openModal)
    }

    const [filter, setFilter] = useState({
        start: null,
        end: null
    })
    const [dataFreq, setDataFreq] = useState([])
    useEffect(() => {
        request.get('v1/kpi/frequency')
            .then((res) => {
                let response = res.data.data.map(item =>
                    ({ value: item.id, label: item.name })
                )
                setDataFreq(response)
            })
    }, [])

    if (loading) {
        return <LoadingAnimation />
    }

    return (
        <div className='animated fadeIn'>
            <PeriodComponent />
            <Card className="my-3 border-0 w-100" style={{ backgroundColor: '#f5f6fa' }}>
                <CardBody className="p-4">
                    <Row className="text-left">
                        <Col md="6">
                            <Row>
                                <Col xs="3" className="my-1">{t('Nama')}</Col><Col xs="9"> : {user.fullName}</Col>
                                <Col xs="3" className="my-1">Email</Col><Col xs="9"> : {user.email}</Col>
                            </Row>
                        </Col>
                        <Col md="6">
                            <Row>
                                <Col xs="3" className="my-1">{t('Jabatan')}</Col><Col xs="9"> : {user.jabatan}</Col>
                                <Col xs="3" className="my-1">{t('Periode KPI')}</Col><Col xs="9"> : {moment(period).format('MMMM YYYY')} </Col>
                            </Row>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
            <h5>{t('Sortir berdasarkan')}</h5>
            <Row className="my-3">
                <Col xs="6" md="4" className="mt-1 mb-3">
                    <Select
                        name="freq"
                        id="freq"
                        className="div-freq"
                        options={dataFreq}
                        isDisabled={dataFreq.length < 1}
                    />
                </Col>
                <Col xs="12" className="my-1">
                    <Row>
                        <Col md="4" className="my-1">
                            <DatePickerInput
                                readOnly
                                showOnInputClick={true}
                                name="start"
                                id="start"
                                placeholder={t('Tanggal Mulai')}
                                onChange={(value) => handleChangeFilter('start', value)}
                                value={filter.start}
                                className='my-custom-datepicker-component'
                                closeOnClickOutside={true}
                                displayFormat="DD MMMM YYYY"
                                maxDate={filter.end ?? new Date()}
                            />
                        </Col>
                        <Col md="1" className="my-1 date-wrapper">
                            <small>{t('Sampai dengan')}</small>
                        </Col>
                        <Col md="4" className="my-1">
                            <DatePickerInput
                                readOnly
                                showOnInputClick={true}
                                name="end"
                                id="end"
                                placeholder={t('Tanggal Selesai')}
                                onChange={(value) => handleChangeFilter('end', value)}
                                value={filter.end}
                                className='my-custom-datepicker-component'
                                closeOnClickOutside={true}
                                displayFormat="DD MMMM YYYY"
                                minDate={filter.start}
                                maxDate={new Date()}
                            />
                        </Col>
                        {(!error && data.length > 0) &&
                            <Col md="3" className="align-items-center d-flex justify-content-end">
                                <Button color="netis-color" onClick={handleDetailTask}>
                                    {t('Lihat Detail')}
                                </Button>
                            </Col>
                        }
                    </Row>
                </Col>
            </Row>
            <Rtable size="sm">
                <Rthead>
                    <Rtr>
                        <Rth>No.</Rth>
                        <Rth>{t('tipe')}</Rth>
                        <Rth>{t('Frekuensi')}</Rth>
                        <Rth>Task</Rth>
                        <Rth>{t('Batas pengerjaan Task')}</Rth>
                        <Rth>Status</Rth>
                        <Rth>{t('Catatan')}</Rth>
                    </Rtr>
                </Rthead>
                <Rtbody>
                    {loading ? <Rtr><Rtd colSpan={7}><LoadingAnimation /></Rtd></Rtr> :
                        (error || data.length) < 1 ? <Rtr><Rtd colSpan={7}><DataNotFound /></Rtd></Rtr> :
                            data?.map((item, idx) =>
                                <Rtr key={idx}>
                                    <Rtd>{idx + 1}</Rtd>
                                    <Rtd>{item?.task?.type?.name}</Rtd>
                                    <Rtd>{item?.task?.frequency?.name}</Rtd>
                                    <Rtd>{item?.task?.name}</Rtd>
                                    <Rtd>{moment(item.task?.dueDate).format('DD MMMM YYYY')}</Rtd>
                                    <Rtd>
                                        <Badge color={item?.task?.status.status === 'Done'
                                            ? 'success'
                                            : (item?.task?.status.status === 'Failed' ? 'danger' : 'info')
                                        }>
                                            {item?.task?.status.status}
                                            {item?.task?.status.status === 'Done' && item?.task?.status.date
                                                ? ' at ' + moment(item?.task?.status.date).format('DD MMMM YYYY')
                                                : null
                                            }
                                        </Badge>
                                    </Rtd>
                                    <Rtd>{item.task.notes === '' ? '-' : item.task.notes}</Rtd>
                                </Rtr>
                            )}
                </Rtbody>
            </Rtable>
            <ModalDetail isOpen={openModal} toggle={handleDetailTask} data={data} mutate={mutate} />
        </div>
    )
}

export default translate(KPIUser)