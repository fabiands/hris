import React from 'react';
import PersonnelProfileInfo from './Profile/PersonnelProfileInfo';
import PersonnelProfileContact from './Profile/PersonnelProfileContact';
import PersonnelProfileIdentity from './Profile/PersonnelProfileIdentity';
import PersonnelProfileAddress from './Profile/PersonnelProfileAddress';
import useSWR from 'swr';
import LoadingAnimation from '../../../components/LoadingAnimation';

function PersonnelProfileTab({personnel}){

    const { data: countryResponse, error: countryError } = useSWR('v1/location/countries');
    const loading = !countryError && !countryResponse;
    
    if (loading) {
        return <LoadingAnimation/>
    }
    
    const country = (countryResponse?.data?.data ?? [])

    return(
        <div className="animated fadeIn">
            <div className="card">
                <div className="card-body py-2">
                    <PersonnelProfileInfo info={personnel} canEdit={personnel.verif === 'unverified'}/>
                </div>
                <div className="card-body py-2 border-top">
                    <PersonnelProfileContact contact={personnel}  canEdit={personnel.verif === 'unverified'}/>
                </div>
                <div className="card-body py-2 border-top">
                    <PersonnelProfileIdentity identity={personnel} country={country} canEdit={personnel.verif === 'unverified'}/>
                </div>
                <div className="card-body py-2 border-top">
                    <PersonnelProfileAddress address={personnel} country={country} canEdit={personnel.verif === 'unverified'}/>
                </div>
            </div>
            {/* <hr className="my-1" />
            <hr className="my-1" />
            <hr className="my-1" /> */}
        </div>
    )
}

export default PersonnelProfileTab;
