import React, {useState } from 'react';
import { Button, Row, Form, FormGroup, Label, Input, Spinner } from 'reactstrap';
import {translate, t} from 'react-switch-lang';
import { useFormik } from 'formik';
import request from '../../../../utils/request';
import { toast } from 'react-toastify';

function PersonnelProfileInfo({ info, canEdit }){

    const [loading, setLoading] = useState(false);
    const [editInfo, setEditInfo] = useState(false)
    const [avatar, setAvatar] = useState(info.avatar)
    const [initial, setInitial] = useState({
        fullName: info.fullName,
        nickName: info.nickName,
    })
    const [image, setImage] = useState({
        preview: '',
        raw: ''
    })
    const {values, ...formik} = useFormik({
        initialValues:{
            fullName: initial.fullName,
            nickName: initial.nickName,
        },
        onSubmit: (values, {setSubmitting, setErrors}) => {
            setLoading(true)
            setSubmitting(true)

            const formData = new FormData();
            formData.append('fullName', values.fullName);
            formData.append('nickName', values.nickName);
            if (image.preview) {
                formData.append('avatar', avatar);
            }
            request.post('v1/personnels/profile', formData)
                .then(res => {
                    toast.success(t('Berhasil Mengedit Informasi Umum'))
                    setEditInfo(false)
                    setInitial({
                        fullName: values.fullName,
                        nickName: values.nickName,
                    })
                })
                .catch(err => {
                    console.log(err)
                    toast.error(t('Gagal Mengedit Informasi Umum'))
                    return;
                })
                .finally(() => {
                    setLoading(false)
                    setSubmitting(false)
                })
        }
    })

    const changeAvatar = (e) => {
        e.preventDefault();
        if (e.target.files.length) {
            let preview = { ...image };
            preview['preview'] = URL.createObjectURL(e.target.files[0]);
            setAvatar(e.target.files[0])
            setImage(preview)
        }
    }

    const cancelEditInfo = () => {
        setEditInfo(false)
        formik.handleReset()
        setImage({
            preview: '',
            raw: ''
        })
        setAvatar(info.avatar)
    }

    return(
        <div>
            <Form onSubmit={formik.handleSubmit}>
                <Row className="my-1">
                    <div className="col-12 d-flex justify-content-between align-items-center mb-3">
                        <h5 className="content-sub-title mb-0">{t('informasiumum')}</h5>
                        {canEdit && <Button color="netis-color" onClick={() => setEditInfo(true)} style={{ visibility: editInfo ? 'hidden' : 'visible' }}>
                            <i className="fa fa-pencil" style={{ marginRight: 5 }}></i>Edit
                        </Button>}
                    </div>
                </Row>
                <Row className="my-1">
                    <div className="col-md-4">
                        <div className="text-center">
                        {avatar === null ?
                            image.preview ?
                                (
                                    <img src={image.preview} alt="dummy" width="200px" height="auto" />
                                ) :
                                (
                                    <div className="frame-profile-picture-empty">
                                        {t('belumadafoto')}
                                    </div>
                                ) :
                            image.preview ?
                                (
                                    <img src={image.preview} alt="dummy" width="200px" height="auto" />
                                ) :
                                (
                                    <div className="frame-profile-picture">
                                        <img src={process.env.REACT_APP_DOMAIN + "" + info.avatar} alt="avatar" className="img-fluid" />
                                    </div>
                                )
                        }
                        </div>
                        {canEdit && <FormGroup style={{ visibility: editInfo ? 'visible' : 'hidden' }}>
                            <p className="text-center"> Upload Avatar <span style={{ fontWeight: 'bold' }} >( Max. 5 MB )</span></p>
                            <Input type="file" id="avatar" accept="image/x-png,image/jpeg" name="avatar" onChange={changeAvatar} />
                        </FormGroup>}
                    </div>
                    <div className="col-md-8 d-flex flex-column">
                        <FormGroup>
                            <Label htmlFor="fullName" className="input-label">{t('namalengkap')}</Label>
                            <Input type="text" id="fullName" name="fullName" placeholder={t('namalengkap')}
                                disabled={editInfo === false ? true : false}
                                value={values.fullName} onChange={formik.handleChange} required
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label htmlFor="nickName" className="input-label">{t('namapanggilan')}</Label>
                            <Input type="text" id="nickName" name="nickName" placeholder={t('namapanggilan')}
                                disabled={editInfo === false ? true : false}
                                value={values.nickName} onChange={formik.handleChange} required
                            />
                        </FormGroup>
                        <div className="form-row">
                            <FormGroup className="col-md-6" >
                                <Label htmlFor="job" className="input-label">{t('jabatan')}</Label>
                                <Input type="text" value={info.job.name} disabled/>
                            </FormGroup>
                            <FormGroup className="col-md-6">
                                <Label htmlFor="unit" className="input-label">Unit</Label>
                                <Input type="text" value={info.unit.name} disabled/>
                            </FormGroup>
                        </div>
                        {canEdit && <div className="d-flex justify-content-end mt-auto" style={{ visibility: editInfo ? 'visible' : 'hidden' }}>
                            <Button color="white" disabled={loading} className="btn-white text-danger mr-3" onClick={cancelEditInfo}>{t('batal')}</Button>
                            <Button type="submit" disabled={loading} color="netis-color" style={{ width: '67px' }}>
                                {loading ? <Spinner color="light" size="sm" /> : t('simpan')}
                            </Button>
                        </div>}
                    </div>
                </Row>
            </Form>
        </div>
    )
}

export default translate(PersonnelProfileInfo)
