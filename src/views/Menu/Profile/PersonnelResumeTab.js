import React from "react";
import DataFormal from "./PersonnelResume/PersonnelResumeDataFormal";
import DataNonFormal from "./PersonnelResume/PersonnelResumeDataNonFormal";
import DataWorkingHistory from "./PersonnelResume/PersonnelResumeDataWorkingHistory";
import DataSkill from "./PersonnelResume/PersonnelResumeDataSkill";

function PersonnelResumeTab({ personnel }) {
    return (
        <div className="animated fadeIn">
            <DataFormal personnel={personnel} />
            <DataNonFormal personnel={personnel} />
            <DataWorkingHistory personnel={personnel} />
            <DataSkill personnel={personnel} />
        </div>
    )
}

export default PersonnelResumeTab
