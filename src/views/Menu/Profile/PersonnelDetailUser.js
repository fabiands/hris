import React, { Component, Fragment } from 'react';
import { Row, Col, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import PersonnelDocumentTab from './PersonnelDocumentTab';
import PersonnelResumeTab from './PersonnelResumeTab';
import PersonnelProfileTab from './PersonnelProfileTab';
import { connect } from 'react-redux';
import {
    translate,
} from 'react-switch-lang';
import request from '../../../utils/request';
import LoadingAnimation from '../../../components/LoadingAnimation';

const TAB_PROFILE = 1;
const TAB_DOCUMENT = 2;
const TAB_RESUME = 3;

class PersonnelDetail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dropdownOpen: [false, false],
            activeTab: TAB_PROFILE,
            loading: true,
            loadingButton: false,
            personnel: null,
        };
    }

    setActiveTab(tab) {
        this.setState({ activeTab: tab });
    }

    toggle(tabPane, tab) {
        const newArray = this.state.activeTab.slice()
        newArray[tabPane] = tab
        this.setState({
            activeTab: newArray,
        });
    }

    componentDidMount = () => {
        request.get('v1/personnels/profile')
            .then(res => {
                const personnel = res.data.data[0];
                this.setState({ personnel });
            })
            .finally(() => {
                this.setState({ loading: false });
            })
    }

    render() {
        if (this.state.loading) {
            return <LoadingAnimation/>
        }

        const { t } = this.props;

        return (
            <div className="">
                < Fragment >
                    <Row>
                        <Col sm="12" md="12">
                            <h4 className="content-title">{t('Detail Karyawan')}</h4>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12" md="12" className="mb-12">
                            <Nav tabs>
                                <NavItem >
                                    <NavLink
                                        active={this.state.activeTab === TAB_PROFILE}
                                        onClick={() => { this.setActiveTab(TAB_PROFILE); }}
                                    >
                                        Profile
                                            </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        active={this.state.activeTab === TAB_DOCUMENT}
                                        onClick={() => { this.setActiveTab(TAB_DOCUMENT); }}
                                    >
                                        {t('dokumen')}
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        active={this.state.activeTab === TAB_RESUME}
                                        onClick={() => { this.setActiveTab(TAB_RESUME); }}
                                    >
                                        Resume
                                            </NavLink>
                                </NavItem>
                            </Nav>
                            <TabContent activeTab={this.state.activeTab}>
                                {this.state.activeTab === TAB_PROFILE && <TabPane tabId={TAB_PROFILE}>
                                    <PersonnelProfileTab personnel={this.state.personnel} />
                                </TabPane>}
                                {this.state.activeTab === TAB_DOCUMENT && <TabPane tabId={TAB_DOCUMENT}>
                                    <PersonnelDocumentTab personnel={this.state.personnel} />
                                </TabPane>}
                                {this.state.activeTab === TAB_RESUME && <TabPane tabId={TAB_RESUME}>
                                   <PersonnelResumeTab personnel={this.state.personnel} />
                                </TabPane>}
                            </TabContent>
                        </Col>
                    </Row>
                </Fragment>
            </div >
        );
    }
}

export default connect(({user}) => ({user}))(translate(PersonnelDetail));
