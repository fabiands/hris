import React, { Component, useMemo, memo } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Card, CardBody, CardTitle, Modal, ModalBody, Alert, Spinner, ModalHeader } from 'reactstrap';
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import Widget01 from './Component/Widget01';
import request from '../../../utils/request';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import { translate, t } from 'react-switch-lang';
import useSWR from 'swr';
import Loader from 'react-loader-spinner';
import { iconClassByOptionType } from '../Calendar/shared';

const localizer = momentLocalizer(moment);

toast.configure()
class DashboardUser extends Component {

    constructor(props) {
        super(props);

        this.state = {
            date: new Date(),
            startDate: moment().subtract(1, 'month').startOf('month'),
            endDate: moment().add(1, 'month').startOf('month'),
            events: [],
            event: null,
            calendarLoading: true,
            modalDetailEvent: false,
        };
    }

    componentDidMount = () => {
        this.getDataEvents(this.state.startDate, this.state.endDate);
    }
    modalDetailEvent = (event) => {
        this.setState({ event })
    }

    cancelModalDetailEvent = () => {
        this.setState({ event: null })
    }

    onRangeChange = (dates) => {
        let start, end, changed = false;
        if (Array.isArray(dates)) {
            start = moment(dates[0]);
            end = moment(dates[dates.length - 1]);
        } else {
            start = moment(dates.start);
            end = moment(dates.end);
        }
        this.setState(prevState => {
            if (start.isBetween(prevState.startDate, prevState.endDate, null, "[]") && end.isBetween(prevState.startDate, prevState.endDate, null, "[]")) {
                return prevState;
            } else {
                changed = true;
                return { ...prevState, startDate: start, endDate: end };
            }
        }, () => {
            if (changed) {
                this.getDataEvents(this.state.startDate, this.state.endDate)
            }
        });
    }

    eventStyleGetter = (event, start, end, isSelected) => {
        return { className: event.type === 'holiday' ? 'bg-danger' : (event.type === 'agenda') ? 'bg-success' : 'bg-info' }
    }

    getDataEvents = async (startDate, endDate) => {
        try {
            this.setState({ calendarLoading: true })
            const { data } = await request.get(`v3/personnel/event?start=${startDate.format('YYYY-MM-DD')}&end=${endDate.format('YYYY-MM-DD')}`);
            this.setState({
                events: data.data.map((event) => ({
                    ...event,
                    id: parseInt(event.id),
                    start: new Date(`${event.date.start}`),
                    end: new Date(`${event.date.end}`),
                    cutiTitle: event.title
                }))
            })
        } catch (err) {
            console.log(err)
            throw err;
        } finally {
            this.setState({ calendarLoading: false })
        }
    }

    render() {
        const { t } = this.props;
        const calendarToolbar = {
            today: t("hariini"),
            previous: <i className="fa fa-angle-left"></i>,
            next: <i className="fa fa-angle-right"></i>,
            month: t("bulanan"),
            week: t("mingguan"),
            day: t("harian")
        }
        return (
            <div className="animated fadeIn">

                <Row className="attendance" >
                    <Col xs="12">
                        <Alert color="primary">
                            <p>{t('keteranganiphone')}</p>
                        </Alert>
                    </Col>
                    {this.props.user.faceId === null ?
                        <Col xs="12">
                            <Link to="/attendance/webcam/register">
                                <div className="card menu-item">
                                    <div className="card-body check-in">
                                        <div className="menu-img mt-2 mb-3 text-center">
                                            <img src={require("../../../assets/assets_ari/register.png")} alt="" />
                                        </div>
                                        <div className="menu-title mb-2 text-center">
                                            <h3 className="mb-0 title-menu-company"><b>Register</b></h3>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        </Col>
                        :
                        <>
                            <Col xs="6">
                                <Link to="/attendance/webcam/clockin">
                                    <div className="card menu-item">
                                        <div className="card-body check-in">
                                            <div className="menu-img mt-2 mb-3 text-center">
                                                <img src={require("../../../assets/assets_ari/check_in.png")} alt="" />
                                            </div>
                                            <div className="menu-title mb-2 text-center">
                                                <h5 className="mb-0 title-menu-company"><b>{t('clockIn')}</b></h5>
                                            </div>
                                        </div>
                                    </div>
                                </Link>
                            </Col>
                            <Col xs="6">
                                <Link to="/attendance/webcam/clockout">
                                    <div className="card menu-item">
                                        <div className="card-body check-out">
                                            <div className="menu-img mt-2 mb-3 text-center">
                                                <img src={require("../../../assets/assets_ari/check_out.png")} alt="" />
                                            </div>
                                            <div className="menu-title mb-2 text-center">
                                                <h5 className="mb-0 title-menu-company"><b>{t('clockOut')}</b></h5>
                                            </div>
                                        </div>
                                    </div>
                                </Link>
                            </Col>
                        </>
                    }
                </Row>
                <Row>
                    <Col className="mb-4" xs="12" sm="12" md="4" lg="4">
                        <Widget01 header={this.props.user.holidayBalance || 0} mainText={t('sisaJatahCutiTahunan')}
                            button={true} buttonText={t('ajukancuti')} to={'/cuti'}
                        />
                    </Col>

                    <Col className="mb-4" xs="12" sm="12" md="4" lg="4">
                        <StatusPengajuanCuti />
                    </Col>

                    <Col className="mb-4" xs="12" sm="12" md="4" lg="4">
                        <StatusPengajuanReimburse />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card className="dashboard-card">
                            <CardBody>
                                {(this.state.calendarLoading) &&
                                    <div style={{ zIndex: 10, position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, background: 'rgba(255,255,255, 0.75)', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                                        <Spinner style={{ width: 48, height: 48 }} />
                                    </div>
                                }
                                <Row className="mb-4">
                                    <Col xs="12">
                                        <CardTitle className="mb-0">
                                            <h4 className="mb-0">{t('kalender')}</h4>
                                        </CardTitle>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs="12">
                                        <Calendar
                                            popup={true}
                                            localizer={localizer}
                                            defaultDate={this.state.date}
                                            messages={calendarToolbar}
                                            defaultView="month"
                                            views={["month", "week", "day"]}
                                            events={this.state.events}
                                            style={{ height: "calc(100vh - 55px - 3rem)" }}
                                            onSelectEvent={event => this.modalDetailEvent(event)}
                                            onRangeChange={this.onRangeChange}
                                            eventPropGetter={(this.eventStyleGetter)}
                                        />
                                    </Col>
                                    <EventDetailModal event={this.state.event} toggle={this.cancelModalDetailEvent} />
                                </Row>

                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        );
    }
}

export const EventDetailModal = memo(({ event, toggle }) => {
    const { data: agendaResponse } = useSWR(() => event.type === 'agenda' ? `v3/company/event/${event.id}` : '');
    const participants = useMemo(() => agendaResponse?.data?.data?.participant_names ?? [], [agendaResponse]);

    return (
        <Modal isOpen={!!event} toggle={toggle} scrollable>
            {!!event && <>
                <ModalHeader tag="div" toggle={toggle}>
                    <h4 className={`content-sub-title mb-2${event.type === 'holiday' ? ' text-danger' : ''}`}>{event.title}</h4>
                    <div>
                        {event.type !== 'paid_leave' ?
                            <>
                                &mdash; <span className="text-muted">{moment(event.date).format('dddd, DD MMMM YYYY')}</span>
                                {event.type === 'holiday' ?
                                    ''
                                    :
                                    (event.start && event.end) &&
                                    <>
                                        <i className="fa fa-circle text-muted ml-2 mr-2" style={{ fontSize: '6px' }}></i>
                                        <span className="text-muted">{moment(event.start, "HH:mm:ss").format("HH:mm")} {t('s/d')} {moment(event.end, "HH:mm:ss").format("HH:mm")}</span>
                                    </>
                                }
                            </>
                            : <>&mdash; <span className="text-muted">{moment(event.start).format('dddd, DD MMMM YYYY')} {t('s/d')} {moment(event.end).format('dddd, DD MMMM YYYY')}</span></>
                        }
                    </div>
                </ModalHeader>
                <ModalBody>
                    {event.type === 'agenda' &&
                        <p className="mb-3 p-3 text-muted" style={{ borderLeft: 'solid 2px #73818f', whiteSpace: 'pre-wrap' }}>
                            {(event.description || t('Tidak ada deskripsi'))}
                        </p>
                    }
                    {event.type === 'paid_leave' &&
                        <div>
                            <div className="mb-3">
                                <div>Nama Karyawan</div>
                                <div className="text-muted">{event.description.personnel.fullName}</div>
                            </div>
                            <div className="mb-3">
                                <div>Kategori Cuti</div>
                                <div className="text-muted">{event.cutiTitle}</div>
                            </div>
                            <div className="mb-3">
                                <div>Deskripsi</div>
                                <div className="text-muted">{(event.description.message || t('Tidak ada deskripsi'))}</div>
                            </div>
                            {event.description.document &&
                                <div className="mb-3">
                                    <div className="text-muted mb-2">Lampiran</div>
                                    <img src={process.env.REACT_APP_DOMAIN + event.description.document} alt="document" width="200px" className="rounded-12" />
                                </div>
                            }
                        </div>
                    }
                    {event.type === 'agenda' &&
                        <>
                            <h5 className="my-3">
                                Peserta
                            </h5>
                            <div className="admin-calendar-selected-participants mt-1">
                                {participants.map((name, index) => (
                                    <div key={index} className="admin-calendar-selected-participants-item">
                                        {name === 'ALL' ?
                                            <>
                                                <i className={`mr-3 ${iconClassByOptionType['all']}`}></i>
                                                {t('Seluruh Karyawan')}
                                            </>
                                            :
                                            <>
                                                <i className={`mr-3 ${iconClassByOptionType[name.includes('@') ? 'email' : 'personnel']}`}></i>
                                                {name}
                                            </>
                                        }
                                    </div>
                                ))}
                            </div>
                        </>
                    }
                    {event.type === 'holiday' &&
                        <>
                            <i className="fa fa-calendar text-muted mr-2"></i>
                            <span className="text-muted">{t('Hari Libur')}</span>
                        </>
                    }
                </ModalBody>
            </>}

        </Modal>
    );
})

const StatusPengajuanReimburse = memo(() => {
    const { data: response, error } = useSWR('v1/reimbursement/status');
    const loading = !response && !error;
    const reimburseStatus = useMemo(() => {
        if (response?.data?.data) {
            const status = response.data.data;
            return {
                pending: status.pending + status.process1 + status.process2,
                done: status.done,
                rejected: status.rejected
            }
        }

        return {
            pending: 0,
            done: 0,
            rejected: 0,
        };
    }, [response])
    return (
        <Widget01 header={t('statuspengajuanreimburse')} >
            <div className="d-flex align-items-center justify-content-between mb-2 pt-2">
                <div><i className="fa fa-circle text-warning mr-1"></i> Pending</div>
                {loading ? <Loader type="ThreeDots" color="#305574" height={20} width={20} /> : reimburseStatus.pending}
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
                <div><i className="fa fa-circle text-success mr-1"></i> {t('ditransfer')}</div>
                {loading ? <Loader type="ThreeDots" color="#305574" height={20} width={20} /> : reimburseStatus.done}
            </div>
            <div className="d-flex align-items-center justify-content-between pb-2">
                <div><i className="fa fa-circle text-danger mr-1"></i> {t('ditolak')}</div>
                {loading ? <Loader type="ThreeDots" color="#305574" height={20} width={20} /> : reimburseStatus.rejected}
            </div>
        </Widget01>
    );
})

const StatusPengajuanCuti = memo(() => {
    const { data: response, error } = useSWR('v2/holidays/status');
    const loading = !response && !error;
    const holidayStatus = useMemo(() => {
        if (response?.data?.data) {
            const status = response.data.data;
            return {
                pending: status.pending + status.approved1,
                approved: status.approved2,
                rejected: status.rejected
            }
        }

        return {
            pending: 0,
            approved: 0,
            rejected: 0,
        };
    }, [response])
    return (
        <Widget01 header={t('statuspengajuancuti')} >
            <div className="d-flex align-items-center justify-content-between mb-2 pt-2">
                <div><i className="fa fa-circle text-warning mr-1"></i> Pending</div>
                {loading ? <Loader type="ThreeDots" color="#305574" height={20} width={20} /> : holidayStatus.pending}
            </div>
            <div className="d-flex align-items-center justify-content-between mb-2">
                <div><i className="fa fa-circle text-success mr-1"></i> {t('disetujui')}</div>
                {loading ? <Loader type="ThreeDots" color="#305574" height={20} width={20} /> : holidayStatus.approved}
            </div>
            <div className="d-flex align-items-center justify-content-between pb-2">
                <div><i className="fa fa-circle text-danger mr-1"></i> {t('ditolak')}</div>
                {loading ? <Loader type="ThreeDots" color="#305574" height={20} width={20} /> : holidayStatus.rejected}
            </div>
        </Widget01>
    );
})

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}
export default connect(mapStateToProps)(translate(DashboardUser));
