import React, { Fragment } from 'react';
import { Card, CardBody, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

class Widget01 extends React.PureComponent {

    render() {
        return (
            <Card className="dashboard-card mb-0">
                <CardBody className="d-flex flex-column">
                    <div className="h4 m-0">{this.props.header}</div>
                    {!!this.props.mainText && <div>{this.props.mainText}</div>}
                    {!!this.props.children && <div className="widget01-children mt-auto">{this.props.children}</div>}
                    {!!this.props.button &&
                        <Fragment>
                            <hr className="mt-auto" />
                            <div className="d-flex justify-content-end mt-2">
                                <Link to={`${this.props.to}`}><Button color="netis-color">{this.props.buttonText}</Button></Link>
                            </div>
                        </Fragment>
                    }
                </CardBody>
            </Card>
        );
    }
}


Widget01.defaultProps = {
    smallText: 'Excepteur sint occaecat...',
    button: false,
    buttonText: 'Button'
}

export default Widget01;
