import React, { useCallback } from 'react';
import { t } from 'react-switch-lang';
import { Button, Input, FormGroup, Label, Form, Spinner } from 'reactstrap';
import { toast } from "react-toastify";
import request from '../../../../utils/request';
import LoadingAnimation from '../../../../components/LoadingAnimation';
import { useFormik } from 'formik';
import { useHistory } from 'react-router-dom';
import useSWR from 'swr';
import ModuleSection from './ModuleSection';
import PrivilegeCheckbox from './PrivilegeCheckbox';

function RoleCreate(props) {
    const history = useHistory();

    const { data: privilegeResponse, error } = useSWR('v1/master/privileges');
    const privileges = privilegeResponse?.data?.data ?? [];
    const loading = !privilegeResponse && !error; 

    const { values, touched, errors, isSubmitting, setFieldValue, setValues, handleChange, ...formik } = useFormik({
        initialValues: {
            roleName: '',
            privileges: [],
        },
        // validationSchema: ValidationFormSchema,
        onSubmit: (values, { setSubmitting, setErrors }) => {
            setSubmitting(true);
            request.post('v1/master/roles', {
                roleName: values.roleName,
                privileges: values.privileges,
            })
                .then(res => {
                    toast.success(t('lowonganberhasildibuat'));
                    props.history.goBack();
                })
                .catch(err => {
                    if (err.response?.status === 422) {
                        setErrors(err.response.data.errors);
                        return;
                    }
                    Promise.reject(err);
                })
                .finally(() => setSubmitting(false));
        }
    });

    const onPrivilegeBadgeClick = useCallback((privilege) => {
        setValues((values) => {
            if (values.privileges.includes(privilege.name)) {
                return { ...values, privileges: values.privileges.filter(name => name !== privilege.name) };
            } else {
                return { ...values, privileges: [...values.privileges, privilege.name] }
            }
        })
    }, [setValues]);

    const onChangeAll = useCallback((privilegeNames) => {
        setValues((values) => {
            console.log(privilegeNames, values.privileges);
            if (privilegeNames.some(privName => values.privileges.includes(privName))) {
                console.log('detach all', values.privileges.filter(privName => !privilegeNames.includes(privName)));
                return {...values, privileges: values.privileges.filter(privName => !privilegeNames.includes(privName)) };
            } else {
                const privilegeSets = new Set([...values.privileges, ...privilegeNames]); 
                console.log('attach all', Array.from(privilegeSets));

                return {...values, privileges: Array.from(privilegeSets) };
            }
        })
    }, [setValues])

    return <div className="animated fadeIn">
        <div className="d-flex align-items-center mb-4">
            <button className="btn btn-sm btn-netis-primary" onClick={() => history.goBack() }><i className="fa fa-chevron-left"></i> {t('kembali')}</button>
            <h5 className="content-sub-title mb-0 border-left pl-3 ml-3">{t('tambah')} {t('Peran')}</h5>
        </div>
        {loading ? <LoadingAnimation /> :
            (
                <div className="card card-body">
                    <Form onSubmit={formik.handleSubmit}>
                        <FormGroup>
                            <Label htmlFor="role-name" className="input-label font-weight-bold"><strong>{t('Nama Peran')}</strong> </Label>
                            <Input type="text" name="roleName" id="role-name" value={values.roleName} onChange={handleChange} required />
                        </FormGroup>
                        <div className="d-flex justify-content-between align-items-center mb-2">
                            <strong>Permissions:</strong>
                        </div>
                        <div className="py-3 pt-2 bg-gray-100" style={{ margin: '0 -1.25rem', paddingLeft: '1.25rem', paddingRight: '1.25rem' }}>
                            {
                                privileges.map((m, km) =>
                                    <ModuleSection key={m.module}
                                        module={m.module}
                                        privileges={m.privileges}
                                        checkedAll={m.privileges.every(p => values.privileges.includes(p.name))}
                                        indeterminate={m.privileges.some(p => values.privileges.includes(p.name))}
                                        onChangeAll={onChangeAll}>
                                    {/* <div key={m.module} style={{ marginTop: 15 }}>
                                        <div className="d-flex align-items-center mb-3">
                                            <h5 className="mr-3 mb-0">{m.module.toUpperCase()}</h5>
                                            <CustomInput type="checkbox" label={t('Check Semua')} indeterminate />
                                        </div>
                                        <div style={{ columns: 3, columnGap: 0 }}> */}
                                            {m.privileges.map((p, kp) =>
                                                <PrivilegeCheckbox
                                                    key={p.name}
                                                    privilege={p}
                                                    onClick={onPrivilegeBadgeClick}
                                                    isActive={values.privileges.includes(p.name)}
                                                />
                                            )}
                                        {/* </div>
                                    </div> */}
                                    </ModuleSection>
                                )
                            }
                        </div>
                        <FormGroup>
                            <Button
                                type="submit"
                                color="netis-primary"
                                size="lg"
                                disabled={isSubmitting}
                                className="float-right"
                            >
                                {isSubmitting ? (
                                    <React.Fragment>
                                        <Spinner size="sm" color="light" /> Saving...
                                    </React.Fragment>
                                ) : (
                                        t("simpan")
                                    )}
                            </Button>
                        </FormGroup>
                    </Form>
                </div>
            )
        }
    </div>;
}

export default RoleCreate;
