import React, { useCallback, useEffect } from 'react';
import { t } from 'react-switch-lang';
import { Button, Input, FormGroup, Label, Form, Spinner } from 'reactstrap';
import { toast } from "react-toastify";
import request from '../../../../utils/request';
import LoadingAnimation from '../../../../components/LoadingAnimation';
import { useFormik } from 'formik';
import { useHistory, useParams } from 'react-router-dom';
import useSWR from 'swr';
import ModuleSection from './ModuleSection';
import PrivilegeCheckbox from './PrivilegeCheckbox';

function RoleEdit(props) {
    const history = useHistory();
    const params = useParams();

    const { data: privilegesResponse, error: privilegesError } = useSWR('v1/master/privileges');
    const privileges = privilegesResponse?.data?.data ?? [];
    const { data: roleResponse, error: roleError } = useSWR(() => 'v1/master/roles/' + params.id, { revalidateOnFocus: false, revalidateOnReconnect: false });
    
    const loading = !privilegesResponse && !privilegesError && !roleResponse && !roleError; 

    const { values, touched, errors, isSubmitting, setFieldValue, setValues, handleChange, ...formik } = useFormik({
        initialValues: {
            roleName: '',
            privileges: [],
        },
        // validationSchema: ValidationFormSchema,
        onSubmit: (values, { setSubmitting, setErrors }) => {
            setSubmitting(true);
            request.put(`v1/master/roles/${params.id}`, {
                roleName: values.roleName,
                privileges: values.privileges,
            })
                .then(res => {
                    toast.success(t('Data berhasil disimpan'));
                    props.history.goBack();
                })
                .catch(err => {
                    if (err.response?.status === 422) {
                        setErrors(err.response.data.errors);
                        return;
                    }
                    Promise.reject(err);
                })
                .finally(() => setSubmitting(false));
        }
    });

    useEffect(() => {
        if (roleResponse?.data?.data) {
            const roleName = roleResponse?.data?.data?.name;
            const privileges = (roleResponse?.data?.data?.privileges ?? []).map(priv => priv.name);
            setValues({ roleName, privileges });
        }
    }, [roleResponse, setValues])

    const onPrivilegeBadgeClick = useCallback((privilege) => {
        setValues((values) => {
            if (values.privileges.includes(privilege.name)) {
                return { ...values, privileges: values.privileges.filter(name => name !== privilege.name) };
            } else {
                return { ...values, privileges: [...values.privileges, privilege.name] }
            }
        })
    }, [setValues]);

    const onChangeAll = useCallback((privilegeNames) => {
        setValues((values) => {
            console.log(privilegeNames, values.privileges);
            if (privilegeNames.some(privName => values.privileges.includes(privName))) {
                console.log('detach all', values.privileges.filter(privName => !privilegeNames.includes(privName)));
                return {...values, privileges: values.privileges.filter(privName => !privilegeNames.includes(privName)) };
            } else {
                const privilegeSets = new Set([...values.privileges, ...privilegeNames]); 
                console.log('attach all', Array.from(privilegeSets));

                return {...values, privileges: Array.from(privilegeSets) };
            }
        })
    }, [setValues])

    return <div className="animated fadeIn">
        <div className="d-flex align-items-center mb-4">
            <button className="btn btn-sm btn-netis-primary" onClick={() => history.push('/company/pengaturan-role') }><i className="fa fa-chevron-left"></i> {t('kembali')}</button>
            <h5 className="content-sub-title mb-0 border-left pl-3 ml-3">{t('Edit')} {t('Peran')}</h5>
        </div>
        {loading ? <LoadingAnimation /> :
            (
                <div className="card">
                    <Form onSubmit={formik.handleSubmit}>
                    <div className="card-body pb-0">
                        <FormGroup>
                            <Label htmlFor="role-name" className="input-label font-weight-bold"><strong>{t('Nama Peran')}</strong> </Label>
                            <Input type="text" name="roleName" id="role-name" value={values.roleName} onChange={handleChange} required />
                        </FormGroup>
                        <div className="d-flex justify-content-between align-items-center mb-2 mt-5">
                            <strong>Permissions:</strong>
                        </div>
                        <div className="py-3 pt-2 bg-gray-100" style={{ margin: '0 -1.25rem', paddingLeft: '1.25rem', paddingRight: '1.25rem' }}>
                            {
                                privileges.map((m, km) =>
                                    <ModuleSection key={m.module}
                                        module={m.module}
                                        privileges={m.privileges}
                                        checkedAll={m.privileges.every(p => values.privileges.includes(p.name))}
                                        indeterminate={m.privileges.some(p => values.privileges.includes(p.name))}
                                        onChangeAll={onChangeAll}>
                                            {m.privileges.map((p, kp) =>
                                                <PrivilegeCheckbox
                                                    key={p.name}
                                                    privilege={p}
                                                    onClick={onPrivilegeBadgeClick}
                                                    isActive={values.privileges.includes(p.name)}
                                                />
                                            )}
                                        {/* </div>
                                    </div> */}
                                    </ModuleSection>
                                )
                            }
                        </div>
                    </div>
                    <div className="card-footer bg-white">
                        <Button
                            type="submit"
                            color="netis-primary"
                            size="lg"
                            disabled={isSubmitting}
                        >
                            {isSubmitting ? (
                                <React.Fragment>
                                    <Spinner size="sm" color="light" /> Saving...
                                </React.Fragment>
                            ) : (
                                    t("simpan")
                                )}
                        </Button>
                    </div>
                    </Form>
                </div>
            )
        }
    </div>;
}

export default RoleEdit;
