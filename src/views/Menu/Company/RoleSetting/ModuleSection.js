import React, { memo, useRef, useEffect } from 'react';
import { CustomInput } from 'reactstrap';
import { t } from 'react-switch-lang';

const ModuleSection = memo((props) => {
    const { module, privileges, checkedAll, indeterminate, onChangeAll } = props;
    const cbRef = useRef();

    useEffect(() => {
        if (cbRef.current) {
            cbRef.current.indeterminate = !checkedAll && indeterminate;
        }
    }, [checkedAll, indeterminate]);

    function handleChange() {
        onChangeAll(privileges.map(p => p.name));
    }

    return (
    <div style={{ marginTop: 15, marginBottom: 46 }}>
        <div className="d-flex align-items-center mb-3">
                <h5 className="mr-3 mb-0">{module.toUpperCase()}</h5>
                <CustomInput id={`checkall-${module}`} type="checkbox" label={t('Check All')} innerRef={cbRef} checked={checkedAll} onChange={handleChange} />
        </div>
        <div style={{ columnGap: 0 }} className="columns-sm-2 columns-md-3 columns-xl-4">
            {props.children}
        </div>
    </div>);
});

export default ModuleSection;
