import React, { memo } from 'react';
import { UncontrolledPopover, PopoverBody, CustomInput } from 'reactstrap';

const PrivilegeCheckbox = memo((props) => {
    const { privilege, isActive, onClick } = props;

    function onCheck() {
        onClick(privilege);
    }

    const label = <>
        {privilege.name}
        {!!privilege.description && <><i id={`popover-${privilege.name}`} className="fa fa-info-circle text-muted ml-1"></i>
        <UncontrolledPopover trigger="hover" target={`popover-${privilege.name}`} placement="right" flip>
            <PopoverBody>{privilege.description}</PopoverBody>
        </UncontrolledPopover></>}
    </>

    return (<div className="mb-2">
        <CustomInput inline id={`privilege-${privilege.name}`} type="checkbox" label={label} onChange={onCheck} checked={isActive}/>
    </div>)
});

export default PrivilegeCheckbox;
