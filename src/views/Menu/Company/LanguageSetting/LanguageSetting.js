import React, { useMemo, useState } from'react';
import { useFormik } from 'formik';
import {Form, Button, CustomInput, FormGroup, ListGroup, ListGroupItem, ListGroupItemHeading, Spinner} from 'reactstrap';
import {translate, t} from 'react-switch-lang';
import request from '../../../../utils/request';
import { toast } from 'react-toastify';

function LanguageSetting({languageData}){
    const [saving, setSaving] = useState(false);

    const {values, ...formik} = useFormik({
        initialValues: {
            initial : languageData,
            current : ''
        },
        onSubmit: (values, { setSubmitting}) => {
            setSubmitting(true);
            setSaving(true);
            let formData = new FormData();
            formData.append('language', values.current)
            request.post('v1/company/settings/language', formData)
                .then(() => {
                    formik.setFieldValue('initial', values.current);
                })
                .catch((err) => {
                    console.log(err)
                    toast.error(t('Gagal Mengubah Wilayah Perusahaan'));
                    return;
                })
                .finally(() => {
                    setSaving(false);
                    setSubmitting(false);
                })
        }
    })

    const isChanged = useMemo(() => {
        if(values.current && values.initial !== values.current){
            return true;
        }
        else {
            return false;
        }
    }, [values])

    const changeLanguage = (e) => {
        const {value} = e.target;
        formik.setFieldValue('current', value);
        formik.setFieldValue('initial', null);
    }

    return(
        <div className="animated fadeIn">
            <div className="md-company-header">
                <h4 className="content-title mb-4">{t('Pengaturan Wilayah Perusahaan')}</h4>
                <hr className="mb-0" />
            </div>

            <Form onSubmit={formik.handleSubmit}>
                <div className="card card-body p-0">
                    <ListGroup flush className="settings-list">
                        <ListGroupItem className={isChanged ? 'is-changed' : undefined}>
                            <ListGroupItemHeading tag="h6">{t('Wilayah Perusahaan')}</ListGroupItemHeading>
                            <FormGroup className="mt-2 mb-0">
                                <CustomInput
                                    id="id"
                                    // disabled={!can('edit-company-reimburse')}
                                    type="radio"
                                    name="CompanyLanguage"
                                    value="id"
                                    onChange={changeLanguage}
                                    checked={values.initial === 'id' || values.current === 'id'}
                                    label={t('Indonesia')}
                                />
                            </FormGroup>
                            <FormGroup className="mt-2 mb-0">
                                <CustomInput
                                    id="en"
                                    // disabled={!can('edit-company-reimburse')}
                                    type="radio"
                                    name="CompanyLanguage"
                                    value="en"
                                    onChange={changeLanguage}
                                    checked={values.initial === 'en' || values.current === 'en'}
                                    label={t('Di luar Indonesia')}
                                />
                            </FormGroup>
                        </ListGroupItem>

                    </ListGroup>
                </div>

                <div className="d-flex mt-3 justify-content-center">
                    <Button
                        color="secondary"
                        type="button"
                        onClick={() => {
                            formik.setFieldValue('initial', languageData);
                            formik.setFieldValue('current', null)
                        }}
                        disabled={!isChanged || saving}
                    >
                        <i className="fa fa-repeat mr-1" /> {t('reset')}
                    </Button>
                    <Button className="ml-2" color="netis-color" type="submit" disabled={!isChanged || saving}>
                        {saving ?
                            <><Spinner size="xs" color="light" /> Saving... </>
                            :
                            <><i className="fa fa-check-circle mr-1" /> {t('simpan')}</>
                        }
                    </Button>
                </div>
            </Form>
        </div>
    )
}

export default translate(LanguageSetting);