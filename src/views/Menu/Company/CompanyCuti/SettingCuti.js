import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from "reactstrap";
import CompanyCuti from "./CompanyCuti";
import MasterCuti from "./MasterCuti";
import { t, translate } from "react-switch-lang";
import { useLocation } from 'react-router-dom';

function SettingCuti() {
    const location = useLocation();
    const activeTab = location.hash || '#policy';

    return (
        <div>
            <Nav tabs>
                <NavItem>
                    <NavLink active={activeTab === '#policy'} href="#policy">
                        {t("kebijakancuti")}
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink active={activeTab === '#master-leaves-category'} href="#master-leaves-category">
                        {t("masterdatacuti")}
                    </NavLink>
                </NavItem>
            </Nav>
            <TabContent activeTab={activeTab}>
                <TabPane tabId="#policy">
                    <CompanyCuti />
                </TabPane>

                <TabPane tabId="#master-leaves-category">
                    <MasterCuti />
                </TabPane>
            </TabContent>
        </div>
    )
}

export default translate(SettingCuti);
