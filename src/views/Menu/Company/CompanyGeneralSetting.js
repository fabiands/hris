import React, { useEffect, useState } from 'react';
import { useAuthUser } from '../../../store';
import LanguageSetting from './LanguageSetting/LanguageSetting';
import OvertimePolicySetting from './OvertimePolicySetting/OvertimePolicySetting';
import ReimburseSetting from './ReimburseSetting/ReimburseSetting';
import LoadingAnimation from '../../../components/LoadingAnimation';
import request from '../../../utils/request';

function CompanyGeneralSetting(){

    const user = useAuthUser();
    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        request.get('v1/company/settings/overtimes')
            .then(res => {
                setData(res.data.data);
            })
            .finally(() => setLoading(false))
    }, [])

    if (loading){
        return <LoadingAnimation />
    }

    return(
        <div className="animated fadeIn pt-2">
            <div className="my-3">
                {data && <OvertimePolicySetting overtimeData={data} />}
            </div>
            <div className="my-3">
                {user.personnel.company && <ReimburseSetting reimburseData={user?.personnel?.company?.reimburseSuperiorApproval} />}
            </div>
            <div className="my-3">
                <LanguageSetting languageData={user?.companyLanguage} />
            </div>
        </div>
    )
}

export default CompanyGeneralSetting;