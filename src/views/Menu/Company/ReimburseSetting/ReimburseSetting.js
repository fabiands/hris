import React, { useState, useMemo } from 'react';
import { t } from 'react-switch-lang';
import { Form, ListGroupItemHeading, ListGroupItem, ListGroup, FormGroup, CustomInput, Button, Spinner } from 'reactstrap';
import { useUserPrivileges } from '../../../../store';
import { useFormik } from 'formik';
import request from '../../../../utils/request';
import { toast } from 'react-toastify';

function ReimburseSetting({ reimburseData }) {
    const [saving, setSaving] = useState(false);
    const { can } = useUserPrivileges();

    const { values, ...formik } = useFormik({
        initialValues: {
            initial: reimburseData ? "yes" : "no",
            current: null
        },
        onSubmit: (values, { setSubmitting }) => {
            setSubmitting(true);
            setSaving(true);
            let value = values.current === "yes" ? true : false;

            request.post('v1/company/settings/reimburses', { reimburseSuperiorApproval: value })
                .then(() => {
                    // toast.success(t('Berhasil Mengubah Bahasa Perusahaan'));
                    formik.setFieldValue('initial', values.current);
                })
                .catch(() => {
                    toast.error(t('Gagal Mengubah Pengaturan Reimburse'));
                    return;
                })
                .finally(() => {
                    setSaving(false);
                    setSubmitting(false);
                })
        }
    })

    function handleChange(e) {
        const { value } = e.target;
        formik.setFieldValue('initial', null);
        formik.setFieldValue('current', value);
    }

    const isChanged = useMemo(() => {
        if (values.current && values.initial !== values.current) {
            return true;
        }
        else {
            return false;
        }
    }, [values]);

    return (
        <div className="animated fadeIn">
            <div className="md-company-header">
                <h4 className="content-title mb-4">{t('Pengaturan Kebijakan Reimburse')}</h4>
                <hr className="mb-0" />
            </div>
            <Form onSubmit={formik.handleSubmit}>
                <div className="card card-body p-0">
                    <ListGroup flush className="settings-list">
                        <ListGroupItem className={isChanged ? 'is-changed' : undefined}>
                            <ListGroupItemHeading tag="h6">{t('Apakah reimburse harus disetujui oleh atasan terlebih dahulu?')}</ListGroupItemHeading>
                            <FormGroup className="mt-2 mb-0">
                                <CustomInput id="reimburseSuperiorApprovalYes" disabled={!can('edit-company-reimburse')} type="radio" name="reimburseSuperiorApproval" value="yes" onChange={handleChange}
                                    checked={values.initial === "yes" || values.current === "yes"}
                                    label={t('Ya')}
                                />
                            </FormGroup>
                            <FormGroup className="mt-2 mb-0">
                                <CustomInput id="reimburseSuperiorApprovalNo" disabled={!can('edit-company-reimburse')} type="radio" name="reimburseSuperiorApproval" value="no" onChange={handleChange}
                                    checked={values.initial === "no" || values.current === "no"}
                                    label={t('Tidak')}
                                />
                            </FormGroup>
                        </ListGroupItem>

                    </ListGroup>
                </div>

                {can('edit-company-reimburse') &&
                    <div className="d-flex mt-3 justify-content-center">
                        <Button color="secondary" type="button" onClick={formik.handleReset} disabled={!isChanged}><i className="fa fa-repeat mr-1"></i> {t('reset')}</Button>
                        <Button className="ml-2" color="netis-color" type="submit" disabled={saving || !isChanged}>
                            {saving ? <React.Fragment><Spinner size="sm" color="light" /> {t('loading')}</React.Fragment> :
                                <React.Fragment><i className="fa fa-check-circle mr-1"></i> {t('simpan')}</React.Fragment>}
                        </Button>
                    </div>
                }
            </Form>
        </div>
    )
}

export default ReimburseSetting
