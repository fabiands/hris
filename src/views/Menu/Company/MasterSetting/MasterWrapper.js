import React, {useState} from 'react'
import classnames from 'classnames';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import {translate, t} from 'react-switch-lang';
import GenderSetting from './GenderSetting';
import useSWR from 'swr';
import LoadingAnimation from '../../../../components/LoadingAnimation';
import { Tooltip } from 'reactstrap'
import DataNotFound from '../../../../components/DataNotFound';
import ReligionSetting from './ReligionSetting';
import DocumentTypeSetting from './DocumentTypeSetting';

function MasterWrapper(){

    const [activeTab, setActiveTab] = useState("gender");
    const {data: dataGender, error: errorGender} = useSWR('v1/master/genders');
    const {data: dataReligion, error: errorReligion} = useSWR('v1/master/religions');
    const {data: dataDocument, error: errorDocument} = useSWR('v1/master/document-types');
    
    const loading = !dataGender && !dataReligion && !dataDocument && !errorGender && !errorReligion && !errorDocument;

    const [tooltipOpen, setTooltipOpen] = useState(false);
    const toggleTooltip = () => {
        setTooltipOpen(!tooltipOpen);
    }

    const toggle = (tab) => {
        if (activeTab !== tab){
            setActiveTab(tab);
        }
    }

    if(loading){
        return <LoadingAnimation />
    }

    return(
        <div className="animated fadeIn">
            <div className="content-title my-4">
                <h4>{t('Pengaturan Data Master')}
                    <i className="fa fa-info-circle text-info ml-2" id="tooltipMaster" style={{cursor:"pointer", fontSize:"12pt"}} />
                </h4>
                <Tooltip placement="right" isOpen={tooltipOpen} target="tooltipMaster" toggle={toggleTooltip}>
                    {t('tooltipmaster')}
                </Tooltip>
            </div>
            <Nav tabs>
                <NavItem>
                    <NavLink className={classnames({ active: activeTab === 'gender' })} onClick={() => { toggle('gender'); }}>
                        Gender
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink className={classnames({ active: activeTab === 'religion' })} onClick={() => { toggle('religion'); }}>
                        {t('agama')}
                    </NavLink>
                </NavItem>
                <NavItem>
                    <NavLink className={classnames({ active: activeTab === 'document' })} onClick={() => { toggle('document'); }}>
                        {t('dokumen')}
                    </NavLink>
                </NavItem>
            </Nav>
            <TabContent activeTab={activeTab}>
                <TabPane tabId="gender">
                    {dataGender && <GenderSetting data={dataGender.data.data} />}
                    {errorGender && <DataNotFound />}
                </TabPane>
                <TabPane tabId="religion">
                    {dataReligion && <ReligionSetting data={dataReligion.data.data} />}
                    {errorReligion && <DataNotFound />}
                </TabPane>
                <TabPane tabId="document">
                    {dataDocument && <DocumentTypeSetting data={dataDocument.data.data} />}
                    {errorDocument && <DataNotFound />}
                </TabPane>
            </TabContent>
        </div>
    )
}

export default translate(MasterWrapper);