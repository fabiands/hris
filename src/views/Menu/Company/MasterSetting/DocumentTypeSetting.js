import React, {useState, useMemo, useEffect} from 'react'
import {translate, t} from 'react-switch-lang';
import { toast } from 'react-toastify';
import {Spinner, Button, Input, Label, InputGroup, InputGroupAddon, InputGroupText, Row, Col, Modal, ModalBody, ModalFooter} from 'reactstrap';
import ModalHeader from 'reactstrap/lib/ModalHeader';
import { mutate } from 'swr';
import DataNotFound from '../../../../components/DataNotFound';
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../../components/ResponsiveTable';
import request from '../../../../utils/request';

function DocumentTypeSetting({data}){
    const [documentData, setDocumentData] = useState(null);
    const [search, setSearch] = useState(null);
    const [name, setName] = useState(null);
    const [loading, setLoading] = useState(false);
    const [modalDocument, setModalDocument] = useState(false);
    const [id, setId] = useState(null);
    const [deletingId, setDeletingId] = useState(false);
    const [type, setType] = useState('create');

    useEffect(() => {
        setDocumentData(data)
    }, [data])

    const toggle = () => {
        setModalDocument(!modalDocument);
    }
    const handleSearch = (e) => {
        setSearch(e.target.value);
    }
    const filtered = useMemo(() => {
        if (search?.trim()) {
            return documentData.filter((person) => person.name.toLowerCase().includes(search.trim().toLowerCase()));
        }
        return documentData;
    }, [documentData, search])
    const addDocument = () => {
        setLoading(true)
        for(let person of documentData){
            if(name.toLowerCase() === person.name.toLowerCase()){
                toast.error(t('Dokumen tersebut sudah ada dalam data'))
                setLoading(false)
                return;
            }
        }
        if(type === 'create'){
            request.post('v1/master/document-types', {documentTypeName: name})
            .then(() => {
                toast.success(t('Berhasil Menambahkan Tipe Dokumen'))
                setModalDocument(false);
                mutate('v1/master/document-types');
            })
            .catch(() => {
                toast.error(t('terjadikesalahan'))
                setLoading(false);
                return;
            })
            .finally(() => {
                setLoading(false);
            })
        }
        else if(type === 'edit'){
            request.put(`v1/master/document-types/${id}`, {documentTypeName: name})
            .then(() => {
                toast.success(t('Berhasil Mengedit Tipe Dokumen'))
                setModalDocument(false);
                mutate('v1/master/document-types');
            })
            .catch(() => {
                toast.error(t('terjadikesalahan'))
                setLoading(false);
                return;
            })
            .finally(() => {
                setLoading(false);
            })
        }
    }
    const handleDelete = () => {
        setLoading(true);
        request.delete(`v1/master/document-types/${id}`)
        .then(() => {
            setDocumentData(null);
            toast.success(t('Berhasil Menghapus Tipe Dokumen'))
            setDeletingId(false);
            mutate('v1/master/document-types');
        })
        .catch(() => {
            toast.error(t('terjadikesalahan'))
            setLoading(false);
            return;
        })
        .finally(() => {
            setLoading(false);
        })
    }

    return(
        <div className="animated fadeIn">
            <div className="md-company-header">
                <h5 className="content-title mb-4">
                    {t('Pengaturan Master Tipe Dokumen')}
                </h5>
            </div>
            <Row className="my-3">
                <Col md="9" className="my-1">
                    <InputGroup className="search-filter-input">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-search" />
                            </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder={t('cari') + ' ' + t('nama')} className="input-search"
                        onChange={handleSearch}
                        />
                    </InputGroup>
                </Col>
                <Col md="3" className="text-right my-1">
                    <Button color="netis-color" onClick={() => {
                        setType('create');
                        setName(null);
                        toggle();
                    }}>
                        <i className="fa fa-plus mr-2" />{t('tambah')} Data
                    </Button>
                </Col>
            </Row>
            <Rtable>
                <Rthead>
                    <Rtr>
                        <Rth className="text-left w-10">No.</Rth>
                        <Rth className="text-left">{t('Nama Dokumen')}</Rth>
                        <Rth className="w-25"></Rth>
                    </Rtr>
                </Rthead>
                <Rtbody>
                    {filtered?.length > 0 ? filtered?.map((item, idx) => (
                        <Rtr key={idx}>
                            <Rtd className="text-left">{idx+1}</Rtd>
                            <Rtd className="text-left">{item.name}</Rtd>
                            <Rtd className="text-left text-nowrap">
                                <Button color="netis-color" className="mr-2"
                                    onClick={() => {
                                        setName(item.name)
                                        setModalDocument(true)
                                        setId(item.id)
                                        setType('edit')
                                    }}
                                >
                                    <i className="fa fa-pencil mr-1" />Edit
                                </Button>
                                <Button color="netis-danger" className="ml-2"
                                    onClick={() => {
                                        setDeletingId(true)
                                        setName(item.name)
                                        setId(item.id)
                                    }}
                                >
                                    <i className="fa fa-trash" />
                                </Button>
                            </Rtd>
                        </Rtr>
                    ))
                    : 
                        <Rtr><Rtd colSpan="9999"><DataNotFound /></Rtd></Rtr>
                    }
                </Rtbody>
            </Rtable>
            <Modal isOpen={modalDocument} toggle={toggle}>
                <ModalHeader className="border-bottom-0">
                    {type === 'create' ? t('Tambah Data Dokumen') : t('Edit Data Dokumen')}
                </ModalHeader>
                <ModalBody>
                    <Label htmlFor="documentName">{t('Tipe Dokumen')}</Label>
                    <Input id="documentName" name="documentName" onChange={(e) => setName(e.target.value)} placeholder={t('Tipe Dokumen')} value={name} />
                </ModalBody>
                <ModalFooter className="border-top-0">
                    <Button color="netis-light" onClick={toggle}>{t('batal')}</Button>
                    <Button color="netis-color" onClick={addDocument} disabled={!name || loading}>
                        {loading ? <><Spinner size="sm" color="light" />&nbsp;Loading</> : t('tambah')}
                    </Button>
                </ModalFooter>
            </Modal>
            <Modal isOpen={deletingId} toggle={() => setDeletingId(false)}>
                <ModalBody>
                    <h5 className="mb-3">{t('yakinmenghapus')}</h5>
                    <div className="d-flex justify-content-end">
                        <Button color="netis-light" onClick={() => setDeletingId(false)}>{t('batal')}</Button>
                        <Button color="netis-danger" onClick={handleDelete} disabled={loading}>
                            {loading ? <><Spinner size="sm" color="light" />&nbsp;Loading</> : t('hapus')}
                        </Button>
                    </div>
                </ModalBody>
            </Modal>
        </div>
    )
}

export default translate(DocumentTypeSetting)