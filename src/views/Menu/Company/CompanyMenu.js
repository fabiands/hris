import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import banner from '../../../assets/assets_ari/company-menu-icon.png';
import leaf from '../../../assets/assets_ari/leaf.svg';
import { FormGroup, Label, Input } from 'reactstrap';
import request from '../../../utils/request';
import { translate, t } from 'react-switch-lang';
import { withPrivileges } from '../../../store';

const menus = [
    {
        link: '/company/pengaturan-profil-perusahaan',
        image: require("../../../assets/assets_ari/336.svg"),
        title: () => (<>{t('Profil Perusahaan')}</>),
        privileges: ['read-company-profile'],
    },
    {
        link: '/company/pengaturan-jabatan',
        image: require("../../../assets/assets_ari/335.svg"),
        title: () => (<>{t('Pengaturan Jabatan & Unit')}</>),
        privileges: ['read-company-structure'],
    },
    {
        link: '/company/pengaturan-cuti-perusahaan',
        image: require("../../../assets/assets_ari/cuti.png"),
        title: () => (<>{t('Pengaturan Kebijakan Cuti')} </>),
        privileges: ['read-company-holiday'],
    },
    {
        link: '/company/pengaturan-role',
        image: require("../../../assets/img/illustrations/pengaturan-role.svg"),
        title: () => (<>{t('Pengaturan Peran & Hak Akses')} </>),
        privileges: ['read-company-role'],
    },
    {
        link: '/company/general-setting',
        image: require("../../../assets/assets_ari/334.svg"),
        title: () => (<>{t('Pengaturan Umum')} </>),
    },
    {
        link: '/company/master-setting',
        image: require("../../../assets/assets_ari/menu-pengaturan-reimburse.png"),
        title: () => (<>{t('Pengaturan Data Master')} </>),
    }

    // Pengaturan lembur & Reimburse (di bawah) digabung jadi satu jadi pengaturan umum (di atas)

    // {
    //     link: '/company/pengaturan-lembur-perusahaan',
    //     image: require("../../../assets/assets_ari/334.svg"),
    //     title: () => (<>{t('Pengaturan Kebijakan Lembur')} </>),
    //     privileges: ['read-company-overtime'],
    // },
    // {
    //     link: '/company/pengaturan-reimburse-perusahaan',
    //     image: require("../../../assets/assets_ari/menu-pengaturan-reimburse.png"),
    //     title: () => (<>{t('Pengaturan Kebijakan Reimburse')} </>),
    //     privileges: ['read-company-reimburse'],
    // },
    // {
    //     link: '/company/pengaturan-asesmen',
    //     image: require("../../../assets/assets_ari/336.svg"),
    //     title: () => (<>{t('pengaturan')}<br/>{t('asesmen')}</>),
    //     privileges: ['read-company-assessment'],
    // }
];

class CompanyMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dropdownOpen: [false, false],
        };
    }

    componentDidMount() {
        this.getDataCompany(this.props);
    }

    getDataCompany = async (props) => {
        try {
            const { data } = await request.get('v1/company');
            const companies = data.data[0]
            this.setState({
                name: companies.name,
                email: companies.email,
                domain: companies.domain,
                address: companies.address,
                country: companies.country.name,
                parent: companies.parent.name
            })
        } catch (err) {
            console.log(err)
            throw err;
        }
    }

    availableMenus = () => {
        const { canAll } = this.props;
        return menus.filter(menu => canAll(menu.privileges));
    }

    render() {
        const { t } = this.props;
        return (
            <div className="animated fadeIn">
                <h4 className="content-title mb-4">{t('informasiperusahaan')}</h4>
                <div className="content-body">
                    <div className="row">
                        <div className="menu col-md-8">
                            <div className="row">
                                {this.availableMenus().map(({ title: MenuTitle, ...menu}) => (
                                    <div className="col-12 col-sm-4 col-lg-4" key={menu.link}>
                                        <Link to={menu.link}>
                                            <div className="card menu-item">
                                                <div className="card-body">
                                                    <div className="menu-img mt-2 mb-3">
                                                        <img src={menu.image} alt="" />
                                                    </div>
                                                    <div className="menu-title mb-2">
                                                        <p className="mb-0 title-menu-company"><MenuTitle/></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>
                                ))}
                            </div>
                        </div>
                        <div className="col-md-4  padding-0">
                            <div className="col-12">
                                <div className="card banner-upgrade" style={{ backgroundImage: `url(${banner})` }}>
                                    <div className="card-body">
                                        <p className="mb-5 title-upgrade">{t('rasakan')} <br /> {t('kemudahan')} <br />{t('ditangananda')}</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12">
                                <div className="card banner-information" style={{ backgroundImage: `url(${leaf})` }}>
                                    <div className="card-body">
                                        <h5 className="mb-5 text-center title-information-company">{t('ringakasaninformasiperusahaan')}</h5>
                                        <FormGroup>
                                            <Label htmlFor="name" className="label-information">{t('namaperusahaan')}</Label>
                                            <Input type="text" id="name" defaultValue={this.state.name} className="input-information" disabled />
                                        </FormGroup>
                                        <br />
                                        <FormGroup>
                                            <Label htmlFor="email" className="label-information">{t('emailperusahaan')}</Label>
                                            <Input type="text" id="email" defaultValue={this.state.email} className="input-information" disabled />
                                        </FormGroup>
                                        <br />
                                        <FormGroup>
                                            <Label htmlFor="negara" className="label-information">{t('negaraperusahaan')}</Label>
                                            <Input type="text" id="negara" defaultValue={this.state.country} className="input-information" disabled />
                                        </FormGroup>
                                        <br />
                                        <FormGroup>
                                            <Label htmlFor="alamat" className="label-information">{t('alamatperusahaan')}</Label>
                                            <Input type="textarea" id="alamat" defaultValue={this.state.address} className="input-information" rows="4" disabled />
                                        </FormGroup>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default withPrivileges(translate(CompanyMenu));
