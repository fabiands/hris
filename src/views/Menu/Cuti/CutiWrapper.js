import React, { Component, Suspense } from 'react';
import { lazyComponent as lazy } from '../../../components/lazyComponent';
import { connect } from 'react-redux';
import { Switch, Redirect } from 'react-router';
import LoadingAnimation from '../../../components/LoadingAnimation'
import { Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import { Link } from 'react-router-dom';
import { translate } from 'react-switch-lang';
import AuthRoute from '../../../components/AuthRoute';

// CUTI VERSI 2 COMING SOON
const CutiHistory = lazy(() => import('./CutiHistory'));
const CutiApproval = lazy(() => import('./CutiApproval'));
const CutiDetail = lazy(() => import('./CutiDetail'));

// DEPRECATED
// const CutiHistory = lazy(() => import('./v1_deprecated/CutiHistory'));
// const CutiApproval = lazy(() => import('./v1_deprecated/CutiApproval'));
// const CutiDetail = lazy(() => import('./v1_deprecated/CutiDetail'));

const CutiQuota = lazy(() => import('./CutiQuota'));
const CutiQuotaHistory = lazy(() => import('./CutiQuotaHistory'));

const PANEL_USER = '3';
const PANEL_ADMIN = '2';

function matchWildcard(str, wildcard) {
    var escapeRegex = (str) => str.replace(/([.*+?^=!:${}()|[\]/\\])/g, "\\$1");
    return new RegExp("^" + wildcard.split("*").map(escapeRegex).join(".*") + "$").test(str);
}

class CutiWrapper extends Component {

    constructor(props) {
        super(props);

        this.state = {
            routes: [],
        }

        this.state.routes = this.getRoutes();
    }

    getRoutes = () => {

        const { t, match, menu } = this.props;
        let routes = []

        if (menu === PANEL_ADMIN) {
            routes = [
                {
                    path: match.path + '/manage', exact: true, privileges: ['browse-holiday'], component: CutiApproval,
                    tab: t('daftarcuti'), active: match.path + '/manage'
                },
                {
                    path: match.path + '/quota', exact: true, component: CutiQuota,
                    tab: t('jatahcuti'), active: match.path + '/quota'
                },
                {
                    path: match.path + '/quotahistory', exact: true, component: CutiQuotaHistory,
                    tab: t('sejarahkuota'), active: match.path + '/quotahistory'
                },
                { path: match.path + '/manage/:id', exact: true, component: CutiDetail },
            ];
        } else if (menu === PANEL_USER) {
            routes = [
                {
                    path: match.path + '/history', exact: true, component: CutiHistory,
                    tab: t('riwayatcuti'), active: match.path + '/history'
                },
                { path: match.path + '/history/:id', exact: true, component: CutiDetail }
            ];
            
            if (this.props.user.hasBawahan && this.props.user.personnel.company.holidaySuperiorApproval) {
                routes.push({
                    path: match.path + '/manage', exact: true, component: CutiApproval,
                    tab: t('kelolacuti'), active: match.path + '/manage'
                });
                routes.push({ path: match.path + '/manage/:id', exact: true, component: CutiDetail });
            }
        }

        return routes;
    }

    render() {
        const { match, location } = this.props;
        const { routes } = this.state;
        return (
            <div className="animated fadeIn">
                <Nav tabs>
                    {routes.filter(route => !!route.tab).map(route => (
                        <NavItem key={route.path}>
                            <NavLink tag={Link} to={route.path} active={route.active && matchWildcard(location.pathname, route.active)}>{route.tab}</NavLink>
                        </NavItem>
                    ))}
                </Nav>
                <TabContent>
                    <TabPane>
                        <Suspense fallback={<LoadingAnimation />}>
                            <Switch>
                                {routes.map(route => (
                                    <AuthRoute key={route.path} {...route} />
                                ))}
                                {routes[0] && <Redirect exact from={match.path} to={routes[0].path} />}
                            </Switch>
                        </Suspense>
                    </TabPane>
                </TabContent>
            </div>
        )
    }
}

const mapStateToProps = ({ menu, user }) => ({ menu, user });
export default connect(mapStateToProps)(translate(CutiWrapper));
