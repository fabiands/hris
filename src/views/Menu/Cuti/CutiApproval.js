import React, { useCallback, useEffect, useState, useMemo } from 'react';
import { Button, Modal, Form, Row, ModalBody, ModalFooter, ModalHeader, FormGroup, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Spinner, Label, Input, Col, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import { toast } from 'react-toastify';
import { DatePickerInput } from 'rc-datepicker';
import StatusBadge from './components/StatusBadge'
import request from '../../../utils/request'
import { formatDate } from '../../../utils/formatter';
import moment from '../../../utils/moment'
import { Link } from 'react-router-dom';
import LoadingAnimation from '../../../components/LoadingAnimation'
import DataNotFound from '../../../components/DataNotFound'
import {
    translate, t
} from 'react-switch-lang';
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../components/ResponsiveTable';
import { useUserPrivileges } from '../../../store';
import Select from 'react-select'
import usePagination from '../../../hooks/usePagination';

toast.configure()

function CutiApproval(props) {
    moment.locale(t('id'))
    const { can } = useUserPrivileges();
    const searchParams = new URLSearchParams(props?.location?.search);
    const [loading, setLoading] = useState(true)
    const [holidays, setHolidays] = useState([])
    const [showModal, setShowModal] = useState(false)
    const [updating, setUpdating] = useState(false)
    const [update, setUpdate] = useState({
        holidayId: null,
        status: '',
        message: ''
    })
    // const [dropdownOpened, setDropdownOpened] = useState({})
    const [showDocument, setShowDocument] = useState(false)
    const [filter, setFilter] = useState({
        start: searchParams.get('start'),
        end: searchParams.get('end')
    })
    const [units, setUnits] = useState([])
    const [jobs, setJobs] = useState([])
    const [search, setSearch] = useState(null)
    const [filterUnit, setFilterUnit] = useState([])
    const [filterJob, setFilterJob] = useState([])
    const [filters, setFilters] = useState([])

    // console.log(props)
    const fetchData = useCallback(() => {
        const params = Object.keys(filter)
            .filter(k => filter[k] !== null)
            .reduce((acc, k) => ({ ...acc, [k]: formatDate(filter[k]) }), {});
        setLoading(true)
        return request.get(localStorage.getItem("menu") === "2" ? 'v2/holidays/admin/request' : 'v2/holidays/request', { params })
            .then((res) => {
                setHolidays(res.data.data)
            })
            .catch((err) => {
                if (err.response) {
                    toast.error(err.message, { autoClose: 2000 });
                }
                console.log(err)
                throw err;
            })
            .finally(() => setLoading(false))
        // eslint-disable-next-line
    }, [])

    const getUnits = () => {
        return request.get(`/v1/master/units`)
            .then((res) => setUnits(res.data.data))
            .catch(() => toast.error(t('Gagal mendapatkan data Unit')))
    }
    const getJobs = () => {
        return request.get(`/v1/master/jobs`)
            .then((res) => setJobs(res.data.data))
            .catch(() => toast.error(t('Gagal mendapatkan data Jabatan')))
    }

    useEffect(() => {
        fetchData()
        getUnits()
        getJobs()
    }, [fetchData])

    const handleChangeFilter = (name, date) => {
        let fil = filter
        fil[name] = date
        setFilter(fil)
    }
    const toggleModal = () => {
        const closing = showModal;
        setShowModal(!showModal)
        if (closing) {
            setUpdate({
                status: '',
                message: '',
                holidayId: null
            })
        }
    }

    const updateStatus = (holidayId, status) => {
        const updt = update
        updt.status = status
        updt.holidayId = holidayId
        setUpdate(updt)
        toggleModal()
    }

    const submitUpdateStatus = (e) => {
        e.preventDefault();
        setUpdating(true)
        const { holidayId, status, message } = update
        if (holidayId) {
            request.post(`v2/holidays/request/${holidayId}`, { status, message })
                .then(() => {
                    fetchData()
                    toast.success(t('berhasilperbarui') || 'Success')
                    toggleModal()
                })
                .catch((err) => {
                    if (err.response && err.response.data) {
                        toast.error(err.response.data.message);
                    }
                    throw err;
                })
                .finally(() => setUpdating(false))
        }
    }

    // const toggleDropdown = (index) => (e) => {
    //     let dropOp = dropdownOpened
    //     dropOp[index] = !dropOp[index]
    //     setDropdownOpened(dropOp)
    // }

    const handleShowDocument = (document) => {
        setShowDocument(process.env.REACT_APP_DOMAIN + document)
    }

    const toggleShowDocument = () => {
        setShowDocument(!showDocument)
    }

    const handleInputChange = (e) => {
        const { value } = e.target;
        setUpdate((state) => ({
            ...state,
            message: value
        }))
    }
    const changeStatus = (data) => {
        switch (data) {
            case 'pending': return t('diproses');
            case 'approved1': return t('menunggupersetujuanadmin');
            case 'approved2': return t('disetujui');
            case 'rejected': return t('ditolak');
            default: return null
        }
    }
    const changeBadge = (data) => {
        switch (data) {
            case 'pending': return 'pending';
            case 'approved1': return 'approved1';
            case 'approved2': return 'approved2';
            case 'rejected': return 'rejected';
            default: return null
        }
    }
    const searchSpace = (e) => {
        setSearch(e.target.value)
    };
    const handleUnit = (e) => {
        let unit = e?.map(item => item.value) ?? []
        setFilterUnit(unit)
    }
    const handleJob = (e) => {
        let job = e?.map(item => item.value) ?? []
        setFilterJob(job)
    }

    const filtered = useMemo(() => {
        let filterData = holidays
            ?.filter(item =>
                search?.trim() ? item?.personnel?.user?.fullName?.toLowerCase().includes(search?.trim().toLowerCase()) : true
            )
            ?.filter(item =>
                filterUnit?.length > 0 ? filterUnit.includes(item?.personnel?.unit?.id) : true
            )
            ?.filter(item =>
                filterJob?.length > 0 ? filterJob.includes(item?.personnel?.job?.id) : true
            )
        return filterData;
    }, [holidays, search, filterUnit, filterJob])

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
    );

    const { data: groupFiltered, PaginationComponent, currentPage } = usePagination(
        filtered,
        8,
        filters?.pagination,
        handleChangeCurrentPage
    );

    return (
        <div className="animated fadeIn mb-3">
            <h4 className="content-title mb-3">{t('Riwayat Cuti')}</h4>
            <Form onSubmit={(e) => { e.preventDefault(); fetchData() }}>
                <Row className="md-company-header mb-3">
                    <Col xs="12" md="9">
                        <Row>
                            <Col xs="6">
                                <FormGroup>
                                    <Label tag="small" htmlFor="start" className="text-muted">{t('dari')}</Label>
                                    <DatePickerInput
                                        readOnly
                                        showOnInputClick={true}
                                        name="start"
                                        id="start"
                                        onChange={(value) => handleChangeFilter('start', value)}
                                        value={filter.start}
                                        className='my-custom-datepicker-component'
                                        closeOnClickOutside={true}
                                        displayFormat="DD MMMM YYYY"
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs="6">
                                <FormGroup style={{ marginBottom: 5 }}>
                                    <Label tag="small" htmlFor="end" className="text-muted">{t('hingga')}</Label>
                                    <DatePickerInput
                                        readOnly
                                        showOnInputClick={true}
                                        name="end"
                                        id="end"
                                        onChange={(value) => handleChangeFilter('end', value)}
                                        value={filter.end}
                                        className='my-custom-datepicker-component'
                                        closeOnClickOutside={true}
                                        displayFormat="DD MMMM YYYY"
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs="6" xl="4" >
                                <FormGroup>
                                    <Label htmlFor="job" className="input-label">
                                        {t("jabatan")}
                                    </Label>
                                    <Select
                                        disabled={jobs.length < 1}
                                        isMulti={true}
                                        isSearchable={false}
                                        name="jabatan"
                                        options={jobs.map(item => ({ label: item.name, value: item.id }))}
                                        placeholder={t("Jabatan")}
                                        onChange={handleJob}
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs="6" xl="4" >
                                <FormGroup>
                                    <Label htmlFor="unit" className="input-label">
                                        {t('Unit')}
                                    </Label>
                                    <Select
                                        disabled={units.length < 1}
                                        isMulti={true}
                                        isSearchable={false}
                                        name="unit"
                                        options={units.map(item => ({ label: item.name, value: item.id }))}
                                        placeholder={t("Unit")}
                                        onChange={handleUnit}
                                    />
                                </FormGroup>
                            </Col>
                            <Col xs="12" xl="4" className="mt-xl-1 pt-xl-4 mb-2" >
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText className="input-group-transparent">
                                            <i className="fa fa-search" />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input
                                        type="text"
                                        placeholder={t('Cari Nama')}
                                        className="input-search"
                                        onChange={searchSpace}
                                    />
                                </InputGroup>
                            </Col>
                        </Row>
                    </Col>
                    <Col xs="12" md="3">
                        <div className="my-2 mt-md-0">
                            <Button type="submit" className="mt-sm-4 w-100" color="netis-color" disabled={loading}>
                                {loading ? <Spinner color="light" size="sm" /> : 'Filter'}
                            </Button>
                        </div>
                    </Col>
                </Row>
            </Form>

            {loading ? <LoadingAnimation />
                : !filtered.length ? <DataNotFound />
                    : (
                        <>
                            <Rtable size="sm">
                                <Rthead>
                                    <Rtr className="grey-tr">
                                        <Rth className="header-sticky text-center w-5">No.</Rth>
                                        <Rth className="header-sticky w-15">{t('kategori')}</Rth>
                                        <Rth className="header-sticky w-20">{t('nama')}</Rth>
                                        <Rth className="header-sticky w-15">{t('Tanggal Mulai')} </Rth>
                                        <Rth className="header-sticky w-15">{t('Tanggal Selesai')}</Rth>
                                        <Rth className="header-sticky">Status</Rth>
                                        <Rth className="header-sticky"></Rth>
                                    </Rtr>
                                </Rthead>
                                <Rtbody className="scroll-table">
                                    {groupFiltered?.map((data, index) => {
                                        let status = changeStatus(data.status);
                                        let badge = changeBadge(data.status);
                                        return (
                                            <Rtr key={index + 1 + (8 * currentPage)}>
                                                <Rtd className="text-center">{index + 1 + (8 * currentPage)}</Rtd>
                                                <Rtd>{data.categoryName}</Rtd>
                                                <Rtd>
                                                    {data.personnel.user.fullName}<br />
                                                    <i><small>{data.personnel.unit.name}</small></i>
                                                </Rtd>
                                                <Rtd className="text-nowrap">{moment(data.start).format('DD MMM YYYY')}</Rtd>
                                                <Rtd className="text-nowrap">{moment(data.end).format('DD MMM YYYY')}</Rtd>
                                                <Rtd className="text-nowrap"><StatusBadge status={badge} />{status}</Rtd>
                                                <Rtd className="text-nowrap">
                                                    {/* eslint-disable-next-line */}
                                                    {(!['approved2', 'rejected'].includes(data.status)) && (
                                                        <UncontrolledDropdown className="d-inline-block mr-1">
                                                            <DropdownToggle color="netis-color" caret>
                                                                {t('ubah')} Status
                                                            </DropdownToggle>
                                                            <DropdownMenu>
                                                                {
                                                                    (data.status === 'pending') || (can('verify-holiday') && data.status === 'approved1') ? (
                                                                        <DropdownItem onClick={() => updateStatus(data.id, 'approved')}>
                                                                            <i className="fa fa-circle text-info"></i> {t('disetujui')}
                                                                        </DropdownItem>)
                                                                        :
                                                                        null
                                                                }
                                                                <DropdownItem onClick={() => updateStatus(data.id, 'rejected')}>
                                                                    <i className="fa fa-circle text-danger"></i> {t('ditolak')}
                                                                </DropdownItem>
                                                            </DropdownMenu>
                                                        </UncontrolledDropdown>)}
                                                    <Link to={'/cuti/manage/' + data.id}>
                                                        <Button className={`${can('read-holiday') ? '' : ' d-none'}`} color="netis-color">{t('detail')}</Button>
                                                    </Link>
                                                </Rtd>
                                            </Rtr>
                                        )
                                    })}
                                </Rtbody>
                            </Rtable>
                            {filtered.length > 8 && <PaginationComponent />}
                        </>
                    )}

            <Modal isOpen={!!showDocument} toggle={toggleShowDocument}>
                <ModalBody>
                    <img src={handleShowDocument} width="100%" alt="document" />
                </ModalBody>
            </Modal>

            <Modal isOpen={showModal} toggle={toggleModal}>
                <form onSubmit={submitUpdateStatus}>
                    <ModalHeader toggle={toggleModal}>Update Status</ModalHeader>
                    <ModalBody>
                        <FormGroup row>
                            <Label htmlFor="status" className="text-md-right" md="3">Status</Label>
                            <Col md={9}>
                                <Input type="select" name="status" id="status" value={update.status} disabled>
                                    <option value="approved">{t('disetujui')}</option>
                                    <option value="rejected">{t('ditolak')}</option>
                                </Input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label htmlFor="message" className="text-md-right" md={3}>{t('pesan')}</Label>
                            <Col md={9}>
                                <Input type="textarea" name="message" id="message" rows="2" value={update.message} onChange={handleInputChange}></Input>
                            </Col>
                        </FormGroup>
                    </ModalBody>
                    <ModalFooter>
                        <Button type="submit" color="netis-color" disabled={updating}>
                            {updating ?
                                (<><Spinner size="sm" color="light"></Spinner> Loading</>) :
                                (<><i className="fa fa-check mr-2"></i> Update Status</>)
                            }
                        </Button>
                    </ModalFooter>
                </form>
            </Modal>
        </div>
    );
}

export default translate(CutiApproval)
