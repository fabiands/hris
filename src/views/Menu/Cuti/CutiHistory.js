import React, { useCallback, useEffect, useMemo, useState, memo } from 'react';
import { Row, Col, Button, Modal, ModalBody, ModalFooter, FormGroup, Label, Input, Spinner, ModalHeader } from 'reactstrap';
import { toast } from 'react-toastify';
import { DatePickerInput } from 'rc-datepicker';
import { Link } from 'react-router-dom';
import StatusBadge from './components/StatusBadge'
import request from '../../../utils/request'
import moment from '../../../utils/moment'
import { t, translate } from 'react-switch-lang';
import useSWR from 'swr';
import Select, { components } from 'react-select';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../components/ResponsiveTable';
import DataNotFound from '../../../components/DataNotFound';

const Option = components.Option;

const OptionCuti = (props) => (
    <Option {...props}>
        <img src={require(`../../../assets/img/cuti/${props.data.icon}.png`)} width={24} height={24} className="mr-2 img-icon" alt="" />
        <span style={{ lineHeight: '24px' }}>
            {props.data.name}
        </span>
    </Option>
);

const statusDefintions = {
    'pending': {
        status: () => t('diproses'),
        badge: 'pending',
    },
    'approved1': {
        status: () => t('menunggupersetujuanadmin'),
        badge: 'approved1',
    },
    'approved2': {
        status: () => t('disetujui'),
        badge: 'approved2',
    },
    'rejected': {
        status: () => t('ditolak'),
        badge: 'rejected',
    },
}

const CutiHistoryV2 = (props) => {
    const { data: responseHolidays, error: errorHolidays, mutate: mutateHolidays } = useSWR('v2/holidays/request/history');
    const holidays = useMemo(() => responseHolidays?.data?.data ?? [], [responseHolidays]);
    
    const { data: responseCategories, error: errorCategories } = useSWR('v1/master/holiday-categories');
    const categories = useMemo(() => responseCategories?.data?.data ?? [], [responseCategories]);

    const loading = !responseHolidays && !errorHolidays && !responseCategories && !errorCategories;
    const [showCreateModal, setShowCreateModal] = useState(false);

    const toggleCreateModal = useCallback(() => {
        setShowCreateModal(prevState => !prevState);
    }, []);

    const onCreated = useCallback(() => {
        mutateHolidays();
    }, [mutateHolidays]);

    if (loading) {
        return (
            <div style={{ position: 'absolute', top: 0, right: 0, bottom: 0, left: 0, background: 'rgba(255,255,255, 0.5)', display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <Spinner style={{width: 48, height: 48 }} />
            </div>
        );
    }

    return (
        <div className="animated fadeIn">
            <div className="d-flex align-items-center mb-4">
                <Button color="netis-color" onClick={toggleCreateModal}>
                    <i className="fa fa-plus mr-2"></i> {t('tambah')} {t('cuti')}
                </Button>
            </div>
            <Row className="md-company-header mb-3">
                <Col xs="12">
                    <Rtable>
                        <Rthead>
                            <Rtr>
                                <Rth className="text-center w-5">No.</Rth>
                                <Rth className="w-20">{t('kategori')} </Rth>
                                <Rth className="w-15">{t('Tanggal Mulai')}</Rth>
                                <Rth className="w-15">{t('Tanggal Selesai')}</Rth>
                                <Rth>Status</Rth>
                                <Rth className="w-20"></Rth>
                            </Rtr>
                        </Rthead>
                        <Rtbody>
                            {holidays.length > 0 ?
                                holidays.map((data, index) => (
                                    <Rtr key={data.id}>
                                        <Rtd className="text-sm-center">{index + 1}</Rtd>
                                        <Rtd>{data.categoryName}</Rtd>
                                        <Rtd className="text-nowrap">{moment(data.start).format('DD MMM YYYY')}</Rtd>
                                        <Rtd className="text-nowrap">{moment(data.end).format('DD MMM YYYY')}</Rtd>
                                        <Rtd className="text-nowrap"><StatusBadge status={statusDefintions[data.status]?.badge} />{statusDefintions[data.status]?.status()}</Rtd>
                                        <Rtd>
                                            <Link to={'/cuti/history/' + data.id}>
                                                <Button color="netis-color">{t('detail')}</Button>
                                            </Link>
                                        </Rtd>
                                    </Rtr>
                                ))
                                :
                                <Rtr><Rtd colSpan="6"><DataNotFound/></Rtd></Rtr>
                            }
                        </Rtbody>
                    </Rtable>
                </Col>
            </Row>
            <ModalCreateCuti isOpen={showCreateModal} toggle={toggleCreateModal} categories={categories} onCreated={onCreated} />
        </div>
    );
}

const ModalCreateCuti = memo(({ isOpen, toggle, categories, onCreated }) => {
    const [selectedCategory, setSelectedCategory] = useState();
    const [previewDocument, setPreviewDocument] = useState();

    const validationSchema = useMemo(() => {
        const schema = Yup.object({
            category: Yup.mixed().required().label(t('kategori')),
            start: Yup.mixed().required().label(`${t('tanggal')} ${t('mulai')}`),
            end: Yup.mixed().required().label(`${t('tanggal')} ${t('selesai')}`),
            document: Yup.lazy(() => {
                if (selectedCategory?.has_file) {
                    return Yup.mixed().required().label(t('lampiran'));
                } else {
                    return Yup.mixed().notRequired().label(t('lampiran'));
                }
            })
        });
        return schema;
    }, [selectedCategory]);
    const { values, touched, errors, resetForm, ...formik} = useFormik({
        initialValues: {
            category: null,
            start: '',
            end: '',
            message: '',
            document: null,
        },
        validationSchema: validationSchema,
        onSubmit: (values, { setSubmitting }) => {
            const formData = new FormData();
            for (let item in values) {
                formData.append(item, values[item]);
            }
            setSubmitting(true);
            request.post('v2/holidays/request', formData)
                .then(res => {
                    setSubmitting(false);
                    onCreated();
                    toggle();
                    toast.success(res.data.message || t('berhasilperbarui'))
                })
                .catch(err => {
                    if (err.response && err.response.data) {
                        formik.setErrors({ message: err.response.data.message });
                    }
                    setSubmitting(false);
                });
        }
    });
    useEffect(() => {
        if (!isOpen) {
            resetForm();
            setSelectedCategory(null);
            setPreviewDocument(null);
        }
    }, [isOpen, resetForm])

    const handleSelectCategory = useCallback((option) => {
        formik.setFieldTouched('category');
        formik.setFieldValue('category', option.id);
        setSelectedCategory(option);

    }, [formik]);

    const handleDatepickerChangeStart = useCallback((_,formattedDate) => {
        formik.setFieldTouched('start');
        formik.setFieldValue('start', formattedDate);
        formik.setFieldError('message', null);
    }, [formik]);

    const handleDatepickerChangeEnd = useCallback((_,formattedDate) => {
        formik.setFieldTouched('end');
        formik.setFieldValue('end', formattedDate);
        formik.setFieldError('message', null);
    }, [formik]);

    const handleDocumentChange = useCallback((event) => {
        const files = event.target.files;
        if (files.length) {
            formik.setFieldValue('document', files[0]);
            setPreviewDocument(URL.createObjectURL(files[0]));
        } else {
            formik.setFieldValue('document', null);
            setPreviewDocument(null);
        }
    }, [formik]);

    return (
        <Modal isOpen={isOpen} toggle={toggle}>
            <ModalHeader toggle={toggle}>
                <h5 className="content-sub-title mb-0">{t('tambah')} {t('cuti')}</h5>
            </ModalHeader>
            <ModalBody>
                <form>
                    {!!errors.message && <div className="alert alert-danger">
                        {errors.message}
                    </div>}
                    <FormGroup>
                        <Label htmlFor="category" className="input-label">{t('kategori')} <span className="required">*</span></Label>
                        <Select 
                            components={{ Option: OptionCuti }}
                            styles={{
                                control: base => ({...base, borderColor: touched.category && errors.category ? 'red' : 'hsl(0,0%,80%)'})
                            }}
                            options={categories}
                            getOptionLabel={(opt) => opt.name}
                            getOptionValue={(opt) => opt.id}
                            onChange={handleSelectCategory}
                            placeholder={`-- ${t('pilih')} ${t('cuti')} ${t('kategori')} --`}
                        />
                        {(touched.category && errors.category) && <span className="small text-danger">{errors.category}</span>}
                        <label className="input-label" style={{ marginTop: 5 }}><b>{selectedCategory?.description}</b></label>
                    </FormGroup>
                    <div className="form-row">
                        <div className="form-group col-md-6">
                            <Label htmlFor="start" className="input-label">{t('Tanggal Mulai')}</Label>
                            <DatePickerInput
                                showOnInputClick={true}
                                name="start"
                                id="start"
                                onChange={handleDatepickerChangeStart}
                                maxDate={values.end}
                                value={values.start}
                                className={`my-custom-datepicker-component ${touched.start && errors.start ? 'is-invalid' : ''}`}
                                closeOnClickOutside={true}
                                displayFormat="DD MMMM YYYY"
                                returnFormat="YYYY-MM-DD"
                                readOnly
                            />
                            {(touched.start && errors.start) && <span className="small text-danger">{errors.start}</span>}
                        </div>
                        <div className="form-group col-md-6">
                            <Label htmlFor="end" className="input-label">{t('Tanggal Selesai')}</Label>
                            <DatePickerInput
                                showOnInputClick={true}
                                name="end"
                                id="end"
                                onChange={handleDatepickerChangeEnd}
                                value={values.end}
                                minDate={values.start}
                                className={`my-custom-datepicker-component ${touched.end && errors.end ? 'is-invalid' : ''}`}
                                closeOnClickOutside={true}
                                displayFormat="DD MMMM YYYY"
                                returnFormat="YYYY-MM-DD"
                                readOnly
                            />
                            {(touched.end && errors.end) && <span className="small text-danger">{errors.end}</span>}
                        </div>
                    </div>
                    <FormGroup>
                        <Label htmlFor="message" className="input-label">{t('deskripsi')}</Label>
                        <Input type="textarea" value={values.message} onChange={formik.handleChange} name="message" id="message" rows="2" placeholder="" />
                    </FormGroup>
                    {!!selectedCategory?.has_file &&
                        <>
                            <FormGroup>
                                <Label htmlFor="document" className="input-label">Upload {t('lampiran')} <span style={{ fontWeight: 'bold' }}>( Max. 5 MB )</span></Label>
                                <Input type="file" id="document" name="document" onChange={handleDocumentChange} />
                                {(touched.document && errors.document) && <span className="small text-danger">{errors.document}</span>}
                            </FormGroup>
                            <div className="row">
                                <div className="col-md-7">
                                    {!!previewDocument && <div className="d-flex justify-content-center" style={{ overflowBlock: "hidden", maxHeight: 300, maxWidth: '100%' }}>
                                        <img width="300" height="300" src={previewDocument} alt="preview dokumen" />
                                    </div>}
                                </div>
                            </div>
                        </>
                    }
                </form>
            </ModalBody>
            <ModalFooter>
                {formik.isSubmitting ?
                    (<Button color="netis-color" type="button" disabled><Spinner size="sm" color="light"></Spinner> Loading</Button>) :
                    (<>
                        <Button type="button" color="link" onClick={toggle}>
                            {t('batal')}
                        </Button>
                        <Button type="submit" color="netis-color" onClick={formik.handleSubmit}>
                            <i className="fa fa-plus mr-2"></i> {t('simpan')}
                        </Button>
                    </>)
                }
            </ModalFooter>
        </Modal>
    );
});

export default translate(CutiHistoryV2);
