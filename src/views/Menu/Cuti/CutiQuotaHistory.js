import React, {useState, useEffect, useMemo, useCallback} from 'react';
import { DatePickerInput } from 'rc-datepicker';
import {Spinner, Col, Row, Label, Button, Input} from 'reactstrap';
import {translate, t, getLanguage} from 'react-switch-lang';
import request from "../../../utils/request";
import moment from '../../../utils/moment';
import { formatDate } from "../../../utils/formatter";
import LoadingAnimation from '../../../components/LoadingAnimation';
import DataNotFound from '../../../components/DataNotFound';
import { Rtable, Rtbody, Rthead, Rtr, Rth, Rtd } from '../../../components/ResponsiveTable';
import { Link } from 'react-router-dom';
import usePagination from '../../../hooks/usePagination';

const badge = {
    adjust: t('Penyesuaian'),
    rejected: t('Pengajuan Cuti ditolak'),
    request: t('Pengajuan Cuti Baru')
}

const badgeClass = {
    adjust: 'badge badge-primary',
    rejected: 'badge badge-danger',
    request: 'badge badge-success',
}

function CutiQuotaHistory(props){
    
    const searchParams = new URLSearchParams(props.location.search);
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [filters, setFilters] = useState([])
    const notFound = useMemo(() => {
        return !data?.length;
    }, [data])
    const [filterDate, setFilterDate] = useState({
        start: searchParams.get('start'),
        end: searchParams.get('end')
    });
    const [searchPersonnel, setSearchPersonnel] = useState('');

    const params = Object.keys(filterDate)
        .filter((k) => filterDate[k] !== null)
        .reduce(
            (acc, k) => ({ ...acc, [k]: formatDate(filterDate[k]) }),
            {}
        );

    function fetchFilter(){
        setLoading(true)
        request.get('v1/personnel/holidays/balances/history', {params})
            .then(res =>{
                const histories = searchPersonnel ? res.data.data.filter(history => history.subject.fullName?.toLowerCase()?.includes(searchPersonnel.toLowerCase())) : res.data.data;
                setData(histories)
            }).finally(() => setLoading(false))
    }

    useEffect(fetchFilter, [])

    const handleChangeFilter = (name, date) => {
        setFilterDate({...filterDate, [name]: date})
    }

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
        );

    const { data: groupFiltered, PaginationComponent } = usePagination(
        data,
        8,
        filters.pagination,
        handleChangeCurrentPage
    );

    return(
        <div className="animated fadeIn md-company-header mb-3">
            <form onSubmit={(e) => {e.preventDefault(); fetchFilter();}}>
                <Row className="mb-2">
                    <Col sm="10" md="4" className="mb-2">
                        <Label tag="small" htmlFor="namaKaryawanSearch" className="text-muted">{t('karyawan')}</Label>
                        <br/>
                        <Input type="text" style={{ borderColor: '#ced0da' }} value={searchPersonnel} onChange={(event) => setSearchPersonnel(event.target.value)}/>
                    </Col>
                    <Col sm="5" md="3" className="mb-2">
                        <Label tag="small" htmlFor="start" className="text-muted">{t('dari')}</Label>
                        <br />
                        <DatePickerInput
                            readOnly
                            showOnInputClick={true}
                            name="start"
                            id="start"
                            onChange={(value) => handleChangeFilter('start', value)}
                            value={filterDate.start}
                            maxDate={filterDate.end}
                            className='my-custom-datepicker-component'
                            closeOnClickOutside={true}
                            displayFormat="DD MMMM YYYY"
                        />
                    </Col>
                    <Col sm="5" md="3" className="mb-3">
                        <Label tag="small" htmlFor="end" className="text-muted">{t('hingga')}</Label>
                        <br />
                        <DatePickerInput
                            readOnly
                            showOnInputClick={true}
                            name="end"
                            id="end"
                            onChange={(value) => handleChangeFilter('end', value)}
                            minDate={filterDate.start}
                            value={filterDate.end}
                            className='my-custom-datepicker-component'
                            closeOnClickOutside={true}
                            displayFormat="DD MMMM YYYY"
                        />
                    </Col>
                    <Col sm="2">
                        <Button type="submit" className="mt-sm-4 px-3 d-block d-sm-inline-block" color="netis-color" disabled={loading}>
                            {loading ? <Spinner color="light" size="sm" /> : t('cari')}
                        </Button>
                    </Col>
                </Row>
            </form>
            {loading ?
                <LoadingAnimation />
            : (notFound ?
                <DataNotFound />
            :
            <>
            <Rtable size="sm" key={getLanguage()} style={{display:'table'}}>
                <Rthead>
                    <Rtr>
                        <Rth></Rth>
                        <Rth>{t('tipe')}</Rth>
                        <Rth>{t('karyawan')}</Rth>
                        <Rth className="text-center text-capitalize">{t('Sebelum')}</Rth>
                        <Rth className="text-center text-capitalize">{t('Setelah')}</Rth>
                        <Rth className="text-capitalize">{t('keterangan')}</Rth>
                    </Rtr>
                </Rthead>
                <Rtbody>
                    {groupFiltered?.length > 0  && groupFiltered?.map((data, idx) => (
                        <Rtr key={data.id}>
                            <Rtd className="text-nowrap py-2">
                                <small className="text-muted d-block mr-3 mr-md-0">                                    
                                    <i className="icon-clock mr-1"></i> {moment(data.datetime).format('DD MMMM YYYY HH:mm')}                             
                                </small>
                                {/* <small className="text-nowrap">{moment(data.datetime).format('DD MMMM YYYY HH:mm')}</small> */}
                                <small className="text-muted d-block mr-3 mr-md-0">                                    
                                    <i className="icon-user mr-1"></i> <span className="d-none d-md-inline">{data.causer.fullName}</span><span className="d-md-none">{data.causer.nickName}</span>                           
                                </small>
                            </Rtd>
                            <Rtd>
                                <span className={`${badgeClass[data.type] ?? "badge badge-light"}`}>
                                    {badge[data.type]}
                                </span>
                            </Rtd>
                            <Rtd>{data.subject.fullName}</Rtd>
                            <Rtd className="text-center table-danger ">{data.from}</Rtd>
                            <Rtd className="text-center table-success ">{data.to}</Rtd>
                            <Rtd>
                                {
                                    ['request', 'rejected'].includes(data.type) ? 
                                        <Link className="ml-n1 text-nowrap btn btn-sm btn-link" to={`/cuti/manage/${data.requestId}`}>{t('lihatdetail')} <i className="icon-arrow-right-circle ml-1"></i></Link>
                                    :
                                        data.message ?? <i className="text-muted">{t('tidak ada deskripsi')}</i>
                                }
                            </Rtd>
                        </Rtr>
                    ))}
                </Rtbody>
            </Rtable>
            {data?.length > 8 && <PaginationComponent />}
            </>
            )}
        </div>
    )
}

export default translate(CutiQuotaHistory);
