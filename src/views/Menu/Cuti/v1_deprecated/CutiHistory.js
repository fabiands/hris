import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Table, Button, Modal, ModalBody, ModalFooter, FormGroup, Label, Input, Spinner } from 'reactstrap';
import { toast } from 'react-toastify';
import { DatePickerInput } from 'rc-datepicker';
import { formatDate } from '../../../../utils/formatter';
import { Link } from 'react-router-dom';
import StatusBadge from './../components/StatusBadge'
import request from '../../../../utils/request'
import moment from '../../../../utils/moment'
import LoadingAnimation from '../../../../components/LoadingAnimation'
import {
    translate,
} from 'react-switch-lang';
toast.configure()
class CutiHistory extends Component {

    constructor(props) {
        super(props);

        const searchParams = new URLSearchParams(props.location.search);

        const { t } = this.props;
        this.state = {
            loading: true,
            session: props.token,
            holidays: [],
            showCreateModal: false,
            sakit: false,
            createData: {
                start: '',
                end: '',
                time: '',
                message: '',
                category: null,
                document: null,
            },
            categories: [
                { id: 'sakit', name: t('sakit') },
                { id: 'tahunan', name: t('tahunan') },
                { id: 'hamil', name: t('hamil') },
                { id: 'menikah', name: t('menikah') },
                { id: 'menikahkanAnak', name: t('menikahkan') },
                { id: 'mengkhitankanAnak', name: t('mengkhitankan') },
                { id: 'membaptiskanAnak', name: t('membaptiskan') },
                { id: 'istriMelahirkan', name: t('melahirkan') },
                { id: 'istriKeguguran', name: t('keguguran') },
                { id: 'keluargaIntiMeninggal', name: t('intimeninggal') },
                { id: 'keluargaBesarMeninggal', name: t('besarmeninggal') },
            ],
            description: '',
            previewDocument: null,
            creating: false,
            filter: {
                start: searchParams.get('start'),
                end: searchParams.get('end')
            }
        };

        this.toggleCreateModal = this.toggleCreateModal.bind(this)
    }
    fetchData = async () => {
        const params = Object.keys(this.state.filter)
            .filter(k => this.state.filter[k] !== null)
            .reduce((acc, k) => ({ ...acc, [k]: formatDate(this.state.filter[k]) }), {});

        this.setState({ loading: true })
        try {
            const { data } = await request.get('v1/holidays/request/history', { params });
            this.setState({ holidays: data.data })
        } catch (err) {
            if (err.response) {
                toast.error(err.message, { autoClose: 2000 });
            }
            console.log(err)
            throw err;
        } finally {
            this.setState({ loading: false })
        }
    }

    componentDidMount() {
        this.fetchData()
    }

    toggleCreateModal() {
        this.setState({
            showCreateModal: !this.state.showCreateModal,
            sakit: false,
            previewDocument: null,
        })
    }

    handleChangeFilter = (name, date) => {
        let filter = { ...this.state.filter }
        filter[name] = date
        this.setState({ filter })
    }

    handleInputChange = (e) => {
        let data = { ...this.state.createData }
        data[e.target.name] = e.target.value;
        if (data['category'] === 'sakit') {
            this.setState({
                sakit: true
            })
        } else {
            this.setState({
                sakit: false
            })
        }
        let description = this.changeDescription(e.target.value);
        this.setState({
            description,
            createData: data
        })
    }

    handleDatepickerChangeStart = (date) => {
        let createData = { ...this.state.createData }
        createData['start'] = formatDate(date)
        this.setState({ createData })
    }
    handleDatepickerChangeEnd = (date) => {
        let createData = { ...this.state.createData }
        createData['end'] = formatDate(date)
        this.setState({ createData })
    }

    handleDocumentChange = (e) => {
        e.preventDefault();
        let createData = { ...this.state.createData }
        let previewDocument;
        if (e.target.files.length) {
            createData.document = e.target.files[0]
            previewDocument = URL.createObjectURL(e.target.files[0])
        } else {
            createData.document = null
        }
        this.setState({ createData, previewDocument })
    }

    submitCreateHolidays = async (e) => {
        e.preventDefault();
        const { t } = this.props;
        this.setState({ creating: true })
        try {
            const formData = new FormData();
            Object.keys(this.state.createData).forEach(name => {
                if (name === 'time') return;
                if (name === 'datetime') {
                    formData.append(name, this.state.createData['datetime'] + ' ' + this.state.createData['time'] + ':00')
                } else if (name === 'nominal') {
                    formData.append(name, parseInt(this.state.createData[name].replace(/,.*|[^0-9]/g, ''), 10))
                } else {
                    formData.append(name, this.state.createData[name]);
                }
            });
            this.setState({
                createData: {
                    start: '',
                    end: '',
                    time: '',
                    message: '',
                    category: null,
                    document: null,
                },
                showCreateModal: false,
                sakit: false,
            })
            const response = await request.post('v1/holidays/request', formData);
            this.fetchData();
            this.setState({ showCreateModal: false });
            toast.success(response.data.message || t('berhasilperbarui'))
        } catch (err) {
            if (err.response && err.response.data) {
                toast.error(err.response.data.message)
            }
            this.setState({
                createData: {
                    start: '',
                    end: '',
                    time: '',
                    message: '',
                    category: null,
                    document: null,
                }
            })
        } finally {
            this.setState({ creating: false });
        }
    }
    changeCategory = (data) => {
        const { t } = this.props;
        switch (data) {
            case 'tahunan': return t('tahunan');
            case 'hamil': return t('hamil');
            case 'sakit': return t('sakit');
            case 'menikah': return t('menikah');
            case 'menikahkanAnak': return t('menikahkan');
            case 'mengkhitankanAnak': return t('mengkhitankan');
            case 'membaptiskanAnak': return t('membaptiskan');
            case 'istriMelahirkan': return t('melahirkan');
            case 'istriKeguguran': return t('keguguran');
            case 'keluargaIntiMeninggal': return t('intimeninggal');
            case 'keluargaBesarMeninggal': return t('besarmeninggal');
            default: return null
        }

    }
    changeDescription = (data) => {
        const { t } = this.props;
        switch (data) {
            case 'tahunan': return t('tahunan_desc');
            case 'hamil': return t('hamil_desc');
            case 'sakit': return t('sakit_desc');
            case 'menikah': return t('menikah_desc');
            case 'menikahkanAnak': return t('menikahkan_desc');
            case 'mengkhitankanAnak': return t('mengkhitankan_desc');
            case 'membaptiskanAnak': return t('membaptiskan_desc');
            case 'istriMelahirkan': return t('melahirkan_desc');
            case 'istriKeguguran': return t('keguguran_desc');
            case 'keluargaIntiMeninggal': return t('intiMeninggal_desc');
            case 'keluargaBesarMeninggal': return t('besarMeninggal_desc');
            default: return null
        }

    }
    changeStatus = (data) => {
        const { t } = this.props;
        switch (data) {
            case 'pending': return t('diproses');
            case 'approved1': return t('menunggupersetujuanadmin');
            case 'approved2': return t('disetujui');
            case 'rejected': return t('ditolak');
            default: return null
        }
    }
    changeBadge = (data) => {
        switch (data) {
            case 'pending': return 'pending';
            case 'approved1': return 'approved1';
            case 'approved2': return 'approved2';
            case 'rejected': return 'rejected';
            default: return null
        }
    }
    render() {
        const { t } = this.props;
        moment.locale(t('id'))
        return (
            <div className="animated fadeIn">
                <div className="d-flex align-items-center mb-4">
                    <Button color="netis-color" onClick={this.toggleCreateModal}>
                        <i className="fa fa-plus mr-2"></i> {t('tambah')} {t('cuti')}
                    </Button>
                </div>
                <Row className="md-company-header mb-3">
                    <Col xs="12">
                        {this.state.loading ? <LoadingAnimation /> :
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th className="text-center w-5">No.</th>
                                        <th className="w-20">{t('kategori')} </th>
                                        <th className="w-15">{t('tanggal')} {t('mulai')} </th>
                                        <th className="w-15">{t('tanggal')} {t('selesai')} </th>
                                        <th>Status</th>
                                        <th className="w-20"></th>
                                    </tr>
                                </thead>
                                {this.state.holidays.length > 0 ?
                                    (
                                        <tbody>{this.state.holidays.map((data, index) => {
                                            let category = this.changeCategory(data.category);
                                            let status = this.changeStatus(data.status);
                                            let badge = this.changeBadge(data.status);
                                            return (
                                                <tr key={index}>
                                                    <td className="text-center">{index + 1}</td>
                                                    <td>{category}</td>
                                                    <td className="text-nowrap">{moment(data.start).format('DD MMM YYYY')}</td>
                                                    <td className="text-nowrap">{moment(data.end).format('DD MMM YYYY')}</td>
                                                    <td className="text-nowrap"><StatusBadge status={badge} />{status}</td>
                                                    <td>
                                                        <Link to={'/cuti/history/' + data.id}>
                                                            <Button color="netis-color">{t('detail')}</Button>
                                                        </Link>
                                                    </td>
                                                </tr>
                                            )
                                        }
                                        )
                                        }</tbody>
                                    ) : null}
                            </Table>}
                    </Col>
                </Row>

                <Modal isOpen={this.state.showCreateModal} toggle={this.toggleCreateModal}>
                    <ModalBody>
                        <form onSubmit={this.submitCreateHolidays}>
                            <div className="d-flex justify-content-between align-items-center mb-4">
                                <h5 className="content-sub-title mb-0">{t('tambah')} {t('cuti')}</h5>
                                <Button color="link" size="lg" className="text-danger" onClick={this.toggleCreateModal}><strong>&times;</strong></Button>
                            </div>
                            <Row form>
                                <Col md="12">
                                    <FormGroup>
                                        <Label htmlFor="category" className="input-label">{t('kategori')} </Label>
                                        <Input type="select" name="category" id="category" onChange={this.handleInputChange} >
                                            <option value={null}>-- {t('pilih')} {t('cuti')} {t('kategori')} --</option>
                                            {this.state.categories.map((data, idx) => {
                                                return (
                                                    <option key={idx} value={data.id}>{data.name}</option>
                                                )
                                            })}
                                        </Input>
                                        <label className="input-label " style={{ marginTop: 5 }}><b>{this.state.description}</b></label>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row form>
                                <Col md="6">
                                    <FormGroup>
                                        <Label htmlFor="start" className="input-label">{t('tanggal')} {t('mulai')}</Label>
                                        <DatePickerInput
                                            showOnInputClick={true}
                                            name="start"
                                            id="start"
                                            onChange={this.handleDatepickerChangeStart}
                                            maxDate={this.state.createData.end}
                                            value={this.state.createData.start}
                                            className="my-custom-datepicker-component"
                                            closeOnClickOutside={true}
                                            displayFormat="DD MMMM YYYY"
                                            readOnly
                                        />
                                    </FormGroup>
                                </Col>
                                <Col md="6">
                                    <FormGroup>
                                        <Label htmlFor="end" className="input-label">{t('tanggal')} {t('selesai')}</Label>
                                        <DatePickerInput
                                            showOnInputClick={true}
                                            name="end"
                                            id="end"
                                            onChange={this.handleDatepickerChangeEnd}
                                            value={this.state.createData.end}
                                            disabled={!this.state.createData.start}
                                            minDate={this.state.createData.start}
                                            className="my-custom-datepicker-component"
                                            closeOnClickOutside={true}
                                            displayFormat="DD MMMM YYYY"
                                            readOnly
                                        />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row form>
                                <Col>
                                    <FormGroup>
                                        <Label htmlFor="message" className="input-label">{t('deskripsi')}</Label>
                                        <Input type="textarea" value={this.state.createData.message} onChange={this.handleInputChange} name="message" id="message" rows="2" placeholder=""></Input>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                {this.state.sakit === true ?
                                    (
                                        <Col md="12">
                                            <FormGroup>
                                                <Label htmlFor="document" className="input-label">Upload {t('foto')} {t('suratsakit')} <span style={{ fontWeight: 'bold' }}>( Max. 5 MB )</span></Label>
                                                <Input type="file" id="document" name="document" onChange={this.handleDocumentChange} />
                                            </FormGroup>
                                        </Col>
                                    )
                                    : null
                                }
                                <Col md="7">{this.state.previewDocument &&
                                    <div className="d-flex justify-content-center" style={{ overflowBlock: "hidden", maxHeight: 300, maxWidth: '100%' }}>
                                        <img width="300" height="300" src={this.state.previewDocument} alt="preview dokumen" />
                                    </div>
                                }</Col>
                            </Row>
                        </form>
                    </ModalBody>
                    <ModalFooter>
                        {this.state.creating ?
                            (<><Spinner size="sm" color="light"></Spinner> Loading</>) :
                            (<>
                                <Button type="button" color="link" onClick={this.toggleCreateModal}>
                                    {t('batal')}
                                </Button>
                                <Button type="submit" color="netis-color" onClick={this.submitCreateHolidays}>


                                    <i className="fa fa-plus mr-2"></i> {t('simpan')}

                                </Button></>)
                        }
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.token
    }
}
export default connect(mapStateToProps)(translate(CutiHistory));
