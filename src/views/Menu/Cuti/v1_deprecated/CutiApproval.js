import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Table, Button, Modal, Form, Row, ModalBody, ModalFooter, ModalHeader, FormGroup, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Spinner, Label, Input, Col } from 'reactstrap';
import { toast } from 'react-toastify';
import { DatePickerInput } from 'rc-datepicker';
import StatusBadge from './../components/StatusBadge'
import request from '../../../../utils/request'
import { formatDate } from '../../../../utils/formatter';
// import { convertToRupiah } from '../../../../utils/formatter'
import moment from '../../../../utils/moment'
import { Link } from 'react-router-dom';
import LoadingAnimation from '../../../../components/LoadingAnimation'
import DataNotFound from '../../../../components/DataNotFound'
import {
    translate,
} from 'react-switch-lang';
toast.configure()

const PANEL_ADMIN = '2';

class CutiApproval extends Component {

    constructor(props) {
        super(props);

        const searchParams = new URLSearchParams(props.location.search);

        this.state = {
            loading: true,
            holidays: [],
            showModal: false,
            updating: false,
            update: {
                holidayId: null,
                status: '',
                message: '',
            },
            dropdownOpened: {},
            showDocument: false,
            user: props.user,
            filter: {
                start: searchParams.get('start'),
                end: searchParams.get('end')
            },
        };
    }

    fetchData = async () => {
        const params = Object.keys(this.state.filter)
            .filter(k => this.state.filter[k] !== null)
            .reduce((acc, k) => ({ ...acc, [k]: formatDate(this.state.filter[k]) }), {});

        try {
            this.setState({ loading: true })
            const menu = this.props.menu;
            const url = menu === PANEL_ADMIN ? 'v1/holidays/admin/request' : 'v1/holidays/request';
            const { data } = await request.get(url, { params });
            this.setState({ holidays: data.data })
        } catch (err) {
            if (err.response) {
                toast.error(err.message, { autoClose: 2000 });
            }
            console.log(err)
            throw err;
        } finally {
            this.setState({ loading: false })
        }
    }

    async componentDidMount() {
        this.fetchData()
    }

    handleChangeFilter = (name, date) => {
        let filter = { ...this.state.filter }
        filter[name] = date
        this.setState({ filter })
    }


    toggleModal = () => {
        const closing = this.state.showModal;
        this.setState({ showModal: !this.state.showModal })
        if (closing) {
            this.setState({ update: { status: '', message: '', holidayId: null } })
        }
    }

    updateStatus = (holidayId, status) => (e) => {
        const update = { ...this.state.update }
        update.status = status
        update.holidayId = holidayId
        this.setState({ update })
        this.toggleModal()
    }

    submitUpdateStatus = async (e) => {
        e.preventDefault();
        this.setState({ updating: true })
        const { holidayId, status, message } = this.state.update
        if (holidayId) {
            try {
                const { data } = await request.post(`v1/holidays/request/${holidayId}`, { status, message })
                this.fetchData();
                toast.success(data.message || 'Success')
                this.toggleModal();
            } catch (err) {
                if (err.response && err.response.data) {
                    toast.error(err.response.data.message);
                }
                throw err;
            } finally {
                this.setState({ updating: false })
            }
        }
    }

    toggleDropdown = (index) => (e) => {
        let dropdownOpened = { ...this.state.dropdownOpened }
        dropdownOpened[index] = !dropdownOpened[index]
        this.setState({ dropdownOpened })
    }

    showDocument = (document) => {
        this.setState({ showDocument: process.env.REACT_APP_DOMAIN + document })
    }

    toggleShowDocument = () => {
        this.setState({ showDocument: false })
    }

    handleInputChange = (e) => {
        const update = { ...this.state.update }
        update[e.target.name] = e.target.value
        this.setState({ update })
    }
    changeCategory = (data) => {
        const { t } = this.props;
        switch (data) {
            case 'tahunan': return t('tahunan');
            case 'hamil': return t('hamil');
            case 'sakit': return t('sakit');
            case 'menikah': return t('menikah');
            case 'menikahkanAnak': return t('menikahkan');
            case 'mengkhitankanAnak': return t('mengkhitankan');
            case 'membaptiskanAnak': return t('membaptiskan');
            case 'istriMelahirkan': return t('melahirkan');
            case 'istriKeguguran': return t('keguguran');
            case 'keluargaIntiMeninggal': return t('intimeninggal');
            case 'keluargaBesarMeninggal': return t('besarmeninggal');
            default: return null
        }

    }
    changeStatus = (data) => {
        const { t } = this.props;
        switch (data) {
            case 'pending': return t('diproses');
            case 'approved1': return t('menunggupersetujuanadmin');
            case 'approved2': return t('disetujui');
            case 'rejected': return t('ditolak');
            default: return null
        }
    }
    changeBadge = (data) => {
        switch (data) {
            case 'pending': return 'pending';
            case 'approved1': return 'approved1';
            case 'approved2': return 'approved2';
            case 'rejected': return 'rejected';
            default: return null
        }
    }
    render() {
        const { t } = this.props;
        const { privileges: userPrivileges = [] } = this.props.user;
        moment.locale(t('id'))
        const dataTable = this.state.holidays.map((data, index) => {
            let category = this.changeCategory(data.category);
            let status = this.changeStatus(data.status);
            let badge = this.changeBadge(data.status);
            return (
                <tr key={index}>
                    <td className="text-center">{index + 1}</td>
                    <td>{category}</td>
                    <td>
                        {data.personnel.user.fullName}
                    </td>
                    <td className="text-nowrap">{moment(data.start).format('DD MMM YYYY')}</td>
                    <td className="text-nowrap">{moment(data.end).format('DD MMM YYYY')}</td>
                    <td className="text-nowrap"><StatusBadge status={badge} />{status}</td>
                    <td className="text-nowrap">
                        {/* eslint-disable-next-line */}
                        {( !['approved2', 'rejected'].includes(data.status) ) && (
                        <Dropdown className="d-inline-block mr-1" isOpen={this.state.dropdownOpened[index]} toggle={this.toggleDropdown(index)}>
                            <DropdownToggle color="netis-color" caret>
                                {t('ubah')} Status
                            </DropdownToggle>
                            <DropdownMenu>
                                {
                                    (data.status === 'pending') || (userPrivileges.includes('verify-holiday') && data.status === 'approved1') ? (
                                        <DropdownItem onClick={this.updateStatus(data.id, 'approved')}>
                                            <i className="fa fa-circle text-info"></i> {t('disetujui')}
                                        </DropdownItem>)
                                        :
                                        null
                                }
                                <DropdownItem onClick={this.updateStatus(data.id, 'rejected')}>
                                    <i className="fa fa-circle text-danger"></i> {t('ditolak')}
                                </DropdownItem>
                            </DropdownMenu>
                        </Dropdown>)}
                        <Link to={'/cuti/manage/' + data.id}>
                            <Button className={`${userPrivileges.includes('read-holiday') ? '' : ' d-none'}`} color="netis-color">{t('detail')}</Button>
                        </Link>
                    </td>
                </tr>
            )
        })
        return (
            <div className="animated fadeIn md-company-header mb-3">
                <div className="d-flex align-items-center mb-4">
                    <Form onSubmit={(e) => { e.preventDefault(); this.fetchData() }}>
                        <Row>
                            <Col sm="5">
                                <Label tag="small" htmlFor="start" className="text-muted">{t('dari')}</Label>
                                <br />
                                <DatePickerInput
                                    readOnly
                                    showOnInputClick={true}
                                    name="start"
                                    id="start"
                                    onChange={(value) => this.handleChangeFilter('start', value)}
                                    value={this.state.filter.start}
                                    className='my-custom-datepicker-component'
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                />
                            </Col>
                            <Col sm="5">
                                <Label tag="small" htmlFor="end" className="text-muted">{t('hingga')}</Label>
                                <br />
                                <DatePickerInput
                                    readOnly
                                    showOnInputClick={true}
                                    name="end"
                                    id="end"
                                    onChange={(value) => this.handleChangeFilter('end', value)}
                                    value={this.state.filter.end}
                                    className='my-custom-datepicker-component'
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                />
                            </Col>
                            <Col sm="2">
                                <div className="pt-sm-0">
                                    <Button type="submit" className="mt-sm-4" color="netis-color" style={{ width: '60px' }} disabled={this.state.loading}>
                                        {this.state.loading ? <Spinner color="light" size="sm" /> : t('cari')}
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </div>

                {this.state.loading ? <LoadingAnimation />
                    : !this.state.holidays.length ? <DataNotFound />
                        : (
                        <div className="table-data">
                        <Table className="table-data">
                            <thead>
                                <tr className="grey-tr">
                                    <th className="header-sticky text-center w-5">No.</th>
                                    <th className="header-sticky w-15">{t('kategori')}</th>
                                    <th className="header-sticky w-20">{t('nama')}</th>
                                    <th className="header-sticky w-15">{t('tanggal')} {t('mulai')} </th>
                                    <th className="header-sticky w-15">{t('tanggal')} {t('selesai')}</th>
                                    <th className="header-sticky">Status</th>
                                    <th className="header-sticky"></th>
                                </tr>
                            </thead>
                            <tbody className="scroll-table">
                                {dataTable}
                            </tbody>
                        </Table>
                        </div>
                        )}

                <Modal isOpen={!!this.state.showDocument} toggle={this.toggleShowDocument}>
                    <ModalBody>
                        <img src={this.state.showDocument} width="100%" alt="document" />
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.showModal} toggle={this.toggleModal}>
                    <form onSubmit={this.submitUpdateStatus}>
                        <ModalHeader toggle={this.toggleModal}>Update Status</ModalHeader>
                        <ModalBody>
                            <FormGroup row>
                                <Label htmlFor="status" className="text-md-right" md="3">Status</Label>
                                <Col md={9}>
                                    <Input type="select" name="status" id="status" value={this.state.update.status} disabled>
                                        <option value="approved">{t('disetujui')}</option>
                                        <option value="rejected">{t('ditolak')}</option>
                                    </Input>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label htmlFor="message" className="text-md-right" md={3}>{t('pesan')}</Label>
                                <Col md={9}>
                                    <Input type="textarea" name="message" id="message" rows="2" value={this.state.update.message} onChange={this.handleInputChange}></Input>
                                </Col>
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            {/*  */}
                            <Button type="submit" color="netis-color" disabled={this.state.updating}>
                                {this.state.updating ?
                                    (<><Spinner size="sm" color="light"></Spinner> Loading</>) :
                                    (<><i className="fa fa-check mr-2"></i> Update Status</>)
                                }
                            </Button>
                        </ModalFooter>
                    </form>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = (reduxState) => ({ user: reduxState.user, menu: reduxState.menu })
export default connect(mapStateToProps)(translate(CutiApproval));
