import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import{translate, t} from 'react-switch-lang';
import request from "../../../utils/request";
import {Col, Row, Input, InputGroup, InputGroupAddon, InputGroupText, Modal } from 'reactstrap';
import LoadingAnimation from '../../../components/LoadingAnimation';
import DataNotFound from '../../../components/DataNotFound';
import useSWR from 'swr';
import ModalHeader from 'reactstrap/lib/ModalHeader';
import ModalBody from 'reactstrap/lib/ModalBody';
import ModalFooter from 'reactstrap/lib/ModalFooter';
import Spinner from 'reactstrap/lib/Spinner';
import Select from 'react-select';
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../components/ResponsiveTable';
import usePagination from '../../../hooks/usePagination';

function CutiQuota(){

    const [search, setSearch] = useState('');
    const [filterUnit, setFilterUnit] = useState([])
    const [filters, setFilters] = useState([])
    const [savingPersonnel, setSavingPersonnel] = useState(null);

    const { data: response, error, mutate } = useSWR('v1/personnel/holidays/balances');
    const {data : dataUnit, error: errorUnit} = useSWR(`/v1/master/units`)
    const loading = !response && !error && !dataUnit && !errorUnit;
    const notFound = !response?.data?.data?.length || !dataUnit?.data?.data?.length;
    
    const personnel = useMemo(() => {
        return response?.data?.data ?? [];
    }, [response])

    const unitOption = useMemo(() => {
        return dataUnit?.data?.data ?? []
    }, [dataUnit])
    
    const filteredPersonnel = useMemo(() => {
        let filterData = personnel
        ?.filter(item =>
            search?.trim() ? item?.userFullName?.toLowerCase().includes(search?.trim().toLowerCase()) : true
        )
        ?.filter(item =>
            filterUnit?.length > 0 ? filterUnit.includes(item?.unitName) : true
        )
        // ?.filter(item =>
        //     filterJob?.length > 0 ? filterJob.includes(item.job.id) : true
        // )
      return filterData;
        // if (search?.trim()) {
        //     return personnel.filter((person) => person?.userFullName?.toLowerCase().includes(search?.trim().toLowerCase()));
        // }
        // return personnel;
    }, [personnel, search, filterUnit]); 

    const handleSearch = (e) => {
        setSearch(e.target.value)
    }
    const handleUnit = (e) => {
        let unit = e?.map(item => item.label) ?? []
        setFilterUnit(unit)
      }

    const handleChangeCurrentPage = useCallback(
    (page) => {
        setFilters((state) => ({ ...state, pagination: page }));
    },
    [setFilters]
    );

    const { data: groupFiltered, PaginationComponent } = usePagination(
        filteredPersonnel,
        8,
        filters.pagination,
        handleChangeCurrentPage
    );

    const toggleSavingModal = useCallback((personnel) => {
        setSavingPersonnel(personnel);
    }, [])

    const toggleModalEdit = useCallback(() => {
        setSavingPersonnel(null);
    }, []);

    const handleSubmit = useCallback((personnelId, { balance, message }) => {
        return request.put(`v1/personnel/holidays/balances/${personnelId}`, { balance, message })
            .then(res => mutate(currentResponse => {
                console.log(currentResponse);
                const newResponse = { ...currentResponse};
                const changedIndex = newResponse.data.data.findIndex(person => person.personnelId === personnelId);
                newResponse.data.data[changedIndex]['personnelHolidayBalance'] = balance;
                return newResponse;
            }, true));
    }, [mutate])

    if (loading) {
        return <LoadingAnimation />;
    }
    else if (notFound) {
        return <DataNotFound />
    }

    return(
        <div className="animated fadeIn" >
            <Row className="md-company-header mb-3">
                <Col xs="12" md="6" className="my-1">
                    <InputGroup className="w-75">
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="input-group-transparent">
                                <i className="fa fa-search"></i>
                            </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder={t('cari') + ' ' + t('nama')} className="input-search"
                        onChange={handleSearch}
                        />
                    </InputGroup>
                </Col>
                <Col md="6" className="my-1">
                    <Select
                        disabled={unitOption.length < 1}
                        isMulti={true}
                        isSearchable={false}
                        name="unit"
                        options={unitOption.map(item => ({ label: item.name, value: item.id }))}
                        placeholder={t("Unit")}
                        onChange={handleUnit}
                    />
                </Col>
            </Row>
            <Row>
                <Col xs="12" lg="12">
                    <Rtable size="sm" style={{display:'table'}}>
                        <Rthead>
                            <Rtr>
                                <Rth className="text-left w-50">{t('nama')}</Rth>
                                <Rth className="text-left">Unit</Rth>
                                <Rth className="text-left text-md-center">{t('jatahcuti')}</Rth>
                                <Rth></Rth>
                            </Rtr>
                        </Rthead>
                        <Rtbody>
                            {groupFiltered.map((person) => (
                                <PersonnelHolidayBalanceRow key={person.personnelId} personnel={person} onEdit={toggleSavingModal}/>
                            ))}
                        </Rtbody>
                    </Rtable>
                </Col>
            </Row>
            {filteredPersonnel?.length > 8 && <PaginationComponent />}
            <ModalEditBalance toggle={toggleModalEdit} personnel={savingPersonnel} onSubmit={handleSubmit}/>
        </div>
    )
}

const PersonnelHolidayBalanceRow = memo(({ personnel, onEdit, saved,  }) => {
    return (
        <Rtr className={saved ? 'text-success' : undefined}>
            <Rtd className="text-left">{personnel.userFullName}</Rtd>
            <Rtd className="text-left">{personnel.unitName}</Rtd>
            <Rtd className="text-left text-md-center">
                <strong>{personnel.personnelHolidayBalance}</strong>
            </Rtd>
            <Rtd className="text-right text-nowrap">
                <button className="ml-md-2 btn btn-sm btn-netis-primary" onClick={() => onEdit(personnel)}><i className="fa fa-pencil mr-1"></i> {t('ubah')}</button>
            </Rtd>
        </Rtr>
    );
});

const ModalEditBalance = memo(({ toggle, personnel, onSubmit }) => {
    const [payload, setPayload] = useState({
        balance: personnel?.personnelHolidayBalance ?? 0,
        message: ''
    });
    const [loading, setLoading] = useState(false);

    const reset = () => {
        setPayload({
            balance: personnel?.personnelHolidayBalance ?? 0,
            message: ''
        });
    }

    useEffect(reset, [personnel]);

    const handleChange = (event) => {
        const name = event.target.name;
        const type = event.target.type ?? 'text';
        const value = type === 'number' ? parseFloat(event.target.value) : event.target.value;
        setPayload(prev => ({...prev, [name]: value}));
    }

    const handleSubmit = (event) => {
        if (payload.balance) {
            setLoading(true);
            onSubmit(personnel.personnelId, payload).finally(() => {
                setLoading(false);
                toggle();
            });
        }
    }

    const currentBalance = personnel?.personnelHolidayBalance ?? 0;
    const changes = payload.balance === '' ? 0 : payload.balance - currentBalance;

    return (
        <div>
            <Modal toggle={toggle} isOpen={personnel !== null}>
                <ModalHeader toggle={toggle}>{t('Ubah Kuota Cuti')}</ModalHeader>
                <ModalBody>
                    <div className="form-group">
                        <label htmlFor="" className="input-label">{t('nama')}</label>
                        <input type="text" className="form-control" readOnly value={personnel?.userFullName ?? ''}/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="" className="input-label">{t('jatahcuti')}</label>
                        <div className="row">
                            <div className="col-md-7 col-sm-10">
                                <div className="d-flex">
                                    <button className="btn  btn-netis-primary d-flex" onClick={() => setPayload(prev => ({...prev, balance: prev.balance - 1}))}>
                                        <i className="fa fa-minus m-auto"></i>
                                    </button>
                                    <input type="number" name="balance" className="form-control text-center mx-2" value={payload.balance} onChange={handleChange}/>
                                    <button className="btn  btn-netis-primary d-flex" onClick={() => setPayload(prev => ({...prev, balance: prev.balance + 1}))}>
                                        <i className="fa fa-plus m-auto"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="form-group mt-4">
                        <label htmlFor="message" className="input-label">{t('pesan')}</label>
                        <textarea className="form-control" name="message" id="message" value={payload.message} onChange={handleChange} placeholder={t('Tulis keterangan disini')}/>
                    </div>
                </ModalBody>
                <ModalFooter className="d-flex">
                    <button className="btn btn-outline-netis-primary" onClick={reset} disabled={loading}>{t('reset')}</button>
                    <button className="btn btn-secondary ml-auto" onClick={toggle} disabled={loading}>{t('batal')}</button>
                    <button className="btn btn-netis-primary" onClick={handleSubmit} disabled={loading || !changes}>
                        {loading ? <><Spinner color="white" size="sm" /> {t('loading')}</> : t('simpan')}
                    </button>
                </ModalFooter>
            </Modal>
        </div>
    );
});

export default translate(CutiQuota);
