import React, { useState, useEffect, useCallback } from "react";
import request from '../../../utils/request'
import { Button, Card, CardBody, Table, Row, Col, CardHeader, UncontrolledDropdown, DropdownToggle, DropdownItem, DropdownMenu } from "reactstrap";
import {
  Form, FormGroup, Label, Spinner, Input, Modal, ModalHeader, ModalBody, ModalFooter,
} from "reactstrap";
import moment from '../../../utils/moment'
import { toast } from "react-toastify";
import StatusBadge from './components/StatusBadge'
import Loading from '../../../components/LoadingAnimation'
import DataNotFound from '../../../components/DataNotFound'
import { t } from 'react-switch-lang';
import { useUserPrivileges, useAuthUser } from "../../../store";
import { useRouteMatch } from "react-router-dom";

const logMessage = {
  pending: t('Permintaan dibuat'),
  approved1: t('Disetujui oleh Atasan'),
  approved2: t('Disetujui oleh Admin'),
  rejected: t('Permintaan ditolak')
}

function CutiDetail(props){
  const user = useAuthUser();
  const {can} = useUserPrivileges();
  const [loading, setLoading] = useState(true);
  const [notFound, setNotFound] = useState(false);
  const [holiday, setHoliday] = useState(null);
  const matchRoute = useRouteMatch();
  const [updateModal, setUpdateModal] = useState(null)

  useEffect(() => {
    request.get(`v2/holidays/request/${matchRoute.params.id}`)
      .then((res) => {
        const data = res.data.data;
        data.category = changeCategory(data?.category);
        setHoliday(data)
      })
      .catch((err) => {
        if (err.response && err.response.status === 404){
          setNotFound(true)
        }
        throw err;
      })
      .finally(() => setLoading(false))
  // eslint-disable-next-line
  }, [matchRoute.params.id])

  const changeCategory = (data) => {
    switch (data) {
      case 'tahunan': return t('tahunan');
      case 'hamil': return t('hamil');
      case 'sakit': return t('sakit');
      case 'menikah': return t('menikah');
      case 'menikahkanAnak': return t('menikahkan');
      case 'mengkhitankanAnak': return t('mengkhitankan');
      case 'membaptiskanAnak': return t('membaptiskan');
      case 'istriMelahirkan': return t('melahirkan');
      case 'istriKeguguran': return t('keguguran');
      case 'keluargaIntiMeninggal': return t('intimeninggal');
      case 'keluargaBesarMeninggal': return t('besarmeninggal');
      default: return null
    }
  }

  const changeStatus = (data) => {
    switch (data) {
      case 'pending': return t('diproses');
      case 'approved1': return t('menunggupersetujuanadmin');
      case 'approved2': return t('disetujui');
      case 'rejected': return t('ditolak');
      default: return null
    }
  }
  const changeStatusLog = (data) => {
    switch (data) {
      case 'pending': return t('diproses');
      case 'approved1': return t('menunggupersetujuanadmin');
      case 'approved2': return t('disetujui');
      case 'rejected': return t('ditolak');
      default: return null
    }
  }
  const changeBadge = (data) => {
    switch (data) {
      case 'pending': return 'pending';
      case 'approved1': return 'approved1';
      case 'approved2': return 'approved2';
      case 'rejected': return 'rejected';
      default: return null
    }
  }

  const updateStatus = (dataId, status) => {
    setUpdateModal({...updateModal, status:status, reimburseId:dataId})
  }

  const onUpdatedStatus = (data) => {
    setHoliday(data)
  };

  const handleBackButton = useCallback(() => {
    const url = props.location.pathname;

    return props.history.push(url.slice(0, url.lastIndexOf('/')));
  }, [props.location, props.history]);

  if (loading) {
    return <Loading />;
  }

  if (notFound) {
    return <DataNotFound />
  }

  function canChangeStatus() {
    // jika status = pending dan memiliki atasan,
    // dan cuti wajib disetujui atasan terlebih dahulu (holidaySuperiorApproval = true)
    // maka yang bisa mengubah status hanya atasannya.
    if (holiday.status === 'pending' && holiday.personnelParent && user.personnel.company.holidaySuperiorApproval) {
      return holiday.personnelParent === user.personnel.id;
    }

    // jika status belum diapprove admin, atau belum ditolak
    // maka status dapat diubah.
    return can('verify-holiday') && !['approved2', 'rejected'].includes(holiday.status);
  } 
  
  return (
    <div className="animated fadeIn">
      <div className="d-flex bd-highlight mb-3">
          <div className="mr-auto bd-highlight mt-2">
            <Button color="netis-color" onClick={handleBackButton}><i className="fa fa-chevron-left mr-2"></i> {t('kembali')} </Button>
          </div>
          {canChangeStatus() && 
            <UncontrolledDropdown className="d-inline-block mt-2">
                <DropdownToggle color="netis-color" caret>
                    {t('ubah')} Status
                </DropdownToggle>
                <DropdownMenu>
                    {
                        (holiday.status === 'pending') || (can('verify-holiday') && holiday.status === 'approved1') ? (
                            <DropdownItem onClick={() => updateStatus(holiday.id, 'approved')}>
                                <i className="fa fa-circle text-info"></i> {t('disetujui')}
                            </DropdownItem>)
                            :
                            null
                    }
                    <DropdownItem onClick={() => updateStatus(holiday.id, 'rejected')}>
                        <i className="fa fa-circle text-danger"></i> {t('ditolak')}
                    </DropdownItem>
                </DropdownMenu>
            </UncontrolledDropdown>
          }
      </div>
      <UpdateStatusModalForm
          toggle={() => setUpdateModal(null)}
          payload={updateModal}
          onUpdated={onUpdatedStatus}
          props={props}
      />
      <Card>
        <CardBody>
          <Row>
            <Col md="6">
              <Table hover>
                <tbody>
                  <tr>
                    <th className="text-capitalize">{t('nama karyawan')}</th>
                    <td>{holiday.personnel.user.fullName}</td>
                  </tr>
                  <tr>
                    <th>{t('Tanggal Mulai')}</th><td>{moment(holiday.start).format('DD MMMM YYYY')}</td>
                  </tr>
                  <tr>
                    <th>{t('Tanggal Selesai')}</th><td>{moment(holiday.end).format('DD MMMM YYYY')}</td>
                  </tr>
                  <tr>
                    <th>{t('kategori')}</th><td>{holiday.categoryName}</td>
                  </tr>
                  <tr>
                    <th>{t('deskripsi')}</th><td>{holiday.message}</td>
                  </tr>
                  <tr>
                    <th>Status</th><td><StatusBadge status={changeBadge(holiday.status)} />{changeStatus(holiday.status)}</td>
                  </tr>
                </tbody>
              </Table>
            </Col>
            <Col md="6">
              {holiday.document !== null ?
                <img src={process.env.REACT_APP_DOMAIN + "" + holiday.document} width="100%" alt="document" />
                : null}
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          {t('riwayatstatus')}
        </CardHeader>
        <CardBody>
          <Table>
            <tbody>
            {holiday.status_log && holiday.status_log.map((statusLog, index) => {
              let status = changeStatusLog(statusLog.status);
              let badge = changeBadge(statusLog.status);
              return (
                <tr key={index}>
                  <td style={{ width: 170 }}>
                    <strong>{(statusLog.subject && statusLog.subject.nickName) || 'system'}</strong><br />
                    <span className="text-muted">{moment(statusLog.created).format('DD MMMM YYYY HH:mm')}</span>
                  </td>
                  <td>
                    <strong><StatusBadge status={badge} />{status}</strong>
                    <br /> {logMessage[statusLog.status]}
                  </td>
                </tr>
              )
            })}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    </div>
  );
}

function UpdateStatusModalForm({ toggle, payload, onUpdated, props }) {
  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setMessage("");
  }, [payload]);

  function submitForm(e) {
    e.preventDefault();
    setLoading(true);
    request.post(`v2/holidays/request/${payload.reimburseId}`, { status: payload.status, message })
      .then(res => {
        const { data } = res.data
        toast.success(t('Berhasil mengubah status Cuti'))
        onUpdated(data);
        toggle();
      })
      .catch((err) => {
        if (err.response && err.response.data) {
            toast.error(err.response.data.message);
        }
        throw err;
      })
      .finally(() => {
        setLoading(false)
        props.history.goBack();
      });
  }

  return (
    <Modal isOpen={!!payload} toggle={() => !loading && toggle()}>
      {!!payload && (
        <Form onSubmit={submitForm}>
          <ModalHeader toggle={() => !loading && toggle()}>
            Update Status
          </ModalHeader>
          <ModalBody>
            <FormGroup row>
              <Label htmlFor="status" className="text-md-right" md="3">
                Status
              </Label>
              <Col md="9">
                <Input type="select" name="status" id="status" value={payload.status} disabled>
                    <option value="approved">{t('disetujui')}</option>
                    <option value="rejected">{t('ditolak')}</option>
                </Input>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label htmlFor="message" className="text-md-right" md="3">
                {t('pesan')}
              </Label>
              <Col md="9">
                <Input
                  type="textarea"
                  name="message"
                  id="message"
                  rows="2"
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                />
              </Col>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button type="submit" color="netis-color" disabled={loading}>
              {loading ? (
                <>
                  <Spinner size="sm" color="light" /> Loading...
                </>
              ) : (
                  <>
                    <i className="fa fa-check mr-2"></i> Update Status
                </>
                )}
            </Button>
          </ModalFooter>
        </Form>
      )}
    </Modal>
  );
}

export default CutiDetail;
