import React, { Component, Suspense } from 'react';
import { lazyComponent as lazy } from '../../../components/lazyComponent';
import { connect } from 'react-redux';
import { Switch, Redirect } from 'react-router';
import LoadingAnimation from '../../../components/LoadingAnimation'
import { Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import { Link } from 'react-router-dom';
import {
    translate,
} from 'react-switch-lang';
import AuthRoute from '../../../components/AuthRoute';
const ReimburseHistory = lazy(() => import('./ReimburseHistory'));
const ReimburseApproval = lazy(() => import('./ReimburseApproval'));
const ReimburseDetail = lazy(() => import('./ReimburseDetail'));

const PANEL_USER = '3';
const PANEL_ADMIN = '2';

function matchWildcard(str, wildcard) {
    var escapeRegex = (str) => str.replace(/([.*+?^=!:${}()|[\]/\\])/g, "\\$1");
    return new RegExp("^" + wildcard.split("*").map(escapeRegex).join(".*") + "$").test(str);
}

class ReimburseWrapper extends Component {

    constructor(props) {
        super(props);

        this.state = {
            routes: [],
        }

        this.state.routes = this.createRoutes()
    }

    createRoutes = () => {
        const { t, match, menu } = this.props;
        let routes = []

        if (menu === PANEL_ADMIN) {
            routes = [
                {
                    path: match.path + '/manage', exact: true, privileges: ['browse-reimburse'], component: ReimburseApproval,
                    tab: t('kelola') + ' Reimburse', active: match.path + '/manage'
                },
                { path: match.path + '/manage/:id', privileges: ['read-reimburse'], exact: true, component: ReimburseDetail },
            ];
        } else if (menu === PANEL_USER) {
            routes = [
                {
                    path: match.path + '/history', exact: true, component: ReimburseHistory,
                    tab: t('Riwayat Reimburse'), active: match.path + '/history'
                },
                {
                    path: `${match.path}/history/:id`, exact: true, component: ReimburseDetail,
                }
            ];

            if (this.props.user.hasBawahan && this.props.user.personnel.company.reimburseSuperiorApproval) {
                routes.push({
                    path: match.path + '/manage', exact: true, component: ReimburseApproval,
                    tab: t('kelola') + ' Reimburse', active: match.path + '/manage'
                });
                routes.push({
                    path: `${match.path}/manage/:id`, exact: true, component: ReimburseDetail,
                })
            }
            // routes.push({ path: match.path + '/detail/:id', exact: true, component: ReimburseDetail });
        }

        return routes;
    }

    render() {
        const { match, location } = this.props;
        const { routes } = this.state;

        return (
            <div className="animated fadeIn">
                <Nav tabs>
                    {routes.filter(route => !!route.tab).map(route => (
                        <NavItem key={route.path}>
                            <NavLink tag={Link} to={route.path} active={route.active && matchWildcard(location.pathname, route.active)}>{route.tab}</NavLink>
                        </NavItem>
                    ))}
                </Nav>
                <TabContent>
                    <TabPane>
                        <Suspense fallback={<LoadingAnimation />}>
                            <Switch>
                                {routes.map(route => (
                                    <AuthRoute key={route.path} {...route} />
                                ))}
                                {routes[0] && <Redirect exact from={match.path} to={routes[0].path} />}
                            </Switch>
                        </Suspense>
                    </TabPane>
                </TabContent>
            </div>
        )
    }
}

const mapStateToProps = ({ token, menu, user }) => ({ token, menu, user });
export default connect(mapStateToProps)(translate(ReimburseWrapper));
