import React, { useState, useCallback, useMemo, useEffect } from 'react';
import { Button, Modal, Form, Row, ModalBody, ModalFooter, ModalHeader, FormGroup, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, Spinner, Label, Input, Col, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import { toast } from 'react-toastify';
import { DatePickerInput } from 'rc-datepicker';
import StatusBadge from './components/StatusBadge'
import request from '../../../utils/request'
import { formatDate } from '../../../utils/formatter';
import { convertToRupiah } from '../../../utils/formatter'
import moment from '../../../utils/moment'
import { Link } from 'react-router-dom';
import LoadingAnimation from '../../../components/LoadingAnimation'
import DataNotFound from '../../../components/DataNotFound'
import { isPdf } from './components/helper'
import {
    translate,t
} from 'react-switch-lang';
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from '../../../components/ResponsiveTable';
import { useUserPrivileges } from '../../../store';
import Select from 'react-select'
import usePagination from '../../../hooks/usePagination';
toast.configure()

function ReimburseApproval(props){
    moment.locale(t('id'))
    const {can} = useUserPrivileges();
    const searchParams = new URLSearchParams(props?.location?.search);
    const [loading, setLoading]  = useState(true)
    const [reimbursement, setReimbursement] = useState([])
    const [showModal, setShowModal] = useState(false)
    const [updating, setUpdating] = useState(false)
    const [update, setUpdate] = useState({
        reimburseId: null,
        status: '',
        message: ''
    })
    // const [dropdownOpened, setDropdownOpened] = useState({})
    const [showDocument, setShowDocument] = useState(false)
    const [filter, setFilter] = useState({
        start: searchParams.get('start'),
        end: searchParams.get('end')
    })
    const [units, setUnits] = useState([])
    const [jobs, setJobs] = useState([])
    const [search, setSearch] = useState(null)
    const [filterUnit, setFilterUnit] = useState([])
    const [filterJob, setFilterJob] = useState([])
    const [filters, setFilters] = useState([])

    const fetchData = useCallback(() => {
        const params = Object.keys(filter)
            .filter(k => filter[k] !== null)
            .reduce((acc, k) => ({ ...acc, [k]: formatDate(filter[k]) }), {});
        setLoading(true)
        return request.get('v1/reimbursement/admin/request', { params })
        .then((res) => {
            setReimbursement(res.data.data)
        })
        .catch((err) => {
            if (err.response) {
                toast.error(err.message, { autoClose: 2000 });
            }
            console.log(err)
            throw err;
        })
        .finally(() => setLoading(false))
        // eslint-disable-next-line
    },[])

    const getUnits = () => {
        return request.get(`/v1/master/units`)
            .then((res) => setUnits(res.data.data))
            .catch(() => toast.error(t('Gagal mendapatkan data Unit')))
    }
    const getJobs = () => {
        return request.get(`/v1/master/jobs`)
            .then((res) => setJobs(res.data.data))
            .catch(() => toast.error(t('Gagal mendapatkan data Jabatan')))
    }

    useEffect(() => {
        fetchData()
        getUnits()
        getJobs()
    }, [fetchData])

    const handleChangeFilter = (name, date) => {
        let fil = filter
        fil[name] = date
        setFilter(fil)
    }
    const toggleModal = () => {
        const closing = showModal;
        setShowModal(!showModal)
        if (closing) {
            setUpdate({
                status:'',
                message:'',
                reimburseId:null
            })
        }
    }

    const updateStatus = (reimburseId, status) => (e) => {
        const updt = update
        updt.status = status
        updt.reimburseId = reimburseId
        setUpdate(updt)
        toggleModal()
    }

    const submitUpdateStatus = (e) => {
        e.preventDefault();
        setUpdating(true)
        const { reimburseId, status, message } = update
        request.post(`v1/reimbursement/request/${reimburseId}`, { status, message })
        .then((res) => {
            setReimbursement(state => {
                const updatedReimburseId = state.reimbursements.findIndex(reimburse => reimburse.id === res.data.id);
                const reimbursements = [...state.reimbursements];
                reimbursements[updatedReimburseId].status = res.data.status;
                return { reimbursements };
            })
            toast.success(t('Berhasil mengubah status reimburse'))
            toggleModal();
        })
        .catch ((err) => {
            if (err.response && err.response.data) {
                toast.error(err.response.data.message);
            }
            throw err;
        })
        .finally(() => setUpdating(false))
    }

    // const toggleDropdown = (id) => {
    //     console.log(id)
    //     let dropOp = dropdownOpened
    //     dropOp[id] = !dropOp[id]
    //     setDropdownOpened(dropOp)
    //     console.log(dropOp)
    // }

    const handleShowDocument = (document) => {
        setShowDocument(process.env.REACT_APP_DOMAIN + document)
    }

    const toggleShowDocument = () => {
        setShowDocument(!showDocument)
    }

    const handleInputChange = (e) => {
        const upd = update
        upd[e.target.name] = e.target.value
        setUpdate(upd)
    }
    const searchSpace = (e) => {
        setSearch(e.target.value)
    };
    const handleUnit = (e) => {
        let unit = e?.map(item => item.value) ?? []
        setFilterUnit(unit)
    }
    const handleJob = (e) => {
        let job = e?.map(item => item.value) ?? []
        setFilterJob(job)
    }

    const filtered = useMemo(() => {
        let filterData = reimbursement
            ?.filter(item =>
                search?.trim() ? item?.personnel?.user?.fullName?.toLowerCase().includes(search?.trim().toLowerCase()) : true
            )
            ?.filter(item =>
                filterUnit?.length > 0 ? filterUnit.includes(item?.personnel?.unit?.id) : true
            )
            ?.filter(item =>
                filterJob?.length > 0 ? filterJob.includes(item?.personnel?.job?.id) : true
            )
        return filterData;
    }, [reimbursement, search, filterUnit, filterJob])

    const handleChangeCurrentPage = useCallback(
        (page) => {
            setFilters((state) => ({ ...state, pagination: page }));
        },
        [setFilters]
    );

    const { data: groupFiltered, PaginationComponent, currentPage } = usePagination(
        filtered,
        8,
        filters?.pagination,
        handleChangeCurrentPage
    );

    return(
        <div className="animated fadeIn md-company-header mb-3">
            <h4 className="content-title mb-3">{t('Riwayat Reimburse')}</h4>
            <Form onSubmit={(e) => { e.preventDefault(); fetchData() }}>
            <Row className="md-company-header mb-3">
                <Col xs="12" md="9">
                    <Row>
                        <Col xs="6">
                            <FormGroup>
                                <Label htmlFor="job" className="input-label">
                                {t("jabatan")}
                                </Label>
                                <Select
                                    disabled={jobs.length < 1}
                                    isMulti={true}
                                    isSearchable={false}
                                    name="job"
                                    options={jobs.map(item => ({ label: item.name, value: item.id }))}
                                    placeholder={t("Jabatan")}
                                    onChange={handleJob}
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="6">
                            <FormGroup style={{ marginBottom: 5 }}>
                                <Label htmlFor="unit" className="input-label">
                                {t('Unit')}
                                </Label>
                                <Select
                                    disabled={units.length < 1}
                                    isMulti={true}
                                    isSearchable={false}
                                    name="unit"
                                    options={units.map(item => ({ label: item.name, value: item.id }))}
                                    placeholder={t("Unit")}
                                    onChange={handleUnit}
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="6" xl="4" >
                            <FormGroup>
                                <Label tag="small" htmlFor="start" className="text-muted">{t('dari')}</Label>
                                <br />
                                <DatePickerInput
                                    readOnly
                                    showOnInputClick={true}
                                    name="start"
                                    id="start"
                                    onChange={(value) => handleChangeFilter('start', value)}
                                    value={filter.start}
                                    className='my-custom-datepicker-component'
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="6" xl="4" >
                            <FormGroup>
                                <Label tag="small" htmlFor="end" className="text-muted">{t('hingga')}</Label>
                                <br />
                                <DatePickerInput
                                    readOnly
                                    showOnInputClick={true}
                                    name="end"
                                    id="end"
                                    onChange={(value) => handleChangeFilter('end', value)}
                                    value={filter.end}
                                    className='my-custom-datepicker-component'
                                    closeOnClickOutside={true}
                                    displayFormat="DD MMMM YYYY"
                                />
                            </FormGroup>
                        </Col>
                        <Col xs="12" xl="4" className="mt-xl-1 pt-xl-3 mb-2" >
                            <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText className="input-group-transparent">
                                        <i className="fa fa-search" />
                                    </InputGroupText>
                                </InputGroupAddon>
                                <Input
                                    type="text"
                                    placeholder={t('Cari Nama')}
                                    className="input-search"
                                    onChange={searchSpace}
                                />
                            </InputGroup>
                        </Col>
                    </Row>
                </Col>
                <Col xs="12" md="3">
                    <div className="pt-lg-2 my-2 mt-md-0">
                        <Button type="submit" className="mt-sm-4 w-100" color="netis-color" disabled={loading}>
                            {loading ? <Spinner color="light" size="sm" /> : 'Filter'}
                        </Button>
                    </div>
                </Col>
            </Row>
            </Form>

                {loading ? <LoadingAnimation />
                    : !filtered.length ? <DataNotFound />
                        : (
                        <>
                        <Rtable size="sm" style={{display: 'table'}}>
                            <Rthead>
                                <Rtr>
                                    <Rth className="text-center w-5">No.</Rth>
                                    <Rth className="w-20">{t('nama')}</Rth>
                                    <Rth className="w-5">{t('lampiran')}</Rth>
                                    <Rth>{t('deskripsi')}</Rth>
                                    <Rth className="w-15">Nominal</Rth>
                                    <Rth>Status</Rth>
                                    <Rth></Rth>
                                </Rtr>
                            </Rthead>
                            <Rtbody>{groupFiltered?.map((data, index) => (
                                <Rtr key={index + 1 + (8 * currentPage)}>
                                    <Rtd className="text-md-center">{index + 1 + (8 * currentPage)}</Rtd>
                                    <Rtd>
                                        {data.personnel.user.fullName} <br />
                                &mdash; <span className="text-muted">{moment(data.datetime).format('DD MMM YYYY')}</span>
                                    </Rtd>
                                    <Rtd>
                                        {isPdf(data.document) ?
                                            <Button href={data.document} target="_blank" rel="noopener noreferrer" color="link"><i className="fa fa-file-pdf-o"></i> PDF</Button> :
                                            <><Button color="link" onClick={() => showDocument(data.document)}><i className="fa fa-file-picture-o"></i></Button> </>
                                        }
                                    </Rtd>
                                    <Rtd className="text-nowrap text-left">{data.description}</Rtd>
                                    <Rtd className="text-nowrap">{convertToRupiah(data.nominal)}</Rtd>
                                    <Rtd className="text-nowrap"><StatusBadge status={data.status} /></Rtd>
                                    <Rtd className="text-nowrap">
                                        {!['done', 'rejected'].includes(data.status) && 
                                            <UncontrolledDropdown className="d-inline-block">
                                                <DropdownToggle className="mr-1" color="netis-color" caret>
                                                    {t('ubah')} Status
                                                </DropdownToggle>
                                                <DropdownMenu>
                                                    {(data.status === 'pending' || (can('verify-reimburse') && data.status === 'process1')) && (<DropdownItem onClick={() => updateStatus(data.id, 'process')}>
                                                        <i className="fa fa-circle text-info"></i> {t('diproses')}
                                                    </DropdownItem>)}
                                                    {can('verify-reimburse') && (<DropdownItem onClick={() => updateStatus(data.id, 'done')}>
                                                        <i className="fa fa-circle text-success"></i> {t('ditransfer')}
                                                    </DropdownItem>)}
                                                    <DropdownItem onClick={() => updateStatus(data.id, 'rejected')}>
                                                        <i className="fa fa-circle text-danger"></i> {t('ditolak')}
                                                    </DropdownItem>
                                                </DropdownMenu>
                                            </UncontrolledDropdown>
                                        }
                                        <Link to={'/reimburse/manage/' + data.id}>
                                            <Button color="netis-color">{t('detail')}</Button>
                                        </Link>
                                    </Rtd>
                                </Rtr>
                            ))}</Rtbody>
                        </Rtable>
                        {filtered?.length > 8 && <PaginationComponent />}
                        </>
                )}

                <Modal isOpen={!!showDocument} toggle={toggleShowDocument}>
                    <ModalBody>
                        <img src={handleShowDocument} width="100%" alt="document" />
                    </ModalBody>
                </Modal>

                <Modal isOpen={showModal} toggle={toggleModal}>
                    <form onSubmit={submitUpdateStatus}>
                        <ModalHeader toggle={toggleModal}>Update Status</ModalHeader>
                        <ModalBody>
                            <FormGroup row>
                                <Label htmlFor="status" className="text-md-right" md="3">Status</Label>
                                <Col md={9}>
                                    <Input type="select" name="status" id="status" value={update.status} disabled>
                                        <option value="process">{t('diproses')}</option>
                                        <option value="rejected">{t('ditolak')}</option>
                                        <option value="done">{t('ditransfer')}</option>
                                    </Input>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label htmlFor="message" className="text-md-right" md={3}>{t('pesan')}</Label>
                                <Col md={9}>
                                    <Input type="textarea" name="message" id="message" rows="2" value={update.message} onChange={handleInputChange}></Input>
                                </Col>
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            {/*  */}
                            <Button type="submit" color="netis-color" disabled={updating}>
                                {updating ?
                                    (<><Spinner size="sm" color="light"></Spinner> Loading</>) :
                                    (<><i className="fa fa-check mr-2"></i> Update Status</>)
                                }
                            </Button>
                        </ModalFooter>
                    </form>
                </Modal>
            </div>
    )
}

export default translate(ReimburseApproval)