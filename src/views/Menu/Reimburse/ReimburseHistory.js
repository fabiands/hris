import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Row,
  Col,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  Form,
  Label,
  Spinner,
} from "reactstrap";
import {
  AvForm,
  AvGroup,
  AvField,
  AvInput,
} from "availity-reactstrap-validation";
// import { Field, Form, Formik } from 'formik';
// import FormikInput from '../../../components/Form/FormikInput';
import { toast } from "react-toastify";
import { DatePickerInput } from "rc-datepicker";
import { formatDate } from "../../../utils/formatter";
import { Link } from "react-router-dom";
import StatusBadge from "./components/StatusBadge";
import NumberFormat from "react-number-format";
import request from "../../../utils/request";
import { convertToRupiah } from "../../../utils/formatter";
import moment from "../../../utils/moment";
import LoadingAnimation from "../../../components/LoadingAnimation";
import DataNotFound from "../../../components/DataNotFound";
import { isPdf } from "./components/helper";
import { translate, t } from "react-switch-lang";
import { Rtable, Rtbody, Rtd, Rth, Rthead, Rtr } from "../../../components/ResponsiveTable";
toast.configure();
class Reimburse extends Component {
  constructor(props) {
    super(props);

    const searchParams = new URLSearchParams(props.location.search);

    this.state = {
      loading: true,
      session: props.token,
      reimbursements: [],
      showCreateModal: false,
      createData: {
        datetime: "",
        description: "",
        nominal: null,
        document: null,
      },
      nominalError: null,
      // descriptionError:'',
      previewDocument: null,
      creating: false,
      filter: {
        start: searchParams.get("start"),
        end: searchParams.get("end"),
      },
    };

    this.toggleCreateModal = this.toggleCreateModal.bind(this);
  }

  fetchData = async () => {
    const params = Object.keys(this.state.filter)
      .filter((k) => this.state.filter[k] !== null)
      .reduce(
        (acc, k) => ({ ...acc, [k]: formatDate(this.state.filter[k]) }),
        {}
      );

    this.setState({ loading: true });
    try {
      const { data } = await request.get("v1/reimbursement/request/history", {
        params,
      });
      this.setState({ reimbursements: data.data });
    } catch (err) {
      toast.error(err.message, { autoClose: 2000 });

      console.log(err);
      throw err;
    } finally {
      this.setState({ loading: false });
    }
  };

  componentDidMount() {
    this.fetchData();
  }

  toggleCreateModal() {
    this.setState({ showCreateModal: !this.state.showCreateModal });
  }

  handleChangeFilter = (name, date) => {
    let filter = { ...this.state.filter };
    filter[name] = date;
    this.setState({ filter });
  };

  handleInputChange = (e) => {
    // const formValues = {
    //     description : this.state.createData.description,
    //     nominal : this.state.createData.nominal,
    //     document : this.state.createData.document
    // }
    // const formValidate = values => {
    //     const errors = {}
    //     if (!value.description) {
    //       errors.description = t('isiantdkbolehkosong')
    //     }

    //     if (!value.nominal) {
    //       errors.nominal = t('isiantdkbolehkosong')
    //     }

    //     if (!value.document) {
    //         errors.document = t('isiantdkbolehkosong')
    //       }

    //     return errors;
    //   }

    // if(!this.state.createData.description){
    //     this.setState({descriptionError:'isi deskripsi'});
    //     return false;
    // } else{this.setState({descriptionError:''});}

    // if (!this.state.createData.nominal){
    //     this.setState({nominalError:'isi form dengan angka'});
    //     return false;
    // }

    let data = { ...this.state.createData };
    data[e.target.name] = e.target.value;
    this.setState({
      createData: data,
    });
  };

  handleDatepickerChange = (date) => {
    let createData = { ...this.state.createData };
    createData.datetime = formatDate(date);
    this.setState({ createData });
  };

  handleDocumentChange = (e) => {
    e.preventDefault();
    let createData = { ...this.state.createData };
    let previewDocument = null;
    if (e.target.files.length) {
      createData.document = e.target.files[0];
      // Preview if the upload file is image only.
      if (!isPdf(e.target.value)) {
        previewDocument = URL.createObjectURL(e.target.files[0]);
      }
    } else {
      createData.document = null;
    }
    this.setState({ createData, previewDocument });
  };

  submitCreateReimburse = async (e) => {
    e.preventDefault();
    if (this.state.creating) {
      return;
    }
    this.setState({ creating: true });
    try {
      const formData = new FormData();
      Object.keys(this.state.createData).forEach((name) => {
        if (name === "datetime") {
          formData.append(name, this.state.createData["datetime"]);
        } else if (name === "nominal") {
          formData.append(
            name,
            parseInt(this.state.createData[name].replace(/,.*|[^0-9]/g, ""), 10)
          );
        } else {
          formData.append(name, this.state.createData[name]);
        }
      });
      console.log(formData);
      const response = await request.post("v1/reimbursement/request", formData);
      const reimbursements = this.state.reimbursements;
      reimbursements.push(response.data.data);
      this.setState({ reimbursements, showCreateModal: false });
      toast.success(response.data.message || "Success");
    } catch (err) {
      if (err.response && err.response.data) {
        toast.error(err.response.data.message || t("Isikan data dengan benar"));
      }
      console.log(err);
    } finally {
      this.setState({ creating: false });
    }
  };

  render() {
    moment.locale(t("id"));
    return (
      <div className="animated fadeIn">
        <div className="d-flex align-items-center mb-4">
          <Button color="netis-color" onClick={this.toggleCreateModal}>
            <i className="fa fa-plus mr-2"></i> {t("tambah")} Reimburse
          </Button>
        </div>
        <div className="d-flex align-items-center mb-4">
          <Form
            onSubmit={(e) => {
              e.preventDefault();
              this.fetchData();
            }}
          >
            <Row>
              <Col sm="5">
                <Label tag="small" htmlFor="start" className="text-muted">
                  {t("dari")}
                </Label>
                <br />
                <DatePickerInput
                  showOnInputClick={true}
                  name="start"
                  id="start"
                  onChange={(value) => this.handleChangeFilter("start", value)}
                  value={this.state.filter.start}
                  maxDate={this.state.filter.end}
                  className="my-custom-datepicker-component"
                  closeOnClickOutside={true}
                  displayFormat="DD MMMM YYYY"
                  readOnly
                />
              </Col>
              <Col sm="5">
                <Label tag="small" htmlFor="end" className="text-muted">
                  {t("hingga")}
                </Label>
                <br />
                <DatePickerInput
                  showOnInputClick={true}
                  name="end"
                  id="end"
                  onChange={(value) => this.handleChangeFilter("end", value)}
                  value={this.state.filter.end}
                  minDate={this.state.filter.start}
                  className="my-custom-datepicker-component"
                  closeOnClickOutside={true}
                  displayFormat="DD MMMM YYYY"
                  readOnly
                />
              </Col>
              <Col sm="2">
                <div className="pt-sm-0">
                  <Button
                    type="submit"
                    className="mt-sm-4"
                    color="netis-color"
                    style={{ width: "60px" }}
                    disabled={this.state.loading}
                  >
                    {this.state.loading ? (
                      <Spinner color="light" size="sm" />
                    ) : (
                      t("cari")
                    )}
                  </Button>
                </div>
              </Col>
            </Row>
          </Form>
        </div>
        <Row className="md-company-header mb-3">
          <Col xs="12">
            {this.state.loading ? (
              <LoadingAnimation />
            ) : !this.state.reimbursements.length ? (
              <DataNotFound />
            ) : (
              <Rtable size="sm">
                <Rthead>
                  <Rtr>
                    <Rth className="text-center w-5">No.</Rth>
                    <Rth className="w-15">{t("tanggal")}</Rth>
                    <Rth>{t("deskripsi")}</Rth>
                    <Rth className="w-15">Nominal</Rth>
                    <Rth className="w-20">Status</Rth>
                    <Rth className="w-20"></Rth>
                  </Rtr>
                </Rthead>
                <Rtbody>
                  {this.state.reimbursements.map((data, index) => (
                    <Rtr key={index}>
                      <Rtd className="text-md-center">{index + 1}</Rtd>
                      <Rtd>{moment(data.created).format("DD MMM YYYY")}</Rtd>
                      <Rtd>{data.description}</Rtd>
                      <Rtd>{convertToRupiah(data.nominal)}</Rtd>
                      <Rtd>
                        <StatusBadge status={data.status} />
                      </Rtd>
                      <Rtd>
                        <Link to={"/reimburse/history/" + data.id}>
                          <Button color="netis-color">{t("detail")}</Button>
                        </Link>
                      </Rtd>
                    </Rtr>
                  ))}
                </Rtbody>
              </Rtable>
            )}
          </Col>
        </Row>

        <Modal
          isOpen={this.state.showCreateModal}
          toggle={this.toggleCreateModal}
        >
          <ModalBody>
            <AvForm
              onSubmit={this.submitCreateReimburse}
              className="need-validation"
            >
              <div className="d-flex justify-content-between align-items-center mb-4">
                <h5 className="content-sub-title mb-0">
                  {t("tambah")} Reimburse
                </h5>
                <Button
                  color="link"
                  size="lg"
                  className="text-danger"
                  onClick={this.toggleCreateModal}
                >
                  <strong>&times;</strong>
                </Button>
              </div>
              <Row form>
                <Col md="6">
                  <AvGroup>
                    <Label htmlFor="datetime" className="input-label">
                      {t("tanggal")}
                      <span className="required"> *</span>
                    </Label>
                    <DatePickerInput
                      name="datetime"
                      id="datetime"
                      onChange={this.handleDatepickerChange}
                      value={this.state.createData.datetime}
                      className="my-custom-datepicker-component"
                      required
                      showOnInputClick={true}
                      closeOnClickOutside={true}
                      displayFormat="DD MMMM YYYY"
                      readOnly
                    />
                  </AvGroup>
                </Col>
              </Row>
              <Row form>
                <Col>
                  <AvGroup>
                    <Label htmlFor="description" className="input-label">
                      {t("deskripsi")}
                      <span className="required"> *</span>
                    </Label>
                    <AvInput
                      type="textarea"
                      value={this.state.createData.description}
                      onChange={this.handleInputChange}
                      name="description"
                      id="description"
                      rows="2"
                      validate={{
                        required: { errorMessage: "Please fill the form" },
                      }}
                    />
                    <small className="form-text text-muted">
                      NB: {t('tambahkan pesan untuk admin, seperti reimburse untuk xxx ditransfer ke rekening yyy')}
                    </small>
                    {/* <div className="text-danger">{this.state.descriptionError}</div> */}
                  </AvGroup>
                </Col>
              </Row>
              <Row form>
                <Col md="6">
                  <AvGroup>
                    <Label htmlFor="nominal" className="input-label">
                      Nominal
                      <span className="required"> *</span>
                    </Label>
                    <NumberFormat
                      thousandSeparator={"."}
                      decimalSeparator={","}
                      prefix={"Rp "}
                      name="nominal"
                      id="nominal"
                      onChange={this.handleInputChange}
                      value={this.state.createData.nominal}
                      className="form-control need-validation"
                      required
                    />
                    {/* <div className="invalid-feedback text-danger">error</div> */}
                    {/* <div className="text-danger">{this.state.nominalError}</div> */}
                  </AvGroup>
                </Col>
              </Row>
              <Row form>
                <Col md="12">
                  <AvGroup className="needs-validation">
                    <Label htmlFor="document" className="input-label">
                      Upload Document &nbsp;
                      <span style={{ fontWeight: "bold" }}>
                        (JPEG/JPG/PNG/PDF)
                      </span>
                    </Label>
                    <AvField
                      name="document"
                      id="document"
                      type="file"
                      accept="image/jpeg,image/gif,image/png,application/pdf"
                      validate={{
                        required: { errorMessage: "Please upload a document" },
                      }}
                      onChange={this.handleDocumentChange}
                    />
                  </AvGroup>
                </Col>
                <Col md="7">
                  {this.state.previewDocument && (
                    <div
                      className="d-flex justify-content-center"
                      style={{
                        overflowBlock: "hidden",
                        maxHeight: 300,
                        maxWidth: "100%",
                      }}
                    >
                      <img
                        style={{ maxHeight: 300, maxWidth: "100%" }}
                        src={this.state.previewDocument}
                        alt="preview document"
                      />
                    </div>
                  )}
                </Col>
              </Row>
            </AvForm>
          </ModalBody>
          <ModalFooter>
            <Button type="button" color="link" onClick={this.toggleCreateModal}>
              {t("batal")}
            </Button>
            <Button
              disabled={this.state.creating}
              type="submit"
              color="netis-color"
              onClick={this.submitCreateReimburse}
            >
              {this.state.creating ? (
                <>
                  <Spinner size="sm" color="light"></Spinner> Loading
                </>
              ) : (
                <>
                  <i className="fa fa-plus mr-2"></i> {t("tambah")} Reimburse
                </>
              )}
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.token,
  };
};
export default connect(mapStateToProps)(translate(Reimburse));
