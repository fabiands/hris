import React, { useCallback, useEffect, useState } from "react";
import request from '../../../utils/request'
import { Button, Card, CardBody, Table, Row, Col, CardHeader, UncontrolledDropdown, DropdownToggle, DropdownItem, DropdownMenu, Form, FormGroup, Label, Spinner, Input, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import { toast } from "react-toastify";
import moment from '../../../utils/moment'
import { convertToRupiah } from '../../../utils/formatter'
import StatusBadge from './components/StatusBadge'
import Loading from '../../../components/LoadingAnimation'
import DataNotFound from '../../../components/DataNotFound'
import { isPdf } from './components/helper'
import { t } from 'react-switch-lang';
import { useUserPrivileges, useAuthUser } from "../../../store";
import { useRouteMatch } from "react-router-dom";

const logMessage = {
  pending: t('Permintaan dibuat'),
  approved1: t('Disetujui oleh Atasan'),
  approved2: t('Disetujui oleh Admin'),
  rejected: t('Permintaan ditolak')
}

function ReimburseDetail(props) {
  const user = useAuthUser();
  const {can} = useUserPrivileges();
  const [loading, setLoading] = useState(true)
  const [notFound, setNotFound] = useState(false)
  const [reimbursement, setReimbursement] = useState(null)
  // const [data, setData] = useState([]);
  const [updateModal, setUpdateModal] = useState(null)
  const matchRoute = useRouteMatch();

  useEffect(() => {
    request.get(`v1/reimbursement/request/${matchRoute.params.id}`)
      .then((res) => {
        setReimbursement(res.data.data)
      })
      .catch((err) => {
        if (err.response && err.response.status === 404){
          setNotFound(true)
        }
        throw err;
      })
      .finally(() => setLoading(false))
  }, [matchRoute.params.id])
  

  const updateStatus = (dataId, status) => {
    setUpdateModal({...updateModal, status:status, reimburseId:dataId})
  }

  const onUpdatedStatus = (data) => {
    setReimbursement(data)
  };

  const handleBackButton = useCallback(() => {
    const url = props.location.pathname;

    return props.history.push(url.slice(0, url.lastIndexOf('/')));
  }, [props.location, props.history]);

  if (loading) {
    return <Loading />;
  }

  if (notFound) {
    return <DataNotFound />
  }

  function canChangeStatus() {
    // jika status == pending dan memiliki atasan,
    // dan reimburse wajib disetujui atasan terlebih dahulu (reimburseSuperiorApproval = true)
    // maka yang bisa mengubah status hanya atasannya saja.
    if (reimbursement.status === 'pending' && reimbursement.personnelParent && user.personnel.company.reimburseSuperiorApproval) {
      return reimbursement.personnelParent === user.personnel.id;
    }

    // jika status belum ditransfer oleh admin, atau belum ditolak
    // maka status dapat idubah
    return can('verify-reimburse') && !['done', 'rejected'].includes(reimbursement.status);
  }

  return (
    <div className="animated fadeIn">
      <div className="d-flex bd-highlight mb-3">
        <div className="mr-auto bd-highlight mt-2">
          <Button color="netis-color" onClick={handleBackButton}><i className="fa fa-chevron-left mr-2"></i> {t('kembali')} </Button>
        </div>
        {/* <Button onClick={() => console.log(reimbursement)}>Data</Button> */}

        {canChangeStatus() && (
          <UncontrolledDropdown className="d-inline-block mt-2">
          <DropdownToggle color="netis-color" caret>
              {t('ubah')} Status
          </DropdownToggle>
          <DropdownMenu>
              {(reimbursement.status === 'pending' || (can('verify-reimburse') && reimbursement.status === 'process1')) && (<DropdownItem onClick={() => updateStatus(reimbursement.id, 'process')}>
                  <i className="fa fa-circle text-info"></i> {t('diproses')}
              </DropdownItem>)}
              {can('verify-reimburse') && (
                <DropdownItem onClick={() => updateStatus(reimbursement.id, 'done')}>
                    <i className="fa fa-circle text-success"></i> {t('ditransfer')}
                </DropdownItem>
              )}
              <DropdownItem onClick={() => updateStatus(reimbursement.id, 'rejected')}>
                  <i className="fa fa-circle text-danger"></i> {t('ditolak')}
              </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      )}
      </div>
      <UpdateStatusModalForm
        toggle={() => setUpdateModal(null)}
        payload={updateModal}
        onUpdated={onUpdatedStatus}
        props={props}
      />
      <Card>
        <CardBody>
          <Row>
            <Col md="6">
              <Table hover>
                <tbody>
                  <tr>
                    <th>{t('tanggal')}</th><td>{moment(reimbursement.datetime).format('DD MMMM YYYY')}</td>
                  </tr>
                  <tr>
                    <th>{t('deskripsi')}</th><td>{reimbursement.description}</td>
                  </tr>
                  <tr>
                    <th>Nominal</th><td>{convertToRupiah(reimbursement.nominal)}</td>
                  </tr>
                  <tr>
                    <th>Status</th><td><StatusBadge status={reimbursement.status} /></td>
                  </tr>
                </tbody>
              </Table>
            </Col>
            <Col md="6">
              {isPdf(reimbursement.document) ?
                <Button href={reimbursement.document} target="_blank" rel="noopener" color="netis-color"><i className="fa fa-pdf"></i> {t('Buka Lampiran')}</Button> :
                <img src={reimbursement.document} width="100%" alt="document" />
              }
            </Col>
          </Row>
        </CardBody>
      </Card>
      <Card>
        <CardHeader>
          {t('Riwayat Status')}
      </CardHeader>
        <CardBody>
          <Table>
            <tbody>
              {reimbursement.status_log && reimbursement.status_log.map((statusLog, index) => (
                <tr key={index}>
                  <td style={{ width: 170 }}>
                    <strong>{(statusLog.subject && statusLog.subject.nickName) || 'system'}</strong><br />
                    <span className="text-muted">{statusLog.created}</span>
                  </td>
                  <td>
                    <strong><StatusBadge status={statusLog.status} /></strong>
                    <br /> {logMessage[statusLog.status]}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </CardBody>
      </Card>
    </div>);
}

function UpdateStatusModalForm({ toggle, payload, onUpdated }) {
  const [message, setMessage] = useState("");
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setMessage("");
  }, [payload]);

  function submitForm(e) {
    e.preventDefault();
    setLoading(true);
    request.post(`v1/reimbursement/request/${payload.reimburseId}`, { status: payload.status, message })
      .then(res => {
        const { data } = res.data
        toast.success(t('Berhasil mengubah status reimburse'))
        toggle();
        return onUpdated(data);
      })
      .catch((err) => {
        if (err.response && err.response.data) {
            toast.error(err.response.data.message);
        }
        throw err;
      })
      .finally(() => {
        setLoading(false)
      });
  }

  return (
    <Modal isOpen={!!payload} toggle={() => !loading && toggle()}>
      {!!payload && (
        <Form onSubmit={submitForm}>
          <ModalHeader toggle={() => !loading && toggle()}>
            Update Status
          </ModalHeader>
          <ModalBody>
            <FormGroup row>
              <Label htmlFor="status" className="text-md-right" md="3">
                Status
              </Label>
              <Col md="9">
                <Input type="select" name="status" id="status" value={payload.status} disabled>
                    <option value="process">{t('diproses')}</option>
                    <option value="rejected">{t('ditolak')}</option>
                    <option value="done">{t('ditransfer')}</option>
                </Input>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label htmlFor="message" className="text-md-right" md="3">
                {t('pesan')}
              </Label>
              <Col md="9">
                <Input
                  type="textarea"
                  name="message"
                  id="message"
                  rows="2"
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                />
              </Col>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button type="submit" color="netis-color" disabled={loading}>
              {loading ? (
                <>
                  <Spinner size="sm" color="light" /> Loading...
                </>
              ) : (
                  <>
                    <i className="fa fa-check mr-2"></i> Update Status
                </>
                )}
            </Button>
          </ModalFooter>
        </Form>
      )}
    </Modal>
  );
}

export default ReimburseDetail;
