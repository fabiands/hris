import React from 'react';
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-super-responsive-table';

const Rtable = ({className, size = 'xs', ...props}) => <Table style={{display:'table'}} className={`table table-responsive responsiveTable-${size}${className ? ' ' + className : ''}`} {...props} />;
const Rthead = Thead;
const Rtbody = Tbody;
const Rtr = Tr;
const Rth = Th;
const Rtd = Td;

export {
    Rtable,
    Rthead,
    Rtbody,
    Rtr,
    Rth,
    Rtd
}
