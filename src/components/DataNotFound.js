import React from 'react';
import Illustration from '../assets/assets_ari/481.png';

const DataNotFound = () => {
    return (
        <div className="text-center my-3">
            <img src={Illustration} alt="not found" width={250} />
        </div>
    );
}

export default DataNotFound;
