import React, { useMemo } from 'react'
import { UncontrolledTooltip } from 'reactstrap'

var usedId = 1;

const generateTooltipId = () => {
    return `my-tooltip-${usedId++}`;
}

function MyTooltip({block, inline, content, id, children, ...props}) {
    const generatedId = useMemo(() => {
        if (id) {
            return id;
        } else {
            return generateTooltipId();
        }
    }, [id])

    const ChildrenComponent = useMemo(() => {
        return () => <div style={{ display: block ? 'block' : inline ? 'inline' : 'block'}} id={generatedId}>{children}</div>
    }, [children, block, inline, generatedId])

    return (
        <>
        <ChildrenComponent />
        <UncontrolledTooltip target={generatedId} {...props}>
            {content}
        </UncontrolledTooltip>
        </>
    )
}

export default MyTooltip
